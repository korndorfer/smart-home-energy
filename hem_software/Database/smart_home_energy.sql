-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 02/07/2019 às 15:54
-- Versão do servidor: 5.7.26-0ubuntu0.18.04.1
-- Versão do PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `smart_home_energy`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Acessa`
--

CREATE TABLE `Acessa` (
  `id_dado` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `alembic_version`
--

CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `alembic_version`
--

INSERT INTO `alembic_version` (`version_num`) VALUES
('1efc79264ca5');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Dados`
--

CREATE TABLE `Dados` (
  `id_dado` int(11) NOT NULL,
  `corrente` float NOT NULL,
  `tensao` float NOT NULL,
  `temperatura` float NOT NULL,
  `horario` datetime NOT NULL,
  `potencia` float NOT NULL,
  `id_tomada` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Tomadas`
--

CREATE TABLE `Tomadas` (
  `id_tomada` int(11) NOT NULL,
  `nome_tomada` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `comando` tinyint(1) DEFAULT NULL,
  `energizada` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Usuarios`
--

CREATE TABLE `Usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `senha` varbinary(1137) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `Usuarios`
--

INSERT INTO `Usuarios` (`id_usuario`, `nome_usuario`, `telefone`, `senha`) VALUES
(1, 'oseias', '1234', 0x2470626b6466322d73686135313224323530303024346e7a502e56394c6958474f30667166552e7139647724323477687852474259636f55466d35373836304654794530646e4c5272306c47336e794d7148626449707a3969636f7a4a52742e485642584239574e4544386878577a474231434a426c524a664d4e5a4e4a44766177);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `Acessa`
--
ALTER TABLE `Acessa`
  ADD PRIMARY KEY (`id_dado`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Índices de tabela `alembic_version`
--
ALTER TABLE `alembic_version`
  ADD PRIMARY KEY (`version_num`);

--
-- Índices de tabela `Dados`
--
ALTER TABLE `Dados`
  ADD PRIMARY KEY (`id_dado`),
  ADD KEY `id_tomada` (`id_tomada`);

--
-- Índices de tabela `Tomadas`
--
ALTER TABLE `Tomadas`
  ADD PRIMARY KEY (`id_tomada`),
  ADD UNIQUE KEY `nome_tomada` (`nome_tomada`);

--
-- Índices de tabela `Usuarios`
--
ALTER TABLE `Usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `nome_usuario` (`nome_usuario`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `Dados`
--
ALTER TABLE `Dados`
  MODIFY `id_dado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `Tomadas`
--
ALTER TABLE `Tomadas`
  MODIFY `id_tomada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `Usuarios`
--
ALTER TABLE `Usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `Acessa`
--
ALTER TABLE `Acessa`
  ADD CONSTRAINT `Acessa_ibfk_1` FOREIGN KEY (`id_dado`) REFERENCES `Dados` (`id_dado`),
  ADD CONSTRAINT `Acessa_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `Usuarios` (`id_usuario`);

--
-- Restrições para tabelas `Dados`
--
ALTER TABLE `Dados`
  ADD CONSTRAINT `Dados_ibfk_1` FOREIGN KEY (`id_tomada`) REFERENCES `Tomadas` (`id_tomada`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
