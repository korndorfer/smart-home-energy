/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (EVK-IPD)
 *
 * COMPONENT:          app_event_handler.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Event Handler
 *
 * $HeadURL $
 *
 * $Revision: 8437 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-05 14:42:12 +0000 (Mon, 05 Dec 2011) $
 *
 * $Id: app_ipd_node.c 8437 2011-12-05 14:42:12Z nxp33194 $
 *
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include "app_sleep_functions.h"
#include "app_zbp_utilities.h"
#include "app_event_handler.h"
#include "app_timer_driver.h"
#include "app_ipd_node.h"
#include "app_zcl_task.h"
#include "app_buttons.h"
#include "app_display.h"
#include "zcl_options.h"
#include "app_adc.h"
#include "app_led.h"
#include "os_gen.h"
#include "Price.h"
#include "dbg.h"
#include "pdm.h"
#include "AppHardwareApi_JN516x.h"
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_EVENT_HANDLER
#define TRACE_EVENT_HANDLER	TRUE
#endif

#define MAXIMUM_NUMBER_OF_READ_ATTRIBUTE_REQUESTS	3

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void vHandleMeteringStateEvent(APP_tsEvent sAppEvent);
PRIVATE void vHandleTimerExpiries(APP_tsEvent sAppEvent);
PRIVATE void vHandleESettingsStateEvent(APP_tsEvent sAppEvent);
PRIVATE void vRequestMeteringData(void);
PRIVATE void vClearLocalActiveMessage(void);
PRIVATE void vChangeCurrentMeterOfInterest(void);
PRIVATE void vHandlePriceStateEvent(APP_tsEvent sAppEvent);
PRIVATE void vHandleDRLCStateEvent(APP_tsEvent sAppEvent);
PRIVATE void vHandleHistoryStateEvent(APP_tsEvent sAppEvent);
PRIVATE void vHandleMessageStateEvent(APP_tsEvent sAppEvent);
PRIVATE void vHandleMessageConfirmationStateEvent(APP_tsEvent sAppEvent);

extern	teSE_MCStatus eSE_MCFindMCCluster(
		uint8 u8SourceEndPointId,
		bool_t bIsServer,
		tsZCL_EndPointDefinition **ppsEndPointDefinition,
		tsZCL_ClusterInstance **ppsClusterInstance,
		tsSE_MCCustomDataStructure **ppsMCCustomDataStructure);

extern	tsSE_MCDisplayMessageCommandPayloadRecord *psCheckToSeeIfMessageExists(
		tsSE_MCCustomDataStructure *ppsMCCustomDataStructure,
		uint32 u32MessageId,
		teSE_MCEventList *peEventList);

extern	teSE_MCStatus eSE_MCRemoveFromSchedulerList(
		tsSE_MCCustomDataStructure *psMCCustomDataStructure,
		tsSE_MCDisplayMessageCommandPayloadRecord *psDisplayMessageCommandPayloadRecord,
		teSE_MCEventList eEventList,
		bool_t bUserConfirm);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC	teRunningStates		eRunningState;
PUBLIC	tsDeviceLevels		sDeviceLevels;
PUBLIC	uint8 				u8CurrentElectricityCostTier;
PUBLIC	uint8				u8CurrentElectricityNumberCostTiers;
PUBLIC	uint8				u8NumberOfSimpleMeteringRequests = 0;
PUBLIC	bool_t				bLostComms;

#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
PUBLIC uint8				u8AppliedUpdatePeriod;
PUBLIC uint32				u32FastPollEndTime;
#endif

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern 	APP_teDisplayState 	app_eDisplayState;
extern	tsSE_IPDDevice		asSE_IPDDevice[SE_NUMBER_OF_ENDPOINTS];
extern 	tsDevice 			s_sDevice;
extern 	uint8 				u8CurrentMeterOfInterest;
extern 	uint8				u8TotalNumberOfDiscoveredMeters;

#ifdef EVK_DEVICE
extern 	uint8 				Ycoord;
#endif

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/
#ifdef	LC_DEVICE
/****************************************************************************
 *
 * NAME: APP_SoftReset
 *
 * DESCRIPTION:
 * Handles clearing the context after a reset button hold
 *
 * PARAMETERS:
 *
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
OS_TASK(APP_SoftReset)
{
	/* The reset button timer has expired... */
	/* Clear context before a software reset */
	if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_tmrResetButton))
	{
		OS_eStopSWTimer(APP_tmrResetButton);
		DBG_vPrintf(TRACE_IPD_NODE, "Leave Network\n");
		ZPS_eAplZdoLeaveNetwork(0, FALSE, FALSE);

		DBG_vPrintf(TRACE_IPD_NODE, "Clear Context\n");
		PDM_vDelete();															// Clear context
		OS_eEnterCriticalSection(hSpiMutex);									// Prevent further access to the flash

		DBG_vPrintf(TRACE_IPD_NODE, "Resetting...\n");

		vAHI_SwReset();
	}
}
#endif

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleRunningEvent
 *
 * DESCRIPTION:
 * Forwards any event to the relevant event handler
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PUBLIC void vHandleRunningEvent(APP_tsEvent sAppEvent)
{
	switch (app_eDisplayState)													// Determine the display state
	{
		case APP_E_DISPLAY_STATE_SPLASH:
		case APP_E_DISPLAY_STATE_SYNC:
		case APP_E_DISPLAY_STATE_SYNC_FAILED:
		break;

		case APP_E_DISPLAY_STATE_CURRENT_KW:
		case APP_E_DISPLAY_STATE_CURRENT_M3:
		case APP_E_DISPLAY_STATE_CURRENT_L:
			vHandleMeteringStateEvent(sAppEvent);
		break;

		case APP_E_DISPLAY_STATE_SETTINGS:
			vHandleESettingsStateEvent(sAppEvent);
		break;

		case APP_E_DISPLAY_STATE_PRICE:
			vHandlePriceStateEvent(sAppEvent);
		break;

		case APP_E_DISPLAY_STATE_HISTORY:
			vHandleHistoryStateEvent(sAppEvent);
		break;

		case APP_E_DISPLAY_STATE_MESSAGE:
		    vHandleMessageStateEvent(sAppEvent);
		break;

		case APP_E_DISPLAY_STATE_MESSAGE_CONFIRMATION:
			vHandleMessageConfirmationStateEvent(sAppEvent);
		break;

		case APP_E_DISPLAY_STATE_DRLC:
			vHandleDRLCStateEvent(sAppEvent);
		break;

		default:
			HandleUnknownDisplayState();
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Unknown Display State\n");		// error - unknown display state
		break;
	}
}


/****************************************************************************
 *
 * NAME: vGetDeviceLevels
 *
 * DESCRIPTION:
 * Mutexes then copies the IPD data for application manipulation
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vGetDeviceLevels(tsDeviceLevels *sParamDeviceLevels)
{
	OS_eEnterCriticalSection(Device_Parameters);
    *sParamDeviceLevels = sDeviceLevels;
    OS_eExitCriticalSection(Device_Parameters);
}


/****************************************************************************
 *
 * NAME: vSetDeviceLevels
 *
 * DESCRIPTION:
 * Mutexes the IPD data allowing the battery level to be updated
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSetDeviceLevels(tsDeviceLevels sParamDeviceLevels, APP_teDeviceLevels APP_eDeviceLevels)
{
	OS_eEnterCriticalSection(Device_Parameters);
	switch (APP_eDeviceLevels)
	{
		case LQI:
			sDeviceLevels.u8Lqi = sParamDeviceLevels.u8Lqi;
		break;

		case BATTERY_VOLTAGE:
			sDeviceLevels.u8BattLevel = sParamDeviceLevels.u8BattLevel;
		break;

		case TEMPERATURE:
			sDeviceLevels.u8Temp = sParamDeviceLevels.u8Temp;
		break;

		default:
		break;
	}
	OS_eExitCriticalSection(Device_Parameters);
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleMeteringStateEvent
 *
 * DESCRIPTION:
 * Handles any real-time event in Metering mode (button pushes etc.)
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleMeteringStateEvent(APP_tsEvent sAppEvent)
{
	/* Action any button events... */
	/* Button press detected */
	if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 pressed
		{
			OS_eStopSWTimer(APP_tmrModeButton);									// Start a two second timer to detect a button hold
			OS_eStartSWTimer(APP_tmrModeButton, APP_TIME_MS(2000), NULL);
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Press Detected\n");
			OS_eStopSWTimer(APP_tmrResetButton);								// Start a five second timer to detect a reset with context clear
			OS_eStartSWTimer(APP_tmrResetButton, APP_TIME_MS(5000), NULL);
		}
#endif
	}
	/* Button release detected */
	else if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 released
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Button 1 (mode) released in running...\n");
			if (OS_E_SWTIMER_EXPIRED != OS_eGetSWTimerStatus(APP_tmrModeButton))// Button timer not yet expired...
			{
				OS_eStopSWTimer(APP_tmrModeButton);
				/* Change display mode - The LC build only supports two *
				 * energy modes as the price is displayed permanently   */
#ifdef LC_DEVICE
				if(sDeviceLevels.u8EnergyMode >= 1)
#else
				if(sDeviceLevels.u8EnergyMode >= 2)
#endif
				{
					sDeviceLevels.u8EnergyMode = 0;
				}
				else
				{
					sDeviceLevels.u8EnergyMode++;
				}
#ifdef LC_DEVICE
				vClearLcd();		// Clear the LCD
#endif

				APP_vDisplayUpdate();
			}
		}
		else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)			// Button 2 released
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Button 2 (action) released in running...\n");
#ifdef CLD_SM_SUPPORT_GET_PROFILE
			APP_vHandleGetProfileRequest();
#endif
		}
		else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)			// Button 3 released
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Button 3 (down) released vHandleMeteringStateEvent...\n");
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_SETTINGS);
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);
		}
		else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)			// Button 4 released
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Button 4 (up) released vHandleMeteringStateEvent...\n");
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_PRICE);
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Released\n");
            vAHI_SwReset();														// Reset button released, do a soft reset
		}
#endif
	}
	else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_tmrModeButton))	// Button timer expired...
	{
		vClearExpiredFlag(APP_tmrModeButton);									// Clear the expired flag
		vChangeCurrentMeterOfInterest();
		APP_vDisplayUpdate();
		if (sDeviceLevels.u8EnergyMode == 0)
		{
			sDeviceLevels.u8EnergyMode = 2;
		}
		else
		{
			sDeviceLevels.u8EnergyMode--;
		}
	}

	/* Process the timers if we are not waiting to sleep */
	else if (!bGetAppSleep())
	{
		vHandleTimerExpiries(sAppEvent);
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "IPD waiting to sleep\n");
#if TRACE_TIMERS
		vDebugTimers();															// Print out timer status
#endif
	}
}


/****************************************************************************
 *
 * NAME: vHandleESettingsStateEvent
 *
 * DESCRIPTION:
 * Handles events and timers in 'settings' mode
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  	R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleESettingsStateEvent(APP_tsEvent sAppEvent)
{
#ifdef EVK_DEVICE
	#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
		uint8 u8MaxY = 4;
	#else
		uint8 u8MaxY = 3;
	#endif
#endif

	/* Action any button events... */
	/* Button press detected */
	if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
	{
#ifdef LC_DEVICE
		if(APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Press Detected\n");
			OS_eStopSWTimer(APP_tmrResetButton);								// Start a five second timer to detect a reset with context clear
			OS_eStartSWTimer(APP_tmrResetButton, APP_TIME_MS(5000), NULL);
		}
#endif
	}
	/* Button release detected */
	else if (sAppEvent.eType == APP_E_EVENT_BUTTON_UP)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 released
		{
#ifdef LC_DEVICE
			switch (app_eSettingsState)
			{
				case APP_E_SETTINGS_STATE_CURRENCY:
					/* Increment currency type whilst preventing an overflow */
					if (E_EUROS == sDeviceLevels.u8CurrencyType)
					{
						sDeviceLevels.u8CurrencyType = E_POUNDS;
					}
					else
					{
						sDeviceLevels.u8CurrencyType++;
					}
				break;

				case APP_E_SETTINGS_STATE_TEMPERATURE:
					/* Cycle the temperature unit */
					if (E_FAHRENHEIT == sDeviceLevels.bTempType)
					{
						sDeviceLevels.bTempType = E_CELSIUS;
					}
					else
					{
						sDeviceLevels.bTempType = E_FAHRENHEIT;
					}
				break;

				case APP_E_SETTINGS_STATE_FASTPOLL:
					APP_vHandleFastPollModeRequest();
				break;

				default:
				break;
			}
#else
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Button 1 up\n");
			if (Ycoord < u8MaxY)
			{
				Ycoord++;
			}
			else
			{
				Ycoord = 2;
			}
#endif
		}
		else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)			// Button 2 released
		{
#ifdef LC_DEVICE
			/* Increment Settings State whilst preventing a state overflow */
			if (APP_E_SETTINGS_STATE_FASTPOLL == app_eSettingsState)
			{
				app_eSettingsState = APP_E_SETTINGS_STATE_CURRENCY;
			}
			else
			{
				app_eSettingsState++;
			}
#else
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Button 2 up\n");
			if (Ycoord == 3)
			{
				if (sDeviceLevels.bTimeType == FALSE)
				{
					sDeviceLevels.bTimeType = TRUE;
				}
				else
				{
					sDeviceLevels.bTimeType = FALSE;
				}
			}
			else if (Ycoord == 2)
			{
				if (sDeviceLevels.u8CurrencyType == 0)
				{
					sDeviceLevels.u8CurrencyType = 1;
				}
				else if (sDeviceLevels.u8CurrencyType == 1)
				{
					sDeviceLevels.u8CurrencyType = 2;
				}
				else
				{
					sDeviceLevels.u8CurrencyType = 0;
				}
			}
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
			else if (Ycoord == 4)
			{
				DBG_vPrintf(TRACE_EVENT_HANDLER, "APP_vHandleFastPollModeRequest up\n");
				APP_vHandleFastPollModeRequest();

				APP_vDisplaySetState(APP_E_DISPLAY_STATE_CURRENT_KW);
				APP_vDisplayUpdate();

			}
#endif
#endif
		}
		else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)			// Down button released
		{
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_HISTORY);
			OS_eActivateTask(APP_IPDTask);
		}
		else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)			// Up button released
		{
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_CURRENT_KW);
			OS_eActivateTask(APP_IPDTask);
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Released\n");
			vAHI_SwReset();														// Reset button released, do a soft reset
		}
#endif
		APP_vDisplayUpdate();													// Update the display
	}

	/* Action any other real-time events... */
	/* Process the timers if we are not waiting to sleep */
	else if (!bGetAppSleep())
	{
		vHandleTimerExpiries(sAppEvent);
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "IPD waiting to sleep\n");
#if TRACE_TIMERS
		vDebugTimers();															// Print out timer status
#endif
	}

#ifdef EVK_DEVICE
#ifndef CLD_SM_SUPPORT_FAST_POLL_MODE
	if (Ycoord == 4)
	{
		Ycoord = 2;
	}
	else if (Ycoord == 1)
	{
		Ycoord = 3;
	}
#else
	if (Ycoord == 5)
	{
		Ycoord = 2;
	}
	else if (Ycoord == 1)
	{
		Ycoord = 4;
	}
#endif
#endif
}


/****************************************************************************
 *
 * NAME: vHandlePriceStateEvent
 *
 * DESCRIPTION:
 * Handles any real-time event in price mode (button pushes etc.)
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandlePriceStateEvent(APP_tsEvent sAppEvent)
{
	/* Action any button events...
	 * Button press detected
	 */
	if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
	{
#ifdef LC_DEVICE
		if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Press Detected\n");
			OS_eStopSWTimer(APP_tmrResetButton);								// Start a five second timer to detect a reset with context clear
			OS_eStartSWTimer(APP_tmrResetButton, APP_TIME_MS(5000), NULL);
		}
#endif
	}
	/* Button release detected */
	else if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 released
		{
			APP_vGetCurrentPrice();												// Request current price information
		}
		else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)			// Button 2 released
		{
			APP_vGetScheduledPrices();											// Request scheduled price information
		}
		else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)			// Button 3 released
		{
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_CURRENT_KW);
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);
		}
		else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)			// Button 4 released
		{
#ifdef CLD_DRLC
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_DRLC);
#else
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_MESSAGE);
			APP_vStartGetMessage();
#endif
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);										// Re-activate the task
		}
	}
	/* Process the timers if we are not waiting to sleep */
	else if (!bGetAppSleep())
	{
		vHandleTimerExpiries(sAppEvent);
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "IPD waiting to sleep\n");
#if TRACE_TIMERS
		vDebugTimers();															// Print out timer status
#endif
	}
}


/****************************************************************************
 *
 * NAME: vHandleDRLCStateEvent
 *
 * DESCRIPTION:
 * Handles any real-time event in DRLC mode (button pushes etc.)
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleDRLCStateEvent(APP_tsEvent sAppEvent)
{
	/* Action any button events...
	 * Button press detected
	 */
	if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 pressed
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\nStart opt in/out timer\n");
			OS_eStopSWTimer(APP_ButtonHoldTimer);
			OS_eStartSWTimer(APP_ButtonHoldTimer, APP_TIME_MS(2000), NULL);
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Press Detected\n");
			OS_eStopSWTimer(APP_tmrResetButton);								// Start a five second timer to detect a reset with context clear
			OS_eStartSWTimer(APP_tmrResetButton, APP_TIME_MS(5000), NULL);
		}
#endif
	}
	/* Button release detected */
	else if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 released
		{
			OS_teStatus teStatus = OS_eGetSWTimerStatus(APP_ButtonHoldTimer);

			DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n******************************************************");
			DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\nTimer Status %d", teStatus);
			DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n******************************************************");

#ifdef CLD_DRLC
			teSE_DRLCStatus eSE_DRLCStatus;
			tsSE_DRLCLoadControlEvent *sSE_DRLCLoadControlEvent;

			eSE_DRLCStatus = eSE_DRLCGetLoadControlEvent(
					1,
					0,
					E_SE_DRLC_EVENT_LIST_SCHEDULED,
					&sSE_DRLCLoadControlEvent);

			if (eSE_DRLCStatus != E_ZCL_SUCCESS)
			{
				if(E_SE_DRLC_EVENT_NOT_FOUND == eSE_DRLCStatus)
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "No Scheduled Events, reading active events\n");

					eSE_DRLCStatus = eSE_DRLCGetLoadControlEvent(
							1,
							0,
							E_SE_DRLC_EVENT_LIST_ACTIVE,
							&sSE_DRLCLoadControlEvent);

					if (eSE_DRLCStatus != E_ZCL_SUCCESS)
					{
						DBG_vPrintf(TRACE_EVENT_HANDLER, "\neSE_DRLCGetLoadControlEvent - Active: 0x%x\n", eSE_DRLCStatus);
						return;
					}
				}
				else
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "\neSE_DRLCGetLoadControlEvent - Scheduled: 0x%x\n", eSE_DRLCStatus);
				}
			}

			if ( OS_E_SWTIMER_RUNNING == teStatus )
			{
				eSE_DRLCStatus = eSE_DRLCSetEventUserOption(
										sSE_DRLCLoadControlEvent->u32IssuerId,
										IPD_BASE_LOCAL_EP,
										E_SE_DRLC_EVENT_USER_OPT_OUT);
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n******************************************************");
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\nOpt out: 0x%x", eSE_DRLCStatus);
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n******************************************************");
			}
			else
			{
				eSE_DRLCStatus = eSE_DRLCSetEventUserOption(
										sSE_DRLCLoadControlEvent->u32IssuerId,
										IPD_BASE_LOCAL_EP,
										E_SE_DRLC_EVENT_USER_OPT_IN);
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n******************************************************");
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\nOpt in: 0x%x", eSE_DRLCStatus);
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n******************************************************");
			}
#endif
		}
		else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)			// Button 2 released
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n GetScheduledEvents\n");
			APP_vGetScheduledEvents();
		}
		else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)			// Button 3 released
		{
			//DBG_vPrintf(TRACE_IPD_NODE, "Button 3 pushed in running...\n");
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_PRICE);
			APP_vDisplayUpdate();
			OS_eActivateTask(APP_IPDTask);
		}
		else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)			// Button 4 released
		{
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_MESSAGE);
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);										// Re-activate the task
			APP_vStartGetMessage();
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Released\n");
            vAHI_SwReset();														// Reset button released, do a soft reset
		}
#endif
	}
	/* Process the timers if we are not waiting to sleep */
	else if (!bGetAppSleep())
	{
		vHandleTimerExpiries(sAppEvent);
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "IPD waiting to sleep\n");
#if TRACE_TIMERS
		vDebugTimers();															// Print out timer status
#endif
	}
}


/****************************************************************************
 *
 * NAME: vHandleMessageStateEvent
 *
 * DESCRIPTION:
 * Handles any real-time event in message mode (button pushes etc.)
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleMessageStateEvent(APP_tsEvent sAppEvent)
{
	/* Action any button events...
	 * Button press detected
	 */
	if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
	{
#ifdef LC_DEVICE
		if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Press Detected\n");
			OS_eStopSWTimer(APP_tmrResetButton);								// Start a five second timer to detect a reset with context clear
			OS_eStartSWTimer(APP_tmrResetButton, APP_TIME_MS(5000), NULL);
		}
#endif
	}
	/* Button release detected */
	else if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 released
		{
			APP_vStartGetMessage();
		}
		else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)			// Button 2 released
		{
			vClearLocalActiveMessage();
			APP_vDisplayUpdate();												// Update the display
		}
		else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)			// Button 3 released
		{
			/* Change display state */
#ifdef CLD_DRLC
			/* This button is not currently used in this state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_DRLC);
#else
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_PRICE);
#endif
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);
		}
		else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)			// Button 4 released
		{
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_HISTORY);
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Released\n");
            vAHI_SwReset();														// Reset button released, do a soft reset
		}
#endif
	}
	/* Process the timers if we are not waiting to sleep */
	else if (!bGetAppSleep())
	{
		vHandleTimerExpiries(sAppEvent);
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "IPD waiting to sleep\n");
#if TRACE_TIMERS
		vDebugTimers();															// Print out timer status
#endif
	}
}


/****************************************************************************
 *
 * NAME: vHandleMessageConfirmationStateEvent
 *
 * DESCRIPTION:
 * Handles any real-time event in message confirmation mode (button pushes etc.)
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleMessageConfirmationStateEvent(APP_tsEvent sAppEvent)
{
	/* Action any button events...
	 * Button press detected
	 */
	if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
	{
#ifdef LC_DEVICE
		if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Press Detected\n");
			OS_eStopSWTimer(APP_tmrResetButton);								// Start a five second timer to detect a reset with context clear
			OS_eStartSWTimer(APP_tmrResetButton, APP_TIME_MS(5000), NULL);
		}
#endif
	}
	/* Button release detected */
	else if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 released
		{
			teSE_MCStatus eMCStatus;
			tsSE_MCDisplayMessageCommandPayload *psPayload;

			eMCStatus = eSE_MCGetMessage(IPD_BASE_LOCAL_EP, 0, E_SE_MC_MESSAGE_ACTIVE, &psPayload);
			if (eMCStatus == E_ZCL_SUCCESS)
			{
				eMCStatus = eSE_MCMessageConfirmationUserSend(
				IPD_BASE_LOCAL_EP,
				psPayload->u32MessageId,
				E_SE_MC_MESSAGE_LIST_ACTIVE);
				if (eMCStatus != E_ZCL_SUCCESS)
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "eSE_MCMessageConfirmationUserSend: 0x%x\n", eMCStatus);
				}

				APP_vDisplaySetState(APP_E_DISPLAY_STATE_MESSAGE);
				APP_vDisplayUpdate();											// Update the display

			}
		}
		else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)			// Button 2 released
		{
			teSE_MCStatus eMCStatus;
			tsSE_MCDisplayMessageCommandPayload *psPayload;

			eMCStatus = eSE_MCGetMessage(IPD_BASE_LOCAL_EP, 0, E_SE_MC_MESSAGE_LIST_CANCEL_PENDING, &psPayload);
			if (eMCStatus == E_ZCL_SUCCESS)
			{
				eMCStatus = eSE_MCMessageConfirmationUserSend(
				IPD_BASE_LOCAL_EP,
				psPayload->u32MessageId,
				E_SE_MC_MESSAGE_LIST_CANCEL_PENDING);
				if (eMCStatus != E_ZCL_SUCCESS)
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "eSE_MCMessageConfirmationUserSend: 0x%x\n", eMCStatus);
				}

				APP_vDisplaySetState(APP_E_DISPLAY_STATE_MESSAGE);
				APP_vDisplayUpdate();											// Update the display
			}
		}
		else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)			// Button 3 released
		{
			/* This button is not currently used in this state */
		}
		else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)			// Button 4 released
		{
			/* This button is not currently used in this state */
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Released\n");
            vAHI_SwReset();														// Reset button released, do a soft reset
		}
#endif
	}
	/* Process the timers if we are not waiting to sleep */
	else if (!bGetAppSleep())
	{
		vHandleTimerExpiries(sAppEvent);
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "IPD waiting to sleep\n");
#if TRACE_TIMERS
		vDebugTimers();															// Print out timer status
#endif
	}
}


/****************************************************************************
 *
 * NAME: vHandleHistoryStateEvent
 *
 * DESCRIPTION:
 * Handles any real-time event in tier mode (button pushes etc.)
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleHistoryStateEvent(APP_tsEvent sAppEvent)
{
	/* Action any button events...
	 * Button press detected
	 */
	if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
	{
#ifdef LC_DEVICE
		if(APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Press Detected\n");
			OS_eStopSWTimer(APP_tmrResetButton);								// Start a five second timer to detect a reset with context clear
			OS_eStartSWTimer(APP_tmrResetButton, APP_TIME_MS(5000), NULL);
		}
#endif
	}
	/* Button release detected */
	else if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
	{
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)				// Button 1 released
		{
			App_QueryNextImageRequest();

		}
		else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)			// Button 2 released
		{
			/* This button is not currently used in this state */
		}
		else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)			// Button 3 released
		{
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_MESSAGE);
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);
			APP_vStartGetMessage();
		}
		else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)			// Button 4 released
		{
			/* Change display state */
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_SETTINGS);
			APP_vDisplayUpdate();												// Update the display
			OS_eActivateTask(APP_IPDTask);
		}
#ifdef LC_DEVICE
		else if (APP_E_BUTTONS_BUTTON_RST == sAppEvent.sButton.u8Button)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Reset Button Released\n");
            vAHI_SwReset();														// Reset button released, do a soft reset
		}
#endif
	}
	/* Process the timers if we are not waiting to sleep */
	else if (!bGetAppSleep())
	{
		vHandleTimerExpiries(sAppEvent);
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "IPD waiting to sleep\n");
#if TRACE_TIMERS
		vDebugTimers();															// Print out timer status
#endif
	}
}


/****************************************************************************
 *
 * NAME: vHandleTimerExpiries
 *
 * DESCRIPTION:
 * Timer expiry and read complete events
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleTimerExpiries(APP_tsEvent sAppEvent)
{
#ifdef CLD_PRICE
	tsSE_PricePublishPriceCmdPayload *pPricePublishPriceCmdPayload;
#endif

	switch (eRunningState)
	{
		case E_INIT:
			vMoveToNextDataReqState();
		break;

#ifdef CLD_TIME
		case E_TIME:
			if ( !bZCL_GetTimeHasBeenSynchronised() )
			{
				/* If time sync not underway, request the time */
				if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_UTC_Timer))
				{
					vRequestTime();
					OS_eStartSWTimer(APP_UTC_Timer, READ_REQUEST_TIMEOUT, NULL);	// Start a 'time request pending' timer. Timer stopped in ZCL task when a read attributes response is rx'd
				}
			}
			else
			{
				vMoveToNextDataReqState();
			}
		break;
#endif

		case E_SAMPLE_ADC:
			APP_vAdcSample();													// Refresh device information
			vMoveToNextDataReqState();
		break;

#ifdef CLD_MC
		case E_MESSAGE_REQUEST:
#if REQUEST_MESSAGE_EVERY_WAKE_CYCLE
			/* Message timer has expired, query again */
			if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_MsgTimer))
			{
				APP_vStartGetMessage();
				OS_eStartSWTimer(APP_MsgTimer, READ_REQUEST_TIMEOUT, NULL);
			}
#else
			vMoveToNextDataReqState();
#endif
		break;

		case E_MESSAGE_RECEIVED:
			vMoveToNextDataReqState();
		break;
#endif


		case E_PRICE:
#ifdef CLD_PRICE
			/* Start a price read if not currently underway, or requesting the time */
			vLockZCLMutex();												// Mutex the price data
			DBG_vPrintf(TRACE_EVENT_HANDLER, "u8CurrentMeterOfInterest: %i, index: %i\n", u8CurrentMeterOfInterest, s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex);
			/* If price data is valid, update price and price tier */
			teSE_PriceStatus eSE_PriceStatus = eSE_PriceGetPriceEntry(IPD_BASE_LOCAL_EP + s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex, FALSE, 0,&pPricePublishPriceCmdPayload);
			if (!eSE_PriceStatus)
			{
				u8CurrentElectricityCostTier = 0x0F & pPricePublishPriceCmdPayload->u8PriceTrailingDigitAndPriceTier;
				u8CurrentElectricityNumberCostTiers = pPricePublishPriceCmdPayload->u8NumberOfPriceTiersAndRegisterTiers >> 4;
				DBG_vPrintf(TRACE_EVENT_HANDLER, "Current Price Tier: %i, Number of Price Tiers: %i\n", u8CurrentElectricityCostTier, u8CurrentElectricityNumberCostTiers);
				vMoveToNextDataReqState();

#ifdef PRICE_CONVERSION_FACTOR
				DBG_vPrintf(TRACE_EVENT_HANDLER,"Get conversion factor\n");
				tsZCL_Address sAddress;
				uint8 u8Trans;
				sAddress.eAddressMode = E_ZCL_AM_SHORT;
				sAddress.uAddress.u16DestinationAddress = 0;
				teSE_PriceStatus eSE_PriceStatus = eSE_PriceGetConversionFactorSend(IPD_BASE_LOCAL_EP,1,&sAddress,&u8Trans,0,5);
				if (eSE_PriceStatus != E_ZCL_SUCCESS)
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "eSE_PriceGetConversionFactorSend Error: 0x%x\r\n", eSE_PriceStatus);

					ZPS_teStatus eStatus = eZCL_GetLastZpsError();
					DBG_vPrintf(TRACE_EVENT_HANDLER, "ZPS Error: 0x%x\n", eStatus);
				}
#endif
			}
			/* There should always be a price, perhaps we were out of range when it was published, re-request it */
			else
			{
				if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_PriceTimer))
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "No Active Price: 0x%02x\n", eSE_PriceStatus);
					DBG_vPrintf(TRACE_EVENT_HANDLER, "EP: 0x%02x, u8CurrentMeterOfInterest: %i\n", IPD_BASE_LOCAL_EP + s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex, u8CurrentMeterOfInterest);
					APP_vGetScheduledPrices();									// Request scheduled price information
					OS_eStartSWTimer(APP_PriceTimer, READ_REQUEST_TIMEOUT, NULL);// Start a 'price request pending' timer. Timer stopped in ZCL task when a read attributes response is rx'd
				}
			}
			vUnlockZCLMutex();
#else
			u8CurrentElectricityCostTier = 1;
			u8CurrentElectricityNumberCostTiers = 1;
			vMoveToNextDataReqState();
#endif
		break;

#ifdef CLD_SIMPLE_METERING
		case E_METERING:
			if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_Meter_Timer))
			{
				vRequestMeteringData();
				OS_eStartSWTimer(APP_Meter_Timer, READ_REQUEST_TIMEOUT, NULL);	// Start a 'metering data request pending' timer. Timer stopped in ZCL task when a read attributes response is rx'd
			}
		break;
#endif

		case E_LED_ON:
			APP_vDisplayUpdate();												// Update the display
			/* Turn on the LED that corresponds to the current price tier, indicating that a message has been received */
			vLedSet(u8CurrentElectricityNumberCostTiers, u8CurrentElectricityCostTier);
			OS_eStartSWTimer(APP_LED_Timer, APP_TIME_MS(50), NULL);				// Set up a timer to turn the LEDs off after 50ms
			vMoveToNextDataReqState();
		break;

		case E_LED_OFF:
			if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_LED_Timer))
			{
				vClearExpiredFlag(APP_LED_Timer);
				vLedClear();
				vMoveToNextDataReqState();
			}
		break;

#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
		case E_FAST_POLL:
			if (u32FastPollEndTime >= u32ZCL_GetUTCTime())
			{
				if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_Meter_Timer))
				{
					OS_eStartSWTimer(APP_Meter_Timer, APP_TIME_MS(u8AppliedUpdatePeriod*1000), NULL);
					vRequestMeteringData();
				}
			}
			else
			{
				vMoveToNextDataReqState();
			}
		break;
#endif

		case E_SLEEP:
			if (app_eDisplayState != APP_E_DISPLAY_STATE_SETTINGS)
			{
				vConfigureSleep();												// Data cycle complete, initiate sleep
			}
		break;

		default:
		break;
	}
}


/****************************************************************************
 *
 * NAME: vMoveToNextDataReqState
 *
 * DESCRIPTION:
 * Move on to the next data request state
 *
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vMoveToNextDataReqState(void)
{
	eRunningState++;
	OS_eActivateTask(APP_IPDTask);
}


/****************************************************************************
 *
 * NAME: vRequestMeteringData
 *
 * DESCRIPTION:
 * Request the metering data
 * events
 *
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vRequestMeteringData(void)
{
	APP_ZCL_RefreshIPDData();													// Request metering information

	if (MAXIMUM_NUMBER_OF_READ_ATTRIBUTE_REQUESTS > u8NumberOfSimpleMeteringRequests)
	{
		u8NumberOfSimpleMeteringRequests++;
	}
	else
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "u8NumberOfSimpleMeteringRequests exceeded\n");

		bLostComms = TRUE;
		APP_vDisplayUpdate();													// Update the display
	}
}


/****************************************************************************
 *
 * NAME: vChangeCurrentMeterOfInterest
 *
 * DESCRIPTION:
 * Increment the current meter of interest
 * events
 *
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vChangeCurrentMeterOfInterest(void)
{
	uint8 u8ChangeCounter = 0;

	DBG_vPrintf(TRACE_EVENT_HANDLER, "Change Meter of Interest: %i\n", u8CurrentMeterOfInterest);

	/* Increment the current meter of interest */
	do
	{
		if (u8CurrentMeterOfInterest >= (SE_NUMBER_OF_ENDPOINTS - 1))			// Move the current meter of interest to the next available meter
		{
			u8CurrentMeterOfInterest = 0;
		}
		else
		{
			u8CurrentMeterOfInterest++;
		}

		if (SE_NUMBER_OF_ENDPOINTS < u8ChangeCounter++)
		{
			break;																// Break from the loop if there are no meters left in the structure
		}
	} while (!s_sDevice.asMeter[u8CurrentMeterOfInterest].bValidKey);
	DBG_vPrintf(TRACE_EVENT_HANDLER, "New meter of interest: %i\n", u8CurrentMeterOfInterest);

	u8NumberOfSimpleMeteringRequests = 0;

	/* Set to display '----' as there will be no valid value */
	bLostComms = TRUE;

	/* Move to meterining state, and stop the timer to trigger
	 *  a read of the new meter */

	eRunningState = E_METERING;
	OS_eStopSWTimer(APP_Meter_Timer);

	/* Clear the sleep flag to allow the request to go out */
	vSetAppSleep(FALSE);
	if ( OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_PollTimer) )			// Don't re-activate the task if the poll timer is already running
	{
		OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(1), NULL);
	}

	APP_vDisplayUpdate();														// Update the display
	OS_eActivateTask(APP_IPDTask);												// Re-activate the task
}


/****************************************************************************
 *
 * NAME: vClearLocalActiveMessage
 *
 * DESCRIPTION:
 * Clear the currently displayed message
 *
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vClearLocalActiveMessage(void)
{
	teSE_MCStatus eSE_MCStatus;
	tsZCL_EndPointDefinition *psEndPoint;
	tsZCL_ClusterInstance *psClusterInstance;
	tsSE_MCCustomDataStructure *psMCCustomDataStructure;

	psEndPoint = &asSE_IPDDevice[u8CurrentMeterOfInterest].sEndPoint;
	psClusterInstance = &asSE_IPDDevice[u8CurrentMeterOfInterest].sClusterInstance.sMCClient;

	eSE_MCStatus = eSE_MCFindMCCluster(
			IPD_BASE_LOCAL_EP,								// uint8 u8SourceEndPointId,
			FALSE,											// bool_t bIsServer,
			&psEndPoint,									// tsZCL_EndPointDefinition **ppsEndPointDefinition,
			&psClusterInstance,								// tsZCL_ClusterInstance **ppsClusterInstance,
			&psMCCustomDataStructure						// tsSE_MCCustomDataStructure **ppsMCCustomDataStructure
			);

	if (eSE_MCStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_EVENT_HANDLER, "Error - eSE_MCFindMCCluster: 0x%x\n", eSE_MCStatus);
	}
	else
	{
		teSE_MCEventList eSE_MCEventList = E_SE_MC_MESSAGE_LIST_ACTIVE;
		tsSE_MCDisplayMessageCommandPayload *psPayload;

		eSE_MCStatus = eSE_MCGetMessage(
				IPD_BASE_LOCAL_EP,
				0,
				eSE_MCEventList,
				&psPayload);

		if (eSE_MCStatus != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Error - eSE_MCGetMessage: 0x%x\n", eSE_MCStatus);
		}
		else
		{
			tsSE_MCDisplayMessageCommandPayloadRecord *psDisplayMessageCommandPayloadRecord;

			psDisplayMessageCommandPayloadRecord = psCheckToSeeIfMessageExists(
					psMCCustomDataStructure,				// tsSE_MCCustomDataStructure *psMCCustomDataStructure,
					psPayload->u32MessageId,				// uint32 u32MessageId,
					&eSE_MCEventList						// teSE_MCEventList *peEventList
					);

			eSE_MCStatus = eSE_MCRemoveFromSchedulerList(
					psMCCustomDataStructure,				// tsSE_MCCustomDataStructure *psMCCustomDataStructure,
					psDisplayMessageCommandPayloadRecord,	// tsSE_MCDisplayMessageCommandPayloadRecord *psDisplayMessageCommandPayloadRecord,
					eSE_MCEventList,						// teSE_MCEventList eEventList,
					FALSE									// bool_t bUserConfirm
					);

			if (eSE_MCStatus != E_ZCL_SUCCESS)
			{
				DBG_vPrintf(TRACE_EVENT_HANDLER, "Error - eSE_MCRemoveFromSchedulerList: 0x%x\n", eSE_MCStatus);
			}
		}
	}
}

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
