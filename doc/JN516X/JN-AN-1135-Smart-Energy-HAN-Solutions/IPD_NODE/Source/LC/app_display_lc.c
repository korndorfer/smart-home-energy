/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (LC-IPD)
 *
 * COMPONENT:          app_display.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Display Functionality
 *
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_display_lc.c $
 *
 * $Revision: 8954 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-04-16 13:01:29 +0100 (Mon, 16 Apr 2012) $
 *
 * $Id: app_display_lc.c 8954 2012-04-16 12:01:29Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/


/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include "jendefs.h"
#include "utilities.h"
#include <AppHardwareApi.h>
#include "os.h"
#include "os_gen.h"
#include "app_display.h"
#include "dbg.h"
#include "zcl.h"
#include "app_zcl_task.h"
#include "app_ipd_node.h"
#include "app_timer_driver.h"
#include "Time.h"
#include "app_sleep_functions.h"
#include "app_lcd_driver.h"
#include "app_event_handler.h"
#include "ovly.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef TRACE_DISPLAY
#define TRACE_DISPLAY FALSE
#endif

#define SEGMENT_TEST_ENABLE														// Enable LCD Segment Test

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/**************************************************************************///

// Generic LCD Control Functions
PRIVATE void vFloodLcd(void);

// LCD State Initialisation Functions
PRIVATE void vDisplaySync(void);
PRIVATE void vDisplaySyncFailed(void);
PRIVATE void vDisplayCurrentKw(void);
PRIVATE void vDisplayCurrentM3(void);
PRIVATE void vDisplayCurrentL(void);
PRIVATE void vDisplayCurrentCo2(void);
PRIVATE void vDisplaySettings(void);
PRIVATE	void vDisplayPrice(void);
PRIVATE void vDisplayDrlc(void);
PRIVATE void vDisplayMessage(void);
PRIVATE void vDisplayMessageConfirm(void);
PRIVATE void vDisplayHistory(void);

// LCD State Update Functions
PRIVATE void vDisplayUpdateCurrentKw(void);
PRIVATE void vDisplayUpdateSettings(void);
PRIVATE void vDisplayUpdatePrice(void);

// LCD Group Display Functions
PRIVATE void vDisplayMain(uint16 u16Number, uint8 u8Exp);
PRIVATE void vDisplayCost(uint16 u16Number, uint8 u8Exp);
PRIVATE void vDisplayTime(uint8 u8Hours, uint8 Minutes, bool_t bCol1);
PRIVATE void vDisplayTemperature(uint8 u8Temperature);
PRIVATE void vDisplayStatus(bool_t bSet, bool_t bMem, bool_t bAlarm, uint8 u8Batt);
PRIVATE void vDisplayUnits (bool_t bKw, bool_t bHours, bool_t bKgCO2, bool_t bDroplet, bool_t bLitre,
							bool_t bM3, bool_t bCost, bool_t bPound, bool_t bDollar, bool_t bEuro);

// LCD Character Display Functions
PRIVATE void vDisplayMainCharacters(uint8 u8Char1, uint8 u8Char2, uint8 u8Char3, uint8 u8Char4, uint8 u8Dp);
PRIVATE void vDisplayCostCharacters(uint8 u8Char5, uint8 u8Char6, uint8 u8Char7, uint8 u8Char8, uint8 u8Dp);
PRIVATE void vDisplayTimeCharacters(uint8 u8Char9, uint8 u8Char10, uint8 u8Char11, uint8 u8Char12, bool_t bCol1);
PRIVATE void vDisplayTemperatureCharacters(bool_t bT1, uint8 u8Char13, uint8 u8Char14, uint8 u8Deg);

PRIVATE void vShowTime(uint32 u32Time, uint8 * u8ParamHour, uint8 * u8ParamMin, uint8 * u8ParamSec);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/* only exported for in line functions */
APP_teDisplayState app_eDisplayState;
APP_teSettingsState app_eSettingsState;

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE volatile bool_t 		s_UpdatePending = FALSE;

PRIVATE uint8 u8LcdShadow[0x16]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

PRIVATE tsSE_IPDDevice			*psSE_IPDDevice;
PRIVATE tsDeviceLevels			sDisplayDeviceLevels;

PRIVATE uint8					u8Lqi;
PRIVATE uint8					u8BattLevel;
PRIVATE uint8					u8Temperature;

PRIVATE uint8					u8TimeHours;
PRIVATE uint8					u8TimeMinutes;

PRIVATE uint32					u32CurrentElectricityCost;
PRIVATE uint8					u8CurrentElectricityCostDp;

PRIVATE APP_teCurrencyState 	app_eCostUnit;
PRIVATE APP_teTemperatureState	app_eTemperatureUnit;

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern	bool_t					bLostComms;
extern 	uint8 					u8CurrentMeterOfInterest;
extern 	uint8 					u8TotalNumberOfDiscoveredMeters;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_DisplayUpdateTask
 *
 * DESCRIPTION:
 * Redraws the display
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_DisplayUpdateTask)
{
	static APP_teDisplayState s_ePrevDisplayState = APP_E_DISPLAY_STATE_INIT;

#ifdef CLD_PRICE
	tsSE_PricePublishPriceCmdPayload *pPricePublishPriceCmdPayload;
#endif

	bool_t bDisplayStateChanged = FALSE;
	uint8 u8SecondsTemp;

	/* Take a fresh copy of the ZCL data for displaying */
	APP_ZCL_GetIPDData(&psSE_IPDDevice);

#ifdef CLD_TIME
	// Update Time Information
	vShowTime(u32ZCL_GetUTCTime(), &u8TimeHours,  &u8TimeMinutes, &u8SecondsTemp);
#endif

#ifdef CLD_PRICE
	// Update Price Information
	vLockZCLMutex();															// Mutex the price data
	if (!eSE_PriceGetPriceEntry(IPD_BASE_LOCAL_EP + u8CurrentMeterOfInterest, FALSE, 0,&pPricePublishPriceCmdPayload))		// If price data is valid
	{
		u32CurrentElectricityCost = pPricePublishPriceCmdPayload->u32Price;		// Store Current Electricity Cost and magnitude
	}
	else
	{
		u32CurrentElectricityCost = 0;											// Else, clear Current Electricity Cost
		u8CurrentElectricityCostDp = 0;
	}
	vUnlockZCLMutex();															// Clear the mutex
#endif

    /* Take a fresh copy of the Device data for displaying */
	vGetDeviceLevels(&sDisplayDeviceLevels);
	/* Currency unit type hard-coded through the setting menu for demo purposes */
	app_eCostUnit = sDisplayDeviceLevels.u8CurrencyType;
	/* Temperature unit type hard-coded through the setting menu for demo purposes */
	app_eTemperatureUnit = sDisplayDeviceLevels.bTempType;

	if ((app_eDisplayState == APP_E_DISPLAY_STATE_CURRENT_KW || app_eDisplayState == APP_E_DISPLAY_STATE_CURRENT_M3) || app_eDisplayState == APP_E_DISPLAY_STATE_CURRENT_L)
	{
		if(E_CLD_SM_MDT_ELECTRIC == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType)
		{
			app_eDisplayState = APP_E_DISPLAY_STATE_CURRENT_KW;
		}
		else if (E_CLD_SM_MDT_GAS == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType || E_CLD_SM_MDT_GAS_MIRRORED == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType)
		{
			app_eDisplayState = APP_E_DISPLAY_STATE_CURRENT_M3;
		}
		else if (E_CLD_SM_MDT_WATER == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType || E_CLD_SM_MDT_WATER_MIRRORED == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType)
		{
			app_eDisplayState = APP_E_DISPLAY_STATE_CURRENT_L;
		}
	}

	u8BattLevel = sDisplayDeviceLevels.u8BattLevel;
	u8Temperature = sDisplayDeviceLevels.u8Temp;

	// Convert the LQI reading so that it can be displayed on the status section of the LCD
	DBG_vPrintf(TRACE_DISPLAY, "LQI: %i\n", u8Lqi);
	if (sDisplayDeviceLevels.u8Lqi > 99)
	{
		u8Lqi = 7;																// All three bars on
	}
	else if (sDisplayDeviceLevels.u8Lqi > 67)
	{
		u8Lqi = 3;																// Two bars on
	}
	else if (sDisplayDeviceLevels.u8Lqi > 35)
	{
		u8Lqi = 1;																// One bar on
	}
	else
	{
		u8Lqi = 0;																// All off
	}

    /* check to see if the state of the display is being changed */
	if (app_eDisplayState != s_ePrevDisplayState)								// If the state has changed...
	{
		bDisplayStateChanged = TRUE;											// Set the changed flag
		vClearLcd();															// Clear the LCD
		s_ePrevDisplayState = app_eDisplayState;								// Store current state for future comparison
	}

	/* update the display ... */
	switch (app_eDisplayState)													// Determine the display state
	{
		case APP_E_DISPLAY_STATE_SPLASH:
			if (bDisplayStateChanged)
			{
				vFloodLcd();													// If it is the first time in this state - generate the display
			}
		break;

		case APP_E_DISPLAY_STATE_SYNC:
			if (bDisplayStateChanged)
			{
				vDisplaySync();													// If it is the first time in this state - generate the display
			}
		break;

		case APP_E_DISPLAY_STATE_SYNC_FAILED:
			if (bDisplayStateChanged)
			{
				vDisplaySyncFailed();											// If it is the first time in this state - generate the display
			}
		break;

		case APP_E_DISPLAY_STATE_CURRENT_KW:
			if (sDisplayDeviceLevels.u8EnergyMode == 1)
			{
				vDisplayCurrentCo2();											// Generate the CO2 screen and update the display
			}
			else
			{
				vDisplayCurrentKw();											// Generate the Kw screen and update the display
			}
		break;

		case APP_E_DISPLAY_STATE_CURRENT_M3:
			if (bDisplayStateChanged)
			{
				vDisplayCurrentM3();											// If it is the first time in this state - generate the display
			}
			else
			{
				vDisplayUpdateCurrentKw();										// If not, update the relevant information
			}
		break;

		case APP_E_DISPLAY_STATE_CURRENT_L:
			if (bDisplayStateChanged)
			{
				vDisplayCurrentL();												// If it is the first time in this state - generate the display
			}
			else
			{
				vDisplayUpdateCurrentKw();										// If not, update the relevant information
			}
		break;

		case APP_E_DISPLAY_STATE_PRICE:
			if (bDisplayStateChanged)											// If it is the first time in this state - generate the display
			{
				vDisplayPrice();
			}
			else																// If not, update the relevant information
			{
				vDisplayUpdatePrice();
			}
		break;

		case APP_E_DISPLAY_STATE_DRLC:
			vDisplayDrlc();
		break;

		case APP_E_DISPLAY_STATE_MESSAGE:
			vDisplayMessage();
		break;

		case APP_E_DISPLAY_STATE_MESSAGE_CONFIRMATION:
			vDisplayMessageConfirm();
		break;

		case APP_E_DISPLAY_STATE_HISTORY:
			vDisplayHistory();
		break;

		case APP_E_DISPLAY_STATE_SETTINGS:
			if (bDisplayStateChanged)											// If it is the first time in this state - generate the display
			{
				vDisplaySettings();
			}
			else																// If not, update the relevant information
			{
				vDisplayUpdateSettings();
			}
		break;

		default:
			DBG_vPrintf(TRACE_DISPLAY, "Unknown Display State\n");				// error - unknown display state
		break;
	}
	/* set flag to indicate we have completed the requested update */
	s_UpdatePending = FALSE;
}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vDisplayInitialise
 *
 * DESCRIPTION:
 * Initialises the LCD display
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vDisplayInitialise(void)
{
	uint8 i;
	APP_vI2cInitialise();

	if (!bGetAppSleep())
	{
		/* Clear the character shadow map on initialisation */
		for ( i = 0 ; i < 16 ; i++ )
		{
			u8LcdShadow[i] = 0x00;
		}
	}
}


/****************************************************************************
 *
 * NAME: APP_vDisplayUpdate
 *
 * DESCRIPTION:
 * Activates the display update task
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void APP_vDisplayUpdate(void)
{
	if (!s_UpdatePending)
	{
        s_UpdatePending = TRUE;
        OS_eActivateTask(APP_DisplayUpdateTask);
    }
}

/****************************************************************************
 *
 * NAME: APP_vGetDisplayState
 *
 * DESCRIPTION:
 * Activates the display update task
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC APP_teDisplayState APP_vGetDisplayState(void)
{
	 return app_eDisplayState;
}
/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vDisplaySync
 *
 * DESCRIPTION:
 * Generate and display the 'Sync' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplaySync(void)
{
	vDisplayMainCharacters(35, 41, 30, 19, 0);									// Display 'Sync'
}


/****************************************************************************
 *
 * NAME: vDisplaySyncFailed
 *
 * DESCRIPTION:
 * Generate and display the 'Sync Failed' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplaySyncFailed(void)
{
	vDisplayMainCharacters(35, 41, 30, 19, 0);									// Display 'Sync'
	vDisplayCostCharacters(22, 17, 25, 28, 0);									// Display 'Fail'
}


/****************************************************************************
 *
 * NAME: vDisplayCurrentKw
 *
 * DESCRIPTION:
 * Generate and display the 'Current Electricity' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayCurrentKw(void)
{
	// Generate the 'Units' section of the display
	if (app_eCostUnit == E_POUNDS)												// Kw = On, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
		vDisplayUnits(1, 0, 0, 0, 0, 0, 1, 1, 0, 0);							// M3 = Off, Cost = On, Pound = On, Dollar = Off, Euro = Off

	else if (app_eCostUnit == E_DOLLARS)										// Kw = On, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
		vDisplayUnits(1, 0, 0, 0, 0, 0, 1, 0, 1, 0);							// M3 = Off, Cost = On, Pound = Off, Dollar = On, Euro = Off

	else																		// Kw = On, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
		vDisplayUnits(1, 0, 0, 0, 0, 0, 1, 0, 0, 1);							// M3 = Off, Cost = On, Pound = Off, Dollar = Off, Euro = On

	vDisplayUpdateCurrentKw();													// Update the 'Current Electricity' numerical data
}


/****************************************************************************
 *
 * NAME: vDisplayCurrentM3
 *
 * DESCRIPTION:
 * Generate and display the 'Current Gas' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayCurrentM3(void)
{
	// Generate the 'Units' section of the display
	if (app_eCostUnit == E_POUNDS)												// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
		vDisplayUnits(0, 0, 0, 0, 0, 1, 1, 1, 0, 0);							// M3 = On, Cost = On, Pound = On, Dollar = Off, Euro = Off

	else if (app_eCostUnit == E_DOLLARS)										// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
		vDisplayUnits(0, 0, 0, 0, 0, 1, 1, 0, 1, 0);							// M3 = On, Cost = On, Pound = Off, Dollar = On, Euro = Off

	else																		// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
		vDisplayUnits(0, 0, 0, 0, 0, 1, 1, 0, 0, 1);							// M3 = On, Cost = On, Pound = Off, Dollar = Off, Euro = On

	vDisplayUpdateCurrentKw();													// Update the 'Current Electricity' numerical data
}


/****************************************************************************
 *
 * NAME: vDisplayCurrentL
 *
 * DESCRIPTION:
 * Generate and display the 'Current Water' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayCurrentL(void)
{
	// Generate the 'Units' section of the display
	if (app_eCostUnit == E_POUNDS)												// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = On,
		vDisplayUnits(0, 0, 0, 0, 1, 0, 1, 1, 0, 0);							// M3 = Off, Cost = On, Pound = On, Dollar = Off, Euro = Off

	else if (app_eCostUnit == E_DOLLARS)										// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = On,
		vDisplayUnits(0, 0, 0, 0, 1, 0, 1, 0, 1, 0);							// M3 = Off, Cost = On, Pound = Off, Dollar = On, Euro = Off

	else																		// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = On,
		vDisplayUnits(0, 0, 0, 0, 1, 0, 1, 0, 0, 1);							// M3 = Off, Cost = On, Pound = Off, Dollar = Off, Euro = On

	vDisplayUpdateCurrentKw();													// Update the 'Current Electricity' numerical data
}


/****************************************************************************
 *
 * NAME: vDisplayUpdateCurrentKw
 *
 * DESCRIPTION:
 * Update the numerical data in the 'Current Electricity' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayUpdateCurrentKw(void)
{
	uint32 u32LocalDemandInteger, u32LocalDemandRemainder, u32LocalDemand, u32Divisor = 0;
	uint32 u32Price;

	// Update the 'Status' section of the display (specifically the battery level)
	vDisplayStatus(0, 0, 0, u8BattLevel | 0x08);								// Set = Off, Mem = Off, Alarm = Off,
																				// combine the battery level reading with the outline

	// Update the 'Main' digits
	if ( bLostComms )															// If the IPD has lost connection to the meter
	{
		vDisplayMainCharacters(10, 10, 10, 10, 0);								// IPD out of range, display '----'
		vDisplayCostCharacters(10, 10, 10, 10, 0);								// IPD out of range, display '----'
	}
	else
	{
		u32LocalDemandInteger = psSE_IPDDevice->sSimpleMeteringCluster.i24InstantaneousDemand;
		if (psSE_IPDDevice->sSimpleMeteringCluster.u24Multiplier)				// If the multiplier is non-zero
		{
			u32LocalDemandInteger *= psSE_IPDDevice->sSimpleMeteringCluster.u24Multiplier;
		}

		u32LocalDemandRemainder = u32LocalDemandInteger;
		if (psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor)					// If the divisor is non-zero
		{
			u32LocalDemandInteger /= psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor;
		}
		u32LocalDemandRemainder -= (u32LocalDemandInteger * psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor);

		/* Demand formatting =
		Bits 0 to 2: Number of Digits to the right of the Decimal Point.
		Bits 3 to 6: Number of Digits to the left of the Decimal Point.
		Bit 7: If set, suppress leading zeros.
		*/
		/* Ignoring demand formatting as only able to display 4 characters with 2 decimal points */
		//u8DigitsRightOfDp = (0x7 & psSE_IPDDevice->sSimpleMeteringCluster.u8DemandFormatting);

		/* Truncate the instantaneous demand to 2 decimal places */
		u32Divisor = psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor;
		while (u32Divisor > 999)
		{
			if (u32LocalDemandRemainder > 9)
			{
				u32LocalDemandRemainder /= 10;
				u32Divisor /= 10;
			}
			else
			{
				u32LocalDemandRemainder = 0;
				break;
			}
		}
		u32LocalDemand = (u32LocalDemandInteger * 100) + u32LocalDemandRemainder;

		if(sDisplayDeviceLevels.u8EnergyMode == 1)
		{
			/* Update the 'CO2' value */
			vDisplayMain((u32LocalDemand * CO2_CONVERSION_CONSTANT / 1000), 2);		// Send the 'current electricity' to the main display function
		}
		else
		{
			/* Update the instantaneous demand value */
			vDisplayMain(u32LocalDemand, 2);										// Send the 'current electricity' to the main display function
		}

		u32Price = u32LocalDemand  * u32CurrentElectricityCost / 100;
		vDisplayCost(u32Price, 2);
	}

	// Update the 'Time' section of the display
	vDisplayTime(u8TimeHours, u8TimeMinutes, TRUE);								// Send the 'current time' to the time display function

	if (1 >= u8TotalNumberOfDiscoveredMeters)
	{
		// Update the 'Temperature' section of the display
		vDisplayTemperature(u8Temperature);										// Send the 'current temperature' to the temperature display function
	}
	else
	{
		// Send segment data
		vEstablishLcdComms();													// Establish I2C comms with the LCD driver
		vSendLcdCharacterPointer(u8LcdLutChar[14]);								// Point to character 14
		vSendLcdCharacterData(u8LcdLut9_14[(u8CurrentMeterOfInterest + 1) % 10]);		// Send segment data for character 14
		if((u8CurrentMeterOfInterest + 1) >= 10)								// If meter identifier is greater or equal to 10
		{
			vSendLcdCharacterData(u8LcdLut9_14[(u8CurrentMeterOfInterest + 1) / 10]);	// Send segment data for character 13
		}
		else																	// Else display a 'space' instead of a leading zero
		{
			vSendLcdCharacterData(u8LcdLut9_14[16]);							// Send segment data for character 13
		}
		vTerminateLcdComms();
	}
}


/****************************************************************************
 *
 * NAME: vDisplayCurrentCo2
 *
 * DESCRIPTION:
 * Generate and display the 'Current Carbon Dioxide' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayCurrentCo2(void)
{
	// Generate the 'Units' section of the display
	if (app_eCostUnit == E_POUNDS)												// Kw = Off, h = Off, KgCO2 = On, Droplet = Off, Litre = off,
		vDisplayUnits(0, 0, 1, 0, 0, 0, 1, 1, 0, 0);							// M3 = Off, Cost = On, Pound = On, Dollar = Off, Euro = Off

	else if (app_eCostUnit == E_DOLLARS)										// Kw = Off, h = Off, KgCO2 = On, Droplet = Off, Litre = off,
		vDisplayUnits(0, 0, 1, 0, 0, 0, 1, 0, 1, 0);							// M3 = Off, Cost = On, Pound = Off, Dollar = On, Euro = Off

	else																		// Kw = Off, h = Off, KgCO2 = On, Droplet = Off, Litre = off,
		vDisplayUnits(0, 0, 1, 0, 0, 0, 1, 0, 0, 1);							// M3 = Off, Cost = On, Pound = Off, Dollar = Off, Euro = On

	vDisplayUpdateCurrentKw();													// Update the 'Current CO2' numerical data
}


/****************************************************************************
 *
 * NAME: vDisplaySettings
 *
 * DESCRIPTION:
 * Generate and display the 'Settings' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplaySettings(void)
{
	// Generate the 'Status' section of the display
	vDisplayStatus(1, 0, 0, u8Lqi);												// Set = On, Mem = Off, Alarm = Off, LQI reading

	// Generate the 'Units' section of the display
	vDisplayUnits(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);								// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
																				// M3 = Off, Cost = Off, Pound = Off, Dollar = Off, Euro = Off

	vDisplayTemperatureCharacters(0, 16, 16, 0);								// T1 = Off, Character 13 = Space, Character 14 = Space, Units = Off
	vDisplayUpdateSettings();
}


/****************************************************************************
 *
 * NAME: vDisplayUpdateSettings
 *
 * DESCRIPTION:
 * Update the numerical data in the 'Settings' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayUpdateSettings(void)
{
	// Update the 'Status' section of the display (specifically the lqi level)
	vDisplayStatus(1, 0, 0, u8Lqi);												// Set = On, Mem = Off, Alarm = Off, LQI reading

	switch (app_eSettingsState)
	{
		case APP_E_SETTINGS_STATE_CURRENCY:
			switch (app_eCostUnit)
			{
				case E_POUNDS:
					vDisplayUnits(0, 0, 0, 0, 0, 0, 0, 1, 0, 0);				// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
				break;															// M3 = Off, Cost = Off, Pound = On, Dollar = Off, Euro = Off

				case E_DOLLARS:
					vDisplayUnits(0, 0, 0, 0, 0, 0, 0, 0, 1, 0);				// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
				break;															// M3 = Off, Cost = Off, Pound = Off, Dollar = On, Euro = Off

				case E_EUROS:
					vDisplayUnits(0, 0, 0, 0, 0, 0, 0, 0, 0, 1);				// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
				break;															// M3 = Off, Cost = Off, Pound = Off, Dollar = Off, Euro = On

				default:
				break;
			}
			vDisplayTemperatureCharacters(0, 16, 16, 0);						// Turn off all temperature unit segments
			vDisplayMainCharacters(16, 16, 16, 16, 0);							// Clear any text
			vDisplayCostCharacters(16, 16, 16, 16, 0);							// Clear any text
		break;

		case APP_E_SETTINGS_STATE_TEMPERATURE:
			switch (app_eTemperatureUnit)
			{
				case E_CELSIUS:
					vDisplayTemperatureCharacters(0, 16, 16, 2);				// Turn celsius segment on
				break;

				case E_FAHRENHEIT:
					vDisplayTemperatureCharacters(0, 16, 16, 1);				// Turn Fahrenheit segment on
				break;

				default:
				break;
			}
			vDisplayUnits(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);						// Turn off all currency segments
			vDisplayMainCharacters(16, 16, 16, 16, 0);							// Clear any text
			vDisplayCostCharacters(16, 16, 16, 16, 0);							// Clear any text
		break;

		case APP_E_SETTINGS_STATE_FASTPOLL:
			vDisplayTemperatureCharacters(0, 16, 16, 0);						// Turn off all temperature unit segments
			vDisplayUnits(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);						// Turn off all currency segments
			vDisplayMainCharacters(22, 17, 35, 36, 0);							// Display 'FASt'
			vDisplayCostCharacters(32, 31, 28, 28, 0);							// Display 'POLL'
		break;

		default:
			// Error - unknown settings state
		break;
	}
}


/****************************************************************************
 *
 * NAME: vDisplayPrice
 *
 * DESCRIPTION:
 * Generate and display the 'Price' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayPrice(void)
{
	// Generate the 'Units' section of the display
	vDisplayUnits(0, 0, 0, 0, 0, 0, 1, 0, 0, 0);								// Kw = Off, h = Off, KgCO2 = Off, Droplet = Off, Litre = Off,
																				// M3 = Off, Cost = On, Pound = Off, Dollar = Off, Euro = Off
}


/****************************************************************************
 *
 * NAME: vDisplayPrice
 *
 * DESCRIPTION:
 * Update the numerical data in the 'Price' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayUpdatePrice(void)
{
	/* TODO * Display prices */
}


/****************************************************************************
 *
 * NAME: vDisplayDrlc
 *
 * DESCRIPTION:
 * Generate and display the 'DRLC' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayDrlc(void)
{
	vDisplayMainCharacters(20, 34, 28, 19, 0);									// Display 'INFO'
}


/****************************************************************************
 *
 * NAME: vDisplayMessage
 *
 * DESCRIPTION:
 * Generate and display the 'Message' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayMessage(void)
{
	vDisplayMainCharacters(25, 30, 22, 31, 0);									// Display 'INFO'
}


/****************************************************************************
 *
 * NAME: vDisplayMessageConfirm
 *
 * DESCRIPTION:
 * Generate and display the 'Message Confirm' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayMessageConfirm(void)
{
	vDisplayMainCharacters(25, 30, 22, 31, 0);									// Display 'INFO'
	vDisplayCostCharacters(19, 31, 30, 22, 0);									// Display 'CONF'
}


/****************************************************************************
 *
 * NAME: vDisplayHistory
 *
 * DESCRIPTION:
 * Generate and display the 'History' screen
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayHistory(void)
{
	vDisplayMainCharacters(24, 25, 35, 36, 0);									// Display 'HISt'
}


/****************************************************************************
 *
 * NAME: vDisplayMain
 *
 * DESCRIPTION:
 * Allows a number to be displayed on the main display without addressing each
 * character individually.
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayMain(uint16 u16Number, uint8 u8Dp)
{
	uint16 u16NumberTemp;
	uint8  u8Char1, u8Char2, u8Char3, u8Char4, u8DpMask;

	u16NumberTemp = u16Number;

	u8Char1 = u16NumberTemp / 1000;												// Determine the first character (i.e. number of thousands)
	u16NumberTemp -= (uint16)(u8Char1) * 1000;									// Once determined, subtract the value from temporary storage
	u8Char2 = u16NumberTemp / 100;												// Determine the second character (i.e. number of hundreds)
	u16NumberTemp -= (uint16)(u8Char2) * 100;									// Once determined, subtract the value from temporary storage
	u8Char3 = u16NumberTemp / 10;												// Determine the third character (i.e. number of tens)
	u16NumberTemp -= (uint16)(u8Char3) * 10;									// Once determined, subtract the value from temporary storage
	u8Char4 = u16NumberTemp;													// The remaining value is the final digit (i.e. number of units)

	if (u8Dp)
	{
		u8DpMask = 1 << (u8Dp - 1);
	}
	else
	{
		u8DpMask = 0;
	}

	vDisplayMainCharacters(u8Char1, u8Char2, u8Char3, u8Char4, u8DpMask);
}


/****************************************************************************
 *
 * NAME: vDisplayCost
 *
 * DESCRIPTION:
 * Allows a number to be displayed on the cost display without addressing each
 * character individually.
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayCost(uint16 u16Number, uint8 u8Dp)
{
	uint16 u16NumberTemp;
	uint8  u8Char5, u8Char6, u8Char7, u8Char8, u8DpMask;

	u16NumberTemp = u16Number;

	u8Char5 = u16NumberTemp / 1000;												// Determine the first character (i.e. number of thousands)
	u16NumberTemp -= (uint16)(u8Char5) * 1000;									// Once determined, subtract the value from temporary storage
	u8Char6 = u16NumberTemp / 100;												// Determine the second character (i.e. number of hundreds)
	u16NumberTemp -= (uint16)(u8Char6) * 100;									// Once determined, subtract the value from temporary storage
	u8Char7 = u16NumberTemp / 10;												// Determine the third character (i.e. number of tens)
	u16NumberTemp -= (uint16)(u8Char7) * 10;									// Once determined, subtract the value from temporary storage
	u8Char8 = u16NumberTemp;													// The remaining value is the final digit (i.e. number of units)

	if (u8Dp)
	{
		u8DpMask = 1 << (u8Dp - 1);
	}
	else
	{
		u8DpMask = 0;
	}

	vDisplayCostCharacters(u8Char5, u8Char6, u8Char7, u8Char8, u8DpMask);
}


/****************************************************************************
 *
 * NAME: vDisplayTime
 *
 * DESCRIPTION:
 * Allows a time to be displayed on the Time Display without addressing each
 * character individually.
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayTime(uint8 u8Hours, uint8 u8Minutes, bool_t bCol)
{
	uint8 u8Temp;
	uint8 u8Char9, u8Char10, u8Char11, u8Char12;

	u8Temp   = u8Hours;
	u8Char9  = u8Temp / 10;														// Determine the first Hour digit (i.e. number of tens)
	u8Temp  -= u8Char9 * 10;													// Once determined, subtract the value from temporary storage
	if ( u8Char9 == 0 ) u8Char9 = 0x10;											// If the first hour digit is '0' clear the character
	u8Char10 = u8Temp;															// The remaining value is the second Hour digit (i.e. number of units)

	u8Temp   = u8Minutes;
	u8Char11 = u8Temp / 10;														// Determine the first Minute digit (i.e. number of tens)
	u8Temp  -= u8Char11 * 10;													// Once determined, subtract the value from temporary storage
	u8Char12 = u8Temp;															// The remaining value is the second Minute digit (i.e. number of units)

	vDisplayTimeCharacters(u8Char9, u8Char10, u8Char11, u8Char12, bCol);
}


/****************************************************************************
 *
 * NAME: vDisplayTemperature
 *
 * DESCRIPTION:
 * Allows a number to be displayed on the Temperature display without addressing
 * each character individually.
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayTemperature(uint8 u8Temperature)
{
	uint8 u8Temp;
	bool_t  bT1;
	uint8 u8Char13, u8Char14;

	u8Temp   = u8Temperature;
	bT1 	 = u8Temp / 100;													// Determine the first temperature digit (i.e. hundreds, maximum = 1)
	u8Temp  -= (uint8)(bT1) * 100;												// Once determined, subtract the value from temporary storage
	u8Char13 = u8Temp / 10;														// Determine the second temperature digit (i.e. tens)
	u8Temp  -= u8Char13 * 10;													// Once determined, subtract the value from temporary storage
	u8Char14 = u8Temp;															// The remaining value is the third temperature digit

	if (app_eTemperatureUnit == E_CELSIUS) 			u8Temp = 2;					// Configure the temperature units to be degrees C
	else if (app_eTemperatureUnit == E_FAHRENHEIT) 	u8Temp = 1;					// Configure the temperature units to be degrees F
	else 											u8Temp = 0;					// Unknown temperature unit, don't display any units

	vDisplayTemperatureCharacters(bT1, u8Char13, u8Char14, u8Temp);
}


/****************************************************************************
 *
 * NAME: vDisplayStatus
 *
 * DESCRIPTION:
 * Display status segments (character 0)
 *
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayStatus(
	bool_t bSet, bool_t bMem, bool_t bAlarm, uint8 u8Batt)
{
	uint8 u8Char0BitMask;
	bool_t  bBattOn, bBatt1, bBatt2, bBatt3;

	// Convert the battery bitmask into individual bits
	bBatt1  = u8Batt & 0x01;
	bBatt2  = ( u8Batt & 0x02 ) >> 1;
	bBatt3  = ( u8Batt & 0x04 ) >> 2;
	bBattOn = ( u8Batt & 0x08 ) >> 3;

	u8Char0BitMask = 	u8LcdShadow[0] & 0x08;									// Clear all segments states except the 'kW';
	u8Char0BitMask = 	u8Char0BitMask 	|										// Merge the status flags with the relevant character segments
						bBatt1  << 7 	|
						bBatt2  << 6 	|
						bBattOn << 5 	|
						bBatt3  << 4 	|
						bAlarm  << 2 	|
						bMem    << 1 	|
						bSet;

	// Send segment data
	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(u8LcdLutChar[0]);									// Point to character 0
	vSendLcdCharacterData(u8Char0BitMask);										// Send segment data for character 0 (Status character)
	vCheckArbitration();														// Check for a loss of arbitration or Nack
	vTerminateLcdComms();														// Terminate the I2C comms

	// Store segment data
	u8LcdShadow[0] = u8Char0BitMask;											// Copy the current LCD state for character 0 into the shadow map
}


/****************************************************************************
 *
 * NAME: vDisplayUnits
 *
 * DESCRIPTION:
 * Display unit segments (character 15)
 *
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayUnits(
	bool_t bKw, bool_t bHours, bool_t bKgCO2, bool_t bDroplet, bool_t bLitre,
	bool_t bM3, bool_t bCost,  bool_t bPound, bool_t bDollar,  bool_t bEuro)
{
	uint8 u8Char0BitMask, u8Char1BitMask, u8Char15BitMask;

	// Load segment data
	u8Char0BitMask  = 	u8LcdShadow[0] 	| ( bKw << 3 );							// Merge the 'kW' segment state with character 0 before updating the LCD;
	u8Char1BitMask  = 	u8LcdShadow[1] 	| ( bHours << 3);						// Merge the 'h' segment state with character 1 before updating the LCD;
	u8Char15BitMask = 	bEuro    << 7	|										// Merge the unit flags with the relevant character segments
						bDollar  << 6	|
						bCost    << 5	|
						bPound   << 4 	|
						bM3      << 3	|
						bLitre   << 2 	|
						bDroplet << 1 	|
						bKgCO2;

	// Send segment data
	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(u8LcdLutChar[15]);									// Point to character 15
	vSendLcdCharacterData(u8Char15BitMask);										// Send segment data for character 15 (Unit character)
	vSendLcdCharacterData(u8Char0BitMask);										// Send segment data for character 0 (Status character)

	// It is more efficient to reaffirm data from the shadow map for characters 2-4
	// rather than shutdown and re-establish the I2C LCD comms with a different pointer
	vSendLcdCharacterData(u8LcdShadow[4]);										// Reaffirm segment data for character 4 to increment the character pointer
	vSendLcdCharacterData(u8LcdShadow[3]);										// Reaffirm segment data for character 3 to increment the character pointer
	vSendLcdCharacterData(u8LcdShadow[2]);										// Reaffirm segment data for character 2 to increment the character pointer
	vSendLcdCharacterData(u8Char1BitMask);										// Send segment data for character 1
	vTerminateLcdComms();														// Terminate the I2C comms

	// Store segment data
	u8LcdShadow[0]  = u8Char0BitMask;											// Copy the current LCD state for character 0 into the shadow map
	u8LcdShadow[1]  = u8Char1BitMask;											// Copy the current LCD state for character 1 into the shadow map
	u8LcdShadow[15] = u8Char15BitMask;											// Copy the current LCD state for character 15 into the shadow map
}


/****************************************************************************
 *
 * NAME: vDisplayMainCharacters
 *
 * DESCRIPTION:
 * Set and display characters 1 to 4
 *
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayMainCharacters(
	uint8 u8Char1, uint8 u8Char2, uint8 u8Char3, uint8 u8Char4, uint8 u8Dp)
{
	uint8 u8Char1BitMask, u8Char2BitMask, u8Char3BitMask, u8Char4BitMask;

	// Load segment data from the character lookup table
	u8Char1BitMask = u8LcdLut1_4[u8Char1] | ( 0x08 & u8LcdShadow[1] );			// load the 'h' segment state from the shadow map and merge with the new character before updating the LCD
	u8Char2BitMask = u8LcdLut1_4[u8Char2] | (( 0x04 & u8Dp )<< 1 );				// merge the desired 'Dot1' segment state with character 2 before updating the LCD
	u8Char3BitMask = u8LcdLut1_4[u8Char3] | (( 0x02 & u8Dp )<< 2 );				// merge the desired 'Dot2' segment state with character 3 before updating the LCD
	u8Char4BitMask = u8LcdLut1_4[u8Char4] | (( 0x01 & u8Dp )<< 3 );				// merge the desired 'Dot3' segment state with character 4 before updating the LCD

	// Send segment data
	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(u8LcdLutChar[4]);									// Point to character 4
	vSendLcdCharacterData(u8Char4BitMask);										// Send segment data for character 4
	vSendLcdCharacterData(u8Char3BitMask);										// Send segment data for character 3
	vSendLcdCharacterData(u8Char2BitMask);										// Send segment data for character 2
	vSendLcdCharacterData(u8Char1BitMask);										// Send segment data for character 1
	vTerminateLcdComms();														// Terminate the I2C comms

	// Store segment data
	u8LcdShadow[1] = u8Char1BitMask;											// Copy the current LCD state for character 1 into the shadow map
	u8LcdShadow[2] = u8Char2BitMask;											// Copy the current LCD state for character 2 into the shadow map
	u8LcdShadow[3] = u8Char3BitMask;											// Copy the current LCD state for character 3 into the shadow map
	u8LcdShadow[4] = u8Char4BitMask;											// Copy the current LCD state for character 4 into the shadow map
}


/****************************************************************************
 *
 * NAME: vDisplayCostCharacters
 *
 * DESCRIPTION:
 * Set and display characters 5 to 8
 *
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayCostCharacters(
	uint8 u8Char5, uint8 u8Char6, uint8 u8Char7, uint8 u8Char8, uint8 u8Dp)
{
	uint8 u8Char5BitMask, u8Char6BitMask, u8Char7BitMask, u8Char8BitMask;

	// Load segment data from the character lookup table
	u8Char5BitMask = u8LcdLut5_8[u8Char5];
	u8Char6BitMask = u8LcdLut5_8[u8Char6] | (( 0x04 & u8Dp )<< 5 );				// merge the desired 'Dot4' segment state with character 6 before updating the LCD
	u8Char7BitMask = u8LcdLut5_8[u8Char7] | (( 0x02 & u8Dp )<< 6 );				// merge the desired 'Dot5' segment state with character 7 before updating the LCD
	u8Char8BitMask = u8LcdLut5_8[u8Char8] | (( 0x01 & u8Dp )<< 7 );				// merge the desired 'Dot6' segment state with character 8 before updating the LCD

	// Send segment data
	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(u8LcdLutChar[5]);									// Point to character 5
	vSendLcdCharacterData(u8Char5BitMask);										// Send segment data for character 5
	vSendLcdCharacterData(u8Char6BitMask);										// Send segment data for character 6
	vSendLcdCharacterData(u8Char7BitMask);										// Send segment data for character 7
	vSendLcdCharacterData(u8Char8BitMask);										// Send segment data for character 8
	vTerminateLcdComms();														// Terminate the I2C comms

	// Store segment data
	u8LcdShadow[5] = u8Char5BitMask;											// Copy the current LCD state for character 5 into the shadow map
	u8LcdShadow[6] = u8Char6BitMask;											// Copy the current LCD state for character 6 into the shadow map
	u8LcdShadow[7] = u8Char7BitMask;											// Copy the current LCD state for character 7 into the shadow map
	u8LcdShadow[8] = u8Char8BitMask;											// Copy the current LCD state for character 8 into the shadow map
}


/****************************************************************************
 *
 * NAME: vDisplayTimeCharacters
 *
 * DESCRIPTION:
 * Set and display characters 9 to 12
 *
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayTimeCharacters(
	uint8 u8Char9, uint8 u8Char10, uint8 u8Char11, uint8 u8Char12, bool_t bCol1)
{
	uint8 u8Char9BitMask, u8Char10BitMask, u8Char11BitMask, u8Char12BitMask;

	if(bZCL_GetTimeHasBeenSynchronised())
	{
		// Load segment data from the character lookup table
		u8Char9BitMask  = u8LcdLut9_14[u8Char9];
		u8Char10BitMask = u8LcdLut9_14[u8Char10];
		u8Char11BitMask = u8LcdLut9_14[u8Char11] | ( bCol1 << 3 );					// merge the desired 'COL1' segment state with character 11 before updating the LCD;
		u8Char12BitMask = u8LcdLut9_14[u8Char12] | ( 0x08 & u8LcdShadow[12] );		// load the 'T1' segment state from the shadow map and merge with the new character before updating the LCD
	}
	else
	{
		/* Time not synchronised, display '--:--' instead */
		u8Char9BitMask = u8LcdLut9_14[10];
		u8Char10BitMask = u8LcdLut9_14[10];
		u8Char11BitMask = u8LcdLut9_14[10];
		u8Char12BitMask = u8LcdLut9_14[10];
	}

	// Send segment data
	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(u8LcdLutChar[12]);									// Point to character 12
	vSendLcdCharacterData(u8Char12BitMask);										// Send segment data for character 12
	vSendLcdCharacterData(u8Char11BitMask);										// Send segment data for character 11
	vSendLcdCharacterData(u8Char10BitMask);										// Send segment data for character 10
	vSendLcdCharacterData(u8Char9BitMask);										// Send segment data for character 9
	vTerminateLcdComms();														// Terminate the I2C comms

	// Store segment data
	u8LcdShadow[9]  = u8Char9BitMask;											// Copy the current LCD state for character 9 into the shadow map
	u8LcdShadow[10] = u8Char10BitMask;											// Copy the current LCD state for character 10 into the shadow map
	u8LcdShadow[11] = u8Char11BitMask;											// Copy the current LCD state for character 11 into the shadow map
	u8LcdShadow[12] = u8Char12BitMask;											// Copy the current LCD state for character 12 into the shadow map
}


/****************************************************************************
 *
 * NAME: vDisplayTemperatureCharacters
 *
 * DESCRIPTION:
 * Set and display characters 13 to 14
 *
 *
 * RETURNS:
 * None
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vDisplayTemperatureCharacters(
	bool_t bT1, uint8 u8Char13, uint8 u8Char14, uint8 u8Deg)
{
	uint8 u8Char12BitMask, u8Char13BitMask, u8Char14BitMask;

	// Load segment data from the character lookup table
	u8Char12BitMask = u8LcdShadow[12] | ( bT1 << 3 );							// merge the desired 'T1' segment state with character 12 before updating the LCD;
	if ( u8Char13 == 0 )														// If the character number is '0'
	{
		u8Char13BitMask = u8LcdLut9_14[0x10] | (( 0x02 & u8Deg )<< 2 );			// merge the desired 'Celcius' segment state with a blank character before updating the LCD;
	}
	else
	{
		u8Char13BitMask = u8LcdLut9_14[u8Char13] | (( 0x02 & u8Deg )<< 2 );		// merge the desired 'Celcius' segment state with character 13 before updating the LCD;
	}
	u8Char14BitMask = u8LcdLut9_14[u8Char14] | (( 0x01 & u8Deg )<< 3 );			// merge the desired 'Fahrenheit' segment state with character 14 before updating the LCD;

	// Send segment data
	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(u8LcdLutChar[14]);									// Point to character 14
	vSendLcdCharacterData(u8Char14BitMask);										// Send segment data for character 14
	vSendLcdCharacterData(u8Char13BitMask);										// Send segment data for character 13
	vSendLcdCharacterData(u8Char12BitMask);										// Send segment data for character 12
	vTerminateLcdComms();														// Terminate the I2C comms

	// Store segment data
	u8LcdShadow[12] = u8Char12BitMask;											// Copy the current LCD state for character 12 into the shadow map
	u8LcdShadow[13] = u8Char13BitMask;											// Copy the current LCD state for character 13 into the shadow map
	u8LcdShadow[14] = u8Char14BitMask;											// Copy the current LCD state for character 14 into the shadow map
}


/****************************************************************************
 *
 * NAME: vFloodLcd
 *
 * DESCRIPTION:
 * Used to fill all of the LCD segments for testing during initialisation
 *
 *
 * RETURNS:
 * None.
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISPLAY) void vFloodLcd(void)
{
	uint8 i;

	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(0x00);												// Send 'Load-Data-Pointer' command to set the data pointer to address 0
	// LCD Configuration Complete, send segment data
	for ( i = 0 ; i < 16 ; i++ )	vSendLcdCharacterData(0xFF);				// For all 16 characters... fill the segments
	vTerminateLcdComms();														// Terminate I2C comms
}


/****************************************************************************
 *
 * NAME: vClearLcd
 *
 * DESCRIPTION:
 * Clear all segments of the LCD
 *
 *
 * RETURNS:
 * None.
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void vClearLcd(void)
{
	uint8 i;

	vEstablishLcdComms();														// Establish I2C comms with the LCD driver
	vSendLcdCharacterPointer(0x00);												// Send 'Load-Data-Pointer' command to set the data pointer to address 0
	// LCD Configuration Complete, send segment data
	for ( i = 0 ; i < 16 ; i++ )	vSendLcdCharacterData(0x00);				// For all 16 characters... clear the segments
	vTerminateLcdComms();														// Terminate I2C comms

	for ( i = 0 ; i < 16 ; i++ )	u8LcdShadow[i] = 0;							// For all 16 characters... clear the shadow map
}


/****************************************************************************
 *
 * NAME: vShowTime
 *
 * DESCRIPTION:
 * Calculates the time from UTC and sends to the UART
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void vShowTime(uint32 u32Time, uint8 * u8ParamHour, uint8 * u8ParamMin, uint8 * u8ParamSec)
{
	uint32 u32Sec, u32Min, u32Hour, u32Day, u32Month, u32Year;
	uint8 au8MonthTable [] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	u32Day  = u32Time / (60 * 60 * 24);
	u32Time = u32Time % (60 * 60 * 24);

	u32Hour = u32Time / (60 * 60);
	u32Time = u32Time % (60 * 60);

	u32Min  = u32Time / 60;
	u32Time = u32Time % 60;

	u32Sec = u32Time;

    u32Year = 0;
    // Using u32Year%4 will fail in 2100, which isn't a leap year.
    // The printf will also fail in 2100.
    for(;;)
    {
        uint32 u32DaysThisYear = ((u32Year % 4) == 0) ? 366 : 365;
        if (u32Day < u32DaysThisYear)
            break;
        u32Day -= u32DaysThisYear;
        u32Year++;
    }

    if ((u32Year % 4) == 0)
        au8MonthTable[1] = 29;
    else
        au8MonthTable[1] = 28;

    u32Month = 0;
    for(;;)
    {
        uint32 u32DaysThisMonth = au8MonthTable[u32Month];
        if (u32Day < u32DaysThisMonth)
            break;
        u32Day -= u32DaysThisMonth;
        u32Month++;
    }

    // Day and month are calculated zero based but displayed one based.
	//DBG_vPrintf(TRACE_ZCL_TASK, "%d/%d/20%02d %02d:%02d:%02d\n", u32Day+1, u32Month+1, u32Year, u32Hour, u32Min, u32Sec);

    if(u8ParamHour != NULL)
    {
		/* Store the time values for the display funtion */
		*u8ParamHour = u32Hour;
		*u8ParamMin =  u32Min;
		*u8ParamSec = u32Sec;
    }
}


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
