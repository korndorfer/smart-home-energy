# -*- coding: utf8 -*-

import os
import time
import smbus
import psutil
import string
import eventlet
from serial import Serial, SerialException
from datetime import datetime
import _strptime
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
                  abort, render_template, flash, Response
                 
from flask_wtf.csrf import CsrfProtect
from flask_mail import Mail, Message
from flask_wtf import Form, RecaptchaField
from flask_socketio import SocketIO
from wtforms import StringField, SubmitField, validators, \
                    DateTimeField, BooleanField
from werkzeug.utils import secure_filename


how_to_run = """
export FLASK_APP=app.py;export FLASK_DEBUG=1;flask run --host=0.0.0.0
"""

# I2C Status values
STATUS = {
    "ENABLE_STATUS"          : 1,
    "DISABLE_STATUS"         : 0,
    "ERROR_STATUS"           : 0xf0,
    "OK_STATUS"              : 0xf1,
    "WARNING_STATUS"         : 0xf2,
    "DANGER_STATUS"          : 0xf3
}

# I2C Config values
CONFIG = {
    "SET_DATE"               : 0xe0,
    "SET_TIME"               : 0xe1,
    "SET_TEMPERATURE_DANGER" : 0xe2,
    "SET_TEMPERATURE_OK"     : 0xe3,
    "SET_HUMIDITY_DANGER"    : 0xe4,
    "SET_HUMIDITY_OK"        : 0xe5,
    "SET_ALARM_STATUS"       : 0xe6,
    "SET_ALARM_ENABLE"       : 0xe7,
    "SET_ALARM_ACKNOWLEDGE"  : 0xe8
}

# I2C Metric values
VARS = {
    "GET_TEMPERATURE_STATUS" : [0xa2, "DANGER_STATUS"],
    "GET_TEMPERATURE"        : [0xa3, "50"],
    "GET_HUMIDITY_STATUS"    : [0xa4, "DANGER_STATUS"],
    "GET_HUMIDITY"           : [0xa5, "90"],
    "GET_PRESSURE"           : [0xa6, "102000"],
    "GET_ALTITUDE"           : [0xa7, "0.00"],
    "GET_ALARM_STATUS"       : [0xa8, "ENABLE_STATUS"]
}

# I2C Metric Params
PARAMS = {
    "GET_DATE"               : [0xa0, "20180101"],
    "GET_TIME"               : [0xa1, "235800"],
    "GET_ALARM_ENABLE"       : [0xa9, "ENABLE_STATUS"],
    "GET_TEMPERATURE_DANGER" : [0xaa, "40"],
    "GET_TEMPERATURE_OK"     : [0xab, "28"],
    "GET_HUMIDITY_DANGER"    : [0xac, "60"],
    "GET_HUMIDITY_OK"        : [0xad, "50"]
}

# Serial and I2C configurations
I2C_PORT = 1
I2C_ADDRESS = 0x08
SERIAL_PORT = '/dev/ttyUSB0'
SERIAL_BAUD = 9600

BUFFER_SIZE = 30
INCIDENT_SIZE = 4
ACTIVITY_SIZE = 4

# Status options
LOG_STATUS = {
    "OK_STATUS"              : 'Temperature is normal.',
    "WARNING_STATUS"         : 'Temperature is in warning state.',
    "DANGER_STATUS"          : 'Temperature in danger state!'
}

# Protect web forms            
csrf = CsrfProtect()

# Flask app
app = Flask(__name__)
app.config.from_object(__name__)
eventlet.monkey_patch()

# Secrete key for app
app.secret_key = '860e541dabfa2263d33a93'

# Socket IO init
socketio = SocketIO(app)

csrf.init_app(app)


# Generic Item class for I2C
class Item (object):
    def __init__(self, name, value = None, default = None):
        self.__name = name
        self.__value = value
        self.__default = default
        self.__changed = False
    
    def _get_name(self):
        return self.__name
        
    def _get_value(self):
        return self.__value
    
    def _set_value(self, data):
        if self.__value not in (data, None, ''):
            self.__changed = True
        else:
            self.__changed = False
        self.__value = data

    def _set_default(self, data):
        self.__default = data

    def _set_demo_data(self):
        self.__value = self.__default
        
    def set_demo_data(self):
        self._set_demo_data()
                
    def get_value(self):
        res = self._get_value()
        if res:
            return res
        return None
        
    def set_value(self, data):
        self._set_value(data)
    
    def has_changed(self):
        return self.__changed

    def as_json(self):
        return {'GET_{}'.format(self.__name): self.__value}

    def __str__(self):
        return "{}".format(self.__value)
    

# Metric class
class Metric (Item):
    def __init__(self, data = None):
        super(Metric, self).__init__(data)
        self.__hexcode = None
        self.__status = Status(data+'_STATUS')
        self.__normal_threshold = Configuration(data+'_OK')
        self.__danger_threshold = Configuration(data+'_DANGER')
        
        if VARS.has_key('GET_'+data):
            var = VARS['GET_'+data]
            
            if isinstance(var, list):
                self.__hexcode = var[0]
                self._set_default(var[1])

    def set_demo_data(self):
        self._set_demo_data()
        self.__status.set_demo_data()
        self.__normal_threshold.set_demo_data()
        self.__danger_threshold.set_demo_data()
        
    def code(self):
        return self.__hexcode
    
    def status(self):
        return self.__status

    def normal_threshold(self):
        return self.__normal_threshold
    
    def danger_threshold(self):
        return self.__danger_threshold


# Command class
class Command(Item):
    def __init__(self, data = None):
        super(Command, self).__init__(data)
        self.__hexcode = None
        
        if CONFIG.has_key('SET_'+data):
            self.__hexcode = CONFIG['SET_'+data]

    def code(self):
        return self.__hexcode
        

# Alarm class
class Alarm (Metric):
    def __init__(self, data = None):
        super(Alarm, self).__init__(data)
        self.__enable = Configuration(data+'_ENABLE')
        self.__acknowledge = Command(data+'_ACKNOWLEDGE')

    def set_demo_data(self):
        super(Alarm, self).set_demo_data()
        self.__enable.set_demo_data()
                
    def enable(self):
        return self.__enable
    
    def acknowledge(self):
        return self.__acknowledge
        
        
# Configuration class
class Configuration (Item):
    def __init__(self, data = None):
        super(Configuration, self).__init__(data)
        self.__getcode = None
        self.__setcode = None
        
        if PARAMS.has_key('GET_'+data):
            res = PARAMS['GET_'+data]
            
            if isinstance(res, list):
                self.__getcode = res[0]
                self._set_default(res[1])
                
        if CONFIG.has_key('SET_'+data):
            res = CONFIG['SET_'+data]
        
            if isinstance(res, list):
                self.__setcode = res[0]
            
    def get_code(self):
        return self.__getcode

    def set_code(self):
        return self.__setcode

# Status class
class Status (Item):
    def __init__(self, data = None):
        super(Status, self).__init__(data)
        self.__hexcode = None
        
        if VARS.has_key('GET_'+data):
            var = VARS['GET_'+data]
            
            if isinstance(var, list):
                self.__hexcode = var[0]
                self._set_default(STATUS[var[1]])
    
    def __status(self):
        data = self.get_value()

        if data:
            data = int(data)
            for key, value in STATUS.items():
                if data == value:
                    return key
        return None
                       
    def code(self):
        return self.__hexcode

    def as_json(self):
        return {'GET_{}'.format(self._get_name()): self.__status()}
    
    def set_status(self, data):
        for key, value in STATUS.items():
            if data == key:
                self._set_value(value)

    def get_status(self):
       return self.__status()
              
              
# Generic Log class                
class Log:
    def __init__(self):
        self.collection = []
        self.latest = {}
        self.has_new = False

    def as_list(self):
        if self.has_new:
            self.has_new = False
            return self.collection
        return None

        
# Incidents log class
class Incidents(Log):
   
    def log(self, incident_type, incident_status, incident_text, 
                                  incident_value, incident_time = None):
        if incident_time == None:
            incident_time = datetime.now().strftime("%b %d, %Y %I:%M %p")
            
        if len(self.collection) >= INCIDENT_SIZE:
            self.collection.pop(0)
        
        self.latest = {
            "type": incident_type,
            "status": incident_status,
            "text": incident_text,
            "value": incident_value,
            "time": incident_time
        }
        
        self.has_new = True
        self.collection.append(self.latest)

    def log_temperature(self):       
        t_value = device.temperature.get_value()
        t_status = device.temperature.status().get_status()
        t_event = device.get_datetime()
            
        if t_value and t_status and t_event:
            t_event = t_event.strftime("%b %d, %Y %I:%M %p")
            self.log('temperature', t_status, LOG_STATUS[t_status], 
                                                       t_value, t_event)


# Activities log class
class Activities(Log):
   
    def log(self, activity_type, activity_text):
        
        if len(self.collection) >= ACTIVITY_SIZE:
            self.collection.pop(0)
        
        self.latest = {
            "type": activity_type,
            "text": activity_text,
            "time": datetime.now().strftime("%b %d, %Y %I:%M %p")
        }
        
        self.has_new = True
        self.collection.append(self.latest)
        print(activity_text)


# Device class. Control arduino I2C or serial communication
class Device:
    def __init__(self, i2c_port, i2c_address, serial_port, serial_baud):
        self.__i2c = None
        self.__serial = None
        self.__i2c_address = i2c_address
        self.__device_data = {}
        self.__device_config = {}
        self.__latest_updates = {}
        
        self.date = Configuration('DATE')
        self.time = Configuration('TIME')
        self.temperature = Metric('TEMPERATURE')
        self.humidity = Metric('HUMIDITY')
        self.pressure = Metric('PRESSURE')
        self.altitude = Metric('ALTITUDE')
        self.alarm = Alarm('ALARM')
        
        self.__items = [
            self.date,
            self.time,
            self.temperature,
            self.humidity,
            self.pressure,
            self.altitude,
            self.alarm
        ]
        
        # Try to connect to I2C.
        try:
            self.__i2c = smbus.SMBus(1)
            time.sleep(1)
            
            log = "Connected to i2c port {0}.".format(i2c_port)
            activities.log("i2c", log)
        
        # Otherwise log and ignore    
        except:
            log = "Cannot connect to i2c port {}.".format(i2c_port)
            activities.log("i2c", log)
            
        # Try to connect to serial.
        try:
            self.__serial = Serial(serial_port, serial_baud, timeout=1)
            time.sleep(1.8)
            
            log = "Connected to serial port {0}.".format(serial_port)
            activities.log("serial", log)
            
        # Otherwise log and ignore         
        except:
            log = "Cannot connect to serial port {}.".format(serial_port)
            activities.log("serial", log)

    # Populate items with demo data
    def __initialize_values(self):
        for item in self.__items:
            item.set_demo_data() 
    
    # Connect with device via I2C or serial and get <param> data            
    def __get_data(self, param):
        received = ''
        
        if self.__i2c:
            
            attempts = 3
            while (attempts):
                try:
                    self.__i2c.write_quick(self.__i2c_address)

                    i2c_data = self.__i2c.read_i2c_block_data(
                                 self.__i2c_address, param, BUFFER_SIZE)

                    for i in range(len(i2c_data)):
                        num = chr(i2c_data[i])
                    
                        if num in string.printable:
                            received += num
                    attempts = 0
                except:
                    time.sleep(.5)
                    attempts -= 1
                    
        elif self.__serial:
            
            self.__serial.write(chr(param))        
            time.sleep(.2)            
            serial_data = self.__serial.read(BUFFER_SIZE)

            for i in range(len(serial_data)):
                num = serial_data[i]
                
                if num in string.printable:
                    received += num
        
        return received
    
    # Get values from device
    def __update_data(self):
        
        for item in self.__items[:2]:
            item.set_value(self.__get_data(item.get_code())) 

        for item in self.__items[2:]:
            if item.code():
                item.set_value(self.__get_data(item.code()))
                
            if item.status().code():
                item.status().set_value(
                                  self.__get_data(item.status().code()))

            if item.normal_threshold().get_code():
                item.normal_threshold().set_value(
                    self.__get_data(item.normal_threshold().get_code()))
            
            if item.danger_threshold().get_code():
                item.danger_threshold().set_value(
                    self.__get_data(item.danger_threshold().get_code()))

        device.alarm.enable().set_value(
            self.__get_data(device.alarm.enable().get_code()))

    
    # Update dictionary data
    def __update_dictionary(self):
        self.__latest_updates = {}
        
        for item in self.__items[:2]:
            if item.get_value():
                self.__device_config.update(item.as_json())

        for item in self.__items[2:]:
            if item.code() and item.get_value():
                self.__device_data.update(item.as_json())

                if item.has_changed():
                    self.__latest_updates.update(item.as_json())

            if item.status().code() and item.status().get_value():
                self.__device_data.update(item.status().as_json())
                
                if item.status().has_changed():
                    self.__latest_updates.update(item.status().as_json())
                                                        
            if item.normal_threshold().get_code() and \
                                    item.normal_threshold().get_value():
                self.__device_config.update(
                                      item.normal_threshold().as_json())
                
            if item.danger_threshold().get_code() and \
                                    item.danger_threshold().get_value():
                self.__device_config.update(
                                      item.danger_threshold().as_json())
            
            self.__device_config.update(device.alarm.enable().as_json())
    
    # Set device data
    def __set_data(self, text_param, text_data):
        param = self.__config[text_param]
        data = bytearray(text_data)

        if self.__i2c:
            self.__i2c.write_i2c_block_data(self.__i2c_address, cmd, 
                                                                   data)
        elif self.__serial:
            self.__serial.write(chr(param))
            self.__serial.write(data)
            time.sleep(.2)  
        else:
            self.__device_data[text_param] = text_data
    
    # Fire command on device
    def __fire_command(self, cmd):

        if self.__i2c:
            self.__i2c.write_byte(self.__i2c_address, int(cmd))
        elif self.__serial:
            self.__serial.write(chr(cmd))
            time.sleep(.2)
    
    # return successful I2C or serial connection
    def has_connection(self):
        return self.__i2c or self.__serial
      
    def demo_data(self):
        self.__initialize_values()
        self.__update_dictionary()
    
    # Return temperature status changed
    def has_temperature_status_changed(self):
        temp = self.temperature
        return temp.has_changed() or temp.status().has_changed()
    
    # Public fucntion to get <param> data 
    def get_data(self, param):
        return self.__device_data[param]
    
    # Return latest item updates
    def get_latest_updates(self):
        self.__update_data()
        self.__update_dictionary()
        return self.__latest_updates
    
    # Return data dictionary
    def get_data_set(self):
        return self.__device_data
    
    # Return configuration dictionary
    def get_configuration_set(self):
        return self.__device_config
    
    # Return date and time as formatted text     
    def get_datetime(self):
        if self.date.get_value() and self.time.get_value():
            data = self.date.get_value()+' '+self.time.get_value()
            return datetime.strptime(data, "%Y%m%d %H%M%S")
        return None
    
    # Send alarm acknowledge (off) to device
    def set_alarm_acknowledge(self):
        self.__fire_command(device.alarm.acknowledge().code())

                
incidents = Incidents()
activities = Activities()

activities.log("system", "Application starting")

device = Device(I2C_PORT, I2C_ADDRESS, SERIAL_PORT, SERIAL_BAUD)


# Get uptme to send to dashboard
def get_uptime():
    uptime = datetime.now() - datetime.fromtimestamp(psutil.boot_time())
    return str(uptime).split('.')[0]

# Send data to dashboard
def send_data():
    
    results = device.get_data_set()

    for key, value in results.items():
        socketio.emit('devicedata', {key: value})

    socketio.emit('devicedata', {"GET_UPTIME": get_uptime()})

    results = device.get_configuration_set()

    for key, value in results.items():
        socketio.emit('deviceconfig', {key: value})
        
    data = device.get_datetime()
    if data:
        data = data.strftime("%d/%m/%Y %H:%M:%S")
        socketio.emit('deviceconfig', {"GET_DATE_TIME": data})
    
    data = incidents.as_list()
    
    if data:
        socketio.emit('incidents', data)
    
    data = activities.as_list()
    
    if data:
        socketio.emit('activities', data)

# Verify device data updatres and send to dashboard
def process_data():
    
    if device.has_connection():
        
        updates = device.get_latest_updates()
        
        if updates:

            if device.has_temperature_status_changed():
                incidents.log_temperature()
            
            log = "Received data: {}".format(",".join(updates))
            activities.log("reception", log)
        else:
            log = "No data received."
            activities.log("reception", log)

    else:
        log = "No data received. Using defaults for test."
        activities.log("reception", log)
        device.demo_data()
        incidents.log_temperature()
        eventlet.sleep(10)
    
    send_data()           


# Threaded function to poll data from device
def poll_data():
    
    while True:
        process_data()
        eventlet.sleep(10)

eventlet.spawn(poll_data)

# Form to show configuration on dashboard
class ConfigurationForm(Form):   
    temp_ok = StringField(u'OK Temp:', description=u'Temperature OK.', default="")
    temp_danger = StringField(u'DANGER Temp:', 
                                     description=u'Temperature DANGER.')
    humi_ok = StringField(u'OK Humi:', description=u'Humidity OK.')
    humi_danger = StringField(u'DANGER Humi:', 
                                        description=u'Humidity DANGER.')
    date_time = DateTimeField('Date/Time', format='%d/%m/%Y %H:%M:%S', 
                   description=u'Date and time as dd/mm/yyyy HH:MM:SS.', 
                   validators=(validators.Optional(),))
                   
    alarm_enable = BooleanField(u'ALARM enable:', 
                                           description=u'Alarm enable.')
    submit = SubmitField(u'Ok')

# Return page with form object
def render_with_form(templ):
    form = ConfigurationForm()
    return render_template(templ, formConfig=form)

# Home page
@app.route('/')
def inicio():
    return render_template('inicio.html')

# Dashboard page
@app.route('/dashboard', methods=['GET', 'POST'])
def dashboard():
    return render_with_form('index.html')

# Process form changes
@app.route("/saveconfig", methods=['POST'])
def save_config():
    return redirect(url_for('dashboard'))

# Socket IO function to update dashboard on connection stablished
@socketio.on('connect')
def connect():    
    activities.log("socket", 'Client connected.')
    send_data();

# Socket IO function to observe alarm knowledge
@socketio.on('alarm_button')
def handle_alarm(message):
    if message["command"] == "SET_ALARM_ACKNOWLEDGE":
        device.set_alarm_acknowledge()
        activities.log("socket", 'Alarm was set to OFF.')

# Main function
if __name__ == "__main__":
    socketio.run(app)
