/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (METER)
 *
 * COMPONENT:          app_event_handler.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Event Handler
 *
 * $HeadURL $
 *
 * $Revision: 8437 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-05 14:42:12 +0000 (Mon, 05 Dec 2011) $
 *
 * $Id: app_ipd_node.c 8437 2011-12-05 14:42:12Z nxp33194 $
 *
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
/*#if JENNIC_CHIP_FAMILY == JN514x
#include "AppHardwareApi_JN514x.h"
#endif*/

#include "app_smartenergy_demo.h"
#include "app_zbp_utilities.h"
#include "app_event_handler.h"
#include "app_timer_driver.h"
#include "app_meter_node.h"
#include "app_zcl_task.h"
#include "app_buttons.h"
#include "zcl_options.h"
#include "string.h"
#include "os_gen.h"
#include "Price.h"
#include "dbg.h"
#include "pdm.h"
#include "Tunneling.h"

#ifdef SE_CERTIFICATION
#include "app_certification.h"
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_EVENT_HANDLER
#define TRACE_EVENT_HANDLER	TRUE
#endif
#define IMAGE_INDEX_1 1
#define IMAGE_INDEX_0 0
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void APP_eSE_TunnelRequestTunnelSend(void);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC	tsZCL_Address			sClientAddress;
PUBLIC	uint8					u8ClientKeyEstablishmentEndPoint;

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern	PDM_tsRecordDescriptor	s_sDevicePDDesc;
extern	tsDevice 				s_sDevice;
extern	uint32 					u32Price;
extern 	uint8  					u8PriceDp;
extern 	uint8  					u8PriceTier;


/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleRunningEvent
 *
 * DESCRIPTION:
 * Forwards any event to the relevant event handler
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PUBLIC void vHandleRunningEvent(ZPS_tsAfEvent sStackEvent, APP_tsEvent sAppEvent)
{
	switch (sStackEvent.eType)
	{
		case ZPS_EVENT_NONE:
		case ZPS_EVENT_APS_DATA_INDICATION:
		case ZPS_EVENT_NWK_JOINED_AS_ROUTER:
		case ZPS_EVENT_NWK_JOINED_AS_ENDDEVICE:
		case ZPS_EVENT_NWK_STARTED:
		case ZPS_EVENT_NWK_FAILED_TO_START:
		case ZPS_EVENT_NWK_FAILED_TO_JOIN:
		break;

		case ZPS_EVENT_NWK_NEW_NODE_HAS_JOINED:
			DBG_vPrintf(TRACE_EVENT_HANDLER, "ZPS_EVENT_NEW_NODE_HAS_JOINED\r\n");

		break;

		case ZPS_EVENT_NWK_DISCOVERY_COMPLETE:
		case ZPS_EVENT_NWK_LEAVE_INDICATION:
		case ZPS_EVENT_NWK_LEAVE_CONFIRM:
		case ZPS_EVENT_NWK_STATUS_INDICATION:
		case ZPS_EVENT_NWK_ROUTE_DISCOVERY_CONFIRM:
		case ZPS_EVENT_APS_DATA_CONFIRM:
		case ZPS_EVENT_ERROR:
		case ZPS_EVENT_NWK_POLL_CONFIRM:
		break;

		case ZPS_EVENT_APS_ZDP_REQUEST_RESPONSE:
			if (0x8006 == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
			{
				if (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8Status != ZPS_E_SUCCESS)
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "Match Failed\n");
					return;
				}

				if(! (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength > 0 ))
				{
					return;
				}

				DBG_vPrintf(TRACE_EVENT_HANDLER, "KE Match Success\n");

				sClientAddress.eAddressMode = E_ZCL_AM_SHORT;
				sClientAddress.uAddress.u16DestinationAddress = sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest;
				u8ClientKeyEstablishmentEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
			}
			else if (NWK_RESP == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
			{
				if(!sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u8Status)
				{
					ZPS_tsNwkNib * thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());
					uint8 i;

					DBG_vPrintf(TRACE_EVENT_HANDLER, "NWK Lookup Resp: 0x%04x 0x%016llx\n",sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u16NwkAddrRemoteDev, sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u64IeeeAddrRemoteDev);

					for(i = 0 ; i < thisNib->sTblSize.u16NtActv ; i++)
					{
						if (( sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u16NwkAddrRemoteDev == thisNib->sTbl.psNtActv[i].u16NwkAddr ) &&
							( ZPS_NWK_NT_AP_RELATIONSHIP_CHILD == thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u2Relationship ))
						{
							/* Remove child from the neighbour table */
							thisNib->sTbl.psNtActv[i].u16NwkAddr = 0xFFFE;
							thisNib->sTbl.psNtActv[i].u64ExtAddr = 0x0000000000000000ull;
							thisNib->sTbl.psNtActv[i].u8LinkQuality = 0x00;
							thisNib->sTbl.psNtActv[i].u8TxFailed = 0x00;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1Authenticated = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1DeviceType = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1ExpectAnnc = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1LinkStatusDone = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1PowerSource = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1RxOnWhenIdle = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1SecurityMode = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1Used = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u2Relationship = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u3Age = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u3OutgoingCost = 0;
						}
					}
				}
				else
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "Lookup Resp - Error: 0x%02x", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u8Status);
				}
			}
		break;

		default:
		break;
	}

	if ( sAppEvent.eType != APP_E_EVENT_NONE)									// If the task was called due to an event
	{
		if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)							// Button release detected
		{
			if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)
			{
				/* Toggle Permit Join */
				if (s_sDevice.bPermitJoining)
				{
					ZPS_eAplZdoPermitJoining(0x00);
				}
				else
				{
					ZPS_eAplZdoPermitJoining(0xFF);
				}
				s_sDevice.bPermitJoining ^= 1;
				DBG_vPrintf(TRACE_EVENT_HANDLER, "PermitJoin: %i\n", s_sDevice.bPermitJoining);

			}
			else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)
			{

#ifdef CLD_OTA
				teZCL_Status eZCL_Status;
				tsCLD_PR_Ota sOTAPara;
				eZCL_Status = eOTA_NewImageLoaded(ESP_METER_LOCAL_EP,FALSE,NULL);
			    if (eZCL_Status != E_ZCL_SUCCESS)
			    {
			        DBG_vPrintf(TRACE_EVENT_HANDLER, "eOTA_NewImageLoaded returned error 0x%x", eZCL_Status);
			    }
			    else
			    {
			    	tsOTA_ImageNotifyCommand sOTA_ImageNotifyCommand;
			    	tsZCL_Address sDestinationAddress;

					/* Fill Image Notify Command */
					sOTA_ImageNotifyCommand.ePayloadType = E_CLD_OTA_QUERY_JITTER;
					sOTA_ImageNotifyCommand.u8QueryJitter = 100;

					/* Fill Destination Address */
					sDestinationAddress.eAddressMode = E_ZCL_AM_BROADCAST;
					sDestinationAddress.uAddress.u16DestinationAddress = 0xFFFF;
					eZCL_Status = eOTA_ServerImageNotify(ESP_METER_LOCAL_EP, 0xFF, &sDestinationAddress, &sOTA_ImageNotifyCommand);

					/* check status */
					if(eZCL_Status != E_ZCL_SUCCESS)
					{
						DBG_vPrintf(TRACE_EVENT_HANDLER, "Error: eOTA_ServerImageNotify 0x%x\r\n", eZCL_Status);
					}
					else
					{
						DBG_vPrintf(TRACE_EVENT_HANDLER, "Success: eOTA_ServerImageNotify 0x%x\r\n", eZCL_Status);
					}
			    }



#ifdef JENNIC_CHIP_FAMILY_JN516x
			    eOTA_GetServerData(ESP_METER_LOCAL_EP,IMAGE_INDEX_0,&sOTAPara);
			    sOTAPara.u32RequestOrUpgradeTime = 0;
			    eZCL_Status = eOTA_SetServerParams(ESP_METER_LOCAL_EP,IMAGE_INDEX_0,&sOTAPara);
#else
			    eOTA_GetServerData(ESP_METER_LOCAL_EP,IMAGE_INDEX_1,&sOTAPara);
			    sOTAPara.u32RequestOrUpgradeTime = 0;
			    eZCL_Status = eOTA_SetServerParams(ESP_METER_LOCAL_EP,IMAGE_INDEX_1,&sOTAPara);
#endif
			    if (eZCL_Status != E_ZCL_SUCCESS)
                {
                    DBG_vPrintf(TRACE_EVENT_HANDLER, "eOTA_SetServerParams returned error 0x%x", eZCL_Status);
                }
#endif
			}
			else if (APP_E_BUTTONS_BUTTON_3 == sAppEvent.sButton.u8Button)
			{
#ifdef SE_CERTIFICATION
				APP_vCertification(sAppEvent.sButton.u8Button);
#endif
			}
			else if (APP_E_BUTTONS_BUTTON_4 == sAppEvent.sButton.u8Button)
			{
#ifdef SE_CERTIFICATION
				APP_vCertification(sAppEvent.sButton.u8Button);
#endif
				APP_eSE_TunnelRequestTunnelSend();
			}

		}
		else if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
		{
			// Button press detected
		}
	}
	else
	{
		if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_UpdatePriceTimer))	// if the price timer has expired
		{
#ifndef SE_CERTIFICATION
#ifdef CLD_PRICE

			char *acRateLabelBase;
			tsSE_PricePublishPriceCmdPayload sPrice;

			acRateLabelBase = "BASE";
			sPrice.u32ProviderId = 0x00000001;
			sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
			sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
			sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

			sPrice.u32IssuerEventId = u32ZCL_GetUTCTime();
			sPrice.u8UnitOfMeasure = 0x00;
			sPrice.u16Currency = 840;
			sPrice.u8PriceTrailingDigitAndPriceTier = 0x10;
			sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x40;
			sPrice.u32StartTime = u32ZCL_GetUTCTime();
			sPrice.u16DurationInMinutes = 0x0002;
			sPrice.u32Price = 0x00000012;
			sPrice.u8PriceRatio = 0x12;
			sPrice.u32GenerationPrice = 0x00000015;
			sPrice.u8GenerationPriceRatio = 0x25;
			sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
			sPrice.u8AlternateCostUnit = 0xFF;
			sPrice.u8AlternateCostTrailingDigit = 0xFF;
			sPrice.u8NumberOfBlockThresholds = 0xFF;
			sPrice.u8PriceControl = 1;
			APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
#ifdef NUMBER_OF_SUPPORTED_COMMODITY_TYPES
			uint8 i;
			for(i = 1 ; i < NUMBER_OF_SUPPORTED_COMMODITY_TYPES ; i++)
			{
				APP_bAddPrice(sPrice, 1 + CLD_SM_NUMBER_OF_MIRRORS + i);	// Set a price and price tier for 1 minute but don't transmit
			}
#endif
#endif

			/* Send DRLC Message */
#ifdef CLD_DRLC
			static uint8 u8DrlcCriticalityLevel = 1;

			vSendNewLceToBound(u32ZCL_GetUTCTime(), E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 1, u8DrlcCriticalityLevel, 0);

			if(u8DrlcCriticalityLevel == 8)
			    u8DrlcCriticalityLevel = 1;
#endif
			OS_eStartSWTimer(APP_UpdatePriceTimer, APP_TIME_MS(60000), NULL);	// Start the timer to update the price again in 60s
#endif
		}

#ifdef CLD_SM_SUPPORT_GET_PROFILE
		else
		{
			static uint32 u32LastProfileUpdate = 0;
			uint8 eProfileIntervalPeriod;
			uint32 u32ProfileIntervalSeconds;
			eZCL_ReadLocalAttributeValue(	ESP_METER_LOCAL_EP,
											SE_CLUSTER_ID_SIMPLE_METERING,
											TRUE,
											FALSE,
											FALSE,
											E_CLD_SM_ATTR_ID_PROFILE_INTERVAL_PERIOD,
											&eProfileIntervalPeriod);
			switch (eProfileIntervalPeriod)
			{
				case E_CLD_SM_TIME_FRAME_DAILY:
					u32ProfileIntervalSeconds = 24*60*60;
					break;
				case E_CLD_SM_TIME_FRAME_60MINS:
					u32ProfileIntervalSeconds = 60*60;
					break;
				case E_CLD_SM_TIME_FRAME_30MINS:
					u32ProfileIntervalSeconds = 30*60;
					break;
				case E_CLD_SM_TIME_FRAME_10MINS:
					u32ProfileIntervalSeconds = 10*60;
					break;
				case E_CLD_SM_TIME_FRAME_7_5MINS:
					u32ProfileIntervalSeconds = (7*60)+30;
					break;
				case E_CLD_SM_TIME_FRAME_5MINS:
					u32ProfileIntervalSeconds = 5*60;
					break;
				case E_CLD_SM_TIME_FRAME_2_5MINS:
					u32ProfileIntervalSeconds = (2*60)+30;
					break;
				case E_CLD_SM_TIME_FRAME_15MINS:
				default:
					u32ProfileIntervalSeconds = 15*60;
					break;

			}

			if (((u32ZCL_GetUTCTime()- u32LastProfileUpdate) > u32ProfileIntervalSeconds) || (0 == u32LastProfileUpdate))
			{
				static uint32 u24CurrentPartialProfileIntervalValueDelivered;
				u24CurrentPartialProfileIntervalValueDelivered += 10;
				if(u24CurrentPartialProfileIntervalValueDelivered >= 50)
				{
					u24CurrentPartialProfileIntervalValueDelivered = 10;
				}
				eZCL_WriteLocalAttributeValue(ESP_METER_LOCAL_EP,
												  SE_CLUSTER_ID_SIMPLE_METERING,
												  TRUE,
												  FALSE,
												  FALSE,
												  E_CLD_SM_ATTR_ID_CURRENT_PARTIAL_PROFILE_INTERVAL_VALUE_DELIVERED,
												  &u24CurrentPartialProfileIntervalValueDelivered);
				u32LastProfileUpdate = u32ZCL_GetUTCTime();
				eSM_ServerUpdateConsumption(ESP_METER_LOCAL_EP,u32LastProfileUpdate);
			}
		}
#endif
	}
}

PRIVATE void APP_eSE_TunnelRequestTunnelSend(void)
{

	tsZCL_Address       			psDestinationAddress;
	uint8							pu8TransactionSequenceNumber;

	tsSE_TunnelRequestTunnelCmdPyld 	sRequestTunnelCmdPyld;

	DBG_vPrintf(TRACE_EVENT_HANDLER, "App Request Tunnel");

	psDestinationAddress.eAddressMode = E_ZCL_AM_IEEE;
	psDestinationAddress.uAddress.u64DestinationAddress = 0x0000000000000003;

	sRequestTunnelCmdPyld.bFlowControlSupport = 0;
	sRequestTunnelCmdPyld.u16ManufCode = 0xffff;
	sRequestTunnelCmdPyld.u16MaxIncmgTransferSize = 200;
	sRequestTunnelCmdPyld.u8ProtocolID = 0;



	teSE_TunnelStatus SE_TunnelStatus = eSE_TunnelRequestTunnelSend(
				        1, 						//u8SourceEndPointId,
				        3, 						//u8DestinationEndPointId,
				        &psDestinationAddress,
				        &pu8TransactionSequenceNumber,
				        &sRequestTunnelCmdPyld);

	DBG_vPrintf(TRACE_EVENT_HANDLER, "eSE_TunnelRequestTunnelSend: %x\r\n", SE_TunnelStatus);
}



/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
