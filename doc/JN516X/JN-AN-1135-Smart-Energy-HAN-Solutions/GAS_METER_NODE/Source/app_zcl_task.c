/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_zcl_task.c
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        ZCL Handler Functions
 *
 * $HeadURL $
 *
 * $Revision: 6165 $
 *
 * $LastChangedBy: tchia $
 *
 * $LastChangedDate: 2010-06-04 13:40:56 +0100 (Fri, 04 Jun 2010) $
 *
 * $Id: app_zcl_task.c 6165 2010-06-04 12:40:56Z tchia $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include <LedControl.h>
#include "os.h"
#include "os_gen.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pdm.h"
#include "pwrm.h"
#include "dbg.h"
#include "zps_apl_af.h"
#include "zps_apl_aib.h"
#include "zps_nwk_sap.h"
#include "zps_nwk_nib.h"
#include "zps_nwk_pub.h"
#include "string.h"

/* Application Includes */
#include "app_timer_driver.h"
#include "zcl.h"
#include "ipd.h"
#include "app_zcl_task.h"
#include "app_meter_node.h"
#include "zcl_options.h"
#include "app_smartenergy_demo.h"
#include "app_zbp_utilities.h"
#include "app_certificates.h"
#include "GenericBoard.h"

#ifdef STACK_MEASURE
	#include "StackMeasure.h"
	tsStackInfo StackInfo;
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_ZCL_TASK
#define TRACE_ZCL_TASK	FALSE
#endif

#ifndef TRACE_ZCL_TASK_HIGH
#define TRACE_ZCL_TASK_HIGH	TRUE
#endif

#ifndef TRACE_ZCL_TASK_VERBOSE
#define TRACE_ZCL_TASK_VERBOSE FALSE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void cbZCL_GeneralCallback(tsZCL_CallBackEvent *psEvent);
PRIVATE void cbZCL_EndpointCallback(tsZCL_CallBackEvent *psEvent);

#ifdef CLD_KEY_ESTABLISHMENT
PRIVATE void vHandleKeyEstablishmentEvent(void *pvParam);
#endif

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC tsSE_MeterDevice sMeter;

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE uint32 u32ZCLMutexCount = 0;

#ifdef APPLICATION_REJOIN_ENABLED
PRIVATE uint8	u8MissedApsAcks = 0;
#endif

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern uint8 		au8CAPublicKey[];
extern uint8 		au8Certificate[];
extern uint8 		au8PrivateKey[];

extern tsDevice 	s_sDevice;
extern uint32		u32LastTimeUpdate;
extern uint64		u64CoordinatorMac;
extern  uint8 		au8MacAddress[];

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_ZCLTask
 *
 * DESCRIPTION:
 * Task to handle to ZCL end point(1) events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_ZCLTask)
{
	tsZCL_CallBackEvent sCallBackEvent;
	ZPS_tsAfEvent sStackEvent;
	static uint8 u8Seconds = 0;

	u8Seconds++;

	/* Clear the ZigBee stack event */
	sStackEvent.eType = ZPS_EVENT_NONE;

	/* Point the ZCL event at the defined ZigBee stack event */
	sCallBackEvent.pZPSevent = &sStackEvent;

	/* 1 second tick, pass it to the ZCL */
	if(OS_eGetSWTimerStatus(APP_ZclTimer) == OS_E_SWTIMER_EXPIRED)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "ZCL Task activated by timer\n");
		sCallBackEvent.eEventType = E_ZCL_CBET_TIMER;
		vZCL_EventHandler(&sCallBackEvent);
		OS_eContinueSWTimer(APP_ZclTimer, ONE_SECOND_TICK_TIME, NULL);

		/* Update time in time clusters */
		sMeter.sTimeCluster.utctTime = u32ZCL_GetUTCTime();

		DBG_vPrintf(TRACE_ZCL_TASK, "Time: ");
		vShowTime(sMeter.sTimeCluster.utctTime);
	}

	/* If there is a stack event to process, pass it on to ZCL */
	if (OS_eCollectMessage(APP_msgZCLEvents, &sStackEvent) == OS_E_OK)
	{
		if( (ZPS_EVENT_APS_DATA_INDICATION == sStackEvent.eType) || (ZPS_EVENT_APS_DATA_ACK == sStackEvent.eType) )
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "ZCL_Task received event %d\r\n",sStackEvent.eType);

			sCallBackEvent.eEventType = E_ZCL_CBET_ZIGBEE_EVENT;
			vZCL_EventHandler(&sCallBackEvent);
		}
#ifdef APPLICATION_REJOIN_ENABLED
		else if( ZPS_EVENT_APS_DATA_CONFIRM == sStackEvent.eType )
		{
			if( sStackEvent.uEvent.sApsDataConfirmEvent.u8Status && ( sStackEvent.uEvent.sApsDataConfirmEvent.u8Status != MAC_ENUM_TRANSACTION_EXPIRED ))
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "Missed Acks: %d, ERR: %x\r\n",u8MissedApsAcks, sStackEvent.uEvent.sApsDataConfirmEvent.u8Status );
				u8MissedApsAcks++;

				if(u8MissedApsAcks >= MAX_MISSED_APS_ACKS)
				{
					u8MissedApsAcks = 0;
					OS_eActivateTask(APP_InitiateRejoin);
				}
			}
			/* received ack, clear down the counter */
			else
			{
				u8MissedApsAcks = 0;
			}
		}
#endif
	}

#ifdef CLD_KEY_ESTABLISHMENT
	if (u8Seconds > 10)
	{
		vDisplayAPSTable();
	}
#endif

	/* Housekeeping, reset the second counter */
	if (u8Seconds > 10)
	{
		u8Seconds = 0;
	}
}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_ZCL_vInitialise
 *
 * DESCRIPTION:
 * Initialises the Meter ZCL
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_ZCL_vInitialise(void)
{
	teZCL_Status eZCL_Status;

	/* Initialise smart energy functions */
	eZCL_Status = eSE_Initialise(&cbZCL_GeneralCallback, apduZCL);
	if (eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "eSE_Init ERR: %x\r\n", eZCL_Status);
	}

	/* Initialise Smart Energy Clusters etc */
	eZCL_Status = eSE_RegisterMeterEndPoint(LOCAL_EP, &cbZCL_EndpointCallback, &sMeter);
	if (eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "eSE_Register ERR: %\r\n", eZCL_Status);
	}

#ifdef CLD_KEY_ESTABLISHMENT
	/* Load certificate and keys */
	eSE_KECLoadKeys(LOCAL_EP, (uint8 *)au8CAPublicKey, (uint8 *)au8Certificate, au8PrivateKey);
#endif

	/* Restore the meter parameters here,
	 * readings etc...
	 */

    sMeter.sSimpleMeteringCluster.u48CurrentTier1SummationDelivered = 0;
    sMeter.sSimpleMeteringCluster.u48CurrentTier2SummationDelivered = 0;
    sMeter.sSimpleMeteringCluster.u48CurrentTier3SummationDelivered = 0;

    sMeter.sSimpleMeteringCluster.i24InstantaneousDemand = 0;
    sMeter.sSimpleMeteringCluster.eMeteringDeviceType = E_CLD_SM_MDT_GAS;

    sMeter.sBasicCluster.ePowerSource = E_CLD_BAS_PS_BATTERY;
}


/****************************************************************************
 *
 * NAME: APP_ZCL_UpdateMeterData
 *
 * DESCRIPTION:
 * Mutexes then copies the Meter data for application manipulation
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_ZCL_UpdateMeterData(tsSE_MeterDevice *psMeter)
{
	vLockZCLMutex();
	sMeter = *psMeter;
	vUnlockZCLMutex();
}


#ifdef CLD_TIME
/****************************************************************************
 *
 * NAME: vRequestTime
 *
 * DESCRIPTION:
 * Requests Time from the Meter
 * events
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PUBLIC void vRequestTime(void)
{
	uint8   u8TransactionSequenceNumber;

	teZCL_Status eZCL_Status = eSE_ReadTimeAttributes(LOCAL_EP, s_sDevice.sEsp.u8TimeEndPoint, &s_sDevice.sEsp.sAddress, &u8TransactionSequenceNumber);
	DBG_vPrintf(TRACE_ZCL_TASK, "Req Time: 0x%02x\r\n", eZCL_Status);
}
#endif


/****************************************************************************
 *
 * NAME: vShowTime
 *
 * DESCRIPTION:
 * Displays the time
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vShowTime(uint32 u32Time)
{
	uint32 u32Sec, u32Min, u32Hour, u32Day;

	u32Day  = u32Time / (60 * 60 * 24);
	u32Time = u32Time % (60 * 60 * 24);

	u32Hour = u32Time / (60 * 60);
	u32Time = u32Time % (60 * 60);

	u32Min  = u32Time / 60;
	u32Time = u32Time % 60;

	u32Sec = u32Time;

	DBG_vPrintf(TRACE_ZCL_TASK, "%d %02d:%02d:%02d\r\n", u32Day, u32Hour, u32Min, u32Sec);
}


/****************************************************************************
 *
 * NAME: vLockZCLMutex
 *
 * DESCRIPTION:
 * Grabs and maintains a counting mutex
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vLockZCLMutex(void)
{
	if (u32ZCLMutexCount == 0)
	{
		OS_eEnterCriticalSection(ZCL);
	}
	u32ZCLMutexCount++;
}


/****************************************************************************
 *
 * NAME: vUnlockZCLMutex
 *
 * DESCRIPTION:
 * Releases and maintains a counting mutex
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vUnlockZCLMutex(void)
{
	u32ZCLMutexCount--;
	if (u32ZCLMutexCount == 0)
	{
		OS_eExitCriticalSection(ZCL);
	}
}


/****************************************************************************
 *
 * NAME: vClearExpiredFlag
 *
 * DESCRIPTION:
 * Starts and immediately stops a timer to clear the expired flag
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vClearExpiredFlag(OS_thSWTimer hSWTimer)
{
	OS_eStartSWTimer(hSWTimer, APP_TIME_MS(10000), NULL);
	OS_eStopSWTimer(hSWTimer);
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: cbZCL_GeneralCallback
 *
 * DESCRIPTION:
 * General callback for ZCL events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void cbZCL_GeneralCallback(tsZCL_CallBackEvent *psEvent)
{
	ZPS_teStatus eStatus;

	switch(psEvent->eEventType)
	{
		case E_ZCL_CBET_LOCK_MUTEX:
			vLockZCLMutex();
		break;

		case E_ZCL_CBET_UNLOCK_MUTEX:
			vUnlockZCLMutex();
		break;

		case E_ZCL_CBET_UNHANDLED_EVENT:
		break;

		case E_ZCL_CBET_READ_ATTRIBUTES_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Read attributes response\r\n");
		break;

		case E_ZCL_CBET_READ_REQUEST:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Read request\r\n");
		break;

		case E_ZCL_CBET_DEFAULT_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Default response\r\n");
		break;

		case E_ZCL_CBET_ERROR:
			eStatus = eZCL_GetLastZpsError();
			DBG_vPrintf(TRACE_ZCL_TASK, "EVT: Error - Stack returned 0x%x\r\n", eStatus);
		break;

		case E_ZCL_CBET_TIMER:
		    DBG_vPrintf(TRACE_ZCL_TASK, "EVT: Timer\r\n");
		break;

		case E_ZCL_CBET_ZIGBEE_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EVT: ZigBee\r\n");
		break;

		case E_ZCL_CBET_CLUSTER_CUSTOM:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Custom\r\n");
		break;

		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "Invalid event type\r\n");
		break;
	}
}


/****************************************************************************
 *
 * NAME: cbZCL_EndpointCallback
 *
 * DESCRIPTION:
 * Endpoint specific callback for ZCL events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void cbZCL_EndpointCallback(tsZCL_CallBackEvent *psEvent)
{
	switch (psEvent->eEventType)
	{
		case E_ZCL_CBET_LOCK_MUTEX:
			vLockZCLMutex();
		break;

		case E_ZCL_CBET_UNLOCK_MUTEX:
			vUnlockZCLMutex();
		break;

		case E_ZCL_CBET_UNHANDLED_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Unhandled event\r\n");
		break;

		case E_ZCL_CBET_READ_INDIVIDUAL_ATTRIBUTE_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Read individual attribute response\r\n");
			DBG_vPrintf(TRACE_ZCL_TASK, "* Attr 0x%04x\r\n", psEvent->uMessage.sIndividualAttributeResponse.u16AttributeEnum);
		break;

		case E_ZCL_CBET_READ_ATTRIBUTES_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "Read Attributes Resp\r\n");
			switch(psEvent->psClusterInstance->psClusterDefinition->u16ClusterEnum)
			{
				case  GENERAL_CLUSTER_ID_TIME:
					vZCL_SetUTCTime(sMeter.sTimeCluster.utctTime);
					u32LastTimeUpdate = sMeter.sTimeCluster.utctTime;				// Store when the time was last updated to trigger a refresh in 24 hrs
					OS_eStopSWTimer(APP_UTC_Timer);
					OS_eActivateTask(APP_MeterTask);
				break;

				default:
				break;
			}
		break;

		case E_ZCL_CBET_READ_REQUEST:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Read request\r\n");

#ifdef POT_READ

			sMeter.sSimpleMeteringCluster.i24InstantaneousDemand = (1.2 * u16ReadPotValue());
			DBG_vPrintf(TRACE_ZCL_TASK, "\r\nPotVal: %d",sMeter.sSimpleMeteringCluster.i24InstantaneousDemand);
#else
			sMeter.sSimpleMeteringCluster.i24InstantaneousDemand = 500;
#endif
			sMeter.sSimpleMeteringCluster.u24Multiplier = 1;
			sMeter.sSimpleMeteringCluster.u24Divisor = 1000;
		break;

		case E_ZCL_CBET_DEFAULT_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Default response\r\n");
			DBG_vPrintf(TRACE_ZCL_TASK, "Command ID: %d Status Code: %d\r\n", psEvent->uMessage.sDefaultResponse.u8CommandId, psEvent->uMessage.sDefaultResponse.u8StatusCode);
		break;

		case E_ZCL_CBET_ERROR:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Error\n");
		break;

		case E_ZCL_CBET_TIMER:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Timer\r\n");
		break;

		case E_ZCL_CBET_ZIGBEE_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: ZigBee\r\n");
		break;

		case E_ZCL_CBET_CLUSTER_CUSTOM:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Custom\r\n");

			switch(psEvent->uMessage.sClusterCustomMessage.u16ClusterId)
			{
#ifdef CLD_KEY_ESTABLISHMENT
				case SE_CLUSTER_ID_KEY_ESTABLISHMENT:
					vHandleKeyEstablishmentEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif

				default:
					DBG_vPrintf(TRACE_ZCL_TASK, "Custom event for unknown cluster %d\r\n", psEvent->uMessage.sClusterCustomMessage.u16ClusterId);
				break;
			}
		break;

		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Invalid event type\r\n");
		break;
	}
}


#ifdef CLD_KEY_ESTABLISHMENT
/****************************************************************************
 *
 * NAME: vHandleKeyEstablishmentEvent
 *
 * DESCRIPTION:
 * Handles the Key Establishment events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleKeyEstablishmentEvent(void *pvParam)
{
    uint64 u64MacAddress = 0;
    tsSE_KECCallBackMessage *psKECMessage = (tsSE_KECCallBackMessage*)pvParam;
    DBG_vPrintf(TRACE_ZCL_TASK, "KEC Event: %d, Command: %d Status: %x\r\n", psKECMessage->eEventType, psKECMessage->u8CommandId, psKECMessage->eKECStatus);

	/* If key establishment was successful */
	if((psKECMessage->eEventType == E_SE_KEC_EVENT_COMMAND) &&
       (psKECMessage->eKECStatus == E_ZCL_SUCCESS) &&
       (psKECMessage->u8CommandId == E_SE_CONFIRM_KEY_DATA_REQUEST))
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "Key est OK\n");
		/* Set the link key to that returned from the key establishment
		 * First parameter is the mac address of the coordinator */
		memcpy(&u64MacAddress, &psKECMessage->psKEC_Common->uMessage.au8RemoteCertificate[22], 8);

#ifdef ZIGBEE_R20
		teZCL_Status eZCL_Status = ZPS_eAplZdoAddReplaceLinkKey(u64MacAddress, psKECMessage->psKEC_Common->au8Key, ZPS_APS_UNIQUE_LINK_KEY);
#else
		teZCL_Status eZCL_Status = ZPS_eAplZdoAddReplaceLinkKey(u64MacAddress, psKECMessage->psKEC_Common->au8Key);
#endif

		if (eZCL_Status)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "eAplZdoAdd ERR: %x\n", eZCL_Status);
		}
		ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_ALL_PERMITED);

        s_sDevice.eState = E_BIND_REQ;

        s_sDevice.bKeyEstComplete = TRUE;
        OS_eActivateTask(APP_MeterTask);
		vLedControl(0, FALSE);
		vLedControl(1, FALSE);
    }
}
#endif


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
