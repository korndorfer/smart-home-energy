/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_meter_node.c
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        SE Meter - Main Source File
 *
 * $HeadURL:
 *
 * $Revision: 8546 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-01-09 10:52:15 +0000 (Mon, 09 Jan 2012) $
 *
 * $Id: app_meter_node.c 8546 2012-01-09 10:52:15Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include <LedControl.h>
#include "dbg.h"
#include "os.h"
#include "os_gen.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pwrm.h"
#include "zps_apl_af.h"
#include "zps_apl_zdp.h"
#include "zps_apl_aib.h"
#include "AppHardwareAPI.h"
#include "zcl.h"
#include "pdm.h"
#include "zps_apl_af.h"
#include "zps_apl_aib.h"
#include "zps_nwk_nib.h"
#include "zps_nwk_pub.h"

/* Application Includes */
#include "string.h"
#include "app_smartenergy_demo.h"
#include "app_event_handler.h"
#include "app_timer_driver.h"
#include "app_zcl_task.h"
#include "app_meter_node.h"
#include "app_buttons.h"
#include "Time.h"
#include "Price.h"
#include "Utilities.h"
#include "zcl_options.h"
#include "AppHardwareAPI.h"
#include "Printf.h"
#ifdef RADIO_RECALIBRATION
#include "recal.h"
#endif
#if(JENNIC_PCB == DEVKIT4)
#include "GenericBoard.h"
#endif


/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_METER_NODE
#define TRACE_METER_NODE TRUE
#endif

#ifndef TRACE_METER_NODE_HIGH
#define TRACE_METER_NODE_HIGH TRUE
#endif

#define IPD_JOINING_TIME  			APP_TIME_MS(10000)

//#define SNIFFER_KEY
//#define ETSI																	// Remove comments to limit the module to +8dB for ETSI compliance



/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void vHandleConfigureNetworkEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleNetworkFormationEvent(ZPS_tsAfEvent sStackEvent);

#ifdef SNIFFER_KEY
PRIVATE void vSetNwkSecurity(void);
#endif

#ifdef PDM_EEPROM
PUBLIC uint8 u8PDM_CalculateFileSystemCapacity(void);
PUBLIC uint8 u8PDM_GetFileSystemOccupancy(void);
#endif

extern PUBLIC void vTAM_MLME_RxInCca(bool_t bEnable);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC PDM_tsRecordDescriptor s_sDevicePDDesc;
PUBLIC tsDevice s_sDevice;


#ifdef JENNIC_CHIP_FAMILY_JN516x
#if defined(PRODUCTION_CERTS) || defined(CLD_OTA)
	extern  uint8 	au8MacAddress[];
#else
	uint8 	au8MacAddress[] ={
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02
	};
#endif
#endif


/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

uint32	u32Price;
uint8	u8PriceDp;
uint8	u8PriceTier;
PRIVATE uint8	u8ChildOfInterest = 0;

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern uint8 s_au8LnkKeyArray[16];
extern tsSE_EspMeterDevice sMeter;
extern uint8 au8CAPublicKey[];

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_MeterTask
 *
 * DESCRIPTION:
 * Main State Machine for Meter Node
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_MeterTask)
{
	ZPS_tsAfEvent sStackEvent;
    APP_tsEvent sAppEvent;

    sStackEvent.eType = ZPS_EVENT_NONE;
    sAppEvent.eType = APP_E_EVENT_NONE;

	if( OS_E_OK  == OS_eCollectMessage(APP_msgZpsEvents, &sStackEvent) )
	{
		DBG_vPrintf(TRACE_METER_NODE, "Stack Evt: 0x%02x\n", sStackEvent.eType);

		if (sStackEvent.eType == ZPS_EVENT_ERROR)
		{
			DBG_vPrintf(TRACE_METER_NODE, "ZigBee ERR: %x \r\n", sStackEvent.uEvent.sAfErrorEvent.eError);
		}
	}

	/* No stack event at this point, check for app events */
	if (ZPS_EVENT_NONE == sStackEvent.eType)
	{
		if( OS_E_OK  == OS_eCollectMessage(APP_Events, &sAppEvent) )
		{
			DBG_vPrintf(TRACE_METER_NODE, "App Event: %x \r\n", sAppEvent.eType);
		}
	}
	else
	{
		DBG_vPrintf(TRACE_METER_NODE, "No Event \r\n" );
	}

	/* The main state machine for the Meter Application */
	switch (s_sDevice.eState)
	{
		case E_CONFIGURE_NETWORK:
			DBG_vPrintf(TRACE_METER_NODE, "In E_CONFIG\r\n");
			vHandleConfigureNetworkEvent(sStackEvent);
		break;

        case E_NETWORK_STARTUP:
            DBG_vPrintf(TRACE_METER_NODE, "E_NETWORK_STARTUP\r\n");
            vHandleNetworkFormationEvent(sStackEvent);
        break;

        case E_NETWORK_RUN:
            DBG_vPrintf(TRACE_METER_NODE, "E_NETWORK_RUN (Event %d)\r\n", sStackEvent.eType);
            vHandleRunningEvent(sStackEvent, sAppEvent);
        break;

        default:
            DBG_vPrintf(TRACE_METER_NODE, "Unhandled Event in Meter Task: %d\r\n", sStackEvent.eType);
        break;
	}
}


/****************************************************************************
 *
 * NAME: APP_AgeOutChildren
 *
 * DESCRIPTION:
 * Cycles through the device's children on a context restore to find out if
 * they now have a different parent
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_AgeOutChildren)
{
	ZPS_tsNwkNib * thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());
	uint8 i;

	for(i = u8ChildOfInterest ; i < thisNib->sTblSize.u16NtActv ; i++)
	{
		if (ZPS_NWK_NT_AP_RELATIONSHIP_CHILD == thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u2Relationship)
		{
			/* Child found in the neighbour table, send out a address request.
			 * If anyone responds we know to age out the child */
			u8ChildOfInterest = i;

			PDUM_thAPduInstance hAPduInst;
			hAPduInst = PDUM_hAPduAllocateAPduInstance(apduZDP);

			if (hAPduInst == PDUM_INVALID_HANDLE)
			{
				DBG_vPrintf(TRACE_METER_NODE, "IEEE Address Request - PDUM_INVALID_HANDLE\n");
			}
			else
			{
				uint8 u8TransactionSequenceNumber;

				/* Broadcast to all Rx-On-When-Idle devices */
				ZPS_tuAddress uAddress;
				uAddress.u16Addr = 0xFFFD;

				ZPS_tsAplZdpNwkAddrReq sAplZdpNwkAddrReq;
				sAplZdpNwkAddrReq.u64IeeeAddr = thisNib->sTbl.psNtActv[u8ChildOfInterest].u64ExtAddr;
				sAplZdpNwkAddrReq.u8RequestType = 0;

				DBG_vPrintf(TRACE_METER_NODE, "Child found in NT, sending route request: 0x%04x\n", thisNib->sTbl.psNtActv[u8ChildOfInterest].u16NwkAddr);
				ZPS_teStatus eStatus = ZPS_eAplZdpNwkAddrRequest(	hAPduInst,
																	uAddress,
																	FALSE,
																	&u8TransactionSequenceNumber,
																	&sAplZdpNwkAddrReq
																	);

				if (eStatus)
				{
					DBG_vPrintf(TRACE_METER_NODE, "Address Request failed: 0x%02x\n", eStatus);
				}
				else
				{
					u8ChildOfInterest++;
					break;
				}
			}
		}
	}

	if (i >= thisNib->sTblSize.u16NtActv)
	{
		OS_eStopSWTimer(APP_AgeOutChildrenTmr);									// No children left in the NT to query for
		u8ChildOfInterest = 0;
	}
	else
	{
		OS_eStartSWTimer(APP_AgeOutChildrenTmr, APP_TIME_MS(1600), NULL);		// Re-activate this task in 1.6s to scan for the next child
	}
}


/****************************************************************************
 *
 * NAME: APP_RadioRecal
 *
 * DESCRIPTION:
 * Recalibrate the radio once every minute
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_RadioRecal)
{
#ifdef RADIO_RECALIBRATION
	if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RadioRecalTimer))
	{
		DBG_vPrintf(TRACE_METER_NODE, "Recalibrate the radio\n");
		uint8 eStatus = eAHI_AttemptCalibration();
		if (eStatus)
		{
			DBG_vPrintf(TRACE_METER_NODE, "Recalibration already underway");
			OS_eStartSWTimer(APP_RadioRecalTimer, APP_TIME_MS(1000), NULL);			// Re-activate this task in 1s
		}
		else
		{
			OS_eStartSWTimer(APP_RadioRecalTimer, APP_TIME_MS(60000), NULL);		// Re-activate this task in 60s
		}
	}
#endif
}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vInitialise
 *
 * DESCRIPTION:
 * Initialises the application
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vInitialise(void)
{
	/* If a button is held down over reset, clear the context */
	//bool bDeleteRecords = APP_bButtonInitialise();
	bool bDeleteRecords = APP_bButtonInitialise();//TRUE;
	//APP_bButtonInitialise();
	uint32 u32UTCTime;

	if (bDeleteRecords)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Clearing Context\n");
		PDM_vDelete();
	}

	/* set the device state to initial state */
   	s_sDevice.eState = E_CONFIGURE_NETWORK;

	/* Restore any application data previously saved to flash */
   	PDM_eLoadRecord(	&s_sDevicePDDesc,
						ESP_STATE,
   						&s_sDevice,
   						sizeof(s_sDevice), FALSE);

	if (E_CONFIGURE_NETWORK == s_sDevice.eState)								// If context is blank
	{
		#ifdef ZIGBEE_R20
   		/* set the security state default link key */
   		ZPS_vAplSecSetInitialSecurityState(ZPS_ZDO_PRECONFIGURED_LINK_KEY, s_au8LnkKeyArray, 0, ZPS_APS_UNIQUE_LINK_KEY );
		#else
		ZPS_vAplSecSetInitialSecurityState(ZPS_ZDO_PRECONFIGURED_LINK_KEY, s_au8LnkKeyArray, 0 );
		#endif

	}

#ifndef JENNIC_CHIP_FAMILY_JN514x
	ZPS_vSetOverrideLocalMacAddress((uint64 *)&au8MacAddress);
#endif

	/* Initialise ZBPro stack */
	ZPS_eAplAfInit();

	/* Turn Rx in CCA feature ON. */
	vTAM_MLME_RxInCca(TRUE);

#ifdef PDM_EEPROM
    /*
     * The functions u8PDM_CalculateFileSystemCapacity and u8PDM_GetFileSystemOccupancy
     * may be called at any time to monitor space available in  the eeprom
     */
    DBG_vPrintf(TRACE_METER_NODE, "PDM: Capacity %d\n", u8PDM_CalculateFileSystemCapacity() );
    DBG_vPrintf(TRACE_METER_NODE, "PDM: Occupancy %d\n", u8PDM_GetFileSystemOccupancy() );
#endif

#ifdef ETSI
	vAHI_ETSIHighPowerModuleEnable(TRUE);										// Limit the module to +8dB for ETSI compliance
#endif

	/* Initialise the Leds */
	vLedInitRfd();

	/* Initialise ZCL */
	APP_ZCL_vInitialise();

	/* If the device state has been restored from flash, re-start the stack
	 *  and set the application running again */
	if (E_CONFIGURE_NETWORK != s_sDevice.eState)								// If the last known state was a running state (i.e. a context restore)
	{
		ZPS_eAplZdoStartStack();
		DBG_vPrintf(TRACE_METER_NODE, "Restoring Context, app state %d, \n", s_sDevice.eState);

		if (s_sDevice.bPermitJoining)
		{
			ZPS_eAplZdoPermitJoining(0xff);
		}
		else
		{
			ZPS_eAplZdoPermitJoining(0x00);
		}

		/* Set the leds to the 'network up' state */
		vLedControl(0, FALSE);
		vLedControl(1, FALSE);

		/* Activate the child aging task on a context restore to make sure any previous
		 * children haven't jumped to another parent whilst we were offline */
		OS_eActivateTask(APP_AgeOutChildren);
	}
	/* else perform any actions required on initial start-up */
	else
	{
		ZPS_eAplZdoPermitJoining(0xff);
		s_sDevice.bPermitJoining = TRUE;

		/* Set the leds to the initial state */
		vLedControl(0, TRUE);
		vLedControl(1, TRUE);
	}

	/* Start the tick timer */
	OS_eStartSWTimer(APP_ZclTimer, ONE_SECOND_TICK_TIME, NULL);

	/* Activate the radio recalibration task in 60s */
	OS_eStartSWTimer(APP_RadioRecalTimer, APP_TIME_MS(60000), NULL);

	/* Time */
	u32UTCTime = 0;
	vZCL_SetUTCTime( u32UTCTime );												// Initialise the time to 0:00 01/01/2000
	u32Price = 10;																// Initialise the price
	u8PriceDp = 2;																// Initialise the price magnitude, number of digits to the right of the decimal point
	u8PriceTier = 1;															// Initialise the price tier

#ifndef SE_CERTIFICATION

	char *acRateLabelBase;
	tsSE_PricePublishPriceCmdPayload sPrice;

	/* Price */
	acRateLabelBase = "BASE";
	sPrice.u32ProviderId =  0x00000001;
	sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
	sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
	sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

	sPrice.u32IssuerEventId = u32ZCL_GetUTCTime();
	sPrice.u8UnitOfMeasure = 0x00;
	sPrice.u16Currency = 840;
	sPrice.u8PriceTrailingDigitAndPriceTier = 0x10;
	sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x40;
	sPrice.u32StartTime = u32ZCL_GetUTCTime();
	sPrice.u16DurationInMinutes = 0x0002;
	sPrice.u32Price = 0x00000012;
	sPrice.u8PriceRatio = 0x12;
	sPrice.u32GenerationPrice = 0x00000015;
	sPrice.u8GenerationPriceRatio = 0x25;
	sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
	sPrice.u8AlternateCostUnit = 0xFF;
	sPrice.u8AlternateCostTrailingDigit = 0xFF;
	sPrice.u8NumberOfBlockThresholds = 0xFF;
	sPrice.u8PriceControl = 1;
	APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
#ifdef NUMBER_OF_SUPPORTED_COMMODITY_TYPES
	uint8 i;
	for(i = 1 ; i < NUMBER_OF_SUPPORTED_COMMODITY_TYPES ; i++)
	{
		APP_bAddPrice(sPrice, 1 + CLD_SM_NUMBER_OF_MIRRORS + i);	// Set a price and price tier for 1 minute but don't transmit
	}
#endif
	OS_eStartSWTimer(APP_UpdatePriceTimer, APP_TIME_MS(60000), NULL);			// Start the timer to update the price again in 60s

	/* Messaging */
	vSendNewMessageToBound(0x00000001, 0, 0, 0xFFFF, "Test Message");
#endif

	/* Mirroring */
#ifdef CLD_SM_SUPPORT_MIRROR
	uint8 u8LoopCntr,u8PhysicalEnviorment;
	uint16 u16FoundEP;
	for(u8LoopCntr = 0; u8LoopCntr < CLD_SM_NUMBER_OF_MIRRORS; u8LoopCntr++)
	{
		eSM_CreateMirror(MIRROR_START_EP+u8LoopCntr,s_sDevice.u64MirroredDeviceAddress[u8LoopCntr]);
	}
	eSM_GetFreeMirrorEndPoint(&u16FoundEP);
	if (u16FoundEP == 0xFFFF)
	{
		u8PhysicalEnviorment = 0x00;
	}
	else
	{
		u8PhysicalEnviorment = 0x01;
	}
	eZCL_WriteLocalAttributeValue(ESP_METER_LOCAL_EP,GENERAL_CLUSTER_ID_BASIC,TRUE,FALSE,FALSE,E_CLD_BAS_ATTR_ID_PHYSICAL_ENVIRONMENT,&u8PhysicalEnviorment);
#endif

	/* OTA */
#ifdef CLD_OTA
	tsNvmDefs sNvmDefs;
	teZCL_Status eZCL_Status;

#if (OTA_MAX_IMAGES_PER_ENDPOINT == 3)
        uint8 u8StartSector[3] = {0,2,4};
#else

#ifdef JENNIC_CHIP_FAMILY_JN516x
        uint8 u8StartSector[1] = {0};
#else
        uint8 u8StartSector[2] = {0,3};
#endif
#endif
	sNvmDefs.u32SectorSize = 64*1024;
	sNvmDefs.u8FlashDeviceType = E_FL_CHIP_AUTO;
	vOTA_FlashInit(NULL,&sNvmDefs);

#ifdef JENNIC_CHIP_FAMILY_JN516x
	eZCL_Status = eOTA_AllocateEndpointOTASpace(ESP_METER_LOCAL_EP, u8StartSector, OTA_MAX_IMAGES_PER_ENDPOINT, 4, TRUE,au8CAPublicKey);
#else
	eZCL_Status = eOTA_AllocateEndpointOTASpace(ESP_METER_LOCAL_EP, u8StartSector, OTA_MAX_IMAGES_PER_ENDPOINT, 3, TRUE,au8CAPublicKey);
#endif
	if (eZCL_Status != E_ZCL_SUCCESS)
	{
	    DBG_vPrintf(TRACE_METER_NODE, "eAllocateEndpointOTASpace returned error 0x%x", eZCL_Status);
	}
	eZCL_Status = eOTA_SetServerAuthorisation(ESP_METER_LOCAL_EP, E_CLD_OTA_STATE_ALLOW_ALL,NULL, 0);
    if (eZCL_Status != E_ZCL_SUCCESS)
    {
        DBG_vPrintf(TRACE_METER_NODE, "eSetServerAuthorisation returned error 0x%x", eZCL_Status);
    }
#endif

    /* Get Profile */
#ifdef CLD_SM_SUPPORT_GET_PROFILE
	uint8 eProfileIntervalPeriod = E_CLD_SM_TIME_FRAME_10MINS;
	eZCL_WriteLocalAttributeValue(ESP_METER_LOCAL_EP,
			                      SE_CLUSTER_ID_SIMPLE_METERING,
			                      TRUE,
			                      FALSE,
			                      FALSE,
			                      E_CLD_SM_ATTR_ID_PROFILE_INTERVAL_PERIOD,
			                      &eProfileIntervalPeriod);
#endif

	/* Fast Polling */
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
	uint8 u8FastPollUpdatePeriod = 2;
	eZCL_WriteLocalAttributeValue(ESP_METER_LOCAL_EP,
			                      SE_CLUSTER_ID_SIMPLE_METERING,
			                      TRUE,
			                      FALSE,
			                      FALSE,
			                      E_CLD_SM_ATTR_ID_FAST_POLL_UPDATE_PERIOD,
			                      &u8FastPollUpdatePeriod);
#endif

	#ifdef POT_READ
		bPotEnable();
	#endif

}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleConfigureNetworkEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its network configuration state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleConfigureNetworkEvent(ZPS_tsAfEvent sStackEvent)
{
   	DBG_vPrintf(TRACE_METER_NODE, "APP: Starting ZPS\r\n");

	ZPS_teStatus eStatus = ZPS_eAplZdoStartStack();

	if (ZPS_E_SUCCESS == eStatus)
	{
		s_sDevice.eState = E_NETWORK_STARTUP;
		OS_eActivateTask(APP_MeterTask);

		DBG_vPrintf(TRACE_METER_NODE, "APP: Stack started\r\n");
#ifdef SNIFFER_KEY
		/* Set NWK key after stack, this simplifies sniffing
		 * #define SNIFFER_KEY to enable
		 */
		vSetNwkSecurity();
#endif
	}
	else
	{
		DBG_vPrintf(TRACE_METER_NODE_HIGH, "APP: ZPS_eZdoStartStack() failed error %d", eStatus);
	}
}


/****************************************************************************
 *
 * NAME: vHandleNetworkFormationEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its network formation state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleNetworkFormationEvent(ZPS_tsAfEvent sStackEvent)
{
	void *thisNet;
	ZPS_tsNwkNib * thisNib;

    /*wait for network stack to start up as a coordinator */
    if (ZPS_EVENT_NONE != sStackEvent.eType)
    {
        if (ZPS_EVENT_NWK_STARTED == sStackEvent.eType)
        {
        	DBG_vPrintf(TRACE_METER_NODE, "Network Started\r\n");

        	thisNet = ZPS_pvAplZdoGetNwkHandle();
			thisNib = ZPS_psNwkNibGetHandle(thisNet);

			DBG_vPrintf(TRACE_METER_NODE, "Channel: %d, PAN: %x", thisNib->sPersist.u8VsChannel, thisNib->sPersist.u16VsPanId);

			/* turn on joining */
			ZPS_eAplZdoPermitJoining(0xff);
			vLedControl(0, FALSE);
			vLedControl(1, FALSE);

			s_sDevice.eState = E_NETWORK_RUN;
			PDM_vSaveRecord(&s_sDevicePDDesc);
			OS_eActivateTask(APP_MeterTask);
        }
        else
        {
        	DBG_vPrintf(TRACE_METER_NODE, "unexpected Event in E_NETWORK_STARTUP\r\n");
        }
    }
}


#ifdef SNIFFER_KEY
/****************************************************************************
 *
 * NAME: vSetNwkSecurity
 *
 * DESCRIPTION:
 * Pre-configures a network key for ease of sniffing
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vSetNwkSecurity(void)
{
    /* Shouldn't pre-configure a network key but let the stack generate a
     * random one, however using a fixed key aids debugging with a sniffer */
    uint8 s_au8NwkKeyArray[16] = {
    		0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
    		0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF
    };

    ZPS_tsAplAib *s_psAib;
    ZPS_tsNwkNib *s_psNib;

    DBG_vPrintf(TRACE_METER_NODE, "Start nwk sec.  ");

    /* Set up IB handles */
    s_psAib = ZPS_psAplAibGetAib();
    if (s_psAib == NULL)
    {
        DBG_vPrintf(TRACE_METER_NODE, "s_psAib N\n");
    }

    s_psNib = ZPS_psAplZdoGetNib();
    if (s_psNib == NULL)
    {
        DBG_vPrintf(TRACE_METER_NODE, "s_psNib N\n");
    }

    /* Store the Network Key */
    s_psNib->sTbl.psSecMatSet[0].u8KeySeqNum = 0;
    s_psNib->sTbl.psSecMatSet[0].u32OutFC = 0;
    memset(s_psNib->sTbl.psSecMatSet[0].psInFCSet, 0, (sizeof(ZPS_tsNwkInFCDesc) * s_psNib->sTblSize.u16InFCSet));
    memcpy(s_psNib->sTbl.psSecMatSet[0].au8Key, s_au8NwkKeyArray, ZPS_SEC_KEY_LENGTH);
    s_psNib->sTbl.psSecMatSet[0].u8KeyType = ZPS_NWK_SEC_NETWORK_KEY;

    /* Make this the Active Key */
    s_psNib->sPersist.u8ActiveKeySeqNumber = 0;
    DBG_vPrintf(TRACE_METER_NODE, "Config Nwk Key\n");
}
#endif


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
