###############################################################################
#
# MODULE:   Makefile
#
# DESCRIPTION: Makefie for the SE IPD Project
# 
###############################################################################
#
# This software is owned by NXP B.V. and/or its supplier and is protected
# under applicable copyright laws. All rights are reserved. We grant You,
# and any third parties, a license to use this software solely and
# exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142, 
# JN5139]. You, and any third parties must reproduce the copyright and 
# warranty notice and any other legend of ownership on each copy or partial 
# copy of the software.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Copyright NXP B.V. 2012. All rights reserved
#
###############################################################################
# Subversion variables
# $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Trunk/IPD_NODE/Build/Makefile $
# $Revision: 9271 $
# $LastChangedBy: nxp33194 $
# $LastChangedDate: 2012-06-01 16:40:44 +0100 (Fri, 01 Jun 2012) $
# $Id: Makefile 9271 2012-06-01 15:40:44Z nxp33194 $ 
#
###############################################################################

# Application target name
TARGET = IPD_NODE

###############################################################################
#User definable make parameters that may be overwritten from the command line

# Default target device is the JN5148

JENNIC_CHIP ?= JN5148

###############################################################################
# Default DK4 development kit target hardware

JENNIC_PCB ?= DEVKIT4

###############################################################################
# Select the network stack (e.g. MAC, ZBPro, SE)

JENNIC_STACK ?= SE

###############################################################################
# ZBPro Stack specific options (ZCR or ZED)
ZBPRO_DEVICE_TYPE ?= ZED

###############################################################################
# Debug options - define DEBUG to enable
# DEBUG ?=HW
#
# Define which UART to use for HW debug
# DEBUG_PORT ?= UART1

###############################################################################
# Define TRACE to use with DBG module
# By default TRACE and OVERLAYS have been turned off
# Set them to 1 to enable TRACE and overlays.
TRACE ?=1
ifeq ($(JENNIC_CHIP),JN5168)
OVERLAY_BUILD ?=0
else
OVERLAY_BUILD ?=1
endif

ifeq ($(OVERLAY_BUILD),1)
CFLAGS += -DOVERLAYS_BUILD
ZBPRO_OVERLAYS = 1
INCFLAGS += -I$(COMPONENTS_BASE_DIR)/OVLY/Include
APPLIBS += OVLY

OVERLAYS += JOINING
OVERLAYS += DISCOVERY
OVERLAYS += DISPLAY
endif

# Application Flags
CFLAGS += -D$(IPD_NODE_TYPE)_DEVICE
CFLAGS += -D$(CERTIFICATES)
CFLAGS += -D$(JENNIC_PCB)

OTA_SUPPORT = 1
ifeq ($(OTA_SUPPORT),1)
OTA_DEFAULT_VERSION = 2
OTA_UPGRADE_VERSION = 3
CFLAGS += -DOTA_SUPPORT_OPTIONS
endif

#SE_CERTIFICATION = 1
ifeq ($(SE_CERTIFICATION),1)
CFLAGS += -DSE_CERTIFICATION
endif

ifeq ($(TRACE), 1)
CFLAGS  += -DDBG_ENABLE
$(info Building trace version ...)

# Enable any debug output here:
CFLAGS  += -DTRACE_ZCL_TASK=1
CFLAGS  += -DTRACE_DISPLAY=1
CFLAGS  += -DTRACE_IPD_NODE=1
CFLAGS  += -DTRACE_EVENT_HANDLER=1
CFLAGS  += -DTRACE_SLEEP=1
CFLAGS	+= -DTRACE_POLL=0
CFLAGS  += -DTRACE_START=1
CFLAGS  += -DTRACE_OVERLAYS=0
CFLAGS  += -DTRACE_BUTTON=0
CFLAGS  += -DTRACE_ADC=1
CFLAGS  += -DTRACE_TIMERS=1
CFLAGS  += -DTRACE_ZBP_UTILS=1
CFLAGS  += -DTRACE_EXCEPTION=1

else

CFLAGS  += -DTRACE_ZCL_TASK=0
CFLAGS  += -DTRACE_DISPLAY=0
CFLAGS  += -DTRACE_IPD_NODE=0
CFLAGS  += -DTRACE_EVENT_HANDLER=0
CFLAGS  += -DTRACE_SLEEP=0
CFLAGS	+= -DTRACE_POLL=0
CFLAGS  += -DTRACE_START=0
CFLAGS  += -DTRACE_OVERLAYS=0
CFLAGS  += -DTRACE_BUTTON=0
CFLAGS  += -DTRACE_ADC=0
CFLAGS  += -DTRACE_TIMERS=0
CFLAGS  += -DTRACE_ZBP_UTILS=0
CFLAGS  += -DTRACE_EXCEPTION=0

endif


###############################################################################
# Path definitions

# Use if application directory contains multiple targets
SDK_BASE_DIR		= $(abspath ../../../..)
APP_BASE		= $(abspath ../..)
APP_BLD_DIR		= $(APP_BASE)/$(TARGET)/Build
APP_SRC_DIR 		= $(APP_BASE)/$(TARGET)/Source
APP_COMMON_SRC_DIR	= $(APP_BASE)/Common/Source
UTIL_SRC_DIR		= $(COMPONENTS_BASE_DIR)/Utilities/Source

###############################################################################
# Application Source files

# Note: Path to source file is found using vpath below, so only .c filename is required
APPSRC = os_gen.c
APPSRC += os_irq.S
APPSRC += os_irq_alignment.S
APPSRC += os_irq_buserror.S
APPSRC += os_irq_illegalinstruction.S
APPSRC += os_irq_stackoverflowexception.S
APPSRC += os_irq_unimplementedmodule.S
APPSRC += pdum_gen.c
APPSRC += zps_gen.c
APPSRC += app_timer_driver.c
APPSRC += app_start.c
APPSRC += app_ipd_node.c
APPSRC += app_zcl_task.c
APPSRC += app_buttons.c
APPSRC += StackMeasure.c
APPSRC += app_sleep_functions.c
APPSRC += app_zbp_utilities.c
APPSRC += app_event_handler.c

ifeq ($(IPD_NODE_TYPE), LC)


APPSRC += app_display_lc.c
APPSRC += app_adc_lc.c
APPSRC += app_led_lc.c
APPSRC += app_lcd_driver.c

endif

ifeq ($(IPD_NODE_TYPE), EVK)


APPSRC += app_display_evk.c
APPSRC += app_adc_evk.c
APPSRC += app_led_evk.c
APPSRC += xsprintf.c

endif

APP_ZPSCFG = app.zpscfg

###############################################################################
# Standard Application header search paths

INCFLAGS += -I$(APP_SRC_DIR)
INCFLAGS += -I$(APP_SRC_DIR)/$(IPD_NODE_TYPE)
INCFLAGS += -I$(APP_SRC_DIR)/Plugins
INCFLAGS += -I$(APP_SRC_DIR)/..
INCFLAGS += -I$(APP_COMMON_SRC_DIR)

# Application specific include files
INCFLAGS += -I$(COMPONENTS_BASE_DIR)/JennicLogo/Include
INCFLAGS += -I$(COMPONENTS_BASE_DIR)/Utilities/Include
INCFLAGS += -I$(COMPONENTS_BASE_DIR)/NXPLogo/Include

###############################################################################
# Application libraries
# Specify additional Component libraries

#APPLIBS += 

###############################################################################

# You should not need to edit below this line

###############################################################################
###############################################################################
# Configure for the selected chip or chip family

include $(SDK_BASE_DIR)/Chip/Common/Build/config.mk
include $(SDK_BASE_DIR)/Platform/Common/Build/Config.mk
include $(SDK_BASE_DIR)/Stack/Common/Build/config.mk

###############################################################################

TEMP = $(APPSRC:.c=.o)
APPOBJS = $(TEMP:.S=.o)

###############################################################################
# Application dynamic dependencies

APPDEPS = $(APPOBJS:.o=.d)

###############################################################################
# Linker

# Add application libraries before chip specific libraries to linker so
# symbols are resolved correctly (i.e. ordering is significant for GCC)

APPLDLIBS := $(foreach lib,$(APPLIBS),$(if $(wildcard $(addprefix $(COMPONENTS_BASE_DIR)/Library/lib,$(addsuffix _$(TEMPCHIP).a,$(lib)))),$(addsuffix _$(TEMPCHIP),$(lib)),$(addsuffix _$(JENNIC_CHIP_FAMILY),$(lib))))
LDLIBS := $(APPLDLIBS) $(LDLIBS)

###############################################################################
# Dependency rules

.PHONY: all clean
# Path to directories containing application source 
vpath % $(APP_SRC_DIR):$(APP_SRC_DIR)/$(IPD_NODE_TYPE):$(APP_SRC_DIR)/Plugins:$(APP_COMMON_SRC_DIR):$(SE_SRC_DIRS):$(UTIL_SRC_DIR)


all: $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).bin

#-include $(APPDEPS)
#%.d:
#	rm -f $*.o

$(APP_SRC_DIR)/Plugins/os_gen.c $(APP_SRC_DIR)/Plugins/os_gen.h $(APP_SRC_DIR)/Plugins/os_irq.S $(APP_SRC_DIR)/Plugins/os_irq_alignment.S $(APP_SRC_DIR)/Plugins/os_irq_buserror.S $(APP_SRC_DIR)/Plugins/os_irq_illegalinstruction.S $(APP_SRC_DIR)/Plugins/os_irq_stackoverflowexception.S $(APP_SRC_DIR)/Plugins/os_irq_unimplementedmodule.S: $(APP_SRC_DIR)/App_$(TARGET)_$(JENNIC_CHIP_FAMILY).oscfgdiag $(OSCONFIG) 
	$(info Configuring the OS ...)
	$(OSCONFIG) -f "$(shell cygpath -w "$<")" -o "$(shell cygpath -w "$(APP_SRC_DIR)/Plugins")" -v $(JENNIC_CHIP)
	@echo

$(APP_SRC_DIR)/Plugins/pdum_gen.c $(APP_SRC_DIR)/Plugins/pdum_gen.h: $(APP_COMMON_SRC_DIR)/$(APP_ZPSCFG) $(PDUMCONFIG)
	$(info Configuring the PDUM ...)
	$(PDUMCONFIG) -z $(TARGET)_$(IPD_NODE_TYPE) -f "$(shell cygpath -w "$<")" -o "$(shell cygpath -w "$(APP_SRC_DIR)/Plugins")"
	@echo

$(APP_SRC_DIR)/Plugins/zps_gen.c $(APP_SRC_DIR)/Plugins/zps_gen.h: $(APP_COMMON_SRC_DIR)/$(APP_ZPSCFG) $(ZPSCONFIG)
	$(info Configuring the Zigbee Protocol Stack ...)
	$(ZPSCONFIG) -n $(TARGET)_$(IPD_NODE_TYPE) -t $(JENNIC_CHIP) -l $(ZPS_NWK_LIB) -a $(ZPS_APL_LIB) -f "$(shell cygpath -w "$<")" -o "$(shell cygpath -w "$(APP_SRC_DIR)/Plugins")"
	@echo

%.o: %.S
	$(info Assembling $< ...)
	$(CC) -c -o $(subst Source,Build,$@) $(CFLAGS) $(INCFLAGS) $< -MD -MF $*.d -MP
	@echo

%.o: %.c 
	$(info Compiling $< ...)
	$(CC) -c -o $(subst Source,Build,$@) $(CFLAGS) $(INCFLAGS) $< -MD -MF $*.d -MP
	@echo

ifeq ($(JENNIC_CHIP_FAMILY),JN516x)
$(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).elf: $(APPOBJS) $(addsuffix.a,$(addprefix $(COMPONENTS_BASE_DIR)/Library/lib,$(APPLDLIBS))) 
	$(info Linking $@ ...)	
	$(CC) -Wl,--gc-sections -Wl,-u_AppColdStart -Wl,-u_AppWarmStart $(LDFLAGS) -TAppBuildSEZBPro_$(JENNIC_CHIP).ld -o $@ -Wl,--start-group $(APPOBJS) $(addprefix -l,$(LDLIBS)) -lm -Wl,--end-group -Wl,-Map,$(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).map 
	$(SIZE) $@
else
$(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP).elf: $(APPOBJS) $(addsuffix.a,$(addprefix $(COMPONENTS_BASE_DIR)/Library/lib,$(APPLDLIBS))) 
	$(info Linking $@ ...)
	$(CC) -Wl,--unique -Wl,-r -Wl,-u_AppColdStart -Wl,-u_AppWarmStart $(LDFLAGS) -TAppBuild_$(JENNIC_CHIP)_Blank.ld -o temp.elf -Wl,--start-group $(APPOBJS) $(addprefix -l,$(LDLIBS)) -Wl,--end-group 
	$(TOOL_BASE_DIR)/OVLYUtils/bin/OVLY_Relocation temp.elf $(OVERLAYS)
	$(info Final Link $@ ...)
	$(CC) -Wl,--gc-sections -Wl,-u_AppColdStart -Wl,-u_AppWarmStart $(LDFLAGS) -TAppBuild_$(JENNIC_CHIP).ld $(addsuffix .ld,$(addprefix -TApp_Overlay_,$(OVERLAYS))) -TAppBuild_$(JENNIC_CHIP)_End.ld -o $@ temp.elf -Wl,-Map,$(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).map
	$(info Generating checksums $@ ...)
	$(TOOL_BASE_DIR)/OVLYUtils/bin/OVLY_Checksum $@ $(addprefix .,$(OVERLAYS))
	rm -f temp.elf
	$(TOOL_BASE_DIR)/OVLYUtils/bin/image_size.sh $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).map
	@echo
endif


ifeq ($(JENNIC_CHIP_FAMILY),JN516x)
$(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).bin: $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).elf 
	$(info Generating binaries ...)
	$(OBJCOPY) -j .version -j .bir -j .flashheader -j .vsr_table -j .vsr_handlers  -j .ro_mac_address -j .ro_ota_header -j .ro_se_lnkKey -j .ro_se_cert -j .ro_se_pvKey -j .ro_se_customData -j .rodata -j .text -j .data -j .bss -j .heap -j .stack -S -O binary $< $@
	
ifeq ($(OTA_SUPPORT),1)
	$(info Generating OTA upgrade binary ...)
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/configOTA6x_BLANK_IPD.txt configOTA6x_BLANK_IPD.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/MACAddr_BLANK.txt MACAddr_BLANK.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/LinkKey_BLANK.txt LinkKey_BLANK.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/ZigbeeCert_BLANK.txt ZigbeeCert_BLANK.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/PrivateKey_BLANK.txt PrivateKey_BLANK.txt
	$(SDK_BASE_DIR)/Tools/OTAUtils/JET.exe -m combine -f $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).bin -x configOTA6x_BLANK_IPD.txt -v 4
	$(SDK_BASE_DIR)/Tools/OTAUtils/JET.exe -m otamerge --ota --embed_hdr -c outputFFFFFFFFFFFFFFFF.bin -o $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX)_OTA_UPGRADE_v$(OTA_UPGRADE_VERSION).bin -v 4 -n $(OTA_UPGRADE_VERSION)
	rm configOTA6x_BLANK_IPD.txt MACAddr_BLANK.txt LinkKey_BLANK.txt ZigbeeCert_BLANK.txt PrivateKey_BLANK.txt outputFFFFFFFFFFFFFFFF.bin
	@echo
	
	$(info Generating OTA application binary ...)
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/configOTA_6x_Cer_Keys_IPD.txt configOTA_6x_Cer_Keys_IPD.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/MACAddr_1.txt MACAddr_1.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/LinkKey_1.txt LinkKey_1.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/ZigbeeCert_1.txt ZigbeeCert_1.txt
	cp $(SDK_BASE_DIR)/Tools/OTAUtils/NXP_Programmer/PrivateKey_1.txt PrivateKey_1.txt
	$(SDK_BASE_DIR)/Tools/OTAUtils/JET.exe -m combine -f $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).bin -x configOTA_6x_Cer_Keys_IPD.txt -v 4
	$(SDK_BASE_DIR)/Tools/OTAUtils/JET.exe -m otamerge --embed_hdr -c output0000000000000001.bin -o $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).bin -v 4 -n $(OTA_DEFAULT_VERSION)
	rm configOTA_6x_Cer_Keys_IPD.txt MACAddr_1.txt LinkKey_1.txt ZigbeeCert_1.txt PrivateKey_1.txt output0000000000000001.bin
	@echo
endif

else
$(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).bin: $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)$(BIN_SUFFIX).elf 
	$(info Generating binary ...)
	$(OBJCOPY) -j .version -j .bir -j .flashheader -j .oad -j .mac -j .heap_location -j .rtc_clt -j .ro_ota_header  -j .ro_se_lnkKey -j .ro_se_cert  -j .ro_se_pvKey -j .ro_se_customData -j .rodata -j .data -j .text -j .overlay_location $(addprefix -j .,$(OVERLAYS)) -j .bss -j .heap -j .stack -S -O binary $< $@ 
endif
	
###############################################################################

clean:
	rm -f $(APPOBJS) $(APPDEPS) $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)*.bin $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)*.elf $(TARGET)_$(IPD_NODE_TYPE)_$(JENNIC_CHIP)*.map
	rm -f $(APP_SRC_DIR)/Plugins/os_gen.c $(APP_SRC_DIR)/Plugins/os_gen.h $(APP_SRC_DIR)/Plugins/os_irq*.S $(APP_SRC_DIR)/Plugins/pdum_gen.* $(APP_SRC_DIR)/Plugins/zps_gen*.*

###############################################################################
