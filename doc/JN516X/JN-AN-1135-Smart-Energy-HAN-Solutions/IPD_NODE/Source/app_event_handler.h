/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (IPD)
 *
 * COMPONENT:          app_event_handler.h
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Event Handler Header
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/LC_IPD_NODE/Source/app_display.h $
 *
 * $Revision: 8435 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-05 13:51:21 +0000 (Mon, 05 Dec 2011) $
 *
 * $Id: app_display.h 8435 2011-12-05 13:51:21Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_EVENT_HANDLER_H_
#define APP_EVENT_HANDLER_H_

#include "app_smartenergy_demo.h"
#include "os.h"
#include "zcl_options.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

typedef enum {
	E_INIT,
	E_SAMPLE_ADC,
#ifdef CLD_TIME
	E_TIME,
#endif
#ifdef CLD_MC
	E_MESSAGE_REQUEST,
	E_MESSAGE_RECEIVED,
#endif
	E_PRICE,
#ifdef CLD_SIMPLE_METERING
	E_METERING,
#endif
	E_LED_ON,
	E_LED_OFF,
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
	E_FAST_POLL,
#endif
	E_SLEEP
} teRunningStates;

typedef enum {
	LQI,
	BATTERY_VOLTAGE,
	TEMPERATURE
} APP_teDeviceLevels;

typedef struct {
	uint8 u8BattLevel;
	uint8 u8Temp;
	uint8 u8Lqi;
	uint8 u8CurrencyType;
	uint8 u8EnergyMode;
	bool bTempType;
	bool bTimeType;
}tsDeviceLevels;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void vMoveToNextDataReqState(void);
PUBLIC void vHandleRunningEvent(APP_tsEvent sAppEvent);
PUBLIC void vGetDeviceLevels(tsDeviceLevels *sParamDeviceLevels);
PUBLIC void vSetDeviceLevels(tsDeviceLevels sParamDeviceLevels, APP_teDeviceLevels APP_eDeviceLevels);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        In line  Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_EVENT_HANDLER_H_*/
