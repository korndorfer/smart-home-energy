/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (LC-IPD)
 *
 * COMPONENT:          app_zbp_utilities.h
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Development Functions Header
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/LC_IPD_NODE/Source/app_display.h $
 *
 * $Revision: 8435 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-05 13:51:21 +0000 (Mon, 05 Dec 2011) $
 *
 * $Id: app_display.h 8435 2011-12-05 13:51:21Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_ZBP_UTILITIES_H_
#define APP_ZBP_UTILITIES_H_


/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void vClearDiscNT(void);
PUBLIC void vRemoveCoordParents(void);
PUBLIC void vDisplayDiscNT(void);
PUBLIC bool bInRoutingTable(uint16 u16ShortAddress);
PUBLIC void vDisplayRoutingTable(void);
PUBLIC void vDisplayTableSizes(void);
PUBLIC void vDisplayAddressMapTable(void);
PUBLIC void vDisplayNT(void);
PUBLIC void vClearNTEntry( uint64 u64AddressToRemove );
PUBLIC void vClearRoutingTable(void);
PUBLIC void vDisplayRouteRecordTable(void);
PUBLIC void vClearRouteRecordTable(void);PUBLIC void vDisplayAPSTable(void);
PUBLIC void vDisplayNWKTransmitTable(void);

#ifdef TRACE_TIMERS
PUBLIC void vDebugTimers(void);
#endif

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        In line  Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_ZBP_UTILITIES_H_*/
