/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (EM)
 *
 * COMPONENT:          app_metrology.c
 *
 * AUTHOR:             Tom Mudryk
 *
 * DESCRIPTION:        Smart plug metrology
 *
 * $HeadURL:
 *
 * $Revision: 8227 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-11-08 14:41:21 +0000 (Tue, 08 Nov 2011) $
 *
 * $Id: app_metrology.c 8227 2011-11-08 14:41:21Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#if JENNIC_CHIP_FAMILY == JN514x
#include "PeripheralRegs_JN514x.h"
#else
#include "PeripheralRegs_JN516x.h"
#endif
#include "app_timer_driver.h"
#include "app_metrology.h"
#ifdef METROLOGY_ENABLED
#include "metrology.h"
#endif
#include "app_zcl_task.h"
#include "os_gen.h"
#include "dbg.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_METROLOGY
#define TRACE_METROLOGY	TRUE
#endif

#ifdef METROLOGY_ENABLED
#define METROLOGY_TIMER		E_AHI_TIMER_2
#endif

#define RELAY_ON_PIN	14
#define RELAY_OFF_PIN	15

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
#ifdef METROLOGY_ENABLED
PUBLIC	bool_t bMetrologyEngineActive;
#endif

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

#ifdef METROLOGY_ENABLED
extern tsSE_MeterDevice sMeter;
#endif

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_readMetrology
 *
 * DESCRIPTION:
 * Task to Display the Engine values in the foreground process
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK( APP_readMetrology )
{
#ifdef METROLOGY_ENABLED
	vGetMetrologyResult();
	OS_eContinueSWTimer(AppReadEngineTimer, APP_TIME_MS(350), NULL);
#endif
}


/****************************************************************************
 *
 * NAME: RelayTask
 *
 * DESCRIPTION:
 * Task to clear relay DIO states
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(RelayTask)
{
#ifdef METROLOGY_ENABLED
	DBG_vPrintf(TRACE_METROLOGY, "\r\nBoth Pins Off");
	vAHI_DioSetOutput(((1 << RELAY_ON_PIN) | (1 << RELAY_OFF_PIN)),0 );
#endif
}

/****************************************************************************/
/***		ISRs														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_isrTimer
 *
 * DESCRIPTION:
 * ISR for timer - this will start the first ADC conversion on channel 1,
 * subsequent conversions will be triggered by the ADC complete interrupt
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrTimer)
{
#ifdef METROLOGY_ENABLED
	// Clear down the Timer interrupt
	u8AHI_TimerFired( METROLOGY_TIMER );

	vIsrTimer();
#endif
}


/****************************************************************************
 *
 * NAME: APP_isrADC
 *
 * DESCRIPTION:
 *
 * Handles the ADC complete interrupt, triggers the next read if
 * required
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR( APP_isrADC )
{
	/* Clear down the ADC interrupt */
	uint32 u32Data = u32REG_AnaRead(REG_ANPER_IS);
	vREG_AnaWrite(REG_ANPER_IS, u32Data);

#ifdef METROLOGY_ENABLED
	vIsrAdc();
#endif
}

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
#ifdef METROLOGY_ENABLED
/****************************************************************************
 *
 * NAME: vConfigureMetrologyEngine
 *
 * DESCRIPTION:
 * Configure the metrology engine and start the sample timer
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PUBLIC void vConfigureMetrologyEngine(void)
{
	teEMmains eMainsHz = eMetrologyInit(EM_AUTO);
	vMetrologyStop();															// Stop the engine if it is already running
	tsMetrologyRanges sMetrologyRanges =
	{
		1007.600,	// V peak-peak in Volt               */
		2.645,		// I peak-peak channel1 in Ampere    */
		42.320,		// I peak-peak channel2 in Ampere    */
		-0.033,    	// phi correction in radian channel1 */
		-0.045		// phi correction in radian channel2 */
	};

	DBG_vPrintf(TRACE_METROLOGY, "Mains Frequency: %dHz\n", eMainsHz);
	vMetrologySetRanges(&sMetrologyRanges);
	//vMetrologyConfigAppAdc(E_APP_ADC_ID_4, vCbAppAdc);
	vMetrologyConfigAppAdc(E_APP_ADC_ID_NONE, NULL);
	vMetrologyStart();
	bMetrologyEngineActive = TRUE;

	OS_eStopSWTimer(AppReadEngineTimer);
	OS_eStartSWTimer(AppReadEngineTimer, APP_TIME_MS(350), NULL);
}


/****************************************************************************
*
* NAME: vGetMetrologyResult
*
* DESCRIPTION:
* Identifies when a new metrology result is available, calls the appropriate
* message handler to format the result into M-Bus packet, and send it to the
* Coordinator
*
* PARAMETERS:      Name            RW  Usage
* None
*
* RETURNS:
* Nothing
*
* NOTES:
*
****************************************************************************/
PUBLIC void vGetMetrologyResult(void)
{
	teEMreturn	eDataStatus;
	tsMetrologyResult sResult;

	eDataStatus = eMetrologyReadData(&sResult);

	DBG_vPrintf(TRACE_METROLOGY, "Instantaneous: %d\n", sResult.S);

	if (eDataStatus == EM_VALID)
    {
    	if (sResult.S < 0)
    		u32MeterDemand = 0;
    	else
    		u32MeterDemand = (uint32)sResult.P;								// The maximum accuracy of a 4 digit LCD is 0.001 kW, as the metrology engine
																			// outputs Watts we don't have to worry about losing any data during the cast

    }

    vLockZCLMutex();
    sMeter.sSimpleMeteringCluster.i24InstantaneousDemand = u32MeterDemand;
	sMeter.sSimpleMeteringCluster.u24Multiplier = 1;
	sMeter.sSimpleMeteringCluster.u24Divisor = 1000;
	vUnlockZCLMutex();
}


/****************************************************************************
*
* NAME: vInitRelay
*
* DESCRIPTION:
* Initialise the relay DIO
*
* PARAMETERS:      Name            RW  Usage
* None
*
* RETURNS:
* Nothing
*
* NOTES:
*
****************************************************************************/
PUBLIC void vInitRelay(void)
{
	DBG_vPrintf(TRACE_METROLOGY, "\r\n Init Relay");
	vAHI_DioSetDirection(0, ((1 << RELAY_ON_PIN) | (1 << RELAY_OFF_PIN)) );
	vAHI_DioSetOutput(((1 << RELAY_ON_PIN) | (1 << RELAY_OFF_PIN)),0 );
}


/****************************************************************************
*
* NAME: vToggleRelay
*
* DESCRIPTION:
* Switch the relay on/off
*
* PARAMETERS:      Name            RW  Usage
* None
*
* RETURNS:
* Nothing
*
* NOTES:
*
****************************************************************************/
PUBLIC void vToggleRelay( bool_t bOnOff )
{

	if(bOnOff)
	{
		DBG_vPrintf(TRACE_METROLOGY, "\r\nRelay Pin On");
		vAHI_DioSetOutput( 0, (1 << RELAY_ON_PIN));
	}
	else
	{
		DBG_vPrintf(TRACE_METROLOGY, "\r\nRelay Pin Off");
		vAHI_DioSetOutput( 0, (1 << RELAY_OFF_PIN));
	}

	OS_eStartSWTimer(APP_RelayTimer, APP_TIME_MS(30), NULL);
}
#endif


/****************************************************************************
 *
 * NAME:            vCbTimer
 *
 * DESCRIPTION:     Callback function to handle Timer interrupt.
 * 					Starts the first ADC conversion on channel 1,
 * 					subsequent conversions will be triggered by the ADC complete
 *  				interrupt
 *
 * PARAMETERS:      Name            RW  Usage
 *                  u32DeviceId     R   Interrupt source peripheral
 *                  u32ItemBitmap   R   Interrupt source within peripheral
 *
 * RETURNS:         Nothing
 *
 ****************************************************************************/
PUBLIC void vCbTimer(uint32 u32DeviceId, uint32 u32ItemBitmap)
{
#ifdef METROLOGY_ENABLED
	//vIsrTimer();
#endif
}


/****************************************************************************
 *
 * NAME:            vCbAdcDone
 *
 * DESCRIPTION:     Callback function to handle ADC complete interrupt.
 *                  Triggers the next read if required, starts user-defined callback
 *                  if registered.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  u32DeviceId     R   Interrupt source peripheral
 *                  u32ItemBitmap   R   Interrupt source within peripheral
 *
 * RETURNS:         Nothing
 *
 ****************************************************************************/
PUBLIC void vCbAdcDone(uint32 u32DeviceId, uint32 u32ItemBitmap)
{
#ifdef METROLOGY_ENABLED
	//vIsrAdc();
#endif
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
