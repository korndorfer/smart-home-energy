/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Common Code)
 *
 * COMPONENT:          app_buttons.c
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        Monitor the state of the buttons on the DK2 development
 *                     board, ensuring they are debounced.
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/Common/Source/app_buttons.c $
 *
 * $Revision: 9271 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-01 16:40:44 +0100 (Fri, 01 Jun 2012) $
 *
 * $Id: app_buttons.c 9271 2012-06-01 15:40:44Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "DBG.h"
#include "AppHardwareApi.h"
#include "app_smartenergy_demo.h"
#include "app_timer_driver.h"
#include "pwrm.h"
#include "app_zcl_task.h"
#include "app_buttons.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_BUTTON
#define TRACE_BUTTON           	FALSE
#endif



/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
#ifdef LC_DEVICE
PRIVATE uint8 s_u8ButtonDebounce[APP_BUTTONS_NUM] =
{ 0xff, 0xff, 0xff, 0xff, 0xff};
PRIVATE uint8 s_u8ButtonState[APP_BUTTONS_NUM] =
{ 0, 0, 0, 0, 0};
PRIVATE const uint8 s_u8ButtonDIOLine[APP_BUTTONS_NUM] =
{ APP_BUTTONS_BUTTON_1, APP_BUTTONS_BUTTON_2, APP_BUTTONS_BUTTON_3, APP_BUTTONS_BUTTON_4, APP_BUTTONS_BUTTON_RST };
#else
#if(JENNIC_PCB == DEVKIT2)
PRIVATE uint8 s_u8ButtonDebounce[APP_BUTTONS_NUM] =
{ 0xff, 0xff, 0xff};
PRIVATE uint8 s_u8ButtonState[APP_BUTTONS_NUM] =
{ 0, 0, 0 };
PRIVATE const uint8 s_u8ButtonDIOLine[APP_BUTTONS_NUM] =
{ APP_BUTTONS_BUTTON_1, APP_BUTTONS_BUTTON_2, APP_BUTTONS_BUTTON_3};
#else
PRIVATE uint8 s_u8ButtonDebounce[APP_BUTTONS_NUM] =
{ 0xff, 0xff, 0xff, 0xff };
PRIVATE uint8 s_u8ButtonState[APP_BUTTONS_NUM] =
{ 0, 0, 0, 0 };
PRIVATE const uint8 s_u8ButtonDIOLine[APP_BUTTONS_NUM] =
{ APP_BUTTONS_BUTTON_1, APP_BUTTONS_BUTTON_2, APP_BUTTONS_BUTTON_3, APP_BUTTONS_BUTTON_4 };
#endif
#endif


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vButtonsInitialise
 *
 * DESCRIPTION:
 * Initialises buttons
 *
 * RETURNS:
 * bool_t   Returns TRUE if any buttons were pressed
 *
 ****************************************************************************/
PUBLIC bool_t APP_bButtonInitialise(void)
{
    /* Set DIO lines to inputs with buttons connected */
    vAHI_DioSetDirection(APP_BUTTONS_DIO_MASK, 0);

    /* Turn on pull-ups for DIO lines with buttons connected */
    vAHI_DioSetPullup(APP_BUTTONS_DIO_MASK, 0);

    /* Set the edge detection for falling edges */
    vAHI_DioWakeEdge(0, APP_BUTTONS_DIO_MASK);

    /* Enable interrupts to occur on selected edge */
    vAHI_DioWakeEnable(APP_BUTTONS_DIO_MASK, 0);

    uint32 u32Buttons = u32AHI_DioReadInput() & APP_BUTTONS_DIO_MASK;
    if (u32Buttons != APP_BUTTONS_DIO_MASK)
    {
        return TRUE;
    }
    return FALSE;

}


OS_ISR(APP_ButtonsDIOChanged)
{
	uint32 u32IOStatus;
    DBG_vPrintf(TRACE_BUTTON,"Syscon\r\n");

    /* clear pending DIO changed bits by reading register */
    u32IOStatus = u32AHI_DioInterruptStatus();

    uint8 u8WakeInt = u8AHI_WakeTimerFiredStatus();

    if( u32IOStatus & APP_BUTTONS_DIO_MASK )
    {

		/* disable edge detection until scan complete */
		vAHI_DioWakeEnable(0, APP_BUTTONS_DIO_MASK);

		OS_eStartSWTimer(APP_ButtonsScanTimer, APP_TIME_MS(5), NULL);
    }


	if (u8WakeInt & E_AHI_WAKE_TIMER_MASK_1)
	{
		/* wake timer interrupt got us here */
		DBG_vPrintf(TRACE_BUTTON, "APP: Wake Timer 1 Interrupt\n");
		PWRM_vWakeInterruptCallback();
	}

}

OS_TASK(APP_ButtonsScanTask)
{
    /*
     * The DIO changed status register is reset here before the scan is performed.
     * This avoids a race condition between finishing a scan and re-enabling the
     * DIO to interrupt on a falling edge.
     */
    (void) u32AHI_DioWakeStatus();

    uint8 u8AllReleased = 0xff;
    unsigned int i;
    uint32 u32DIOState = u32AHI_DioReadInput() & APP_BUTTONS_DIO_MASK;
    DBG_vPrintf(TRACE_BUTTON, "DIO=%x\n", u32DIOState);

    for (i = 0; i < APP_BUTTONS_NUM; i++)
    {
        uint8 u8Button = (uint8) ((u32DIOState >> s_u8ButtonDIOLine[i]) & 1);

        s_u8ButtonDebounce[i] <<= 1;
        s_u8ButtonDebounce[i] |= u8Button;
        u8AllReleased &= s_u8ButtonDebounce[i];

        if (0 == s_u8ButtonDebounce[i] && !s_u8ButtonState[i])
        {
            s_u8ButtonState[i] = TRUE;

            /*
             * button consistently depressed for 8 scan periods
             * so post message to application task to indicate
             * a button down event
             */
            APP_tsEvent sButtonEvent;
            sButtonEvent.eType = APP_E_EVENT_BUTTON_DOWN;
            sButtonEvent.sButton.u8Button = i;

            DBG_vPrintf(TRACE_BUTTON, "Button DN=%d\n", i);

            OS_ePostMessage(APP_Events, &sButtonEvent);
        }
        else if (0xff == s_u8ButtonDebounce[i] && s_u8ButtonState[i] != FALSE)
        {
            s_u8ButtonState[i] = FALSE;

            /*
             * button consistently released for 8 scan periods
             * so post message to application task to indicate
             * a button up event
             */
            APP_tsEvent sButtonEvent;
            sButtonEvent.eType = APP_E_EVENT_BUTTON_UP;
            sButtonEvent.sButton.u8Button = i;

            DBG_vPrintf(TRACE_BUTTON, "Button UP=%i\n", i);

            OS_ePostMessage(APP_Events, &sButtonEvent);
        }
    }

    if (0xff == u8AllReleased)
    {
        /*
         * all buttons high so set dio to interrupt on change
         */
        DBG_vPrintf(TRACE_BUTTON, "ALL UP\n", i);
        vAHI_DioWakeEnable(APP_BUTTONS_DIO_MASK, 0);
        vClearExpiredFlag(APP_ButtonsScanTimer);
    }
    else
    {
        /*
         * one or more buttons is still depressed so continue scanning
         */
        OS_eContinueSWTimer(APP_ButtonsScanTimer, APP_TIME_MS(10), NULL);
    }
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
