/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (LC-IPD)
 *
 * COMPONENT:          app_adc.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        ADC monitoring
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/LC_IPD_NODE/Source/app_adc.c $
 *
 * $Revision: 8491 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-15 14:32:54 +0000 (Thu, 15 Dec 2011) $
 *
 * $Id: app_adc.c 8491 2011-12-15 14:32:54Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#include "AppHardwareApi.h"
#include "app_event_handler.h"
#include "app_adc.h"
#include "dbg.h"


/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_ADC
#define TRACE_ADC FALSE
#endif

#if JENNIC_CHIP_FAMILY == JN516x
	#define ADC_MAX 1024
#else
	#define ADC_MAX 4096
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE uint8 vReadBattLevel(void);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
*
* NAME: APP_vAdcInitialise
*
* DESCRIPTION:
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PUBLIC void APP_vAdcInitialise(void)
{
	vAHI_ApConfigure(	E_AHI_AP_REGULATOR_ENABLE,								// Initialise the ADC peripheral
						E_AHI_AP_INT_DISABLE,
						E_AHI_AP_SAMPLE_2,
						E_AHI_AP_CLOCKDIV_500KHZ,
						E_AHI_AP_INTREF);

	while (!bAHI_APRegulatorEnabled());											// Wait for ADC to power up
	DBG_vPrintf(TRACE_ADC, "ADC Initialised\n");
}


/****************************************************************************
*
* NAME: APP_vAdcSample
*
* DESCRIPTION:
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PUBLIC void APP_vAdcSample(void)
{
	tsDeviceLevels sDeviceLevels;
	vGetDeviceLevels(&sDeviceLevels);											// Take a local copy of the device data structure

	/* Configure the ADC for battery voltage measurement and begin sampling */
	DBG_vPrintf(TRACE_ADC, "Start ADC Batt Sample\n");

	vAHI_AdcEnable(	E_AHI_ADC_SINGLE_SHOT,
					E_AHI_AP_INPUT_RANGE_2,
					E_AHI_ADC_SRC_VOLT);

	uint8 i;

	for (i = 0 ; i < 5 ; i++)
	{
		vAHI_AdcStartSample();
		while(bAHI_AdcPoll());													// Poll for read completion
	}
	sDeviceLevels.u8BattLevel = vReadBattLevel();								// Update the battery value

	DBG_vPrintf(TRACE_ADC, "Read ADC - Battery Level: %d\n", sDeviceLevels.u8BattLevel);
	vSetDeviceLevels(sDeviceLevels, BATTERY_VOLTAGE);							// Store the updated information
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
*
* NAME: vReadBattLevel
*
* DESCRIPTION: Function to read the battery level from the ADC and convert this
* 			   into a usable number.
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE uint8 vReadBattLevel(void)
{
	uint8 Lev;
    uint16 u16AdcReading;
    uint32 u32BattLevelmV;

	u16AdcReading = u16AHI_AdcRead();

	DBG_vPrintf(TRACE_ADC, "u16AHI_AdcRead() bat: %d", u16AdcReading );

	u32BattLevelmV = (((uint32)(u16AdcReading)) * 3 * 2400) / (2 * ADC_MAX);		// ADC Reading * Potential Divider Denominator (3) * Reference Voltage (2400mV) / Potential Divider Numerator (2) * 12bit ADC (2^12)

	if (u32BattLevelmV > 2900)
	{
		Lev = 6;
	}
	else if (u32BattLevelmV > 2822)
	{
		Lev = 5;
	}
	else if (u32BattLevelmV > 2744)
	{
		Lev = 4;
	}
	else if (u32BattLevelmV > 2688)
	{
		Lev = 3;
	}
	else if (u32BattLevelmV > 2510)
	{
		Lev = 2;
	}
	else if (u32BattLevelmV > 2432)
	{
		Lev = 1;
	}
	else
	{
		Lev = 0;
	}

	return Lev;
}


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
