/****************************************************************************
 *
 * MODULE              JN-AN-1166 Smart Lamp Test Application
 *
 * DESCRIPTION         Remote control software to control bulbs
 *
 ****************************************************************************
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142, JN5139].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 ****************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <jendefs.h>
#include <AppHardwareApi.h>
#include <AppQueueApi.h>
#include <mac_sap.h>
#include <mac_pib.h>
#include <string.h>

#include "dbg.h"
#include "dbg_uart.h"
#include "LcdDriver.h"
#include "LcdExtras.h"
#include "Button.h"
#include "config.h"
#include "xsprintf.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define SYSCON_WK_ET 0x02000008
#define SYSCON_WK_ES 0x02000010

#define GPIO_DIN_REG 0x02002008UL

#define MENU_COUNT 5
#define KEY_COUNT 4
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
    E_STATE_IDLE,
    E_STATE_ACTIVE_SCANNING,
    E_STATE_ASSOCIATING,
    E_STATE_ASSOCIATED
}teState;

typedef struct
{
    teState eState;
    uint8   u8Channel;
    uint8   u8TxPacketSeqNb;
    uint8   u8RxPacketSeqNb;
    uint16  u16Address;
    uint8 au8RxParams[4];
}tsEndDeviceData;

typedef enum
{
	E_SW1   = E_AHI_DIO11_INT,
	E_SW2   = E_AHI_DIO12_INT,
	E_SW3   = E_AHI_DIO17_INT,
	E_SW4   = E_AHI_DIO1_INT,
	E_SW5   = E_AHI_DIO8_INT,
	E_SWITCH_STOP_MONO = E_SW3 | E_SW4,
	E_SWITCH_STOP_TW   = E_SW1 | E_SW2 | E_SW3 | E_SW4,
	E_SWITCH_ALL       = E_SW1 | E_SW2 | E_SW3 | E_SW4 | E_SW5
}teDK4Switches;

typedef enum
{
	E_MODE_MONOCHROME,
	E_MODE_TUNABLE_WHITE,
	E_MODE_TUNABALE_WHITE_ALGORITHM,
	E_MODE_RGB_UP,
	E_MODE_RGB_DN
}teAppMode;

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vInitSystem(void);
PRIVATE void vProcessEventQueues(void);
PRIVATE void vProcessIncomingMlme(MAC_MlmeDcfmInd_s *psMlmeInd);
PRIVATE void vProcessIncomingMcps(MAC_McpsDcfmInd_s *psMcpsInd);
PRIVATE void vProcessIncomingHwEvent(AppQApiHwInd_s *psAHI_Ind);

PRIVATE void vHandleActiveScanResponse(MAC_MlmeDcfmInd_s *psMlmeInd);
PRIVATE void vHandleAssociateResponse(MAC_MlmeDcfmInd_s *psMlmeInd);
PRIVATE void vHandleMcpsDataInd(MAC_McpsDcfmInd_s *psMcpsInd);
PRIVATE void vHandleMcpsDataDcfm(MAC_McpsDcfmInd_s *psMcpsInd);
PRIVATE void vTransmitDataPacket(uint8 u8Cmd);

/* Helper functions */
PRIVATE void vUpdateSw1Sw2Legend(teAppMode eAppMode);
PRIVATE void vLcdRgbHelper(void);

/* Interrupt handlers */
PRIVATE void vSystemControllerISR(uint32 u32DeviceId, uint32 u32ItemBitmap);
PRIVATE void vTimer0ISR(uint32 u32DeviceId, uint32 u32ItemBitmap);

/*key processing */
PRIVATE void vProcessKeys(volatile uint32 *pu32KeyId);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
/* Handles from the MAC */
PRIVATE void *s_pvMac;
PRIVATE MAC_Pib_s *s_psMacPib;
PRIVATE tsEndDeviceData sEndDeviceData = {.au8RxParams = {255,255,255,255}};

PRIVATE volatile uint32 u32ButtonPressedMask;
PRIVATE volatile uint32 u32ButtonEvent = -1;
PRIVATE volatile uint32 u32TickCount;

PRIVATE teOnAirCommands aeCommandMatrix[MENU_COUNT][KEY_COUNT] = { {E_CMD_ON,E_CMD_OFF,E_CMD_LEVEL_UP,E_CMD_LEVEL_DOWN},
		                                                           {E_CMD_BALANCE_LEFT,E_CMD_BALANCE_RIGHT,E_CMD_LEVEL_UP,E_CMD_LEVEL_DOWN},
		                                                           {E_CMD_CCT_COOLER,E_CMD_CCT_WARMER,E_CMD_LEVEL_UP,E_CMD_LEVEL_DOWN},
		                                                           {E_CMD_RED_DOWN,E_CMD_GREEN_DOWN,E_CMD_BLUE_DOWN,E_CMD_LEVEL_DOWN},
                                                                   {E_CMD_RED_UP,E_CMD_GREEN_UP,E_CMD_BLUE_UP,E_CMD_LEVEL_UP}};


PRIVATE teAppMode geColourMenuActive= E_MODE_MONOCHROME;
/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
/****************************************************************************
 *
 * NAME: AppColdStart
 *
 * DESCRIPTION:
 * Entry point for application from boot loader. Initialises system and runs
 * main loop.
 *
 * RETURNS:
 * Never returns.
 *
 ****************************************************************************/
PUBLIC void AppColdStart(void)
{

	/* Disable watchdog if enabled by default */
	#ifdef WATCHDOG_ENABLED
	vAHI_WatchdogStop();
	#endif

	 while (TRUE == bAHI_GetClkSource());
	 vAHI_OptimiseWaitStates();

	vInitSystem();

	while (1)
	{
		vAHI_CpuDoze();
		vProcessEventQueues();
		vProcessKeys(&u32ButtonEvent);
	}
}

/****************************************************************************
 *
 * NAME: AppWarmStart
 *
 * DESCRIPTION:
 * Entry point for application from boot loader. Simply jumps to AppColdStart
 * as, in this instance, application will never warm start.
 *
 * RETURNS:
 * Never returns.
 *
 ****************************************************************************/
PUBLIC void AppWarmStart(void)
{
    AppColdStart();
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vInitSystem
 *
 * DESCRIPTION:
 * Initialises the HW, MAC and Queue modules
 *
 ****************************************************************************/
PRIVATE void vInitSystem(void)
{
    /* Setup interface to MAC */
    (void)u32AHI_Init();
    (void)u32AppQApiInit(NULL, NULL, NULL);
    /*override HW ebevent queue */
    vAHI_SysCtrlRegisterCallback(vSystemControllerISR);

    /* Set up a timer for switch debouncing */
  	vAHI_TimerEnable(E_AHI_TIMER_0,4,FALSE, TRUE, FALSE);
  	vAHI_TimerConfigure(E_AHI_TIMER_0, TRUE, TRUE);
  	vAHI_TimerClockSelect(E_AHI_TIMER_0, FALSE, TRUE);

  	vAHI_Timer0RegisterCallback(vTimer0ISR);

  	DBG_vUartInit(DBG_E_UART_0, DBG_E_UART_BAUD_RATE_115200);


    /* Initialise end device state */
    sEndDeviceData.eState = E_STATE_IDLE;
    sEndDeviceData.u8TxPacketSeqNb = 0;
    sEndDeviceData.u8RxPacketSeqNb = 0;



    /* Set up the MAC handles. Must be called AFTER u32AppQApiInit() */
    s_pvMac = pvAppApiGetMacHandle();
    s_psMacPib = MAC_psPibGetHandle(s_pvMac);

    /* Set Pan ID in PIB (also sets match register in hardware) */
    MAC_vPibSetPanId(s_pvMac, PAN_ID);

    /* Set the channel to use only one */
    sEndDeviceData.u8Channel = CHANNEL;
    (void)eAppApiPlmeSet(PHY_PIB_ATTR_CURRENT_CHANNEL, sEndDeviceData.u8Channel);

    /* Enable receiver to be on when idle */
    MAC_vPibSetRxOnWhenIdle(s_pvMac, TRUE, FALSE);

    /* Set the first interrupt type to be falling edge */
    vAHI_DioWakeEdge(0,E_SWITCH_ALL);
    vAHI_DioWakeEnable(E_SWITCH_ALL,0);

    /* Display setup and button functions */
   	vLcdResetDefault();
   	vLcdWriteText("IEEE 802 15 4 Remote", 0, 10);
   	vLcdWriteText("Temperature ", 2, 10);
   	vLcdWriteText("Level ", 3, 10);
   	vLcdWriteText("Balance ", 4, 10);


   	vLcdWriteText("--[C",2,80);
   	vLcdWriteText("100.0%",3,80);
   	vLcdWriteText("50.0%",4,80);

   	vLcdWriteText(" State  ",6,4);
   	vLcdWriteText("On", 7, 0);
   	vLcdWriteText("Off", 7, 35);
   	vLcdWriteText("Level ",6,84);
   	vLcdWriteText("Up", 7, 75);
   	vLcdWriteText("Down", 7, 100);
   	vLcdRefreshAll();

}
/****************************************************************************
 *
 * NAME: vProcessEventQueues
 *
 * DESCRIPTION:
 * Check each of the three event queues and process and items found.
 *
 ****************************************************************************/
PRIVATE void vProcessEventQueues(void)
{
    MAC_MlmeDcfmInd_s *psMlmeInd;
	MAC_McpsDcfmInd_s *psMcpsInd;


    /* Check for anything on the MCPS upward queue */
    do
    {
        psMcpsInd = psAppQApiReadMcpsInd();

        if (psMcpsInd != NULL)
        {
            vProcessIncomingMcps(psMcpsInd);
            vAppQApiReturnMcpsIndBuffer(psMcpsInd);
        }
    } while (psMcpsInd != NULL);

    /* Check for anything on the MLME upward queue */
    do
    {
        psMlmeInd = psAppQApiReadMlmeInd();
        if (psMlmeInd != NULL)
        {
            vProcessIncomingMlme(psMlmeInd);
            vAppQApiReturnMlmeIndBuffer(psMlmeInd);
        }
    } while (psMlmeInd != NULL);

#if 0
    /* Check for anything on the AHI upward queue */
    do
    {
        psAHI_Ind = psAppQApiReadHwInd();
        if (psAHI_Ind != NULL)
        {
            vProcessIncomingHwEvent(psAHI_Ind);
            vAppQApiReturnHwIndBuffer(psAHI_Ind);
        }
    } while (psAHI_Ind != NULL);
#endif
}

/****************************************************************************
 *
 * NAME: vProcessIncomingHwEvent
 *
 * DESCRIPTION:
 * Process any hardware events.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psAHI_Ind       R   pointer to hw event buffer entry
 *
 ****************************************************************************/

PRIVATE void vProcessIncomingHwEvent(AppQApiHwInd_s *psAHI_Ind)
{
	/* handled by system controller ISR below queue is bypassed */
}

PRIVATE void vSystemControllerISR(uint32 u32DeviceId, uint32 u32ItemBitmap)
{
	if (  (u32ItemBitmap & E_SWITCH_ALL) !=0 ) /*check for valid button press/release */
	{
		/* loop back the DIO onto edge type (to track rising /falling edges) */
		*(uint32 *)SYSCON_WK_ET = (*(uint32 *)GPIO_DIN_REG) & E_SWITCH_ALL;
		u32ButtonPressedMask = u32ItemBitmap;     /* note the DIO that caused the event */
		u32TickCount = 16;                        /* start the debounce timer (5ms ticks) */
		vAHI_DioInterruptEnable(0,E_SWITCH_ALL);
		vAHI_TimerStartRepeat(E_AHI_TIMER_0,5000,5000);
	}
}

PRIVATE void vTimer0ISR(uint32 u32DeviceId, uint32 u32ItemBitmap)
{
	static uint32 u32ShiftReg = 0;
     if (u32TickCount)
     {
    	 u32TickCount--;
    	 u32ShiftReg = (u32ShiftReg << 1) & 0xff;
    	 if (u32AHI_DioReadInput() & u32ButtonPressedMask) /* shuffle in 0 or 1 depending on state */
    	 {
    		 u32ShiftReg |= 0x1;
    	 }

    	 /* all ones or all 0's so generate the event adding bit31 if all 1's = a release */
    	 if ((u32ShiftReg == 0xff) ||(u32ShiftReg == 0))  /* released (MSB set)*/
		 {
			 u32AHI_DioWakeStatus();
			 u32ButtonEvent = u32ButtonPressedMask | (u32ShiftReg <<31);
			 u32TickCount = 0;
		 }
     }
     else
     {
    	 u32AHI_DioWakeStatus();
    	 vAHI_DioInterruptEnable(E_SWITCH_ALL,0); /* re-enable switch interrupts */
     }
}

PRIVATE void vProcessKeys(volatile uint32 *pu32KeyId)
{
	static teAppMode eAppMode = E_MODE_MONOCHROME;

	switch(*pu32KeyId)
	{
		case E_SW1: vTransmitDataPacket(aeCommandMatrix[eAppMode][0]); break;
		case E_SW2: vTransmitDataPacket(aeCommandMatrix[eAppMode][1]); break;
		case E_SW3: vTransmitDataPacket(aeCommandMatrix[eAppMode][2]); break;
		case E_SW4: vTransmitDataPacket(aeCommandMatrix[eAppMode][3]); break;

		case E_SW5:
			eAppMode = (eAppMode>3) ? 3 : eAppMode + 1;
			vUpdateSw1Sw2Legend(eAppMode);
			geColourMenuActive  = (eAppMode >E_MODE_TUNABALE_WHITE_ALGORITHM) ? eAppMode : 0;
			break;

		/* Get here if we pick up key releases (MSB set) so strip it off to use key ID */
		default:
			switch (*pu32KeyId & 0x7fffffff)
			{
			    /* all  modes on Sw3/4 generate a stop command */
				case E_SW3:
				case E_SW4:
					vTransmitDataPacket(E_CMD_STOP);
					break;
				/* 	non-monochrome modes (TW CCT or TW balance ) on SW1/Sw2 generate stop commands */
				case E_SW1:
				case E_SW2:
					if (eAppMode != E_MODE_MONOCHROME)
					{
						vTransmitDataPacket(E_CMD_STOP);
					}
					break;
				default:
					break;
			}
	}

	if (*pu32KeyId != -1)/* Consume a valid event */
	{
		*pu32KeyId = -1;
	}
}
/****************************************************************************
 *
 * NAME: vProcessIncomingMlme
 *
 * DESCRIPTION:
 * Process any incoming management events from the stack.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psMlmeInd       R   MAC indication to App.
 *
 ****************************************************************************/
PRIVATE void vProcessIncomingMlme(MAC_MlmeDcfmInd_s *psMlmeInd)
{
    /* We respond to several MLME indications and confirmations, depending
       on mode */
    switch (psMlmeInd->u8Type)
    {
    /* Deferred confirmation that the scan is complete */
    case MAC_MLME_DCFM_SCAN:
        if (sEndDeviceData.eState == E_STATE_ACTIVE_SCANNING)
        {
            vHandleActiveScanResponse(psMlmeInd);
        }
        break;

    /* Deferred confirmation that the association process is complete */
    case MAC_MLME_DCFM_ASSOCIATE:
        /* Only respond to this if associating */
        if (sEndDeviceData.eState == E_STATE_ASSOCIATING)
        {
            vHandleAssociateResponse(psMlmeInd);
        }
        break;

    default:
        break;
    }
}

/****************************************************************************
 *
 * NAME: vProcessIncomingData
 *
 * DESCRIPTION:
 * Process incoming data events from the stack.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psMcpsInd
 *
 ****************************************************************************/
PRIVATE void vProcessIncomingMcps(MAC_McpsDcfmInd_s *psMcpsInd)
{
    /* Only handle incoming data events one device has been started as a
       coordinator */
        switch(psMcpsInd->u8Type)
        {
        case MAC_MCPS_IND_DATA:  /* Incoming data frame */
            vHandleMcpsDataInd(psMcpsInd);
            break;
        case MAC_MCPS_DCFM_DATA: /* Incoming acknowledgement or ack timeout */
            vHandleMcpsDataDcfm(psMcpsInd);
            break;
        default:
            break;
        }
}

/****************************************************************************
 *
 * NAME: vHandleMcpsDataDcfm
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 * NOTES:
 ****************************************************************************/
PRIVATE void vHandleMcpsDataDcfm(MAC_McpsDcfmInd_s *psMcpsInd)
{
    if (psMcpsInd->uParam.sDcfmData.u8Status == MAC_ENUM_SUCCESS)
    {
        /* Data frame transmission successful */
    }
    else
    {
        /* Data transmission falied after 3 retries at MAC layer. */
    }
}

/****************************************************************************
 *
 * NAME: vHandleMcpsDataInd
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 * NOTES:
 ****************************************************************************/
PRIVATE void vHandleMcpsDataInd(MAC_McpsDcfmInd_s *psMcpsInd)
{
    MAC_RxFrameData_s *psFrame;
    char acLcdString[16];


    psFrame = &psMcpsInd->uParam.sIndData.sFrame;
    uint8 u8RxFromBulb = psFrame->au8Sdu[0];


    DBG_vPrintf(TRUE,"\n CMD RX = %s", apcDebugStrings[u8RxFromBulb]);

    /* Handle the two byte packets temperature */
	if ((psFrame->u8SduLength == 2) && (psFrame->au8Sdu[0] == E_STATUS_TEMPERATURE))
	{

		xsprintf(acLcdString,"%d[C  ",psFrame->au8Sdu[1]);
		vLcdWriteText(acLcdString, 2, 80);

	}
	/* Did we get a colour temperature updated response (3byte packet) */
	if ((psFrame->u8SduLength == 3) && (psFrame->au8Sdu[0]== E_STATUS_CCT))
	{
		uint32 u32ColourTemperature = psFrame->au8Sdu[1] + 256*(psFrame->au8Sdu[2]);

		xsprintf(acLcdString,"%dK  ",u32ColourTemperature);
		vLcdWriteText(acLcdString, 4, 80);
	}

	/* Did we get a level  updated response (3byte packet) */
	if ((psFrame->u8SduLength == 3) && ((psFrame->au8Sdu[0]== E_STATUS_LEVEL) ||(psFrame->au8Sdu[0]== E_STATUS_BALANCE)) )
	{
		uint32 u32PerThou = psFrame->au8Sdu[1] + 256*(psFrame->au8Sdu[2]);
		uint8 u8PerCent = u32PerThou/10;
		uint8 u8PerCentTenths = u32PerThou %10;

		xsprintf(acLcdString,"%d`%d%s  ",u8PerCent,u8PerCentTenths,"%");

		if (psFrame->au8Sdu[0]== E_STATUS_LEVEL)
		{
			DBG_vPrintf(TRUE,"\n RX Level = %d",u32PerThou);
			vLcdWriteText(acLcdString, 3, 80);
	    }

	    if (psFrame->au8Sdu[0]== E_STATUS_BALANCE)
		{
	    	DBG_vPrintf(TRUE,"\n RX Bal = %d",u32PerThou);
			vLcdWriteText(acLcdString, 4, 80);
	    }
	}

	if ((psFrame->u8SduLength == 3) && ((psFrame->au8Sdu[0]== E_STATUS_CCT)))
	{
		uint16 u16ColourTemperature = psFrame->au8Sdu[1] + 256*(psFrame->au8Sdu[2]);
		xsprintf(acLcdString,"%dK  ",u16ColourTemperature);
		vLcdWriteText(acLcdString, 4, 80);
	}

	if (geColourMenuActive > E_MODE_TUNABALE_WHITE_ALGORITHM)
	{
		switch (psFrame->au8Sdu[0])
		{
			case E_STATUS_RED    : sEndDeviceData.au8RxParams[0] = psFrame->au8Sdu[1] ; vLcdRgbHelper(); break;
			case E_STATUS_GREEN  : sEndDeviceData.au8RxParams[1] = psFrame->au8Sdu[1] ; vLcdRgbHelper(); break;
			case E_STATUS_BLUE   : sEndDeviceData.au8RxParams[2] = psFrame->au8Sdu[1] ; vLcdRgbHelper(); break;
			case E_STATUS_LEVEL  :
			{
				uint32 u32Level = psFrame->au8Sdu[1] + 256*(psFrame->au8Sdu[2]);
				sEndDeviceData.au8RxParams[3] = (uint8)(u32Level);
				vLcdRgbHelper();
				break;
			}
			default : break;
		}
	}

	vLcdRefreshAll();/* Apply any LCD display changes resulting from received information */
}

/****************************************************************************
 *
 * NAME: vHandleAssociateResponse
 *
 * DESCRIPTION:
 * Handle the response generated by the stack as a result of the associate
 * start request.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psMlmeInd       R   Pointer to Management entity structure
 *
 ****************************************************************************/
PRIVATE void vHandleAssociateResponse(MAC_MlmeDcfmInd_s *psMlmeInd)
{
    /* If successfully associated with network coordinator */
    if (psMlmeInd->uParam.sDcfmAssociate.u8Status == MAC_ENUM_SUCCESS)
    {
        sEndDeviceData.u16Address = psMlmeInd->uParam.sDcfmAssociate.u16AssocShortAddr;
        sEndDeviceData.eState = E_STATE_ASSOCIATED;
    }
}

/****************************************************************************
 *
 * NAME: vHandleActiveScanResponse
 *
 * DESCRIPTION:
 * Handle the response generated by the stack as a result of the network scan.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psMlmeInd
 *
 ****************************************************************************/
PRIVATE void vHandleActiveScanResponse(MAC_MlmeDcfmInd_s *psMlmeInd)
{
    /* Should not get here */
}


 /****************************************************************************
 *
 * NAME: vTransmitDataPacket
 *
 * DESCRIPTION:
 * Assembles data structure for a MAC broadcast frame and requests a transmit
 *
 * PARAMETERS:      Name            RW  Usage
 *                  pu8Data         R   Pointer to data to Send
 *                  u8Len           R   Length of data
 *
 ****************************************************************************/
PRIVATE void vTransmitDataPacket(uint8 u8Cmd)
{
    MAC_McpsReqRsp_s  sMcpsReqRsp;
    MAC_McpsSyncCfm_s sMcpsSyncCfm;
    uint8 *pu8Payload;

    /* Create frame transmission request */
    sMcpsReqRsp.u8Type = MAC_MCPS_REQ_DATA;
    sMcpsReqRsp.u8ParamLength = sizeof(MAC_McpsReqData_s);
    /* Set handle so we can match confirmation to request */
    sMcpsReqRsp.uParam.sReqData.u8Handle = 1;
    /* Use short address for source */
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u8AddrMode = 3;
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u16PanId        = PAN_ID;
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.sExt.u32H = *( (uint32 *)pvAppApiGetMacAddrLocation());
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.sExt.u32L = *(((uint32 *)pvAppApiGetMacAddrLocation()) + 1);
    /* Use short address for destination */
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u8AddrMode = 2;
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u16PanId = PAN_ID;
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.u16Short = BROADCAST_ADR;
    /* Frame requires ack but not security, indirect transmit or GTS */
    sMcpsReqRsp.uParam.sReqData.sFrame.u8TxOptions = MAC_TX_OPTION_INDIRECT;

    pu8Payload = sMcpsReqRsp.uParam.sReqData.sFrame.au8Sdu;


    pu8Payload[0] = u8Cmd;

    DBG_vPrintf(TRUE,"\nTX=%s", apcDebugStrings[u8Cmd]);

    /* Set frame length */
    sMcpsReqRsp.uParam.sReqData.sFrame.u8SduLength = 1;

    /* Request transmit */
    vAppApiMcpsRequest(&sMcpsReqRsp, &sMcpsSyncCfm);
}



PRIVATE void vUpdateSw1Sw2Legend(teAppMode eAppMode)
{
	switch (eAppMode)
	{
	    case E_MODE_RGB_UP:
	    	vLcdRgbHelper();
	    	vLcdClearLine(7);
	    	vLcdWriteText("R]", 7, 0);
	    	vLcdWriteText("G]", 7, 40);
	    	vLcdWriteText("B]", 7, 78);
	    	vLcdWriteText("L]", 7, 114);
	    	break;

	    case E_MODE_RGB_DN:
	    	vLcdRgbHelper();
	    	vLcdClearLine(7);
	    	vLcdWriteText("R\\", 7, 0);
			vLcdWriteText("G\\", 7, 40);
			vLcdWriteText("B\\", 7, 78);
			vLcdWriteText("L\\", 7, 114);
	    	break;

		case E_MODE_TUNABALE_WHITE_ALGORITHM:
			vLcdWriteText("Col Temp",4,10);
			vLcdWriteText("4500K ", 4, 80);
			vLcdWriteText("Col Temp",6,3);
			vLcdWriteText(" ] ", 7, 0);
			vLcdWriteText(" \\  ", 7, 35);
			break;
		case E_MODE_TUNABLE_WHITE:
			vLcdWriteText("Balance ", 4, 10);
			vLcdWriteText("Balance",6,4);
			vLcdWriteText(" L ", 7, 0);
			vLcdWriteText(" R  ", 7, 35);
			break;
		default:
			vLcdWriteText(" State  ",6,4);
			vLcdWriteText("On", 7, 0);
			vLcdWriteText("Off", 7, 35);
			break;

	}
	vLcdRefreshAll();
}

PRIVATE void vLcdRgbHelper(void)
{
	char acLcdString[32];
	vLcdClearLine(3);
	vLcdClearLine(4);

	xsprintf(acLcdString,"R:%d G:%d B:%d L:%d",sEndDeviceData.au8RxParams[0],
			                                   sEndDeviceData.au8RxParams[1],
			                                   sEndDeviceData.au8RxParams[2],
			                                   sEndDeviceData.au8RxParams[3]);
	vLcdWriteText(acLcdString, 4, 0);
	vLcdClearLine(6);
	vLcdWriteText("PWM Channels",6,30);

}

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
