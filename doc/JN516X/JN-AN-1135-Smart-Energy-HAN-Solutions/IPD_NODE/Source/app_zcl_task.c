/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (IPD)
 *
 * COMPONENT:          app_zcl_task.c
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        ZCL Handler Functions
 *
 * $HeadURL $
 *
 * $Revision: 9281 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-08 15:13:02 +0100 (Fri, 08 Jun 2012) $
 *
 * $Id: app_zcl_task.c 9281 2012-06-08 14:13:02Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pdm.h"
#include "pwrm.h"
#include "dbg.h"
#include "string.h"

/* Application Includes */
#include "app_timer_driver.h"
#include "zcl.h"
#include "app_zcl_task.h"
#include "app_ipd_node.h"
#include "zcl_options.h"
#include "app_event_handler.h"
#include "app_sleep_functions.h"
#include "app_smartenergy_demo.h"
#include "app_zbp_utilities.h"
#include "app_certificates.h"
#include "app_display.h"
#include "app_led.h"
#include "ipd.h"

#ifdef STACK_MEASURE
	#include "StackMeasure.h"
	tsStackInfo StackInfo;
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_ZCL_TASK
#define TRACE_ZCL_TASK	FALSE
#endif

#ifndef TRACE_ZCL_TASK_HIGH
#define TRACE_ZCL_TASK_HIGH	TRUE
#endif

#ifndef TRACE_ZCL_TASK_VERBOSE
#define TRACE_ZCL_TASK_VERBOSE FALSE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void cbZCL_GeneralCallback(tsZCL_CallBackEvent *psEvent);
PRIVATE void cbZCL_EndpointCallback(tsZCL_CallBackEvent *psEvent);

#ifdef CLD_KEY_ESTABLISHMENT
PRIVATE void vHandleKeyEstablishmentEvent(void *pvParam);
#endif

#ifdef CLD_DRLC
PRIVATE void vHandleDrlcEvent(void *pvParam);
#endif

#ifdef CLD_PRICE
PRIVATE void vHandlePriceEvent(tsSE_PriceCallBackMessage *psPriceMessage);
PRIVATE void vDisplayPrices(void);
#endif

#ifdef CLD_MC
PRIVATE void vHandleMessageEvent(void *pvParam);
#endif

#ifdef CLD_OTA
PRIVATE void vHandleOtaEvent(void *pvParam);
#endif

PRIVATE void vHandleMeteringEvent(void *pvParam);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC	tsSE_IPDDevice	asSE_IPDDevice[SE_NUMBER_OF_ENDPOINTS];

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE uint8	u8TransactionSequenceNumber = 0;

PRIVATE uint32	u32ZCLMutexCount = 0;

#ifdef APPLICATION_REJOIN_ENABLED
PRIVATE uint8	u8MissedApsAcks = 0;
#endif
#ifdef OTA_CLD_ATTR_REQUEST_DELAY
uint32 u32MsTimer;
bool_t bStopTimer = TRUE;
#endif
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern uint8 			au8CAPublicKey[];
extern uint8 			au8Certificate[];
extern uint8 			au8PrivateKey[];

extern tsDevice 		s_sDevice;
extern teRunningStates	eRunningState;
extern uint32			u32LastTimeUpdate;
extern uint64			u64CoordinatorMac;
extern uint8			u8MeterOfInterest;
extern uint8			u8CurrentMeterOfInterest;
extern uint8 			u8NumberOfSimpleMeteringRequests;
extern bool_t			bLostComms;
extern uint8 			u8CurrentElectricityCostTier;
extern uint8			u8CurrentElectricityNumberCostTiers;
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
extern uint8        	u8AppliedUpdatePeriod;
extern uint32       	u32FastPollEndTime;
#endif

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_ZCLTask
 *
 * DESCRIPTION:
 * Task to handle to ZCL end point(1) events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_ZCLTask)
{
	tsZCL_CallBackEvent sCallBackEvent;
	ZPS_tsAfEvent sStackEvent;
	static uint8 u8Seconds = 0;

	u8Seconds++;

	/* Clear the ZigBee stack event */
	sStackEvent.eType = ZPS_EVENT_NONE;

	/* Point the ZCL event at the defined ZigBee stack event */
	sCallBackEvent.pZPSevent = &sStackEvent;

	/* 1 second tick, pass it to the ZCL */
	if(OS_eGetSWTimerStatus(APP_ZclTimer) == OS_E_SWTIMER_EXPIRED)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "ZCL Task activated by timer\n");
		sCallBackEvent.eEventType = E_ZCL_CBET_TIMER;
		vZCL_EventHandler(&sCallBackEvent);
		if(!bGetAppSleep())
		{
			OS_eContinueSWTimer(APP_ZclTimer, ONE_SECOND_TICK_TIME, NULL);
		}
	}
#ifdef OTA_CLD_ATTR_REQUEST_DELAY
	/* mili second tick, pass it to the ZCL */
	if(OS_eGetSWTimerStatus(APP_MsTimer) == OS_E_SWTIMER_EXPIRED)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "ZCL Task activated by  mili second timer\n");
		sCallBackEvent.eEventType = E_ZCL_CBET_TIMER_MS;

		vZCL_EventHandler(&sCallBackEvent);

		DBG_vPrintf(TRACE_ZCL_TASK, "bStopTimer Value %d\n", bStopTimer);
		if(!bStopTimer)
		{
			OS_eContinueSWTimer(APP_MsTimer, APP_TIME_MS(u32MsTimer), NULL);
		}
		else
		{
			OS_eStopSWTimer(APP_MsTimer);
		}
	}
#endif
	/* If there is a stack event to process, pass it on to ZCL */
	if (OS_eCollectMessage(APP_msgZCLEvents, &sStackEvent) == OS_E_OK)
	{
		if( (ZPS_EVENT_APS_DATA_INDICATION == sStackEvent.eType) || (ZPS_EVENT_APS_DATA_ACK == sStackEvent.eType) )
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "ZCL_Task received event %d\r\n",sStackEvent.eType);

			if(ZPS_EVENT_APS_DATA_INDICATION == sStackEvent.eType)
			{
				tsDeviceLevels sDeviceLevels;

				vGetDeviceLevels(&sDeviceLevels);								// Take a local copy of the device data
				sDeviceLevels.u8Lqi = sStackEvent.uEvent.sApsDataIndEvent.u8LinkQuality;
				vSetDeviceLevels(sDeviceLevels, LQI);							// Store the updated information
		    }
			sCallBackEvent.eEventType = E_ZCL_CBET_ZIGBEE_EVENT;
			vZCL_EventHandler(&sCallBackEvent);
		}

#ifdef APPLICATION_REJOIN_ENABLED
		else if( ZPS_EVENT_APS_DATA_CONFIRM == sStackEvent.eType )
		{
			if( sStackEvent.uEvent.sApsDataConfirmEvent.u8Status )
			{
				u8MissedApsAcks++;
				DBG_vPrintf(TRACE_ZCL_TASK, "Missed Acks: %d, ERR: %x\r\n",u8MissedApsAcks, sStackEvent.uEvent.sApsDataConfirmEvent.u8Status );

				if(u8MissedApsAcks >= MAX_MISSED_APS_ACKS)
				{
					u8MissedApsAcks = 0;
					OS_eActivateTask(APP_InitiateRejoin);
				}
			}
			/* received ack, clear down the counter */
			else
			{
				u8MissedApsAcks = 0;
			}
		}
#endif
		else
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "Unhandled Stack Event: %d\n", sStackEvent.eType);
		}
	}

	asSE_IPDDevice[0].sTimeCluster.utctTime = u32ZCL_GetUTCTime();

#ifdef CLD_KEY_ESTABLISHMENT
	if (u8Seconds > 10)
	{
		vDisplayAPSTable();
	}
#endif

#ifdef STACK_MEASURE
	if (u8Seconds > 10)
	{
		/* Send CPU stack measure info to the UART */
		vGetStackMeasure( &StackInfo );

		DBG_vPrintf(TRACE_ZCL_TASK, "CPU Stack:\r\n");

		DBG_vPrintf(TRACE_ZCL_TASK, "Total %d\n",StackInfo.u32TotalSize);
		DBG_vPrintf(TRACE_ZCL_TASK, "Current %d\n",StackInfo.u32CurrentMeasure);
		DBG_vPrintf(TRACE_ZCL_TASK, "Peak %d\r\n",StackInfo.u32PeakMeasure);
	}
#endif

#ifdef CLD_PRICE
	if (u8Seconds > 10)
	{
		/* Send Prices to the UART */
		vDisplayPrices();
	}
#endif

	/* Housekeeping, reset the second counter */
	if (u8Seconds > 10)
	{
		u8Seconds = 0;
	}
}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_ZCL_vInitialise
 *
 * DESCRIPTION:
 * Initialises the IPD ZCL
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_ZCL_vInitialise(void)
{
	teZCL_Status eZCL_Status;
	uint8 i;

	/* Initialise smart energy functions */
	eZCL_Status = eSE_Initialise(&cbZCL_GeneralCallback, apduZCL);
	if (eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "eSE_Init ERR: %x\r\n", eZCL_Status);
	}

	for( i = 0 ; i < SE_NUMBER_OF_ENDPOINTS ; i++)
	{
		/* Initialise Smart Energy Clusters etc */
		eZCL_Status = eSE_RegisterIPDEndPoint(IPD_BASE_LOCAL_EP + i, &cbZCL_EndpointCallback, &asSE_IPDDevice[i]);
		if (eZCL_Status != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "eSE_Register ERR: 0x%02x\r\n", eZCL_Status);
		}
#ifdef CLD_DRLC
		/* Set Device Class as smart appliance for DRLC */
		asSE_IPDDevice[i].sDRLCCluster.u16DeviceClassValue = E_SE_DRLC_SMART_APPLIANCES_BIT;
#endif
	}

#ifdef CLD_KEY_ESTABLISHMENT
	/* Load certificate and keys */
	eSE_KECLoadKeys(IPD_BASE_LOCAL_EP, (uint8 *)au8CAPublicKey, (uint8 *)au8Certificate, au8PrivateKey);
#endif

	asSE_IPDDevice[0].sLocalBasicCluster.ePowerSource = E_CLD_BAS_PS_BATTERY;
}


/****************************************************************************
 *
 * NAME: APP_ZCL_GetIPDData
 *
 * DESCRIPTION:
 * Mutexes then copies the IPD data for application manipulation
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_ZCL_GetIPDData(tsSE_IPDDevice **psSE_IPDDevice)
{
    vLockZCLMutex();
    *psSE_IPDDevice = &asSE_IPDDevice[u8CurrentMeterOfInterest];
    vUnlockZCLMutex();
}


/****************************************************************************
 *
 * NAME: APP_ZCL_RefreshIPDData
 *
 * DESCRIPTION:
 * Requests all defined attributes from the IPD
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_ZCL_RefreshIPDData(void)
{
    tsZCL_Address sAddress;
    sAddress.eAddressMode = E_ZCL_AM_SHORT;
    sAddress.uAddress.u16DestinationAddress = s_sDevice.asMeter[u8CurrentMeterOfInterest].u16DestinationAddress;
    DBG_vPrintf(TRACE_ZCL_TASK, "Current meter of interest: %i\n", u8CurrentMeterOfInterest);
    DBG_vPrintf(TRACE_ZCL_TASK, "Desination Address: 0x%04x\r\n", sAddress.uAddress.u16DestinationAddress);
    DBG_vPrintf(TRACE_ZCL_TASK, "Valid: %d, SAddr: 0x%04x, EP: 0x%02x \r\n",
			s_sDevice.asMeter[u8CurrentMeterOfInterest].bBindSuccess,
			s_sDevice.asMeter[u8CurrentMeterOfInterest].u16DestinationAddress,
			s_sDevice.asMeter[u8CurrentMeterOfInterest].u8MeterEndPoint);

    if(s_sDevice.asMeter[u8CurrentMeterOfInterest].bBindSuccess)
    {
		teZCL_Status eZCL_Status = eSE_ReadMeterAttributes(	IPD_BASE_LOCAL_EP + u8CurrentMeterOfInterest,
				s_sDevice.asMeter[u8CurrentMeterOfInterest].u8MeterEndPoint,
				&sAddress,
				&u8TransactionSequenceNumber);
		if (eZCL_Status != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "eSE_Read ERR: 0x%02x\n", eZCL_Status);

			ZPS_teStatus eStatus = eZCL_GetLastZpsError();
			DBG_vPrintf(TRACE_ZCL_TASK, "ZPS ERR: %x\r\n",eStatus );
		}
    }
}


#ifdef CLD_TIME
/****************************************************************************
 *
 * NAME: vRequestTime
 *
 * DESCRIPTION:
 * Requests Time from the Meter
 * events
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PUBLIC void vRequestTime(void)
{
	uint8   u8TransactionSequenceNumber;

	teZCL_Status eZCL_Status = eSE_ReadTimeAttributes(IPD_BASE_LOCAL_EP, s_sDevice.sEsp.u8TimeEndPoint, &s_sDevice.sEsp.sAddress, &u8TransactionSequenceNumber);
	DBG_vPrintf(TRACE_ZCL_TASK, "Req Time: 0x%02x\r\n", eZCL_Status);

	if (eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "Time ERR: %x\r\n", eZCL_Status);
	}
}
#endif


#ifdef CLD_PRICE
/****************************************************************************
 *
 * NAME: APP_vGetScheduledPrices
 *
 * DESCRIPTION:
 * Sends out a GetScheduledPrices command to the price server
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vGetScheduledPrices(void)
{
	uint8 u8TransactionSequenceNumber = 0;
    tsZCL_Address sAddress;
	sAddress.eAddressMode = E_ZCL_AM_SHORT;

	DBG_vPrintf(TRACE_ZCL_TASK, "Request Scheduled Prices\n" );
	DBG_vPrintf(TRACE_ZCL_TASK, "Current meter of interest: %i\n", u8CurrentMeterOfInterest);
	DBG_vPrintf(TRACE_ZCL_TASK, "Price server address: 0x%04x EP: 0x%02x\n", s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u16DestinationAddress, s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u8PriceEndPoint);

	if(s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].bBindSuccess)
	{
		sAddress.uAddress.u16DestinationAddress = s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u16DestinationAddress;
		teSE_PriceStatus eSE_PriceStatus = eSE_PriceGetScheduledPricesSend(
				IPD_BASE_LOCAL_EP + u8CurrentMeterOfInterest,
				s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u8PriceEndPoint,
				&sAddress,
				&u8TransactionSequenceNumber,
				0,
				SE_PRICE_NUMBER_OF_CLIENT_PRICE_RECORD_ENTRIES);

		if (eSE_PriceStatus != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "GetScheduledPrice Error: 0x%x\r\n", eSE_PriceStatus);

			ZPS_teStatus eStatus = eZCL_GetLastZpsError();
			DBG_vPrintf(TRACE_ZCL_TASK, "ZPS Error: 0x%x\n", eStatus);
		}
	}
}


/****************************************************************************
 *
 * NAME: APP_vGetCurrentPrice
 *
 * DESCRIPTION:
 * Sends out a GetCurrentPrice command to the price server
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vGetCurrentPrice(void)
{
	uint8 u8TransactionSequenceNumber = 0;
    tsZCL_Address sAddress;

	DBG_vPrintf(TRACE_ZCL_TASK, "Request Current Price\n" );
	DBG_vPrintf(TRACE_ZCL_TASK, "Current meter of interest: %i\n", u8CurrentMeterOfInterest);
	DBG_vPrintf(TRACE_ZCL_TASK, "Price server address: 0x%04x EP: 0x%02x\n", s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u16DestinationAddress, s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u8PriceEndPoint);

	if(s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].bBindSuccess)
	{
		sAddress.eAddressMode = E_ZCL_AM_SHORT;
		sAddress.uAddress.u16DestinationAddress = s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u16DestinationAddress;

		teSE_PriceStatus eSE_PriceStatus = eSE_PriceGetCurrentPriceSend(
				IPD_BASE_LOCAL_EP + u8CurrentMeterOfInterest,
				s_sDevice.asPrice[s_sDevice.asMeter[u8CurrentMeterOfInterest].u8PriceIndex].u8PriceEndPoint,
				&sAddress,
				&u8TransactionSequenceNumber,
				0);

		if (eSE_PriceStatus != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "GetCurrentPrice Error: 0x%x\r\n", eSE_PriceStatus);

			ZPS_teStatus eStatus = eZCL_GetLastZpsError();
			DBG_vPrintf(TRACE_ZCL_TASK, "ZPS Error: 0x%x\n", eStatus);
		}
	}
}
#endif


#ifdef CLD_DRLC
/****************************************************************************
 *
 * NAME: APP_vGetScheduledEvents
 *
 * DESCRIPTION:
 * Sends out a APP_vGetScheduledEvents command to the DRLC server
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vGetScheduledEvents(void)
{
	uint8 u8TransactionSequenceNumber = 0;

	DBG_vPrintf(TRACE_ZCL_TASK, "Request Scheduled DRLC events\n" );

	if (s_sDevice.sEsp.bDrlcBindSuccess)
	{
		tsSE_DRLCGetScheduledEvents sGetScheduledEvents;
		sGetScheduledEvents.u32StartTime = 0;
		sGetScheduledEvents.u8numberOfEvents = SE_DRLC_NUMBER_OF_CLIENT_LOAD_CONTROL_ENTRIES;

		teSE_DRLCStatus eSE_DRLCStatus = eSE_DRLCGetScheduledEventsSend(
				IPD_BASE_LOCAL_EP,
				s_sDevice.sEsp.u8DrlcEndPoint,
				&s_sDevice.sEsp.sAddress,
				&sGetScheduledEvents,
				&u8TransactionSequenceNumber);

		if (eSE_DRLCStatus != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "DRLC - GetScheduledEvents Error: 0x%x\r\n", eSE_DRLCStatus);

			ZPS_teStatus eStatus = eZCL_GetLastZpsError();
			DBG_vPrintf(TRACE_ZCL_TASK, "ZPS Error: 0x%x\n", eStatus);
		}
	}
}
#endif


#ifdef CLD_MC
/****************************************************************************
 *
 * NAME: APP_vStartGetMessage
 *
 * DESCRIPTION:
 * Requests messages from the Meter
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vStartGetMessage(void)
{
	teSE_MCStatus eSE_MCStatus;

	DBG_vPrintf(TRACE_ZCL_TASK, "APP_vStartGetMessage\n");

	eSE_MCStatus  = eSE_MCSendGetLastMessageRequest(IPD_BASE_LOCAL_EP, s_sDevice.sEsp.u8MessageEndPoint, &s_sDevice.sEsp.sAddress,
			&u8TransactionSequenceNumber);

	if (eSE_MCStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "Req Msg ERR: 0x%02x\n", eSE_MCStatus);
	}
}
#endif


#ifdef CLD_SM_SUPPORT_GET_PROFILE
/****************************************************************************
 *
 * NAME: APP_vHandleGetProfileRequest
 *
 * DESCRIPTION:
 * Requests messages from the Meter
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vHandleGetProfileRequest(void)
{
	teZCL_Status eStatus;
	tsZCL_Address sAddress;

	if(s_sDevice.asMeter[u8CurrentMeterOfInterest].bBindSuccess)
	{
		sAddress.eAddressMode = E_ZCL_AM_SHORT;
		sAddress.uAddress.u16DestinationAddress = s_sDevice.asMeter[u8CurrentMeterOfInterest].u16DestinationAddress;

		eStatus = eSM_ClientGetProfileCommand(IPD_BASE_LOCAL_EP,
						 s_sDevice.asMeter[u8CurrentMeterOfInterest].u8MeterEndPoint,
						 &sAddress,
						 E_CLD_SM_CONSUMPTION_DELIVERED,
						 0,
						 1);

		if (eStatus == E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "Get Profile Command Sent\n");
		}
		else
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "eSM_ClientGetProfileCommand error 0x%x\n", eStatus);
		}
	}
}
#endif


#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
/****************************************************************************
 *
 * NAME: APP_vHandleFastPollModeRequest
 *
 * DESCRIPTION:
 *
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vHandleFastPollModeRequest(void)
{
	teZCL_Status eStatus;
	tsZCL_Address sAddress;

	if(s_sDevice.asMeter[u8CurrentMeterOfInterest].bBindSuccess)
	{
		sAddress.eAddressMode = E_ZCL_AM_SHORT;
		sAddress.uAddress.u16DestinationAddress = s_sDevice.asMeter[u8CurrentMeterOfInterest].u16DestinationAddress;

		eStatus = eSM_ClientRequestFastPollCommand(IPD_BASE_LOCAL_EP,
						 s_sDevice.asMeter[u8CurrentMeterOfInterest].u8MeterEndPoint,
						 &sAddress,
						 8,
						 10);

		if(eStatus == E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "RequestFastPollCommand Sent\n");
        }
		else
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "RequestFastPollCommand error 0x%x\n",eStatus);
		}
	}
}
#endif


/****************************************************************************
 *
 * NAME: vLockZCLMutex
 *
 * DESCRIPTION:
 * Grabs and maintains a counting mutex
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vLockZCLMutex(void)
{
	if (u32ZCLMutexCount == 0)
	{
		OS_eEnterCriticalSection(ZCL);
	}
	u32ZCLMutexCount++;
}


/****************************************************************************
 *
 * NAME: vUnlockZCLMutex
 *
 * DESCRIPTION:
 * Releases and maintains a counting mutex
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vUnlockZCLMutex(void)
{
	u32ZCLMutexCount--;
	if (u32ZCLMutexCount == 0)
	{
		OS_eExitCriticalSection(ZCL);
	}
}


/****************************************************************************
 *
 * NAME: vClearExpiredFlag
 *
 * DESCRIPTION:
 * Starts and immediately stops a timer to clear the expired flag
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vClearExpiredFlag(OS_thSWTimer hSWTimer)
{
	OS_eStartSWTimer(hSWTimer, APP_TIME_MS(10000), NULL);
	OS_eStopSWTimer(hSWTimer);
}


/****************************************************************************
 *
 * NAME: u8QueryOtaImageUpgradeStatus
 *
 * DESCRIPTION:
 * Querys the local OTA status attribute
 *
 * RETURNS:
 * uint8
 *
 ****************************************************************************/
PUBLIC uint8 u8QueryOtaImageUpgradeStatus(void)
{
	uint8 u8ImageUpgradeStatus;

	eZCL_ReadLocalAttributeValue(
			IPD_BASE_LOCAL_EP,
			0x0019,
			FALSE,
			FALSE,
			TRUE,
			0x0006,
			&u8ImageUpgradeStatus);

	return u8ImageUpgradeStatus;
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: cbZCL_GeneralCallback
 *
 * DESCRIPTION:
 * General callback for ZCL events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void cbZCL_GeneralCallback(tsZCL_CallBackEvent *psEvent)
{
	ZPS_teStatus eStatus;

	switch(psEvent->eEventType)
	{
        case E_ZCL_CBET_LOCK_MUTEX:
			vLockZCLMutex();
        break;

		case E_ZCL_CBET_UNLOCK_MUTEX:
			vUnlockZCLMutex();
		break;

		case E_ZCL_CBET_UNHANDLED_EVENT:
		break;

		case E_ZCL_CBET_READ_ATTRIBUTES_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Read attributes response\r\n");
		break;

		case E_ZCL_CBET_READ_REQUEST:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Read request\r\n");
		break;

		case E_ZCL_CBET_DEFAULT_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Default response\r\n");
		break;

		case E_ZCL_CBET_ERROR:
			eStatus = eZCL_GetLastZpsError();
			DBG_vPrintf(TRACE_ZCL_TASK, "EVT: Error - Stack returned 0x%x\r\n", eStatus);
		break;

		case E_ZCL_CBET_TIMER:
		    DBG_vPrintf(TRACE_ZCL_TASK, "EVT: Timer\r\n");
		break;

		case E_ZCL_CBET_ZIGBEE_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EVT: ZigBee\r\n");
		break;

		case E_ZCL_CBET_CLUSTER_CUSTOM:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Custom\r\n");
		break;

		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "Invalid event type\r\n");
		break;
	}
}


/****************************************************************************
 *
 * NAME: cbZCL_EndpointCallback
 *
 * DESCRIPTION:
 * Endpoint specific callback for ZCL events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void cbZCL_EndpointCallback(tsZCL_CallBackEvent *psEvent)
{
	switch (psEvent->eEventType)
	{
		case E_ZCL_CBET_LOCK_MUTEX:
			vLockZCLMutex();
		break;

		case E_ZCL_CBET_UNLOCK_MUTEX:
			vUnlockZCLMutex();
		break;

		case E_ZCL_CBET_UNHANDLED_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Unhandled event\r\n");
		break;

		case E_ZCL_CBET_READ_INDIVIDUAL_ATTRIBUTE_RESPONSE:
			if(SE_CLUSTER_ID_SIMPLE_METERING == psEvent->psClusterInstance->psClusterDefinition->u16ClusterEnum)
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: SM Individual Attribute Response 0x%04x\n", psEvent->uMessage.sIndividualAttributeResponse.u16AttributeEnum);
				if ( E_CLD_SM_ATTR_ID_INSTANTANEOUS_DEMAND == psEvent->uMessage.sIndividualAttributeResponse.u16AttributeEnum )
				{
					if(E_ZCL_CMDS_SUCCESS == psEvent->uMessage.sIndividualAttributeResponse.eAttributeStatus)
					{
						/* Instantaneous demand received */
						s_sDevice.asMeter[u8CurrentMeterOfInterest].bInstantaneousDemandSupported = TRUE;
						DBG_vPrintf(TRACE_ZCL_TASK, "**** Instantaneous Demand Supported ****\n");
					}
					else
					{
						/* Instantaneous demand unsupported, set flag to display the current summation delivered instead */
						s_sDevice.asMeter[u8CurrentMeterOfInterest].bInstantaneousDemandSupported = FALSE;
						DBG_vPrintf(TRACE_ZCL_TASK, "**** Instantaneous Not Demand Supported ****\n");
					}
				}
				else if (E_CLD_SM_ATTR_ID_METERING_DEVICE_TYPE == psEvent->uMessage.sIndividualAttributeResponse.u16AttributeEnum)
				{
					if(E_ZCL_CMDS_SUCCESS == psEvent->uMessage.sIndividualAttributeResponse.eAttributeStatus)
					{
						s_sDevice.asMeter[u8MeterOfInterest].u8MeterDeviceType = *((uint8*)(psEvent->uMessage.sIndividualAttributeResponse.pvAttributeData));
					}
				}
			}
			else if(( E_CLD_P_ATTR_COMMODITY_TYPE == psEvent->uMessage.sIndividualAttributeResponse.u16AttributeEnum)&&
			   (SE_CLUSTER_ID_PRICE == psEvent->psClusterInstance->psClusterDefinition->u16ClusterEnum))
			{
				if(E_ZCL_CMDS_SUCCESS == psEvent->uMessage.sIndividualAttributeResponse.eAttributeStatus)
				{
					/* Commodity Type received */
					s_sDevice.asPrice[u8MeterOfInterest].u8CommodityType = *((uint8*)(psEvent->uMessage.sIndividualAttributeResponse.pvAttributeData));
				}
				else
				{
					DBG_vPrintf(TRACE_ZCL_TASK, "Commodity type not supported by the price server: 0x%x\n", psEvent->uMessage.sIndividualAttributeResponse.eAttributeStatus);
					DBG_vPrintf(TRACE_ZCL_TASK, "Harding coding value to Electricity\n");
					s_sDevice.asPrice[u8MeterOfInterest].u8CommodityType = E_CLD_SM_MDT_ELECTRIC;
				}
			}
		break;

		case E_ZCL_CBET_READ_ATTRIBUTES_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "Read Attributes Resp\r\n");
			if(eZCL_HandleReadAttributesResponse(psEvent, &u8TransactionSequenceNumber) != E_ZCL_SUCCESS)
			{
			    DBG_vPrintf(TRACE_ZCL_TASK, "Handle Read Attribute Response failed\r\n");
			}

			switch(psEvent->psClusterInstance->psClusterDefinition->u16ClusterEnum)
			{

				case  GENERAL_CLUSTER_ID_TIME:
					vZCL_SetUTCTime(asSE_IPDDevice[u8CurrentMeterOfInterest].sTimeCluster.utctTime);
					u32LastTimeUpdate = asSE_IPDDevice[u8CurrentMeterOfInterest].sTimeCluster.utctTime;				// Store when the time was last updated to trigger a refresh in 24 hrs
					OS_eStopSWTimer(APP_UTC_Timer);
					vMoveToNextDataReqState();
				break;

				case SE_CLUSTER_ID_SIMPLE_METERING:
					/* See if all attributes have been read */
					if(!s_sDevice.asMeter[u8CurrentMeterOfInterest].bInstantaneousDemandSupported)
					{
						/* Instantaneous demand unsupported by the meter,
						 * display the 16 least significant bits of the
						 * current summation instead */
						asSE_IPDDevice[u8CurrentMeterOfInterest].sSimpleMeteringCluster.i24InstantaneousDemand = (uint32)asSE_IPDDevice[u8CurrentMeterOfInterest].sSimpleMeteringCluster.u48CurrentSummationDelivered;
						asSE_IPDDevice[u8CurrentMeterOfInterest].sSimpleMeteringCluster.u8DemandFormatting = asSE_IPDDevice[u8CurrentMeterOfInterest].sSimpleMeteringCluster.u8SummationFormatting;
					}

					if ( APP_E_DISPLAY_STATE_CURRENT_KW == APP_vGetDisplayState() )
					{
						/*jp*/
						APP_vDisplaySetState(APP_E_DISPLAY_STATE_CURRENT_KW);
						APP_vDisplayUpdate();
					}

					OS_eStopSWTimer(APP_Meter_Timer);
					u8NumberOfSimpleMeteringRequests = 0;					// Reset the number of failed metering data requests
					bLostComms = FALSE;

#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
					if (E_FAST_POLL != eRunningState)
#endif
					{
						vMoveToNextDataReqState();
					}
				break;

				default:
				break;
			}
		break;

		case E_ZCL_CBET_READ_REQUEST:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Read request\r\n");
		break;

		case E_ZCL_CBET_DEFAULT_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Default response\r\n");
			DBG_vPrintf(TRACE_ZCL_TASK, "Command ID: %d Status Code: %d\r\n", psEvent->uMessage.sDefaultResponse.u8CommandId, psEvent->uMessage.sDefaultResponse.u8StatusCode);

			if(SE_CLUSTER_ID_MESSAGE == psEvent->pZPSevent->uEvent.sApsDataIndEvent.u16ClusterId)
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "\r\n No Messages on server \r\n");
				vMoveToNextDataReqState();
			}
			else if( SE_CLUSTER_ID_PRICE == psEvent->pZPSevent->uEvent.sApsDataIndEvent.u16ClusterId)
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "\r\n No Prices on server \r\n");
				/* No prices available therefore set the default tier information */
				u8CurrentElectricityCostTier = 1;
				u8CurrentElectricityNumberCostTiers = 1;
				vMoveToNextDataReqState();
			}

		break;

		case E_ZCL_CBET_ERROR:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Error\n");
		break;

		case E_ZCL_CBET_CLUSTER_CUSTOM:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Custom\r\n");

			switch(psEvent->uMessage.sClusterCustomMessage.u16ClusterId)
			{
#ifdef CLD_PRICE
				case SE_CLUSTER_ID_PRICE:
					vHandlePriceEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
					DBG_vPrintf(TRACE_ZCL_TASK, "Price Cluster Event Actioned\r\n");
				break;
#endif

#ifdef CLD_MC
				case SE_CLUSTER_ID_MESSAGE:
					vHandleMessageEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif

#ifdef CLD_KEY_ESTABLISHMENT
				case SE_CLUSTER_ID_KEY_ESTABLISHMENT:
					vHandleKeyEstablishmentEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif

#ifdef CLD_DRLC
				case SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL:
					vHandleDrlcEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif

#ifdef CLD_OTA
				case OTA_CLUSTER_ID:
					vHandleOtaEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif

				case SE_CLUSTER_ID_SIMPLE_METERING:
					vHandleMeteringEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;

				default:
					DBG_vPrintf(TRACE_ZCL_TASK, "Custom event for unknown cluster %d\r\n", psEvent->uMessage.sClusterCustomMessage.u16ClusterId);
				break;
			}
		break;
#ifdef OTA_CLD_ATTR_REQUEST_DELAY
		case E_ZCL_CBET_ENABLE_MS_TIMER:
			OS_eStartSWTimer(APP_MsTimer, APP_TIME_MS(psEvent->uMessage.u32TimerPeriodMs), NULL);
			u32MsTimer = psEvent->uMessage.u32TimerPeriodMs;
			DBG_vPrintf(TRACE_ZCL_TASK, "Timer update event %d\r\n",psEvent->uMessage.u32TimerPeriodMs);
			bStopTimer = FALSE;
		break;
		case E_ZCL_CBET_DISABLE_MS_TIMER:
			//OS_eStopSWTimer(APP_MsTimer);
			DBG_vPrintf(TRACE_ZCL_TASK, "Timer update event timer stop... %d\r\n",OS_eStopSWTimer(APP_MsTimer));
			bStopTimer = TRUE;
			break;
#endif
		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Invalid event type\r\n");
		break;
	}
}


#ifdef CLD_PRICE
/****************************************************************************
 *
 * NAME: vHandlePriceEvent
 *
 * DESCRIPTION:
 * Handles the price events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandlePriceEvent(tsSE_PriceCallBackMessage *psPriceMessage)
{
	vZCL_SetUTCTime(psPriceMessage->u32CurrentTime);

	DBG_vPrintf(TRACE_ZCL_TASK, "Price event %d\r\n", psPriceMessage->eEventType);

	switch(psPriceMessage->eEventType)
	{
		case E_SE_PRICE_TABLE_ADD:
			if(psPriceMessage->uMessage.sPriceTableCommand.ePriceStatus == E_ZCL_SUCCESS)
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "Added price\r\n");
			}
			else
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "Add Price ERR:\r\n", psPriceMessage->uMessage.sPriceTableCommand.ePriceStatus);
			}

			if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_PriceTimer))
			{
				/* If the main task is waiting for price information to continue,
				 * and that price information has been received */
				OS_eActivateTask(APP_IPDTask);
			}
		break;

		case E_SE_PRICE_TABLE_ACTIVE:
			DBG_vPrintf(TRACE_ZCL_TASK, "Price active\r\n");
		break;

		case E_SE_PRICE_GET_CURRENT_PRICE_RECEIVED:
			DBG_vPrintf(TRACE_ZCL_TASK, "get current price received event\r\n");
		break;

		case E_SE_PRICE_TIME_UPDATE:
			// If the 'price time' has been updated, i.e. a publish price command has been received
			OS_eStopSWTimer(APP_PriceTimer);
			OS_eActivateTask(APP_IPDTask);
		break;

		case E_SE_PRICE_CBET_ENUM_END:
		break;

		default:
		break;
	}
	APP_vDisplayUpdate();														// Update the display with the new price information (where applicable)
}


/****************************************************************************
 *
 * NAME: vDisplayPrices
 *
 * DESCRIPTION:
 * Loops through the price list sends to the UART
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vDisplayPrices(void)
{
	uint8 i = 0;
	tsSE_PricePublishPriceCmdPayload *pPricePublishPriceCmdPayload;

	/* Any prices in the table will be display to the UART */
	for( i=0;i<SE_PRICE_NUMBER_OF_CLIENT_PRICE_RECORD_ENTRIES;i++)
	{
		vLockZCLMutex();
		if(	0 == eSE_PriceGetPriceEntry(IPD_BASE_LOCAL_EP + u8CurrentMeterOfInterest, FALSE, i,&pPricePublishPriceCmdPayload ))
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "Index: %d, Start: %x, Duration: %d, Price: %d\r\n",
					i,
					pPricePublishPriceCmdPayload->u32StartTime,
					pPricePublishPriceCmdPayload->u16DurationInMinutes,
					pPricePublishPriceCmdPayload->u32Price
					);
		}
		vUnlockZCLMutex();
	}
}
#endif


#ifdef CLD_KEY_ESTABLISHMENT
/****************************************************************************
 *
 * NAME: vHandleKeyEstablishmentEvent
 *
 * DESCRIPTION:
 * Handles the Key Establishment events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleKeyEstablishmentEvent(void *pvParam)
{

    uint64 u64MacAddress = 0;
    tsSE_KECCallBackMessage *psKECMessage = (tsSE_KECCallBackMessage*)pvParam;
    DBG_vPrintf(TRACE_ZCL_TASK, "KEC Event: %d, Command: %d Status: %x\r\n", psKECMessage->eEventType, psKECMessage->u8CommandId, psKECMessage->eKECStatus);

	/* If key establishment was successful */
	if((psKECMessage->eEventType == E_SE_KEC_EVENT_COMMAND) &&
       (psKECMessage->eKECStatus == E_ZCL_SUCCESS) &&
       (psKECMessage->u8CommandId == E_SE_CONFIRM_KEY_DATA_REQUEST))
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "Key est OK\n");
		/* Set the link key to that returned from the key establishment
		 * First parameter is the mac address of the coordinator */
		memcpy(&u64MacAddress, &psKECMessage->psKEC_Common->uMessage.au8RemoteCertificate[22], 8);

#ifdef ZIGBEE_R20
		teZCL_Status eZCL_Status = ZPS_eAplZdoAddReplaceLinkKey(u64MacAddress, psKECMessage->psKEC_Common->au8Key, ZPS_APS_UNIQUE_LINK_KEY);
#else
		teZCL_Status eZCL_Status = ZPS_eAplZdoAddReplaceLinkKey(u64MacAddress, psKECMessage->psKEC_Common->au8Key);
#endif

		if (eZCL_Status)
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "eAplZdoAdd ERR: %x\n", eZCL_Status);
		}
		ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_ALL_PERMITED);

        s_sDevice.eState = E_SEND_MATCH;

        s_sDevice.bKeyEstComplete = TRUE;
        OS_eActivateTask(APP_IPDTask);
	}
	else if((psKECMessage->eEventType == E_SE_KEC_EVENT_COMMAND) &&
			(psKECMessage->eKECStatus == E_ZCL_SUCCESS) &&
	        (psKECMessage->u8CommandId == E_SE_TERMINATE_KEY_ESTABLISHMENT))
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "Ter res suite: 0x%04x\n", psKECMessage->psKEC_Common->sTerminateKeyEstablishmentCmdPayload.u16KeyEstablishmentSuite);
		DBG_vPrintf(TRACE_ZCL_TASK, "Ter res status: 0x%02x\n", psKECMessage->psKEC_Common->sTerminateKeyEstablishmentCmdPayload.u8StatusCode);
		DBG_vPrintf(TRACE_ZCL_TASK, "Ter res time: 0x%02x\n", psKECMessage->psKEC_Common->sTerminateKeyEstablishmentCmdPayload.u8WaitTime);
    }
}
#endif


#ifdef CLD_DRLC
/****************************************************************************
 *
 * NAME: vHandleDrlcEvent
 *
 * DESCRIPTION:
 * Handles the Demand Response Load Control events
 *
 * PARAMETERS: Name                     RW  Usage
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleDrlcEvent(void *pvParam)
{
	tsSE_DRLCCallBackMessage *psDrlcMessage = (tsSE_DRLCCallBackMessage*) (pvParam);

	DBG_vPrintf(TRACE_ZCL_TASK, "\npsDrlcMessage event %d, u8CommandId %d, u32CurrentTime %d, eDRLCStatus %d\n",
			psDrlcMessage->eEventType, psDrlcMessage->u8CommandId, psDrlcMessage->u32CurrentTime,
			psDrlcMessage->eDRLCStatus);

	APP_vDisplayUpdate();														// Update the display with the new DRLC information (where applicable)
}
#endif


#ifdef CLD_MC
/****************************************************************************
 *
 * NAME: vHandleMessageEvent
 *
 * DESCRIPTION:
 * Handles the Message Cluster events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleMessageEvent(void *pvParam)
{
#define E_SE_MC_EVENT_COMMAND 0

	tsSE_MCCallBackMessage *psMcMessage = (tsSE_MCCallBackMessage*) (pvParam);

	DBG_vPrintf(TRACE_ZCL_TASK, "psMcMessage evt: %d, CmdId: %d, Time: %d, Status: %x\n",
			psMcMessage->eEventType, psMcMessage->u8CommandId, psMcMessage->u32CurrentTime, psMcMessage->eMCStatus);

	if (((SE_MC_DISPLAY_MESSAGE == psMcMessage->u8CommandId) && (0x80 == psMcMessage->uMessage.sDisplayMessageCommandPayload.u8MessageControl)) ||
	    ((SE_MC_CANCEL_MESSAGE  == psMcMessage->u8CommandId) && (0x80 == psMcMessage->uMessage.sCancelMessageCommandPayload.u8MessageControl)))
	{
		/* Switch to the message confirm screen and await user input */
		APP_vDisplaySetState(APP_E_DISPLAY_STATE_MESSAGE_CONFIRMATION);
	}
#if REQUEST_MESSAGE_EVERY_WAKE_CYCLE
	else if (E_SE_MC_EVENT_COMMAND == psMcMessage->eEventType)
	{
		OS_eStopSWTimer(APP_MsgTimer);
		eRunningState = E_MESSAGE_RECEIVED;
		OS_eActivateTask(APP_IPDTask);
	}
#endif
	APP_vDisplayUpdate();														// Update the display with the new messaging information (where applicable)
}
#endif


#ifdef CLD_OTA
/****************************************************************************
 *
 * NAME: vHandleOtaEvent
 *
 * DESCRIPTION:
 * Handles the OTA Cluster events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleOtaEvent(void *pvParam)
{
	tsOTA_CallBackMessage *psOTAMessage = (tsOTA_CallBackMessage*) (pvParam);

	DBG_vPrintf(TRACE_ZCL_TASK,"IN OTA Event Handle function\n");
	switch( psOTAMessage -> eEventId)
	{
	    case E_CLD_OTA_INTERNAL_COMMAND_LOCK_FLASH_MUTEX:
	        OS_eEnterCriticalSection(hSpiMutex);
	        break;

	    case E_CLD_OTA_INTERNAL_COMMAND_FREE_FLASH_MUTEX:
	   	    OS_eExitCriticalSection(hSpiMutex);
	   	    break;

		case E_CLD_OTA_INTERNAL_COMMAND_VERIFY_SIGNER_ADDRESS:
			DBG_vPrintf(TRACE_ZCL_TASK,"In Signer Verification Callback\n");
			if(psOTAMessage->uMessage.sSignerMacVerify.u64SignerMac  == OTA_SIGNER_ADDRESS)
					psOTAMessage->uMessage.sSignerMacVerify.eMacVerifyStatus = E_ZCL_SUCCESS;
			else
					psOTAMessage->uMessage.sSignerMacVerify.eMacVerifyStatus = E_ZCL_FAIL;
		break;
		case E_CLD_OTA_INTERNAL_COMMAND_OTA_START_IMAGE_VERIFICATION_IN_LOW_PRIORITY:

			DBG_vPrintf(TRACE_EVENT_HANDLER, "In E_CLD_OTA_INTERNAL_COMMAND_OTA_START_IMAGE_VERIFICATION_IN_LOW_PRIORITY\n");
			#ifdef OTA_ACCEPT_ONLY_SIGNED_IMAGES
				/* Invoke low priority task to verify image */
				OS_eActivateTask(APP_ImageVerifyTask);
			#endif
			break;

#if (OTA_MAX_CO_PROCESSOR_IMAGES != 0)
		case E_CLD_OTA_INTERNAL_COMMAND_CO_PROCESSOR_BLOCK_RESPONSE:

			/*
			 * The Co-Processor client should confirm here that Image block
			 * downloaded is for the co-processor which will cause client to
			 * automatically invoke function to write the block of data received to
			 * appropriate memory location.
			 *
			 */
			 /* Image index for the co-processor image is 2, because in dependent image download
			  * index 0 and 1 is used for JN51xx self image */
			if(psOTAMessage->uMessage.sImageBlockResponsePayload.u8Status == E_ZCL_SUCCESS)
			{
				bool_t bWriteStatus;
				uint32 u32FlashOffset;
				uint8 i;
				if(psOTAMessage->uMessage.sImageBlockResponsePayload.uMessage.sBlockPayloadSuccess.u32FileOffset == 0)
				{ /* Erase the Flash sectors before start to write */
					for(i=0;i<psOTAMessage->u8MaxNumberOfSectors;i++)
					{
						bAHI_FlashEraseSector(psOTAMessage->u8ImageStartSector[2]+i);
					}
				}
				u32FlashOffset = (psOTAMessage->u8ImageStartSector[2] *(64*1024)) ;
				u32FlashOffset += psOTAMessage->uMessage.sImageBlockResponsePayload.uMessage.sBlockPayloadSuccess.u32FileOffset;
				bWriteStatus = bAHI_FullFlashProgram(u32FlashOffset,
						psOTAMessage->uMessage.sImageBlockResponsePayload.uMessage.sBlockPayloadSuccess.u8DataSize,
						psOTAMessage->uMessage.sImageBlockResponsePayload.uMessage.sBlockPayloadSuccess.pu8Data);
				if(bWriteStatus == FALSE)
				{
					DBG_vPrintf(TRACE_ZCL_TASK, "Event : OTA flash write fail\n");
				}
			}

		break;


		case E_CLD_OTA_INTERNAL_COMMAND_CO_PROCESSOR_IMAGE_DL_COMPLETE :
        {
			/* This is generated to indicate that Image transfer is complete,
			 * this is done by comparing image size quoted in Query Next Image
			 * response before download began and after all the blocks have been
			 * received.
			 *
			 * Image can be verified here using eOTA_VerifyImage command or custom
			 * command to co-processor to verify image.
			 *
			 * If image is not corrupted, we need to send End Request to OTA server
			 * this has to be done by invoking function -
			 * eOTA_CoProcessorUpgradeEndRequest()
			 *
			 */
			 				teZCL_Status eStatus;
#ifdef OTA_ACCEPT_ONLY_SIGNED_IMAGES
				uint32 u32FlashOffset;
				uint8  aCoProcessorOTAHeader[OTA_MAX_HEADER_SIZE] ;
				u32FlashOffset = (psOTAMessage->u8ImageStartSector[2] *(64*1024));
				bAHI_FullFlashRead(u32FlashOffset,
						           OTA_MAX_HEADER_SIZE,
						           aCoProcessorOTAHeader);
                uint16 u16FieldLength = aCoProcessorOTAHeader[7] << 8 | aCoProcessorOTAHeader[6];
				eStatus = eOTA_VerifyImage(IPD_BASE_LOCAL_EP,
									FALSE,
									2,
									aCoProcessorOTAHeader,
									u16FieldLength,
									TRUE);
				if(eStatus)
					eStatus = OTA_STATUS_IMAGE_INVALID;
#else
				eStatus = E_ZCL_SUCCESS;
#endif /* OTA_ACCEPT_ONLY_SIGNED_IMAGES */
				eOTA_CoProcessorUpgradeEndRequest(IPD_BASE_LOCAL_EP,eStatus);
        }
		break;

		case E_CLD_OTA_INTERNAL_COMMAND_CO_PROCESSOR_SWITCH_TO_NEW_IMAGE :

			/*
			 * The event is generated to indicate to the Co-processor by the OTA
			 * cluster that it can switch to new downloaded image.
			 * The responsibility to update itself with the new image lies
			 * with the co-processor-application.
			 *
			 */
			DBG_vPrintf(TRACE_ZCL_TASK, "Event : E_CLD_OTA_INTERNAL_COMMAND_CO_PROCESSOR_SWITCH_TO_NEW_IMAGE\n");
			/* If multiple image download is dependent then JN51xx image will not upgrade automatically
			   application have to call below function */
			eOTA_ClientSwitchToNewImage(IPD_BASE_LOCAL_EP);
		break;
		case E_CLD_OTA_INTERNAL_COMMAND_REQUEST_QUERY_NEXT_IMAGES:
		{

			tsZCL_Address sDestinationAddress;
			sDestinationAddress.eAddressMode = E_ZCL_AM_SHORT;
			sDestinationAddress.uAddress.u16DestinationAddress = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;

			tsOTA_QueryImageRequest sOTA_QueryImageRequest;
			sOTA_QueryImageRequest.u32CurrentFileVersion = COPROCESSOR_APPLICATION_VERSION;
			//sOTA_QueryImageRequest.u16HardwareVersion = N/A;
			sOTA_QueryImageRequest.u16ImageType = COPROCESSOR_IMAGE_TYPE;
			sOTA_QueryImageRequest.u16ManufacturerCode = MANUFACTURER_CODE;
			sOTA_QueryImageRequest.u8FieldControl = 0;

			eOTA_ClientQueryNextImageRequest(
					IPD_BASE_LOCAL_EP,
					s_sDevice.sEsp.u8OtaEndPoint,
					&sDestinationAddress,
					&sOTA_QueryImageRequest);
		}
	    break;
#endif
		default:
		break;

	}
}
#endif


/****************************************************************************
 *
 * NAME: vHandleMeteringEvent
 *
 * DESCRIPTION:
 * Handles the Message Cluster events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleMeteringEvent(void *pvParam)
{
	tsSM_CallBackMessage *psMessage = (tsSM_CallBackMessage*) (pvParam);

	switch( psMessage -> eEventType)
	{

		case E_CLD_SM_CLIENT_RECEIVED_COMMAND:
#ifdef CLD_SM_SUPPORT_GET_PROFILE
            if(E_CLD_SM_GET_PROFILE_RESPONSE  == psMessage->u8CommandId)
            {
            	tsSM_GetProfileResponseCommand sSMGetProfileResponse;
            	if(0xFFFFFFFF != u32SM_GetReceivedProfileData(&sSMGetProfileResponse))
            	{
            		DBG_vPrintf(TRACE_ZCL_TASK,"Get Profile Response Rcvd Success\n");
            	}
            	else
            	{
            		DBG_vPrintf(TRACE_ZCL_TASK,"Get Profile Response Rcvd with error\n");
            	}
            }
#endif
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
            if(E_CLD_SM_REQUEST_FAST_POLL_MODE_RESPONSE  == psMessage->u8CommandId)
            {
                if(psMessage->uMessage.sRequestFastPollResponseCommand.u8AppliedUpdatePeriod >= 2)
                {
                	u8AppliedUpdatePeriod = psMessage->uMessage.sRequestFastPollResponseCommand.u8AppliedUpdatePeriod;
                	u32FastPollEndTime = psMessage->uMessage.sRequestFastPollResponseCommand.u32FastPollEndTime;
                }
            }
#endif
		break;

		default:
		break;
	}
}

PUBLIC teZCL_Status App_QueryNextImageRequest( void )
{

		tsZCL_Address sDestinationAddress;
		tsOTA_ImageHeader          psOTAHeader;
		sDestinationAddress.eAddressMode = E_ZCL_AM_SHORT;
		sDestinationAddress.uAddress.u16DestinationAddress = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;

		teZCL_Status ZCL_Status =  eOTA_GetCurrentOtaHeader(
									IPD_BASE_LOCAL_EP,				//u8Endpoint
									FALSE,							//bIsServer,
									&psOTAHeader );					//*psOTAHeader);

		DBG_vPrintf(TRACE_IPD_NODE, "eOTA_GetCurrentOtaHeader returned 0x%x", ZCL_Status);

		tsOTA_QueryImageRequest sOTA_QueryImageRequest;
		sOTA_QueryImageRequest.u32CurrentFileVersion = psOTAHeader.u32FileVersion;		//APPLICATION_VERSION;
		//sOTA_QueryImageRequest.u16HardwareVersion = N/A;
		sOTA_QueryImageRequest.u16ImageType = psOTAHeader.u16ImageType;					//IMAGE_TYPE;
		sOTA_QueryImageRequest.u16ManufacturerCode = psOTAHeader.u16ManufacturerCode;	//MANUFACTURER_CODE;
		sOTA_QueryImageRequest.u8FieldControl = 0;

		uint8 u8QueryStatus = eOTA_ClientQueryNextImageRequest(
				IPD_BASE_LOCAL_EP,
				s_sDevice.sEsp.u8OtaEndPoint,
				&sDestinationAddress,
				&sOTA_QueryImageRequest);

		DBG_vPrintf(TRACE_IPD_NODE, "eOTA_ClientQueryNextImageRequest returned 0x%x", u8QueryStatus);

		return ZCL_Status;
}


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
