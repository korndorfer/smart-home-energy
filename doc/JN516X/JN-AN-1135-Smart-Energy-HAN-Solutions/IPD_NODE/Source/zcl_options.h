/*****************************************************************************
 *
 * MODULE:             ZCL Options
 *
 * COMPONENT:          zcl_options.h
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        Options Header for ZigBee Cluster Library functions
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/zcl_options.h $
 *
 * $Revision: 9281 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-08 15:13:02 +0100 (Fri, 08 Jun 2012) $
 *
 * $Id: zcl_options.h 9281 2012-06-08 14:13:02Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef ZCL_OPTIONS_H
#define ZCL_OPTIONS_H

#include <jendefs.h>

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define ZCL_ATTRIBUTE_READ_SERVER_SUPPORTED
#define ZCL_ATTRIBUTE_READ_CLIENT_SUPPORTED
#define ZCL_ATTRIBUTE_WRITE_SERVER_SUPPORTED
#define ZCL_ATTRIBUTE_WRITE_CLIENT_SUPPORTED


#define SE_MANUFACTURER_CODE								0x0000
#define SE_NUMBER_OF_ZCL_APPLICATION_TIMERS					3

/****************************************************************************/
/*             			   End Point Configuration                			*/
/****************************************************************************/
/* Sets the number of endpoints that will be created by the ZCL library */
#define SE_NUMBER_OF_ENDPOINTS								2

/****************************************************************************/
/*             			   Cluster Configuration                			*/
/****************************************************************************/
/* Enable/disable the Basic Cluster */
#define CLD_BASIC
#ifdef	CLD_BASIC
	#define BASIC_SERVER
	#define BASIC_CLIENT
#endif


/* Enable/disable the Identify Cluster */
#define CLD_IDENTIFY
#ifdef	CLD_IDENTIFY
	#define IDENTIFY_SERVER
#endif


/* Enable/disable the Key Establishment Cluster */
#define CLD_KEY_ESTABLISHMENT


/* Enable/disable the Simple Metering Cluster */
#define CLD_SIMPLE_METERING
#ifdef	CLD_SIMPLE_METERING
	//#define SM_SERVER
	#define SM_CLIENT
	#define CLD_SM_SUPPORT_GET_PROFILE
	#define CLD_SM_SUPPORT_FAST_POLL_MODE
#endif

#define	NUMBER_OF_SUPPORTED_COMMODITY_TYPES		2

/* Enable/disable the Time Cluster */
#define CLD_TIME
#ifdef	CLD_TIME
	//#define TIME_SERVER
	#define TIME_CLIENT
#endif


/* Enable/disable the Demand Response Load Control Cluster */
#define CLD_DRLC
#ifdef	CLD_DRLC
	//#define DRLC_SERVER
	#define DRLC_CLIENT
#endif


/* Enable/disable the Messaging Cluster */
#define CLD_MC
#ifdef	CLD_MC
	//#define MC_SERVER
	#define MC_CLIENT
	#define SE_MESSAGE_SERVER_MAX_STRING_LENGTH 98
#endif


/* Enable/disable the Price Cluster */
#define CLD_PRICE
#ifdef	CLD_PRICE
	//#define PRICE_SERVER
	#define PRICE_CLIENT

	/* if user not defined PRICE_CHARGING_MODE macro, by default code compiles for TOU mode */
	#define PRICE_CHARGING_MODE									1		/* 1 - TOU , 2 - Block, 3 - Combination */


	//#define PRICE_CONVERSION_FACTOR
	//#define PRICE_CALORIFIC_VALUE
	#ifdef PRICE_CONVERSION_FACTOR
	#define CLD_P_ATTR_CONVERSION_FACTOR
	#define CLD_P_ATTR_CONVERSION_FACTOR_TRAILING_DIGIT
	#define SE_PRICE_NUMBER_OF_CONVERSION_FACTOR_ENTRIES		2
	#endif
	#ifdef PRICE_CALORIFIC_VALUE
	#define CLD_P_ATTR_CALORIFIC_VALUE
	#define CLD_P_ATTR_CALORIFIC_VALUE_UNIT
	#define CLD_P_ATTR_CALORIFIC_VALUE_TRAILING_DIGIT
	#endif
#endif


#ifdef OTA_SUPPORT_OPTIONS		// Option flag passed in from the makefile
	/* Enable/disable the OTA Cluster */
	#define CLD_OTA
	#ifdef	CLD_OTA
		//#define OTA_SERVER
		#define OTA_CLIENT
		#define OTA_MAX_CO_PROCESSOR_IMAGES						0
		#define OTA_MAX_BLOCK_SIZE								100
		#define OTA_TIME_INTERVAL_BETWEEN_RETRIES				10		// Valid only if OTA_TIME_INTERVAL_BETWEEN_REQUESTS not defined
		//#define OTA_TIME_INTERVAL_BETWEEN_REQUESTS			1
		//#define OTA_ACCEPT_ONLY_SIGNED_IMAGES
        #define OTA_COPY_MAC_ADDRESS
		#ifdef JENNIC_CHIP_FAMILY_JN516x
			//#define KEC_DECRYPT_PRIVATE_KEY
			#define OTA_MAX_IMAGES_PER_ENDPOINT					1
		#else
			#define OTA_MAX_IMAGES_PER_ENDPOINT					2
		#endif
		//#define OTA_MAINTAIN_CUSTOM_SERIALISATION_DATA
		#define OTA_SIGNER_ADDRESS 2
	#endif
#endif


/* Some Meters use their own ZCL sequence number, rather than the one sent in
 * the packet, so ignore checking by defining this macro
 * rather than the TSN of the message it is replying to */
#define KEC_DISABLE_TSN_CHECK


/****************************************************************************/
/*             Basic Cluster - Optional Attributes                          */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the basic cluster.                                         */
/****************************************************************************/
#ifdef	CLD_BASIC
//#define   CLD_BAS_ATTR_APPLICATION_VERSION
//#define   CLD_BAS_ATTR_STACK_VERSION
//#define   CLD_BAS_ATTR_HARDWARE_VERSION
//#define   CLD_BAS_ATTR_MANUFACTURER_NAME
//#define   CLD_BAS_ATTR_MODEL_IDENTIFIER
//#define   CLD_BAS_ATTR_DATE_CODE
//#define   CLD_BAS_ATTR_LOCATION_DESCRIPTION
//#define   CLD_BAS_ATTR_PHYSICAL_ENVIRONMENT
//#define   CLD_BAS_ATTR_DEVICE_ENBLED
//#define   CLD_BAS_ATTR_ALARM_MASK
#endif

/****************************************************************************/
/*             Time Cluster - Optional Attributes                           */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the time cluster.                                          */
/****************************************************************************/
#ifdef	CLD_TIME
//#define E_CLD_TIME_ATTR_TIME_ZONE
//#define E_CLD_TIME_ATTR_DST_START
//#define E_CLD_TIME_ATTR_DST_END
//#define E_CLD_TIME_ATTR_DST_SHIFT
//#define E_CLD_TIME_ATTR_STANDARD_TIME
//#define E_CLD_TIME_ATTR_LOCAL_TIME
#endif

/****************************************************************************/
/*             Simple Metering Cluster - Optional Attributes                */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the simple metering cluster.                               */
/****************************************************************************/
#ifdef	CLD_SIMPLE_METERING
	/* Reading information attribute set attribute ID's (D.3.2.2.1) */
	//#define   CLD_SM_ATTR_CURRENT_SUMMATION_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_RECEIVED
	//#define   CLD_SM_ATTR_DFT_SUMMATION
	//#define   CLD_SM_ATTR_DAILY_FREEZE_TIME
	//#define   CLD_SM_ATTR_POWER_FACTOR
	//#define   CLD_SM_ATTR_READING_SNAPSHOT_TIME
	//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_DELIVERED_TIME
	//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_RECEIVED_TIME

	/* Time Of Use Information attribute attribute ID's set (D.3.2.2.2) */
	//#define   CLD_SM_ATTR_CURRENT_TIER_1_SUMMATION_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_TIER_1_SUMMATION_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_TIER_2_SUMMATION_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_TIER_2_SUMMATION_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_TIER_3_SUMMATION_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_TIER_3_SUMMATION_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_TIER_4_SUMMATION_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_TIER_4_SUMMATION_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_TIER_5_SUMMATION_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_TIER_5_SUMMATION_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_TIER_6_SUMMATION_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_TIER_6_SUMMATION_RECEIVED

	/* Formatting attribute set attribute ID's (D.3.2.2.4) */
	#define   CLD_SM_ATTR_MULTIPLIER
	#define   CLD_SM_ATTR_DIVISOR
	#define   CLD_SM_ATTR_DEMAND_FORMATING
	//#define   CLD_SM_ATTR_HISTORICAL_CONSUMPTION_FORMATTING

	/* ESP Historical Consumption set attribute ID's (D.3.2.2.5) */
	#define   CLD_SM_ATTR_INSTANTANEOUS_DEMAND
	//#define   CLD_SM_ATTR_CURRENT_DAY_CONSUMPTION_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_DAY_CONSUMPTION_RECEIVED
	//#define   CLD_SM_ATTR_PREVIOUS_DAY_CONSUMPTION_DELIVERED
	//#define   CLD_SM_ATTR_PREVIOUS_DAY_CONSUMPTION_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_START_TIME_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_START_TIME_RECEIVED
	//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_VALUE_DELIVERED
	//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_VALUE_RECEIVED

	/* Load Profile attribute set attribute ID's (D.3.2.2.6) */
	//#define   CLD_SM_ATTR_MAX_NUMBER_OF_PERIODS_DELIVERED

	/* Supply Limit attribute set attribute ID's (D.3.2.2.7) */
	//#define   CLD_SM_ATTR_CURRENT_DEMAND_DELIVERED
	//#define   CLD_SM_ATTR_DEMAND_LIMIT
	//#define   CLD_SM_ATTR_DEMAND_INTEGRATION_PERIOD
	//#define   CLD_SM_ATTR_NUMBER_OF_DEMAND_SUBINTERVALS
#endif


/****************************************************************************/
/*             Price Cluster - Optional Attributes                          */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the price cluster.                                         */
/****************************************************************************/
#ifdef	CLD_PRICE
#if PRICE_CHARGING_MODE == 1
    /* Price Cluster Tier Label Attribute Set (D.4.2.2.1)*/
    #define CLD_P_ATTR_TIER_PRICE_LABEL_MAX_COUNT       	5   /* Max. no of Price Tiers; Max Value 15 */
#endif

#if PRICE_CHARGING_MODE == 2
    /* Price Cluster Block Threshold Attribute Set (D.4.2.2.2) */
    //#define   CLD_P_ATTR_BLOCK_THRESHOLD_MAX_COUNT        3   /* Max. no of Block Threshold; Max Value 15 */
    /* Price Cluster Block Price Information Attribute Set (D.4.2.2.4) */
    /* No Tier Block */
    //#define CLD_P_ATTR_NO_TIER_BLOCK_PRICES_MAX_COUNT     4
#endif


	/* Price Cluster Block Period Attribute Set (D.4.2.2.3) */
	//#define CLD_P_ATTR_START_OF_BLOCK_PERIOD
	//#define CLD_P_ATTR_BLOCK_PERIOD_DURATION
	#define CLD_P_ATTR_THRESHOLD_MULTIPLIER
	#define CLD_P_ATTR_THRESHOLD_DIVISOR

	/* Price Cluster Commodity Attribute Set (D.4.2.2.4) */
	#define CLD_P_ATTR_COMMODITY_TYPE
	//#define CLD_P_ATTR_STANDING_CHARGE

#if PRICE_CHARGING_MODE == 3
    #define CLD_P_ATTR_TIER_PRICE_LABEL_MAX_COUNT       	5   /* Max. no of Price Tiers; Max Value 15 */
    //#define   CLD_P_ATTR_BLOCK_THRESHOLD_MAX_COUNT        3   /* Max. no of Block Threshold; Max Value 15 */
    /* Tier1 Block */
    //#define CLD_P_ATTR_NUM_OF_TIERS_PRICE 				5
    //#define CLD_P_ATTR_NUM_OF_BLOCKS_IN_EACH_TIER_PRICE 	4
#endif
#endif

/****************************************************************************/
/*             OTA Cluster - Optional Attributes                            */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the OTA cluster.                                           */
/****************************************************************************/
#ifdef	CLD_OTA
	//#define OTA_CLD_ATTR_FILE_OFFSET
//#define OTA_CLD_ATTR_CURRENT_FILE_VERSION
	//#define E_CLD_OTA_ATTR_CURRENT_ZIGBEE_STACK_VERSION
	//#define E_CLD_OTA_ATTR_DOWNLOADED_FILE_VERSION
	//#define E_CLD_OTA_ATTR_DOWNLOADED_ZIGBEE_STACK_VERSION
	//#define E_CLD_OTA_ATTR_MANF_ID
	//#define E_CLD_OTA_ATTR_IMAGE_TYPE

	/* Optional OTA header field */
	//#define OTA_CLD_HARDWARE_VERSIONS_PRESENT
#endif

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
#endif /* ZCL_OPTIONS_H */
