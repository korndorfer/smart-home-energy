/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (IPD)
 *
 * COMPONENT:          app_display.h
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Display Functionality Header
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_display.h $
 *
 * $Revision: 9271 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-01 16:40:44 +0100 (Fri, 01 Jun 2012) $
 *
 * $Id: app_display.h 9271 2012-06-01 15:40:44Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_DISPLAY_H_
#define APP_DISPLAY_H_

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define CO2_CONVERSION_CONSTANT		420											// 420g of CO2 per kWh
#define CO2_CONVERSION_CONSTANT_DP	2											// Number of digits to the right of the decimal point

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

// --- Functional States --- //
typedef enum
{
	APP_E_DISPLAY_STATE_INIT,
	APP_E_DISPLAY_STATE_SPLASH,
	APP_E_DISPLAY_STATE_SYNC,
	APP_E_DISPLAY_STATE_SYNC_FAILED,
	APP_E_DISPLAY_STATE_CURRENT_KW,
	APP_E_DISPLAY_STATE_CURRENT_M3,
	APP_E_DISPLAY_STATE_CURRENT_L,
	APP_E_DISPLAY_STATE_PRICE,
	APP_E_DISPLAY_STATE_MESSAGE,
	APP_E_DISPLAY_STATE_MESSAGE_CONFIRMATION,
	APP_E_DISPLAY_STATE_HISTORY,
	APP_E_DISPLAY_STATE_SETTINGS,
	APP_E_DISPLAY_STATE_DRLC,
} APP_teDisplayState;

// --- Sub-States --- //
// General Settings States
typedef enum
{
	APP_E_SETTINGS_STATE_CURRENCY,
	APP_E_SETTINGS_STATE_TEMPERATURE,
	APP_E_SETTINGS_STATE_FASTPOLL,
}APP_teSettingsState;

// --- Formatting --- //
// Currency Units
typedef enum
{
	E_POUNDS,
	E_DOLLARS,
	E_EUROS,
}APP_teCurrencyState;

// Temperature Units
typedef enum
{
	E_CELSIUS,
	E_FAHRENHEIT,
}APP_teTemperatureState;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void APP_vDisplayInitialise(void);
PUBLIC void APP_vDisplayUpdate(void);
PUBLIC void vClearLcd(void);
PUBLIC APP_teDisplayState APP_vGetDisplayState(void);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern APP_teDisplayState app_eDisplayState;
extern APP_teSettingsState app_eSettingsState;

#if LC_DEVICE

PUBLIC static const uint8 u8LcdLutChar[16]={									// Initialise the character memory map
											0x16,								// 0  = Special Characters - Status Bar
											0x1E,								// 1  = Character 1
											0x1C,								// 2  = Character 2
											0x1A,								// 3  = Character 3
											0x18,								// 4  = Character 4
											0x0C,								// 5  = Character 5
											0x0E,								// 6  = Character 6
											0x10,								// 7  = Character 7
											0x12,								// 8  = Character 8
											0x0A,								// 9  = Character 9
											0x08,								// 10 = Character 10
											0x06,								// 11 = Character 11
											0x04,								// 12 = Character 12
											0x02,								// 13 = Character 13
											0x00,								// 14 = Character 14
											0x14};								// 15 = Special Characters - Units/Cost

PUBLIC static const uint8 u8LcdLut1_4[44]={										// Lookup table for characters 1 to 4
											0xF5,								// 0  = '0'
											0x60,								// 1  = '1'
											0xB6,								// 2  = '2'
											0xF2,								// 3  = '3'
											0x63,								// 4  = '4'
											0xD3,								// 5  = '5'
											0xD7,								// 6  = '6'
											0x70,								// 7  = '7'
											0xF7,								// 8  = '8'
											0xF3,								// 9  = '9'
											0x02,								// 10 = '-'
											0x80,								// 11 = '_'
											0x82,								// 12 = '='
											0x60,								// 13 = ' |'
											0x05,								// 14 = '| '
											0x65,								// 15 = '||'
											0x00,								// 16 = ' ' (blank/space)
											0x77,								// 17 = 'A'
											0xC7,								// 18 = 'b'
											0x95,								// 19 = 'C'
											0xE6,								// 20 = 'd'
											0x97,								// 21 = 'E'
											0x17,								// 22 = 'F'
											0xF3,								// 23 = 'g'
											0x67,								// 24 = 'H'
											0x60,								// 25 = 'I'
											0xE0,								// 26 = 'J'
											0x00,								// 27 = unused
											0x85,								// 28 = 'L'
											0x00,								// 29 = unused
											0x75,								// 30 = 'n'
											0xF5,								// 31 = 'O'
											0x37,								// 32 = 'P'
											0x00,								// 33 = unused
											0x06,								// 34 = 'r'
											0xD3,								// 35 = 'S'
											0x87,								// 36 = 't'
											0xE7,								// 37 = 'U'
											0x00,								// 38 = unused
											0x00,								// 39 = unused
											0x00,								// 40 = unused
											0xE3,								// 41 = 'y'
											0x00,								// 42 = unused
											0x08};								// 43 = Segment 8 (i.e. decimal point, colon etc.)

PUBLIC static const uint8 u8LcdLut5_8[44]={										// Lookup table for characters 5 to 8
											0x5F,								// 0  = '0'
											0x06,								// 1  = '1'
											0x6B,								// 2  = '2'
											0x2F,								// 3  = '3'
											0x36,								// 4  = '4'
											0x3D,								// 5  = '5'
											0x7D,								// 6  = '6'
											0x07,								// 7  = '7'
											0x7F,								// 8  = '8'
											0x3F,								// 9  = '9'
											0x20,								// 10 = '-'
											0x08,								// 11 = '_'
											0x28,								// 12 = '='
											0x06,								// 13 = ' |'
											0x50,								// 14 = '| '
											0x56,								// 15 = '||'
											0x00,								// 16 = ' ' (blank/space)
											0x77,								// 17 = 'A'
											0x7C,								// 18 = 'b'
											0x6A,								// 19 = 'C'
											0x6E,								// 20 = 'd'
											0x79,								// 21 = 'E'
											0x71,								// 22 = 'F'
											0x3F,								// 23 = 'g'
											0x76,								// 24 = 'H'
											0x06,								// 25 = 'I'
											0x0E,								// 26 = 'J'
											0x00,								// 27 = unused
											0x58,								// 28 = 'L'
											0x00,								// 29 = unused
											0x57,								// 30 = 'n'
											0x5F,								// 31 = 'O'
											0x73,								// 32 = 'P'
											0x00,								// 33 = unused
											0x60,								// 34 = 'r'
											0x3D,								// 35 = 'S'
											0x78,								// 36 = 't'
											0x5E,								// 37 = 'U'
											0x00,								// 38 = unused
											0x00,								// 39 = unused
											0x00,								// 40 = unused
											0x3E,								// 41 = 'y'
											0x00,								// 42 = unused
											0x80};								// 43 = Segment 8 (i.e. decimal point, colon etc.)

PUBLIC static const uint8 u8LcdLut9_14[44]={									// Lookup table for characters 9 to 14
											0xF5,								// 0  = '0'
											0x60,								// 1  = '1'
											0xD3,								// 2  = '2'
											0xF2,								// 3  = '3'
											0x66,								// 4  = '4'
											0xB6,								// 5  = '5'
											0xB7,								// 6  = '6'
											0xE0,								// 7  = '7'
											0xF7,								// 8  = '8'
											0xF6,								// 9  = '9'
											0x02,								// 10 = '-'
											0x10,								// 11 = '_'
											0x12,								// 12 = '='
											0x60,								// 13 = ' |'
											0x05,								// 14 = '| '
											0x65,								// 15 = '||'
											0x00,								// 16 = ' ' (blank/space)
											0xE7,								// 17 = 'A'
											0x37,								// 18 = 'b'
											0x95,								// 19 = 'C'
											0x73,								// 20 = 'd'
											0x97,								// 21 = 'E'
											0x87,								// 22 = 'F'
											0xF6,								// 23 = 'g'
											0x67,								// 24 = 'H'
											0x60,								// 25 = 'I'
											0x70,								// 26 = 'J'
											0x00,								// 27 = unused
											0x15,								// 28 = 'L'
											0x00,								// 29 = unused
											0xE5,								// 30 = 'n'
											0xF5,								// 31 = 'O'
											0xC7,								// 32 = 'P'
											0x00,								// 33 = unused
											0x03,								// 34 = 'r'
											0xB6,								// 35 = 'S'
											0x17,								// 36 = 't'
											0x75,								// 37 = 'U'
											0x00,								// 38 = unused
											0x00,								// 39 = unused
											0x00,								// 40 = unused
											0x76,								// 41 = 'y'
											0x00,								// 42 = unused
											0x08};								// 43 = Segment 8 (i.e. decimal point, colon etc.)

#endif

/****************************************************************************/
/***        In line  Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vDisplaySetState
 *
 * DESCRIPTION:
 * Sets the display state
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
static inline void APP_vDisplaySetState(APP_teDisplayState eDisplayState)
{
    app_eDisplayState = eDisplayState;
}


/****************************************************************************
 *
 * NAME: APP_vSettingsSetState
 *
 * DESCRIPTION:
 * Sets the Settings state
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
static inline void APP_vSettingsSetState(APP_teSettingsState eSettingsState)
{
    app_eSettingsState = eSettingsState;
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_DISPLAY_H_*/
