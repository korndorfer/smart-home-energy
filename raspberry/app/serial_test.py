# -*- coding: utf8 -*-
import time
import string
from serial import Serial, SerialException

SERIAL_PORT = '/dev/ttyUSB0'
SERIAL_BAUD = 9600

GET_DATE                 = 0xa0;
GET_TIME                 = 0xa1;
GET_TEMPERATURE_STATUS   = 0xa2;
GET_TEMPERATURE          = 0xa3; 
GET_HUMIDITY_STATUS      = 0xa4; 
GET_HUMIDITY             = 0xa5; 
GET_PRESSURE             = 0xa6; 
GET_ALTITUDE             = 0xa7; 
GET_ALARM_STATUS         = 0xa8; 
GET_ALARM_ENABLE         = 0xa9;
GET_TEMPERATURE_DANGER   = 0xaa; 
GET_TEMPERATURE_OK       = 0xab; 
GET_HUMIDITY_DANGER      = 0xac;
GET_HUMIDITY_OK          = 0xad;
SET_DATE                 = 0xe0;
SET_TIME                 = 0xe1;
SET_TEMPERATURE_DANGER   = 0xe2;
SET_TEMPERATURE_OK       = 0xe3;
SET_HUMIDITY_DANGER      = 0xe4;
SET_HUMIDITY_OK          = 0xe5;
SET_ALARM_STATUS         = 0xe6;
SET_ALARM_STATUS         = 0xe7;
SET_ALARM_AKNOWLEDGE     = 0xe8;

OK_STATUS                = 0xf1;
WARNING_STATUS           = 0xf2;
DANGER_STATUS            = 0xf3;

params = {GET_TEMPERATURE_STATUS,
          GET_TEMPERATURE,
          GET_HUMIDITY,
          GET_PRESSURE,
          GET_ALTITUDE,
          GET_DATE,
          GET_TIME,
          GET_ALARM_STATUS,
          GET_HUMIDITY_STATUS,
          GET_TEMPERATURE_DANGER,
          GET_TEMPERATURE_OK, 
          GET_HUMIDITY_DANGER,
          GET_HUMIDITY_OK
          }

with Serial(SERIAL_PORT, SERIAL_BAUD, timeout=1) as serial:
    time.sleep(2)
    
    for param in params:
        print('S {}'.format(param))
        serial.write(chr(param))
        time.sleep(.2)
        received = ""

        data = serial.read(30)
        
        for i in range(len(data)):
            
            num = data[i]
            
            if num in string.printable:
                received += num

        print('R {}'.format(received))
    
    print('S {0} ({1} - {2}'.format(SET_ALARM_AKNOWLEDGE, serial.write(chr(SET_ALARM_AKNOWLEDGE)), chr(SET_ALARM_STATUS)))
    time.sleep(.2)
    received = ""

    data = serial.read(30)
    
    for i in range(len(data)):
        
        num = data[i]
        
        if num in string.printable:
            received += num

    print('R {}'.format(received))
            
    #for param in params:
        #serial.write(chr(param))
    
        ##serial.write('2')
        #time.sleep(.2)
        #received = ""
        
        #data = serial.read(30)
        
        #for i in range(len(data)):
            
            #num = data[i]
            
            #if num in string.printable:
                #received += num
                
        #print(received)
