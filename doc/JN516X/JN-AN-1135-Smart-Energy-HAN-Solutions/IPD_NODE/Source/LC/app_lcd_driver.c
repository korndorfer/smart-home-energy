/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (LC-IPD)
 *
 * COMPONENT:          app_lcd_driver.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        LCD Driver (I2C) functions
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_lcd_driver.c $
 *
 * $Revision: 8954 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-04-16 13:01:29 +0100 (Mon, 16 Apr 2012) $
 *
 * $Id: app_lcd_driver.c 8954 2012-04-16 12:01:29Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include "jendefs.h"
#include "utilities.h"

#include <AppHardwareApi.h>
#include "app_lcd_driver.h"
#include "dbg.h"
#include "ovly.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/**************************************************************************///

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vI2cInitialise
 *
 * DESCRIPTION:
 * Initialises the I2C bus
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vI2cInitialise(void)
{
	vAHI_SiMasterConfigure(														// Configure the device to be the master of the I2C bus
							TRUE,												// Pulse suppression enabled,
							FALSE,												// Interrupts disabled,
							0x07);												// With an operating frequency of 400 kHz = (16 / (('7'+ 1) * 5))
}


/****************************************************************************
 *
 * NAME: vEstablishLcdComms
 *
 * DESCRIPTION:
 * Establishes communication with the I2C LCD driver
 *
 *
 * RETURNS:
 * None.
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void vEstablishLcdComms(void)
{
	vAHI_SiMasterWriteSlaveAddr(0x38, FALSE);									// Set the slave address to 0111000 and the read bit to false (i.e. write)
	bAHI_SiMasterSetCmdReg(	E_AHI_SI_START_BIT,									// Transmit a start & write sequence
							E_AHI_SI_NO_STOP_BIT,
							E_AHI_SI_NO_SLAVE_READ,
							E_AHI_SI_SLAVE_WRITE,
							E_AHI_SI_SEND_ACK,
							E_AHI_SI_IRQ_ACK);
	while(bAHI_SiMasterPollTransferInProgress());								// Wait until the transfer is complete
	vCheckArbitration();														// Check for a loss of arbitration or Nack

	vAHI_SiMasterWriteData8(0xC8);												// Send 'Mode-Set' command to enable the display with a 1/3 bias and a 1:4 multiplex
	bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a write sequence
							E_AHI_SI_NO_STOP_BIT,
							E_AHI_SI_NO_SLAVE_READ,
							E_AHI_SI_SLAVE_WRITE,
							E_AHI_SI_SEND_ACK,
							E_AHI_SI_IRQ_ACK);
	while(bAHI_SiMasterPollTransferInProgress());								// Wait until the transfer is complete
	vCheckArbitration();														// Check for a loss of arbitration or Nack

	vAHI_SiMasterWriteData8(0xE0);												// Send 'Device-Select' command to target the correct I2C slave
	bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a write sequence
							E_AHI_SI_NO_STOP_BIT,
							E_AHI_SI_NO_SLAVE_READ,
							E_AHI_SI_SLAVE_WRITE,
							E_AHI_SI_SEND_ACK,
							E_AHI_SI_IRQ_ACK);
	while(bAHI_SiMasterPollTransferInProgress());								// Wait until the transfer is complete
	vCheckArbitration();														// Check for a loss of arbitration or Nack
}


/****************************************************************************
 *
 * NAME: vSendLcdCharacterPointer
 *
 * DESCRIPTION:
 * Transmits character pointer data to the I2C LCD driver
 *
 *
 * RETURNS:
 * None.
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void vSendLcdCharacterPointer(uint8 CharacterPointer)
{
	vAHI_SiMasterWriteData8(CharacterPointer);									// Send 'Load-Data-Pointer' command to the I2C LCD driver to point to the correct character address
	bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a write sequence
							E_AHI_SI_NO_STOP_BIT,
							E_AHI_SI_NO_SLAVE_READ,
							E_AHI_SI_SLAVE_WRITE,
							E_AHI_SI_SEND_ACK,
							E_AHI_SI_IRQ_ACK);
	while(bAHI_SiMasterPollTransferInProgress());								// Wait until the transfer is complete
	vCheckArbitration();														// Check for a loss of arbitration or Nack
}


/****************************************************************************
 *
 * NAME: vSendLcdCharacterData
 *
 * DESCRIPTION:
 * Transmits character data to the I2C LCD driver
 *
 *
 * RETURNS:
 * None.
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void vSendLcdCharacterData(uint8 CharacterData)
{
	vAHI_SiMasterWriteData8(CharacterData);										// Send character data to the I2C LCD driver
	bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a write sequence
							E_AHI_SI_NO_STOP_BIT,
							E_AHI_SI_NO_SLAVE_READ,
							E_AHI_SI_SLAVE_WRITE,
							E_AHI_SI_SEND_ACK,
							E_AHI_SI_IRQ_ACK);
	while(bAHI_SiMasterPollTransferInProgress());								// Wait until the transfer is complete
	vCheckArbitration();														// Check for a loss of arbitration or Nack
}


/****************************************************************************
 *
 * NAME: vTerminateLcdComms
 *
 * DESCRIPTION:
 * Ceases communication with the I2C LCD driver
 *
 *
 * RETURNS:
 * None.
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void vTerminateLcdComms(void)
{
	bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,								// Transmit a stop sequence
							E_AHI_SI_STOP_BIT,
							E_AHI_SI_NO_SLAVE_READ,
							E_AHI_SI_NO_SLAVE_WRITE,
							E_AHI_SI_SEND_ACK,
							E_AHI_SI_IRQ_ACK);
	while(bAHI_SiMasterPollTransferInProgress());								// Wait until the transfer is complete
}


/****************************************************************************
 *
 * NAME: vCheckArbitration
 *
 * DESCRIPTION:
 * Check to see if arbitration has been lost or if the slave transmits a Nack
 *
 *
 * RETURNS:
 * None.
 *
 ****************************************************************************/
PUBLIC OVERLAY(DISPLAY) void vCheckArbitration(void)
{
	if (bAHI_SiMasterCheckRxNack() ||  bAHI_SiMasterPollArbitrationLost())		// If a Nack is received (i.e. the slave can no longer receive data)
	{																			// or the master has lost arbitration, release and restart the bus
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_NO_START_BIT,							// Transmit a stop sequence
								E_AHI_SI_STOP_BIT,
								E_AHI_SI_NO_SLAVE_READ,
								E_AHI_SI_NO_SLAVE_WRITE,
								E_AHI_SI_SEND_NACK,
								E_AHI_SI_NO_IRQ_ACK);
		bAHI_SiMasterSetCmdReg(	E_AHI_SI_START_BIT,								// Transmit a start sequence
								E_AHI_SI_NO_STOP_BIT,
								E_AHI_SI_NO_SLAVE_READ,
								E_AHI_SI_NO_SLAVE_WRITE,
								E_AHI_SI_SEND_NACK,
								E_AHI_SI_NO_IRQ_ACK);
	}
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
