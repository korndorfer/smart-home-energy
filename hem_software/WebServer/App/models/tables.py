from App import db
from sqlalchemy_utils import PasswordType
from datetime import datetime
#from flask_sqlalchemy import Model

class User(db.Model):
    __tablename__ = "Usuarios"

    id_usuario = db.Column(db.Integer, primary_key = True)
    nome_usuario = db.Column(db.String(255), unique=True, nullable=False)
    telefone = db.Column(db.String(255), nullable=False)
    senha = db.Column(PasswordType(schemes=['pbkdf2_sha512','md5_crypt'], deprecated=['md5_crypt']), nullable = False)

    @property
    def is_authenticated(self):
        return True
    
    @property
    def is_active(self):
        return True
    
    @property
    def is_anonymous(self):
        return False
    
    def get_id(self):
        return str(self.id_usuario)

    def __init__(self, nome_usuario, senha, telefone):
        self.nome_usuario = nome_usuario
        self.senha = senha
        self.telefone = telefone
    
    def __repr__(self):
        return '<ID {0}>'.format(self.id_usuario)

class Data(db.Model):
    __tablename__ = "Dados"

    id_dado = db.Column(db.Integer, primary_key = True)
    corrente = db.Column(db.Float, nullable=False)
    tensao = db.Column(db.Float, nullable=False)
    temperatura = db.Column(db.Float, nullable=False)
    horario = db.Column(db.DateTime, nullable=False)
    potencia = db.Column(db.Float, nullable=False)
    
    id_tomada = db.Column(db.Integer, db.ForeignKey('Tomadas.id_tomada'))
    fonte = db.relationship('Socket', foreign_keys=id_tomada)
    
    def __init__(self, corrente, tensao, temperatura, horario, potencia, id_tomada):
        self.corrente = corrente
        self.tensao = tensao
        self.temperatura = temperatura
        self.horario = horario
        self.potencia = potencia
        self.id_tomada = id_tomada

    def __repr__(self):
        return '<ID {0}>'.format(self.id_dado)

class Socket(db.Model):
    __tablename__ = "Tomadas"

    id_tomada = db.Column(db.Integer, primary_key = True)
    nome_tomada = db.Column(db.String(255), unique = True, nullable = False)
    estado = db.Column(db.Boolean, nullable = False)
    comando = db.Column(db.Boolean)
    energizada = db.Column(db.Boolean)

    def __init__(self, nome_tomada, estado):
        self.nome_tomada = nome_tomada
        self.estado = estado
    
    def __repr__(self):
        return '<ID {0}>'.format(self.id_tomada)

class Access(db.Model):
    __tablename__ = "Acessa"
    
    id_dado = db.Column(db.Integer, db.ForeignKey('Dados.id_dado'), primary_key = True, nullable = False)
    id_usuario = db.Column(db.Integer, db.ForeignKey('Usuarios.id_usuario'))

    solicitante = db.relationship('User', foreign_keys=id_usuario)
    solicitado = db.relationship('Data', foreign_keys=id_dado)

    def __init__(self):
        pass
    
    def __repr__(self):
        pass