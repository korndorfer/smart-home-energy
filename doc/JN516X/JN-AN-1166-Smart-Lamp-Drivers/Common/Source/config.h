/****************************************************************************
 *
 * MODULE          JN-AN-1166 Smart Lamp Test Software
 *
 * COMPONENT       config.h
 *
 * DESCRIPTION     Common MAC/APP Macros for remote and lamp nodes
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5148]
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2013. All rights reserved
 *****************************************************************************/

#ifndef  CONFIG_H_INCLUDED
#define  CONFIG_H_INCLUDED

#if defined __cplusplus
extern "C" {
#endif

/****************************************************************************/
/***        Include Files                                                 ***/
/****************************************************************************/
#include <jendefs.h>

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/* Network parameters */
#define PAN_ID                  0xCAFE
#define BROADCAST_ADR           0xFFFF

/* Specify the default radio channel */
#define CHANNEL                 14


/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

typedef enum
{
	E_CMD_OFF,
	E_CMD_ON,
	E_CMD_STOP,
	/* Primary PWM channel is light level */
	E_CMD_LEVEL_UP,
	E_CMD_LEVEL_DOWN,
	/* Secondary PWM channel controls balance */
	E_CMD_BALANCE_LEFT,
	E_CMD_BALANCE_RIGHT,
	/* Secondary PWM also controls colour temperature */
	/* via PLS supplied algorithm                     */
	E_CMD_CCT_COOLER,
	E_CMD_CCT_WARMER,

	/* RGB Commands ]*/
	E_CMD_RED_UP,
	E_CMD_GREEN_UP,
	E_CMD_BLUE_UP,
	E_CMD_RED_DOWN,
    E_CMD_GREEN_DOWN,
	E_CMD_BLUE_DOWN,
	/* status from Light to LCD */
	E_STATUS_ON,
	E_STATUS_OFF,
	E_STATUS_LEVEL,
	E_STATUS_RED,
	E_STATUS_GREEN,
	E_STATUS_BLUE,
	E_STATUS_TEMPERATURE,
	E_STATUS_BALANCE,
	E_STATUS_CCT,

}teOnAirCommands;


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC char *apcDebugStrings[]={"C:OFF",
		                        "C:ON",
		                        "C:STOP",
		                        "C:LEVEL UP",
		                        "C:LEVEL DOWN",
		                        "C:BALANCE LEFT",
		                        "C:BALANCE RIGHT",
		                        "C:COLOUR TEMP COOLER",
		                        "C:COLOUR TEMP WARMER",
		                        "C:RED +",
		                        "C:GREEN +",
		                        "C:BLUE +",
		                        "C:RED -",
		                        "C:GREEN -",
		                        "C:BLUE -",
		                        "S:ON",
		                        "S:OFF",
		                        "S:LEVEL",
		                        "S:RED",
		                        "S:BLUE",
		                        "S:GREEN",
		                        "S:TEMPERATURE",
		                        "S:BALANCE",
		                        "S:CCT"};

#if defined __cplusplus
}
#endif

#endif  /* CONFIG_H_INCLUDED */

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
