/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (IPD)
 *
 * COMPONENT:          app_ipd_node.c
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        SE In-Premise Display - Main Source File
 *
 * $HeadURL $
 *
 * $Revision: 9281 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-08 15:13:02 +0100 (Fri, 08 Jun 2012) $
 *
 * $Id: app_ipd_node.c 9281 2012-06-08 14:13:02Z nxp33194 $
 *
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pwrm.h"
#include "dbg.h"
#include "zps_apl_af.h"
#include "zps_apl_zdp.h"
#include "zps_apl_aib.h"
#include "AppHardwareAPI.h"
#include "zcl.h"
#include "pdm.h"
#include "zps_apl_af.h"
#include "zps_apl_aib.h"
#include "zps_nwk_nib.h"
#include "zps_nwk_pub.h"

/* Application Includes */
#include "app_smartenergy_demo.h"
#include "app_timer_driver.h"
#include "app_zcl_task.h"
#include "app_display.h"
#include "app_ipd_node.h"
#include "app_buttons.h"
#include "Time.h"
#include "Price.h"
#include "string.h"
#include "zcl_options.h"
#include "Utilities.h"
#include "app_adc.h"
#include "app_sleep_functions.h"
#include "app_event_handler.h"
#include "app_zbp_utilities.h"
#include "ovly.h"
#ifdef CLD_OTA
#include "ota.h"
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_IPD_NODE
#define TRACE_IPD_NODE					TRUE
#endif

#define MAX_SINGLE_CHANNEL_NETWORKS		8

//#define ETSI																	// Remove comments to limit the module to +8dB for ETSI compliance

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vHandleConfigEvent(APP_tsEvent sAppEvent);
PRIVATE void vHandleStartupEvent(ZPS_tsAfEvent sStackEvent);
#ifdef SE_CERTIFICATION
PRIVATE void vHandleIdleEvent(APP_tsEvent sAppEvent);
#endif
PRIVATE void vHandleNetworkDiscoveryEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleNetworkJoinEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleRescanEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vJoinedNetwork(void);
PRIVATE void vHandleMaxJoinRequests(APP_tsEvent sAppEvent);
PRIVATE void vJoinStoredNWK(void);
PRIVATE void vSaveDiscoveredNWKS(ZPS_tsAfEvent sStackEvent);
PRIVATE void vDisplayDiscoveredNWKS(void);

#ifdef CLD_KEY_ESTABLISHMENT
PRIVATE void APP_vStartKeyEstablishment(void);
PRIVATE void APP_vWaitKeyEstablishment(void);
#endif

PRIVATE bool APP_bMaxJoinAttempts(void);
PRIVATE void vLeaveNWK(void);
PRIVATE void APP_vWaitLeave(ZPS_tsAfEvent sStackEvent);
PRIVATE void vUnhandledEvent(teState eState, ZPS_teAfEventType eType);

PRIVATE void vHandleGetMatchDesc(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleWaitMatchDesc(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleIeeeLookupEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleIeeeLookupRespEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vSendBindRequest(void);
PRIVATE void vHandleBindRespEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandlePriceKeyRequest(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleWaitPriceKey(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleMeterKeyRequest(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleWaitMeterKey(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleReadMeteringDeviceType(void);
PRIVATE void vHandleWaitMeteringDeviceType(void);
PRIVATE void vHandleReadCommodityType(void);
PRIVATE void vHandleWaitCommodityType(void);
PRIVATE void vFinaliseStartup(void);

PRIVATE PDUM_thAPduInstance APP_hAPduAllocateAPduInstance( void );
#ifdef CLD_OTA
PRIVATE void vOtaInitialisation(void);
#endif

#ifdef PDM_EEPROM
PUBLIC uint8 u8PDM_CalculateFileSystemCapacity(void);
PUBLIC uint8 u8PDM_GetFileSystemOccupancy(void);
#endif

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC	uint32		u32LastTimeUpdate;
PUBLIC 	tsDevice 	s_sDevice;
PUBLIC	bool_t		bRejoining = FALSE;
PUBLIC 	uint8		u8NetworkDiscoveryAttempts = 0;

PUBLIC	uint64		u64CoordinatorMac = 0x00;
PUBLIC	uint64		u64IPDMac = 0x00;

PUBLIC	uint8		u8TotalNumberOfDiscoveredMeters = 0;
PUBLIC	uint8		u8CurrentMeterOfInterest = 0;
PUBLIC	uint8		u8MeterOfInterest = 0;

PUBLIC	bool_t		bDataPending = FALSE;

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE PDM_tsRecordDescriptor s_sDevicePDDesc;

PRIVATE ZPS_tsNwkNetworkDescr tsDiscovedNWKList[MAX_SINGLE_CHANNEL_NETWORKS];
PRIVATE uint8 u8DiscovedNWKListCount = 0;
PRIVATE uint8 u8DiscovedNWKJoinCount = 0;
PRIVATE uint8 u8KecTimeoutCounter = 0;


#ifdef JENNIC_CHIP_FAMILY_JN516x
#if defined(PRODUCTION_CERTS) || defined(CLD_OTA)
	extern  uint8 	au8MacAddress[];
#else
	uint8 	au8MacAddress[] ={
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
	};
#endif
#endif
uint8	u8ZDP_TSN;


#ifdef SE_CERTIFICATION
bool bJoinNWKOnly = FALSE;
#endif



/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern	uint8			s_au8LnkKeyArray[16];
extern	teRunningStates	eRunningState;
extern	tsSE_IPDDevice	asSE_IPDDevice[SE_NUMBER_OF_ENDPOINTS];
extern	uint8			au8CAPublicKey[];
extern	uint8			u8NumberOfSimpleMeteringRequests;
extern	bool_t			bLostComms;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_IPDTask
 *
 * DESCRIPTION:
 * Called within the ZCL task context to consume events
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_IPDTask)
{
	ZPS_tsAfEvent sStackEvent;
	APP_tsEvent sAppEvent;

	sStackEvent.eType = ZPS_EVENT_NONE;
	sAppEvent.eType = APP_E_EVENT_NONE;

	if(OS_E_OK  == OS_eCollectMessage(APP_msgZpsEvents, &sStackEvent))
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Stack Evt: 0x%02x\n", sStackEvent.eType);

		if (sStackEvent.eType == ZPS_EVENT_ERROR)
		{
				DBG_vPrintf(TRACE_IPD_NODE, "ZigBee ERR: %x\r\n", sStackEvent.uEvent.sAfErrorEvent.eError);
		}
	}

	if  ((ZPS_EVENT_NWK_LEAVE_INDICATION == sStackEvent.eType))
	{

		/* Told to leave the NWK, correct the state */
		DBG_vPrintf(TRACE_IPD_NODE, "Leave Indication\r\n");

		//sStackEvent
		/* Presume the rejoin is true, so go to state waiting rejoin */
		//DBG_vPrintf(TRACE_IPD_NODE, "Rejoin Call Success");
		bRejoining = TRUE;														// Set the rejoin flag
		bLostComms = TRUE;
		u8NumberOfSimpleMeteringRequests = 0;
		APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC);
		APP_vDisplayUpdate();													// Update the display
		s_sDevice.eState = E_JOINING_NETWORK;									// Set the state machine to handle a rejoin event

		s_sDevice.bKeyEstComplete = FALSE;

		PDM_vSaveRecord(&s_sDevicePDDesc);
	}


	/* If there is a 'failed to join' event and the application state is not in joining/discovery mode
	 * the stack must have triggered a rejoin */
	else if  ((ZPS_EVENT_NWK_FAILED_TO_JOIN == sStackEvent.eType) &&
		((s_sDevice.eState != E_JOINING_NETWORK) &&
		 (s_sDevice.eState != E_DISCOVERING_NETWORKS)))
	{
		/* Stack rejoin failed, force the app to take over and continue retrying */
		DBG_vPrintf(TRACE_IPD_NODE, "Stack failed to rejoin\r\n");

		APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC);
		OS_eActivateTask(APP_InitiateRejoin);
	}
	else if (ZPS_EVENT_NWK_POLL_CONFIRM == sStackEvent.eType)
	{
		if (MAC_ENUM_SUCCESS == sStackEvent.uEvent.sNwkPollConfirmEvent.u8Status)
		{
			bDataPending = TRUE;
		}
		else if (MAC_ENUM_NO_DATA == sStackEvent.uEvent.sNwkPollConfirmEvent.u8Status)
		{
			bDataPending = FALSE;
		}
	}
	/* No stack event at this point, check for app events */
	else if (ZPS_EVENT_NONE == sStackEvent.eType)
	{
		if( OS_E_OK  == OS_eCollectMessage(APP_Events, &sAppEvent) )
		{
			DBG_vPrintf(TRACE_IPD_NODE, "App Event: %x\r\n", sAppEvent.eType);
		}
		else
		{
			DBG_vPrintf(TRACE_IPD_NODE, "No Event\r\n" );
		}
	}

	/* If the time has not been updated for 24 hrs and is not currently being updated... */
	if ((u32ZCL_GetUTCTime() >= u32LastTimeUpdate + SECONDS_IN_A_DAY) &&
	    (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_UTC_Timer)))
	{
		vZCL_ClearTimeHasBeenSynchronised();									// ... invalidate the time
		DBG_vPrintf(TRACE_IPD_NODE, "Invalidate Time\n");
	}

	/* The main state machine for the IPD Application */
	switch (s_sDevice.eState)
	{
		case E_CONFIG:
			vHandleConfigEvent(sAppEvent);
		break;

		case E_START_NETWORK:
			/* Start the network as an End Device and start network discovery */
			DBG_vPrintf(TRACE_IPD_NODE, "E_START_NETWORK");

			if (!APP_bMaxJoinAttempts())
			{
				APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC);
				APP_vDisplayUpdate();											// Update the display
				vHandleStartupEvent(sStackEvent);
			}
			else
			{
				vHandleMaxJoinRequests(sAppEvent);
			}
		break;

#ifdef SE_CERTIFICATION
		case E_IDLE:
			/* Handle the network discovery process */
			DBG_vPrintf(TRACE_IPD_NODE, "E_IDLE\r\n");
			vHandleIdleEvent(sAppEvent);
		break;
#endif

		case E_DISCOVERING_NETWORKS:
			/* Handle the network discovery process */
			DBG_vPrintf(TRACE_IPD_NODE, "E_DISCOVERING_NETWORKS\r\n");
			vHandleNetworkDiscoveryEvent(sStackEvent);
		break;

		case E_JOINING_NETWORK:
			DBG_vPrintf(TRACE_IPD_NODE, "E_JOINING_NETWORK\r\n");
			vHandleNetworkJoinEvent(sStackEvent);
		break;

		case E_RESCAN:
			DBG_vPrintf(TRACE_IPD_NODE, "E_RESCAN\r\n");
			if (!APP_bMaxJoinAttempts())
			{
				vHandleRescanEvent(sStackEvent);
			}
		break;

		case E_STATE_START_KEY_ESTABLISHMENT:
			APP_vStartKeyEstablishment();
		break;

		case E_STATE_WAIT_KEY_ESTABLISHMENT:
			APP_vWaitKeyEstablishment();
		break;

		case E_STATE_WAIT_LEAVE:
			APP_vWaitLeave(sStackEvent);
		break;

		case E_SEND_MATCH:
			DBG_vPrintf(TRACE_IPD_NODE, "E_SEND_MATCH\n");
			vHandleGetMatchDesc(sStackEvent);
		break;

		case E_WAIT_MATCH:
			DBG_vPrintf(TRACE_IPD_NODE, "E_WAIT_MATCH\n");
			vHandleWaitMatchDesc(sStackEvent);
		break;

		case E_IEEE_ADDR_LOOKUP:
			DBG_vPrintf(TRACE_IPD_NODE, "E_IEEE_ADDR_LOOKUP\n");
			vHandleIeeeLookupEvent(sStackEvent);
		break;

		case E_IEEE_ADDR_LOOKUP_RESPONSE:
			DBG_vPrintf(TRACE_IPD_NODE, "E_IEEE_ADDR_LOOKUP_RESPONSE\n");
			vHandleIeeeLookupRespEvent(sStackEvent);
		break;

		case E_REQ_PRICE_KEY:
			DBG_vPrintf(TRACE_IPD_NODE, "E_REQ_PRICE_KEY\n");
			vHandlePriceKeyRequest(sStackEvent);
		break;

		case E_WAIT_PRICE_KEY:
			DBG_vPrintf(TRACE_IPD_NODE, "E_WAIT_PRICE_KEY\n");
			vHandleWaitPriceKey(sStackEvent);
		break;

		case E_REQ_METER_KEY:
			DBG_vPrintf(TRACE_IPD_NODE, "E_REQ_METER_KEY\n");
			vHandleMeterKeyRequest(sStackEvent);
		break;

		case E_WAIT_METER_KEY:
			DBG_vPrintf(TRACE_IPD_NODE, "E_WAIT_METER_KEY\n");
			vHandleWaitMeterKey(sStackEvent);
		break;

		case E_BIND_REQ:
			DBG_vPrintf(TRACE_IPD_NODE, "E_BIND_REQ\n");
			vSendBindRequest();
		break;

		case E_BIND_RESP:
			DBG_vPrintf(TRACE_IPD_NODE, "E_BIND_RESP\n");
			vHandleBindRespEvent(sStackEvent);
		break;

		case E_READ_METERING_DEVICE_TYPE:
			DBG_vPrintf(TRACE_IPD_NODE, "E_READ_METERING_DEVICE_TYPE\n");
			vHandleReadMeteringDeviceType();
		break;

		case E_WAIT_METERING_DEVICE_TYPE:
			DBG_vPrintf(TRACE_IPD_NODE, "E_WAIT_METERING_DEVICE_TYPE\n");
			vHandleWaitMeteringDeviceType();
		break;

		case E_READ_COMMODITY_TYPE:
			DBG_vPrintf(TRACE_IPD_NODE, "E_READ_COMMODITY_TYPE\n");
			vHandleReadCommodityType();
		break;

		case E_WAIT_COMMODITY_TYPE:
 			DBG_vPrintf(TRACE_IPD_NODE, "E_WAIT_COMMODITY_TYPE\n");
			vHandleWaitCommodityType();
		break;


		case E_RUNNING:
			DBG_vPrintf(TRACE_IPD_NODE, "E_RUNNING\n");
			vHandleRunningEvent(sAppEvent);
		break;

		default:
			vUnhandledEvent(s_sDevice.eState,sStackEvent.eType);
		break;
	}

	/* Catch any un-handled data indications and free the apdu */
	if (ZPS_EVENT_APS_DATA_INDICATION == sStackEvent.eType)
	{
		PDUM_eAPduFreeAPduInstance(sStackEvent.uEvent.sApsDataIndEvent.hAPduInst);
	}
}


/****************************************************************************
 *
 * NAME: APP_InitiateRejoin
 *
 * DESCRIPTION:
 * Prepare the application and initiate a rejoin
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_InitiateRejoin)
{
	ZPS_teStatus eStatus = ZPS_eAplZdoRejoinNetwork();							// Tell the stack to initiate a rejoin
	if (ZPS_E_SUCCESS != eStatus)
	{
		/* Stack not currently able to initiate
		 * a rejoin, back off and try again later */
		DBG_vPrintf(TRACE_IPD_NODE, "Rejoin Error: %x, stack may already be rejoining\n", eStatus);
		OS_eStartSWTimer(APP_RejoinTimer, APP_TIME_MS(1000), NULL);
	}
	else
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Rejoin Call Success");
		bRejoining = TRUE;														// Set the rejoin flag
		bLostComms = TRUE;
		u8NumberOfSimpleMeteringRequests = 0;
		APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC);
		APP_vDisplayUpdate();													// Update the display
		s_sDevice.eState = E_JOINING_NETWORK;									// Set the state machine to handle a rejoin event
		PDM_vSaveRecord(&s_sDevicePDDesc);
	}
}
/****************************************************************************
 *
 * NAME: APP_ImageVerifyTask
 *
 * DESCRIPTION:
 * Called to verify the signature of down loaded image as a low priority task
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_ImageVerifyTask)
{

	DBG_vPrintf(TRACE_IPD_NODE, "In APP_ImageVerifyTask \n");

#ifdef OTA_ACCEPT_ONLY_SIGNED_IMAGES

	teZCL_Status eStatus;
	eStatus = eOTA_VerifyImage(IPD_BASE_LOCAL_EP,
							FALSE,
							0, //image location
							FALSE);
	if(E_ZCL_SUCCESS != eStatus)
	{

		DBG_vPrintf(TRACE_IPD_NODE, " eOTA_VerifyImage Failed %d\n",eStatus);
	}

	eStatus = eOTA_HandleImageVerification(IPD_BASE_LOCAL_EP,
			 s_sDevice.sEsp.u8OtaEndPoint,
			 eStatus);

	if(E_ZCL_SUCCESS != eStatus)
	{

		DBG_vPrintf(TRACE_IPD_NODE, " eOTA_HandleImageVerificatione Failed %d, Dest endpoint=%d \n",eStatus,s_sDevice.sEsp.u8OtaEndPoint );

	}
	else
	{
		DBG_vPrintf(TRACE_IPD_NODE, " eOTA_HandleImageVerificatione Success %d, Dest endpoint=%d \n",eStatus,s_sDevice.sEsp.u8OtaEndPoint );
	}
#endif


}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vInitialise
 *
 * DESCRIPTION:
 * Initialises the application
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vInitialise(void)
{

	DBG_vPrintf(TRACE_IPD_NODE, "APP_vInitialise\n");

	/* If a button is held down over reset, clear the context */
	bool bDeleteRecords = APP_bButtonInitialise();

	if (bDeleteRecords)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Clearing Context\n");
		PDM_vDelete();															// Clear context
	}

	/* set the device state to initial state */
	s_sDevice.eState = E_CONFIG;
	s_sDevice.bKeyEstComplete = FALSE;

	DBG_vPrintf(TRACE_IPD_NODE, "PDM_eLoadRecord\n");

	/* Restore any application data previously saved to flash */
	PDM_eLoadRecord(	&s_sDevicePDDesc,
						IPD_STATE, &s_sDevice,
						sizeof(s_sDevice),
						FALSE);

	DBG_vPrintf(TRACE_IPD_NODE, "ZPS_vAplSecSetInitialSecurityState\n");

	if (E_CONFIG == s_sDevice.eState)											// If context is blank
	{
		/* set the security state to pre-configured link key */
		#ifdef ZIGBEE_R20
			ZPS_vAplSecSetInitialSecurityState(
				ZPS_ZDO_PRECONFIGURED_LINK_KEY, s_au8LnkKeyArray, 0, ZPS_APS_UNIQUE_LINK_KEY);
		#else
			ZPS_vAplSecSetInitialSecurityState(
				ZPS_ZDO_PRECONFIGURED_LINK_KEY, s_au8LnkKeyArray, 0);
		#endif
	}

#ifndef JENNIC_CHIP_FAMILY_JN514x
	ZPS_vSetOverrideLocalMacAddress((uint64 *)&au8MacAddress);
#endif

	/* Initialise ZBPro stack */
	ZPS_eAplAfInit();

#ifdef PDM_EEPROM
    /*
     * The functions u8PDM_CalculateFileSystemCapacity and u8PDM_GetFileSystemOccupancy
     * may be called at any time to monitor space available in  the eeprom
     */
    DBG_vPrintf(TRACE_IPD_NODE, "PDM: Capacity %d\n", u8PDM_CalculateFileSystemCapacity() );
    DBG_vPrintf(TRACE_IPD_NODE, "PDM: Occupancy %d\n", u8PDM_GetFileSystemOccupancy() );
#endif

#ifdef ETSI
	vAHI_ETSIHighPowerModuleEnable(TRUE);										// Limit the module to +8dB for ETSI compliance
#endif

	/* force the state to E_CONFIG if not in a valid contect state */
	if( s_sDevice.eState <= E_STATE_WAIT_LEAVE )
		s_sDevice.eState = 	E_CONFIG;


	/* If the device state has been restored from flash, re-start the stack
	 *  and set the application running again */
	if (E_CONFIG != s_sDevice.eState)											// If the last known state was a running state (i.e. a context restore)
	{
		ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_ALL_PERMITED);

		ZPS_eAplZdoStartStack();

		DBG_vPrintf(TRACE_IPD_NODE, "Restoring Context, app state: %d \n", s_sDevice.eState);

		OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(500), NULL);

	}
	/* else perform any actions required on initial start-up */
	else
	{

	}

	/* Start the tick timer */
	OS_eStartSWTimer(APP_ZclTimer, ONE_SECOND_TICK_TIME, NULL);

	DBG_vPrintf(TRACE_IPD_NODE, "APP_ZCL_vInitialise\n");

	/* Register functions for the ZCL and SE */
	APP_ZCL_vInitialise();

	if (E_CONFIG != s_sDevice.eState)
	{
		/* If running here, must be a context restore cannot
		 * call ZPS_eAplZdoSetDevicePermission until after ZCL init()
		 */
		if (s_sDevice.bKeyEstComplete)
		{
			ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_ALL_PERMITED);

			if (E_RUNNING == s_sDevice.eState)									// If the last known state was a running state (i.e. a context restore)
			{
				s_sDevice.eState = E_SEND_MATCH;
			}
			else
			{
				if (E_JOINING_NETWORK != s_sDevice.eState)
				{
					/* Unexpected state, recover by triggering a rejoin */
					OS_eActivateTask(APP_InitiateRejoin);
				}
				else
				{
					/* already attempting to join */
				}
			}
			APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC);
			APP_vDisplayUpdate();												// Update the display
		}
		else
		{
			s_sDevice.eState = E_START_NETWORK;
		}

		/* Re-discover metering devices */
		//ResetServiceDiscovery();
	}

}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleConfigEvent
 *
 * DESCRIPTION:
 * Waiting for start state, if the button is pushed, moves to start stack
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleConfigEvent(APP_tsEvent sAppEvent)
{
	ZPS_tsAplAib * tsAplAib  = ZPS_psAplAibGetAib();

	DBG_vPrintf(TRACE_IPD_NODE, "E_CONFIG, EPID %016llx \r\n", tsAplAib->u64ApsUseExtendedPanid );

	APP_vDisplaySetState(APP_E_DISPLAY_STATE_SPLASH);
	APP_vDisplayUpdate();

	if (sAppEvent.eType != APP_E_EVENT_NONE)
	{

		if (APP_E_EVENT_BUTTON_DOWN == sAppEvent.eType)
		{
			if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)			// Button 1 pressed
			{
#ifdef SE_CERTIFICATION
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\nStart NWK join timer\n");
				OS_eStopSWTimer(APP_ButtonHoldTimer);
				OS_eStartSWTimer(APP_ButtonHoldTimer, APP_TIME_MS(5000), NULL);
#endif
			}
		}
		/* If join button pressed, update states */
		if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button && APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
		{
			u8NetworkDiscoveryAttempts = 0;
			s_sDevice.eState = E_START_NETWORK;
			OS_eStopSWTimer(APP_RestartTimer);
			OS_eActivateTask(APP_IPDTask);

#ifdef SE_CERTIFICATION
			OS_teStatus teStatus = OS_eGetSWTimerStatus(APP_ButtonHoldTimer);

			if ( OS_E_SWTIMER_RUNNING != teStatus )
			{
				DBG_vPrintf(TRACE_EVENT_HANDLER, "\r\n NWK join only\n");
				bJoinNWKOnly = TRUE;
			}
#endif
		}
	}
}


/****************************************************************************
 *
 * NAME: vJoinedNetwork
 *
 * DESCRIPTION:
 * Set the joined network parameters
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vJoinedNetwork(void)
{
	ZPS_tsNwkNib * thisNib;
	thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());

	u64CoordinatorMac = ZPS_psAplAibGetAib()->u64ApsTrustCenterAddress;

	ZPS_bNwkNibAddrMapAddEntry(ZPS_pvAplZdoGetNwkHandle(), 0x0000, u64CoordinatorMac);

	DBG_vPrintf(TRACE_IPD_NODE, "Joined CH: %d, PAN: %x, TrustC: %016llx\n", thisNib->sPersist.u8VsChannel, thisNib->sPersist.u16VsPanId, u64CoordinatorMac);

	OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(500), NULL);

	/* For handling rejoin */
#ifdef CLD_KEY_ESTABLISHMENT

	if( TRUE == s_sDevice.bKeyEstComplete)
	{
		vZCL_ClearTimeHasBeenSynchronised();
		DBG_vPrintf(TRACE_IPD_NODE, "KEC Already Complete\r\n");
	}
	else
	{
	    ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_DATA_REQUEST_DISALLOWED);
	}
	s_sDevice.eState = E_SEND_MATCH;
	ResetServiceDiscovery();

	/* let the joining and tables settle before we start discovery */
	OS_eStopSWTimer(APP_RestartTimer);
	OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(3000), NULL);

#ifdef SE_CERTIFICATION
	if( bJoinNWKOnly )
	{
		s_sDevice.eState = E_IDLE;
		bJoinNWKOnly = FALSE;
	}
#endif


#else
	s_sDevice.eState = E_RUNNING;
	eRunningState = E_INIT;
	OS_eActivateTask(APP_IPDTask);
#endif
}


/****************************************************************************
 *
 * NAME: vHandleStartupEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its startup state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 *
 *
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleStartupEvent(ZPS_tsAfEvent sStackEvent)
{
	static uint8 u8LocalChannelMask = 11;
	static bool bFirstTime = TRUE;
	static uint32 u32ConfigMask = 0;

	ZPS_tsNwkNib * thisNib;
	thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());

	DBG_vPrintf(TRACE_IPD_NODE, "Channel: %d, NWKPAN: %0llx, APSPAN: %0llx", thisNib->sPersist.u8VsChannel, thisNib->sPersist.u64ExtPanId, ZPS_psAplAibGetAib()->u64ApsUseExtendedPanid);

	/* timer started for 7.5 seconds in leave, prevents new join polls collecting old data */
	if( OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_RestartTimer) )
	{

		if (bFirstTime)
		{
			bFirstTime = FALSE;
			u32ConfigMask = ZPS_psAplAibGetAib()->apsChannelMask;
			DBG_vPrintf(TRACE_IPD_NODE, "Saved Channels: %08x\n", u32ConfigMask);
		}

		/* Clear the discovery neighbour table on each scan */
		vClearDiscNT();

		/* Check channel is in config mask, if so, start stack */
		if((u32ConfigMask) & (1 << u8LocalChannelMask))
		{
			ZPS_psAplAibGetAib()->apsChannelMask = 1 << u8LocalChannelMask;
			ZPS_teStatus eStatus;

			if(bRejoining)
			{
				APP_vDisplayUpdate();												// Update the display to show '----'
				OS_eActivateTask(APP_InitiateRejoin);
			}
			else
			{
				APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC);						// Update the display to say 'sync'
				APP_vDisplayUpdate();

				eStatus = ZPS_eAplZdoStartStack();

				if (ZPS_E_SUCCESS == eStatus)
				{
					DBG_vPrintf(TRACE_IPD_NODE, "StartStack: CH: %d \r\n", u8LocalChannelMask);
					s_sDevice.eState = E_DISCOVERING_NETWORKS;
				}
				else
				{
					DBG_vPrintf(TRACE_IPD_NODE, "StartStack: ERR: %x \r\n", eStatus);
					/* Try again ... */
					OS_eActivateTask(APP_IPDTask);
				}
			}
		}
		else
		{
			DBG_vPrintf(TRACE_IPD_NODE, "No Scan CH: %d\n", u8LocalChannelMask);
			OS_eActivateTask(APP_IPDTask);
		}

		u8LocalChannelMask++;

		if(u8LocalChannelMask >= 27)
		{
			u8LocalChannelMask = 11;
		}
	} //OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_RestartTimer)
}


/****************************************************************************
 *
 * NAME: vHandleMaxJoinRequests
 *
 * DESCRIPTION:
 * Handles multiple 'failed to join' events
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 *
 *
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleMaxJoinRequests(APP_tsEvent sAppEvent)
{
	/* Button 1 released */
	if ((APP_E_EVENT_BUTTON_UP == sAppEvent.eType) && (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button))
	{
		u8NetworkDiscoveryAttempts = 0;
		s_sDevice.eState = E_RESCAN;
		OS_eActivateTask(APP_IPDTask);

		APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC);
		APP_vDisplayUpdate();
	}
	else
	{
		APP_vDisplaySetState(APP_E_DISPLAY_STATE_SYNC_FAILED);
		APP_vDisplayUpdate();
		vScheduleSleep(INDEFINATE_SLEEP_PERIOD);
		vConfigureSleep();
	}
}


/****************************************************************************
 *
 * NAME: vHandleNetworkDiscoveryEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its network discovery state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE OVERLAY(JOINING) void vHandleNetworkDiscoveryEvent(ZPS_tsAfEvent sStackEvent)
{
	/* wait for node to discover networks... */
	if (ZPS_EVENT_NONE != sStackEvent.eType)
	{
		if (ZPS_EVENT_NWK_DISCOVERY_COMPLETE == sStackEvent.eType)
		{
#ifdef JOIN_ROUTER_ONLY
        	vRemoveCoordParents();
#endif

			DBG_vPrintf(TRACE_IPD_NODE, "Discovery Complete\n");

			if (ZPS_E_SUCCESS == sStackEvent.uEvent.sNwkDiscoveryEvent.eStatus)
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Found %d NWKS\n", sStackEvent.uEvent.sNwkDiscoveryEvent.u8NetworkCount);
				DBG_vPrintf(TRACE_IPD_NODE, "Unscanned CH: %08x\n", sStackEvent.uEvent.sNwkDiscoveryEvent.u32UnscannedChannels);
			}
			else
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Discovery ERR: %x\n",sStackEvent.uEvent.sNwkDiscoveryEvent.eStatus);

				OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(3000), NULL);
				s_sDevice.eState = E_START_NETWORK;
			}

			/* Networks were found */
			if (0 != sStackEvent.uEvent.sNwkDiscoveryEvent.u8NetworkCount)
			{
				vSaveDiscoveredNWKS(sStackEvent);

				vJoinStoredNWK();
			}
			/* No Networks found */
			else
			{
				OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(3000), NULL);
				s_sDevice.eState = E_START_NETWORK;
			}
		}
		else if (ZPS_EVENT_NWK_FAILED_TO_JOIN == sStackEvent.eType)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Join ERR: = %x\n", sStackEvent.uEvent.sNwkJoinFailedEvent.u8Status);

			s_sDevice.eState = E_DISCOVERING_NETWORKS;

			vJoinStoredNWK();
		}
		else if (ZPS_EVENT_NWK_JOINED_AS_ENDDEVICE == sStackEvent.eType)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Rejoined Addr: 0x%04x\n",
					sStackEvent.uEvent.sNwkJoinedEvent.u16Addr);

			/* Store EPID for future rejoin */
			uint64 u64ExtPANID = ZPS_u64NwkNibGetEpid(ZPS_pvAplZdoGetNwkHandle());
			ZPS_eAplAibSetApsUseExtendedPanId(u64ExtPANID);

			vJoinedNetwork();
		}
		else if (ZPS_EVENT_ERROR == sStackEvent.eType)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "ZPS ERR: %x\n", sStackEvent.uEvent.sAfErrorEvent.eError);
		}
		else
		{
			vUnhandledEvent(s_sDevice.eState,sStackEvent.eType);
		}
	}
}


#ifdef SE_CERTIFICATION
/****************************************************************************
 *
 * NAME: vHandleIdleEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its network discovery state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE OVERLAY(JOINING) void vHandleIdleEvent(APP_tsEvent sAppEvent)
{
	/* Button 1 released */
	if ((APP_E_EVENT_BUTTON_UP == sAppEvent.eType) && (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button))
	{
		vJoinedNetwork();
	}

}
#endif


/****************************************************************************
 *
 * NAME: vSaveDiscoveredNWKS
 *
 * DESCRIPTION:
 * Stores the list of NWKS from a scan
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vSaveDiscoveredNWKS(ZPS_tsAfEvent sStackEvent)
{
	uint8 i;

	u8DiscovedNWKJoinCount = 0;
	u8DiscovedNWKListCount = 0;

	memset( &tsDiscovedNWKList, 0, ((sizeof(ZPS_tsNwkNetworkDescr))* MAX_SINGLE_CHANNEL_NETWORKS));

	for(i=0; i<sStackEvent.uEvent.sNwkDiscoveryEvent.u8NetworkCount; i++)
	{
#ifndef JENNIC_CHIP_FAMILY_JN514x
		if( sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i].u8PermitJoining &&
				sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i].u8EndDeviceCapacity )
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Permit Join - %d\n", sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i].u8PermitJoining);

			tsDiscovedNWKList[u8DiscovedNWKListCount] = sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i];
			u8DiscovedNWKListCount++;
		}
#else
		if( sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i].u8PermitJoining &&
			sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i].u8EndDeviceCapacity )
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Permit Join - %d\n", sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i].u8PermitJoining);

			tsDiscovedNWKList[u8DiscovedNWKListCount] = sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i];
			u8DiscovedNWKListCount++;
		}

#endif

	}
	vDisplayDiscoveredNWKS();
}


/****************************************************************************
 *
 * NAME: vDisplayDiscoveredNWKS
 *
 * DESCRIPTION:
 * Lists the discovered NWKs to the UART.
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vDisplayDiscoveredNWKS(void)
{
	uint8 i;

	for (i = 0; i < MAX_SINGLE_CHANNEL_NETWORKS; i++)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Index: %d PAN: %016llx, Permit J %d\n", i, tsDiscovedNWKList[i].u64ExtPanId, tsDiscovedNWKList[i].u8PermitJoining);
	}
}


/****************************************************************************
 *
 * NAME: vJoinStoredNWK
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its rescan state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vJoinStoredNWK(void)
{
	ZPS_teStatus eStatus = 0xFF;

	DBG_vPrintf(TRACE_IPD_NODE, "Join Count: %d,NWKs Found: %d\n", u8DiscovedNWKJoinCount,u8DiscovedNWKListCount);


	while (u8DiscovedNWKJoinCount < u8DiscovedNWKListCount)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Joining PAN: %016llx, Index: %d, Total NWKs: %d\n", tsDiscovedNWKList[u8DiscovedNWKJoinCount].u64ExtPanId, u8DiscovedNWKJoinCount, u8DiscovedNWKListCount);
		eStatus = ZPS_eAplZdoJoinNetwork(&tsDiscovedNWKList[u8DiscovedNWKJoinCount]);

		if (ZPS_E_SUCCESS == eStatus)
		{
		   s_sDevice.eState = E_JOINING_NETWORK;
		   u8DiscovedNWKJoinCount++;
		   return;
		}
		else
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Join ERR: %x\n", eStatus);
			u8DiscovedNWKJoinCount++;
		}
	}

	if (u8DiscovedNWKJoinCount >= u8DiscovedNWKListCount)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Exhausted NWK Join\r\n");

		s_sDevice.eState = E_START_NETWORK;
		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, RESTART_TIME, NULL);
	}
}


/****************************************************************************
 *
 * NAME: vHandleRescanEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its rescan state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleRescanEvent(ZPS_tsAfEvent sStackEvent)
{
	/* restart scan with configured channel mask */
	DBG_vPrintf(TRACE_IPD_NODE, "Rescan %08x\n", ZPS_psAplAibGetAib()->apsChannelMask);
	ZPS_eAplZdoDiscoverNetworks(ZPS_psAplAibGetAib()->apsChannelMask);
	s_sDevice.eState = E_DISCOVERING_NETWORKS;
}


/****************************************************************************
 *
 * NAME: vHandleNetworkJoinEvent
 *
 * DESCRIPTION:
 * Handles stack events when the router is in its network join state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE OVERLAY(JOINING) void vHandleNetworkJoinEvent(ZPS_tsAfEvent sStackEvent)
{
	switch (sStackEvent.eType)
	{
		case ZPS_EVENT_NWK_JOINED_AS_ENDDEVICE:
			DBG_vPrintf(TRACE_IPD_NODE, "Joined Addr: 0x%04x\n", sStackEvent.uEvent.sNwkJoinedEvent.u16Addr);

			/* Store EPID for future rejoin */
			uint64 u64ExtPANID = ZPS_u64NwkNibGetEpid(ZPS_pvAplZdoGetNwkHandle());
			ZPS_eAplAibSetApsUseExtendedPanId(u64ExtPANID);

			vJoinedNetwork();
		break;

		case ZPS_EVENT_NWK_FAILED_TO_JOIN:
			DBG_vPrintf(TRACE_IPD_NODE, "Join Failed: %x\n", sStackEvent.uEvent.sNwkJoinFailedEvent.u8Status);
			if(bRejoining)														// Failed to rejoin the network...
			{
				vConfigureSleep();												// ... sleep and retry when we wake up
			}
			else
			{
				vJoinStoredNWK();
			}
		break;

		case ZPS_EVENT_NWK_POLL_CONFIRM:
			DBG_vPrintf(TRACE_IPD_NODE, "Poll Confirmed\n");
		break;

		default:
			vUnhandledEvent(s_sDevice.eState,sStackEvent.eType);
		break;
	}
}


/****************************************************************************
 *
 * NAME: vUnhandledEvent
 *
 * DESCRIPTION:
 * Displays the unhandled state and event
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vUnhandledEvent(teState eState, ZPS_teAfEventType eType)
{
	DBG_vPrintf(TRACE_IPD_NODE, "Unhandled State: %d, Event: 0x%02x\n", eState, eType);
}


/****************************************************************************
 *
 * NAME: APP_bMaxJoinAttempts
 *
 * DESCRIPTION:
 * Checks if the max number of join attempts has been reached. if so
 * returns TRUE, else returns FALSE
 *
 * PARAMETERS: Name         RW  Usage
 *             void
 *
 * RETURNS:
 *   returns TRUE if max reached, else returns FALSE
 *
 ****************************************************************************/
PRIVATE bool APP_bMaxJoinAttempts(void)
{
	if (++u8NetworkDiscoveryAttempts > MAX_DISCOVERY_ATTEMPT)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Failed To Join NWK\r\n");
		return (TRUE);
	}
	else
	{
		return (FALSE);
	}
}


#ifdef CLD_KEY_ESTABLISHMENT
/****************************************************************************
 *
 * NAME: APP_vStartKeyEstablishment
 *
 * DESCRIPTION:
 * Sends an initiate key establishment command to the Meter
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vStartKeyEstablishment(void)
{
	uint8	u8TransactionSequenceNumber;
	teSE_KECStatus eStatus;

	eStatus = eSE_KECInitiateKeyEstablishment(IPD_BASE_LOCAL_EP, s_sDevice.sEsp.u8KeyEstablishmentEndPoint, &s_sDevice.sEsp.sAddress, &u8TransactionSequenceNumber);
	if(eStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Key Req ERR: %x", eStatus);
	}

	OS_teStatus OS_eStatus = OS_eStopSWTimer(APP_RestartTimer);
	DBG_vPrintf(TRACE_IPD_NODE, "Stop timer: 0x%x", OS_eStatus);
	OS_eStatus = OS_eStartSWTimer(APP_RestartTimer, KEY_ESTABLISHMENT_TIMEOUT, NULL);
	DBG_vPrintf(TRACE_IPD_NODE, "Start timer: 0x%x", OS_eStatus);

	s_sDevice.eState = E_STATE_WAIT_KEY_ESTABLISHMENT;
	OS_eActivateTask(APP_IPDTask);
	u8KecTimeoutCounter = 0;
}


/****************************************************************************
 *
 * NAME: APP_vWaitKeyEstablishment
 *
 * DESCRIPTION:
 * Waits for the key establishment response
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vWaitKeyEstablishment(void)
{
	if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		if (++u8KecTimeoutCounter >= 10)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Key establishment timed out\n");
			vLeaveNWK();
		}
		else
		{
			OS_eStartSWTimer(APP_RestartTimer, KEY_ESTABLISHMENT_TIMEOUT, NULL);
		}
	}
}
#endif


/****************************************************************************
 *
 * NAME: APP_vWaitLeave
 *
 * DESCRIPTION:
 * Waits for the leave confirm event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vWaitLeave(ZPS_tsAfEvent sStackEvent)
{
	if (ZPS_EVENT_NWK_LEAVE_CONFIRM == sStackEvent.eType)
	{
#if 0
		DBG_vPrintf(TRACE_IPD_NODE, "Leave Confirm :%02x",sStackEvent.uEvent.sNwkLeaveConfirmEvent.eStatus );

		vJoinStoredNWK();
#endif
		PDM_vDelete();
		vAHI_SwReset();
	}
}


/****************************************************************************
 *
 * NAME: vLeaveNWK
 *
 * DESCRIPTION:
 * Calls the stack leave function
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vLeaveNWK(void)
{
	ZPS_teStatus teStatus = ZPS_eAplZdoLeaveNetwork(0, FALSE, FALSE);
	s_sDevice.bKeyEstComplete = FALSE;

	/* Clear out the extended PAN as we are not rejoining */
	ZPS_eAplAibSetApsUseExtendedPanId(0);

	/* Reset as we are not leaving with rejoin */
	ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_ALL_PERMITED);

	if(teStatus)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Failed Leave: %x", teStatus);
		vAHI_SwReset();
	}
	s_sDevice.eState = E_STATE_WAIT_LEAVE;
}


/****************************************************************************
 *
 * NAME: vHandleGetMatchDesc
 *
 * DESCRIPTION:
 * Send a match descriptor request to the coordinator
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleGetMatchDesc(ZPS_tsAfEvent sStackEvent)
{
	ZPS_tuAddress uDestinationAddress;
	ZPS_tsAplZdpMatchDescReq matchDesc;

	uint16 pu16InClusterList[1];
	uint16 pu16OutClusterList[0] = {};

	/* Set Destination/Interest address for the requests */
	uDestinationAddress.u16Addr = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;
	matchDesc.u16NwkAddrOfInterest = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;

	if (!s_sDevice.bKeyEstComplete)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "sending key match\n");
		pu16InClusterList[0] = SE_CLUSTER_ID_KEY_ESTABLISHMENT;
	}
#ifdef CLD_TIME
	else if( !s_sDevice.bTimeDiscComplete )
	{
		DBG_vPrintf(TRACE_IPD_NODE, "sending time match\n");
		pu16InClusterList[0] = GENERAL_CLUSTER_ID_TIME;
	}
#endif
#ifdef CLD_MC
	else if( !s_sDevice.bMessageDiscComplete )
	{
		DBG_vPrintf(TRACE_IPD_NODE, "sending message match\n");
		pu16InClusterList[0] = SE_CLUSTER_ID_MESSAGE;
    }
#endif
#ifdef CLD_DRLC
    else if( !s_sDevice.bDrlcDiscComplete )
    {
        DBG_vPrintf(TRACE_IPD_NODE, "sending DRLC match\n");
        pu16InClusterList[0] = SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL;
    }
#endif
#ifdef CLD_OTA
	else if(!s_sDevice.bOtaDiscComplete)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "sending OTA match\n");
		pu16InClusterList[0] = OTA_CLUSTER_ID;
	}
#endif
#ifdef CLD_PRICE
	else if( !s_sDevice.bPriceDiscComplete )
	{
	    uDestinationAddress.u16Addr = 0xFFFD;                               // Broadcast to 'Rx On When Idle' devices
	    matchDesc.u16NwkAddrOfInterest = 0xFFFD;                            // Broadcast to 'Rx On When Idle' devices

	    DBG_vPrintf(TRACE_IPD_NODE, "sending price match\n");
		pu16InClusterList[0] = SE_CLUSTER_ID_PRICE;
	}
#endif
	else
	{
		DBG_vPrintf(TRACE_IPD_NODE, "sending simple metering match\n");
		uDestinationAddress.u16Addr = 0xFFFD;								// Broadcast to 'Rx On When Idle' devices
		matchDesc.u16NwkAddrOfInterest = 0xFFFD;							// Broadcast to 'Rx On When Idle' devices
		pu16InClusterList[0] = SE_CLUSTER_ID_SIMPLE_METERING;
	}

	matchDesc.u16ProfileId			= SE_PROFILE_ID;
	matchDesc.u8NumInClusters		= 1;
	matchDesc.pu16InClusterList		= pu16InClusterList;
	matchDesc.u8NumOutClusters		= 0;
	matchDesc.pu16OutClusterList	= pu16OutClusterList;

	PDUM_thAPduInstance hAPduInst = APP_hAPduAllocateAPduInstance();

	if (hAPduInst == PDUM_INVALID_HANDLE)
	{
		/* no resource, stay in this state, and trigger a retry
		 * in 5 seconds */
		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(5000), NULL);

		return;
	}

	ZPS_teStatus eStatus = ZPS_eAplZdpMatchDescRequest(hAPduInst, uDestinationAddress, FALSE, &u8ZDP_TSN, &matchDesc);

	if (ZPS_E_SUCCESS == eStatus)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Sent Match: 0x%04x\n", pu16InClusterList[0]);
		s_sDevice.eState = E_WAIT_MATCH;
	}
	else
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Match ERR: %x", eStatus);
	    PDUM_eAPduFreeAPduInstance(hAPduInst);
	}

	OS_eStopSWTimer(APP_RestartTimer);
	OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(5000), NULL);

}


/****************************************************************************
 *
 * NAME: vHandleWaitMatchDesc
 *
 * DESCRIPTION:
 * Get the match descriptor response from the coordinator
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleWaitMatchDesc(ZPS_tsAfEvent sStackEvent)
{
	bool_t bValidEntry, bDuplicateEntry;
	uint8 i, j;
	uint8 u8NextFree;

	if (ZPS_EVENT_APS_ZDP_REQUEST_RESPONSE == sStackEvent.eType)
    {
		if (0x8006 == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
		{
			if (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8Status != ZPS_E_SUCCESS)
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Match Failed\n");

				s_sDevice.eState = E_SEND_MATCH;

				if (!s_sDevice.bKeyEstComplete)
				{
					vLeaveNWK();
				}
				return;
			}

			if(! (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength > 0 &&
					(sStackEvent.uEvent.sApsZdpEvent.u8SequNumber == u8ZDP_TSN) ) )
			{
				return;
			}

			if (!s_sDevice.bKeyEstComplete)
			{
				DBG_vPrintf(TRACE_IPD_NODE, "KE Match Success\n");

				s_sDevice.sEsp.sAddress.eAddressMode = E_ZCL_AM_SHORT;
				s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress = sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest;
				s_sDevice.sEsp.u8KeyEstablishmentEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];

				s_sDevice.eState = E_STATE_START_KEY_ESTABLISHMENT;
				OS_eActivateTask(APP_IPDTask);

			}
#ifdef CLD_TIME
			else if(!s_sDevice.bTimeDiscComplete)
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Time Match Success, Addr: 0x%04x, MatchLength: %d\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest, sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength);

				s_sDevice.sEsp.u8TimeEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
				s_sDevice.bTimeDiscComplete = TRUE;

				s_sDevice.eState = E_SEND_MATCH;
				OS_eActivateTask(APP_IPDTask);

			}
#endif
#ifdef CLD_MC
			else if(!s_sDevice.bMessageDiscComplete)
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Message Match Success, Addr: 0x%04x, MatchLength: %d\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest, sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength);

				s_sDevice.sEsp.u8MessageEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
				s_sDevice.bMessageDiscComplete = TRUE;

				s_sDevice.eState = E_SEND_MATCH;
				OS_eActivateTask(APP_IPDTask);

			}
#endif
#ifdef CLD_DRLC
            else if(!s_sDevice.bDrlcDiscComplete)
            {
                DBG_vPrintf(TRACE_IPD_NODE, "DRLC Match Success, Addr: 0x%04x, MatchLength: %d\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest, sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength);

                s_sDevice.sEsp.u8DrlcEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
                s_sDevice.bDrlcDiscComplete = TRUE;

                s_sDevice.eState = E_SEND_MATCH;
                OS_eActivateTask(APP_IPDTask);

            }
#endif
#ifdef CLD_OTA
			else if(!s_sDevice.bOtaDiscComplete)
			{
				s_sDevice.sEsp.u8OtaEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
				s_sDevice.bOtaDiscComplete = TRUE;

				s_sDevice.eState = E_SEND_MATCH;
				OS_eActivateTask(APP_IPDTask);
			}
#endif
#ifdef CLD_PRICE
			else if(!s_sDevice.bPriceDiscComplete)
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Price Match Success, Addr: 0x%04x, MatchLength: %d\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest, sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength);

				for (i = 0 ; i < sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength ; i++ )
				{
					bDuplicateEntry = FALSE;
					u8NextFree = 0xFF;

					/* Scan the price list for duplicate price entries */
					for(j = 0 ; j < NUMBER_OF_SUPPORTED_COMMODITY_TYPES ; j++)
					{
						if ((s_sDevice.asPrice[j].u16DestinationAddress == sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest)&&
							(s_sDevice.asPrice[j].u8PriceEndPoint == sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[i]))
						{
							bDuplicateEntry = TRUE;
							DBG_vPrintf(TRACE_IPD_NODE, "Duplicate entry: 0x%04x, Ep: 0x%x\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest,sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[i]);
						}
						else if (s_sDevice.asPrice[j].u16DestinationAddress == 0xFFFF && u8NextFree == 0xFF)
						{
							u8NextFree = j;
						}
					}

					/* If the newly discovered price is not a duplicate, use the next available record */
					if ( FALSE == bDuplicateEntry && u8NextFree!= 0xFF )
					{
						/* Store the newly discovered meter in the empty space */
						s_sDevice.asPrice[u8NextFree].u16DestinationAddress = sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest;
						s_sDevice.asPrice[u8NextFree].u8PriceEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[i];

						DBG_vPrintf(TRACE_IPD_NODE, "Price server added to local price structure, Array Index: %i, Addr: 0x%04x, EP: 0x%x\n", u8NextFree, s_sDevice.asPrice[u8NextFree].u16DestinationAddress, s_sDevice.asPrice[u8NextFree].u8PriceEndPoint);
					}
				}
			}
#endif
			/* All ESP endpoints discovered, discover any available meters */
			else
			{
				DBG_vPrintf(TRACE_IPD_NODE, "SM Match Success, Addr: 0x%04x, MatchLength: %d\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest, sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength);

				for (i = 0 ; i < sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength ; i++ )
				{
					bDuplicateEntry = FALSE;
					u8NextFree = 0xFF;

					/* Scan the array for duplicate meter entries */
					for (j = 0 ; j < SE_NUMBER_OF_ENDPOINTS ; j++)
					{
						if ((s_sDevice.asMeter[j].u16DestinationAddress == sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest)&&
						    (s_sDevice.asMeter[j].u8MeterEndPoint == sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[i]))
						{
							bDuplicateEntry = TRUE;
							DBG_vPrintf(TRACE_IPD_NODE, "Duplicate entry: 0x%04x, Ep: 0x%x\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest,sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[i]);
						}
						if (s_sDevice.asMeter[j].u16DestinationAddress == 0xFFFF && u8NextFree == 0xFF)
						{
							u8NextFree = j;
						}
					}

					/* If the newly discovered meter is not a duplicate, scan the array for an empty record */
					if ( (FALSE == bDuplicateEntry) && (u8NextFree!= 0xFF) )
					{
						/* Store the newly discovered meter in the empty space */
						s_sDevice.asMeter[u8NextFree].u16DestinationAddress = sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest;
						s_sDevice.asMeter[u8NextFree].u8MeterEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[i];
					}
				}
			}
		}
		else
		{
			DBG_vPrintf(TRACE_IPD_NODE,"ZDP Event ignored\r\n");
		}
    }
	else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		bValidEntry = FALSE;
		s_sDevice.eState = E_SEND_MATCH;

		if ( !s_sDevice.bKeyEstComplete )
		{
			DBG_vPrintf(TRACE_IPD_NODE, "key match failed, retrying\n");
		}
#ifdef CLD_TIME
		else if( !s_sDevice.bTimeDiscComplete )
		{
			DBG_vPrintf(TRACE_IPD_NODE, "time match failed, retrying\n");
		}
#endif
#ifdef CLD_MC
		else if( !s_sDevice.bMessageDiscComplete )
		{
			DBG_vPrintf(TRACE_IPD_NODE, "message match failed, retrying\n");
	    }
#endif
#ifdef CLD_DRLC
	    else if( !s_sDevice.bDrlcDiscComplete )
	    {
	        DBG_vPrintf(TRACE_IPD_NODE, "DRLC match failed, retrying\n");
	    }
#endif
#ifdef CLD_OTA
		else if( !s_sDevice.bOtaDiscComplete )
		{
			DBG_vPrintf(TRACE_IPD_NODE, "OTA match failed, skipping OTA discovery\n");
			s_sDevice.bOtaDiscComplete = TRUE;

		}
#endif
		else if( !s_sDevice.bPriceDiscComplete )
		{
			for (i = 0 ; i < SE_NUMBER_OF_ENDPOINTS ; i++)
			{
				if (0xFFFF != s_sDevice.asPrice[i].u16DestinationAddress)
				{
					bValidEntry = TRUE;
				}
			}

			if(!bValidEntry) // No price found within the timeout period
			{
				DBG_vPrintf(TRACE_IPD_NODE,"No price servers discovered, continue anyway\n");
			}
			s_sDevice.bPriceDiscComplete = TRUE;
			s_sDevice.eState = E_SEND_MATCH;
		}
		else
		{
			for (i = 0 ; i < SE_NUMBER_OF_ENDPOINTS ; i++)
			{
				if (0xFFFF != s_sDevice.asMeter[i].u16DestinationAddress)
				{
					bValidEntry = TRUE;
				}
			}

			if(!bValidEntry) // No meters found within the timeout period, retry
			{
				s_sDevice.eState = E_SEND_MATCH;
				DBG_vPrintf(TRACE_IPD_NODE, "No meters found, retrying\n");
			}
			else
			{
			    u8MeterOfInterest = 0;
			    s_sDevice.eState = E_IEEE_ADDR_LOOKUP;
			}
		}
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vHandleIeeeLookupEvent
 *
 * DESCRIPTION:
 * Temporary workaround. Request the meter's short address.
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleIeeeLookupEvent(ZPS_tsAfEvent sStackEvent)
{
	uint8				u8TransactionSequenceNumber;
	PDUM_thAPduInstance hAPduInst;

	if (u8MeterOfInterest >= SE_NUMBER_OF_ENDPOINTS )
	{
		/* All IEEE address obtained, continue starting up */
		u8MeterOfInterest = 0;
		s_sDevice.eState = E_BIND_REQ;
		OS_eActivateTask(APP_IPDTask);
	}
	else if ((0xFFFF != s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress) && (!s_sDevice.asMeter[u8MeterOfInterest].u64DestinationAddress))
	{
		hAPduInst = APP_hAPduAllocateAPduInstance();
		if (hAPduInst == PDUM_INVALID_HANDLE)
		{
		    return;
		}

		ZPS_tuAddress uDstAddr;
		uDstAddr.u16Addr = s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress;

		ZPS_tsAplZdpIeeeAddrReq sAplZdpIeeeAddrReq;
		sAplZdpIeeeAddrReq.u16NwkAddrOfInterest = s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress;
		sAplZdpIeeeAddrReq.u8RequestType = 0x00;
		sAplZdpIeeeAddrReq.u8StartIndex = 0x00;

		ZPS_teStatus eStatus = ZPS_eAplZdpIeeeAddrRequest(	hAPduInst,
															uDstAddr,
															FALSE,
															&u8TransactionSequenceNumber,
															&sAplZdpIeeeAddrReq
															);
		if (!eStatus)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "IEEE Lookup Sent: 0x%04x\n", s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress);
		}
		else
		{
			DBG_vPrintf(TRACE_IPD_NODE, "IEEE Fail: - Error: 0x%02x\n", eStatus);
			PDUM_eAPduFreeAPduInstance(hAPduInst);
		}

		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(2000), NULL);

		s_sDevice.eState = E_IEEE_ADDR_LOOKUP_RESPONSE;
	}
	else if ((0xFFFF != s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress) && (!s_sDevice.asPrice[u8MeterOfInterest].u64DestinationAddress))
	{
		hAPduInst = APP_hAPduAllocateAPduInstance();
		if (hAPduInst == PDUM_INVALID_HANDLE)
		{
		    return;
		}

		ZPS_tuAddress uDstAddr;
		uDstAddr.u16Addr = s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress;

		ZPS_tsAplZdpIeeeAddrReq sAplZdpIeeeAddrReq;
		sAplZdpIeeeAddrReq.u16NwkAddrOfInterest = s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress;
		sAplZdpIeeeAddrReq.u8RequestType = 0x00;
		sAplZdpIeeeAddrReq.u8StartIndex = 0x00;

		ZPS_teStatus eStatus = ZPS_eAplZdpIeeeAddrRequest(  hAPduInst,
		                                                    uDstAddr,
		                                                    FALSE,
		                                                    &u8TransactionSequenceNumber,
		                                                    &sAplZdpIeeeAddrReq
		                                                    );
		if (!eStatus)
		{
		    DBG_vPrintf(TRACE_IPD_NODE, "IEEE Lookup Sent: 0x%04x\n", s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress);
		}
		else
		{
		    DBG_vPrintf(TRACE_IPD_NODE, "IEEE Fail: - Error: 0x%02x\n", eStatus);
		    PDUM_eAPduFreeAPduInstance(hAPduInst);
		}

		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(2000), NULL);

		s_sDevice.eState = E_IEEE_ADDR_LOOKUP_RESPONSE;
	}
	else
	{
		/* Check the next entry in the array and request the IEEE address if necessary */
		u8MeterOfInterest++;
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vHandleIeeeLookupRespEvent
 *
 * DESCRIPTION:
 * Temporary workaround. Receive the meter's short address.
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleIeeeLookupRespEvent(ZPS_tsAfEvent sStackEvent)
{
	if (ZPS_EVENT_APS_ZDP_REQUEST_RESPONSE == sStackEvent.eType)
    {
		if (IEEE_RESP == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
		{
			if(!sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u8Status)
			{
				uint8 i;
				DBG_vPrintf(TRACE_IPD_NODE, "IEEE Lookup Resp: 0x%016llx\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u64IeeeAddrRemoteDev);

				for (i = 0 ; i < SE_NUMBER_OF_ENDPOINTS ; i++)
				{
					if (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u16NwkAddrRemoteDev == s_sDevice.asMeter[i].u16DestinationAddress)
					{
					    s_sDevice.asMeter[i].u64DestinationAddress = sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u64IeeeAddrRemoteDev;
					}
					if (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u16NwkAddrRemoteDev == s_sDevice.asPrice[i].u16DestinationAddress)
					{
					    s_sDevice.asPrice[i].u64DestinationAddress = sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u64IeeeAddrRemoteDev;
					}
				}

				/* Add the short and long address to the address map table before commencing secure communication */
				ZPS_eAplZdoAddAddrMapEntry(	sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u16NwkAddrRemoteDev,
											sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u64IeeeAddrRemoteDev);

				u8MeterOfInterest++;
				s_sDevice.eState = E_IEEE_ADDR_LOOKUP;
				OS_eActivateTask(APP_IPDTask);
			}
			else
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Addr Lookup Resp - Error: 0x%02x", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sIeeeAddrRsp.u8Status);
			}
		}
    }
	else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Addr Lookup Timeout, retrying...\n");
		s_sDevice.eState = E_IEEE_ADDR_LOOKUP;
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vSendBindRequest
 *
 * DESCRIPTION:
 * Sends out a bind request to the meter
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vSendBindRequest(void)
{
	uint8 u8SeqNum = 0;
	ZPS_tuAddress tuAddress;
	ZPS_tsAplZdpBindUnbindReq BindUnbindReq;

	BindUnbindReq.u8DstAddrMode = 0x03;
	BindUnbindReq.uAddressField.sExtended.u64DstAddress = ZPS_u64NwkNibGetExtAddr(ZPS_pvNwkGetHandle());
	BindUnbindReq.uAddressField.sExtended.u8DstEndPoint = IPD_BASE_LOCAL_EP;
	BindUnbindReq.u64SrcAddress = u64CoordinatorMac;
	tuAddress.u16Addr = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;

#ifdef CLD_TIME
	if (!s_sDevice.sEsp.bTimeBindSuccess)
	{
		BindUnbindReq.u8SrcEndpoint = s_sDevice.sEsp.u8TimeEndPoint;
		BindUnbindReq.u16ClusterId = GENERAL_CLUSTER_ID_TIME;
	}
	else
#endif
#ifdef CLD_MC
	if (!s_sDevice.sEsp.bMessageBindSuccess)
	{
		BindUnbindReq.u8SrcEndpoint = s_sDevice.sEsp.u8MessageEndPoint;
		BindUnbindReq.u16ClusterId = SE_CLUSTER_ID_MESSAGE;
	}
	else
#endif
#ifdef CLD_DRLC
    if (!s_sDevice.sEsp.bDrlcBindSuccess)
    {
        BindUnbindReq.u8SrcEndpoint = s_sDevice.sEsp.u8DrlcEndPoint;
        BindUnbindReq.u16ClusterId = SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL;
    }
    else
#endif
#ifdef CLD_PRICE
	if (!s_sDevice.sEsp.bPriceBindSuccess)
    {
	    if (u8MeterOfInterest >= NUMBER_OF_SUPPORTED_COMMODITY_TYPES )
        {
            /* All binds successful, continue starting up */
            u8MeterOfInterest = 0;
            s_sDevice.sEsp.bPriceBindSuccess = TRUE;
            OS_eActivateTask(APP_IPDTask);
            return;
        }
        else if (0xFFFF != s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress && !s_sDevice.asPrice[u8MeterOfInterest].bBindSuccess)
        {
   			BindUnbindReq.uAddressField.sExtended.u8DstEndPoint = IPD_BASE_LOCAL_EP + u8MeterOfInterest;
        	BindUnbindReq.u64SrcAddress =  ZPS_u64AplZdoLookupIeeeAddr(s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress);
			BindUnbindReq.u8SrcEndpoint = s_sDevice.asPrice[u8MeterOfInterest].u8PriceEndPoint;
			BindUnbindReq.u16ClusterId = SE_CLUSTER_ID_PRICE;
			tuAddress.u16Addr = s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress;
        }
        else
        {
            /* Check the next entry in the array and send out a bind request as necessary */
            u8MeterOfInterest++;
            OS_eActivateTask(APP_IPDTask);
            return;
        }
    }
	else
#endif
	{
		if (u8MeterOfInterest >= SE_NUMBER_OF_ENDPOINTS )
		{
			/* All binds successful, continue starting up */
			u8MeterOfInterest = 0;
			s_sDevice.eState = E_REQ_PRICE_KEY;
			OS_eActivateTask(APP_IPDTask);
			return;
		}
		else if (0xFFFF != s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress && !s_sDevice.asMeter[u8MeterOfInterest].bBindSuccess)
		{
			BindUnbindReq.u64SrcAddress =  ZPS_u64AplZdoLookupIeeeAddr(s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress);
			BindUnbindReq.u8SrcEndpoint = s_sDevice.asMeter[u8MeterOfInterest].u8MeterEndPoint;
			BindUnbindReq.u16ClusterId = SE_CLUSTER_ID_SIMPLE_METERING;
			BindUnbindReq.uAddressField.sExtended.u8DstEndPoint = IPD_BASE_LOCAL_EP + u8MeterOfInterest;
			tuAddress.u16Addr = s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress;
		}
		else
		{
			/* Check the next entry in the array and send out a bind request as necessary */
			u8MeterOfInterest++;
			OS_eActivateTask(APP_IPDTask);
			return;
		}
	}

	PDUM_thAPduInstance hAPduInst = APP_hAPduAllocateAPduInstance();

	if (hAPduInst == PDUM_INVALID_HANDLE)
	{
		return;
	}

	ZPS_teStatus eStatus = ZPS_eAplZdpBindUnbindRequest(	hAPduInst,
															tuAddress,
															FALSE,
															&u8SeqNum,
															TRUE,
															&BindUnbindReq
															);

	if (eStatus)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Bind Request - Cluster ID: 0x%04x, ERR: 0x%02x \n", BindUnbindReq.u16ClusterId, eStatus);
		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(2000), NULL);
		PDUM_eAPduFreeAPduInstance(hAPduInst);
	}
	else
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Bind Request - Cluster ID: 0x%04x\n", BindUnbindReq.u16ClusterId);
		s_sDevice.eState = E_BIND_RESP;
		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(2000), NULL);
	}
}


/****************************************************************************
 *
 * NAME: vHandleBindRespEvent
 *
 * DESCRIPTION:
 * Handles the ZDP response from the bind request
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleBindRespEvent(ZPS_tsAfEvent sStackEvent)
{
	if (ZPS_EVENT_APS_ZDP_REQUEST_RESPONSE == sStackEvent.eType)
	{
		if (BIND_RESP == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
		{
			if((sStackEvent.uEvent.sApsZdpEvent.uZdpData.sBindRsp.u8Status ==  ZPS_E_SUCCESS) ||
			        (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sBindRsp.u8Status ==  ZPS_APL_APS_E_TABLE_FULL) )
			{
#ifdef CLD_TIME
				if (!s_sDevice.sEsp.bTimeBindSuccess)
				{
					DBG_vPrintf(TRACE_IPD_NODE, "Time Bind Response\n");
					s_sDevice.sEsp.bTimeBindSuccess = TRUE;
				}
				else
#endif
#ifdef CLD_MC
				if (!s_sDevice.sEsp.bMessageBindSuccess)
				{
					DBG_vPrintf(TRACE_IPD_NODE, "Message Bind Response\n");
					s_sDevice.sEsp.bMessageBindSuccess = TRUE;
				}
				else
#endif
#ifdef CLD_DRLC
                if (!s_sDevice.sEsp.bDrlcBindSuccess)
                {
                    DBG_vPrintf(TRACE_IPD_NODE, "DRLC Bind Response\n");
                    s_sDevice.sEsp.bDrlcBindSuccess = TRUE;
                }
                else
#endif
#ifdef CLD_PRICE
				if (!s_sDevice.sEsp.bPriceBindSuccess)
                {
                    DBG_vPrintf(TRACE_IPD_NODE, "Price Bind Response\n");
                    s_sDevice.asPrice[u8MeterOfInterest].bBindSuccess = TRUE;

                    /* Check the next entry in the array and send out a bind request as necessary */
                    u8MeterOfInterest++;
                }
				else
#endif
				{
					DBG_vPrintf(TRACE_IPD_NODE, "SM Bind Response\n");
					s_sDevice.asMeter[u8MeterOfInterest].bBindSuccess = TRUE;

					/* Check the next entry in the array and send out a bind request as necessary */
					u8MeterOfInterest++;
				}
				s_sDevice.eState = E_BIND_REQ;
				OS_eActivateTask(APP_IPDTask);
			}
			else
			{
				DBG_vPrintf(TRACE_IPD_NODE, "Bind Error: 0x%x\n", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sBindRsp.u8Status);
			}
		}
	}
	else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Bind Req Timeout, retrying...\n");
		s_sDevice.eState = E_BIND_REQ;
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vHandlePriceKeyRequest
 *
 * DESCRIPTION:
 * Request the meter's APS key
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandlePriceKeyRequest(ZPS_tsAfEvent sStackEvent)
{
	if (u8MeterOfInterest >= NUMBER_OF_SUPPORTED_COMMODITY_TYPES )
	{
		/* All Price APS keys obtained, continue starting up */
		u8MeterOfInterest = 0;
		s_sDevice.eState = E_REQ_METER_KEY;
		OS_eActivateTask(APP_IPDTask);
	}
	/* If the current record is the ESP */
	else if (0x0000 == s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress)
    {
        /* No need to request the another key from the ESP */
        s_sDevice.asPrice[u8MeterOfInterest].bValidKey = TRUE;

        /* Check the next entry in the array and request an APS key as necessary */
        u8MeterOfInterest++;
        OS_eActivateTask(APP_IPDTask);
    }
	/* If the current record is a valid entry in the array */
	else if ((0xFFFF != s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress) && (!s_sDevice.asPrice[u8MeterOfInterest].bValidKey))
	{
		ZPS_teStatus eStatus = ZPS_eAplZdoRequestKeyReq(2, ZPS_u64AplZdoLookupIeeeAddr(s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress));
		if (!eStatus)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Price Key Request Sent: 0x%04x\n", s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress);
			s_sDevice.eState = E_WAIT_PRICE_KEY;
		}
		else
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Price Key Request Failed - Error 0x%02x\n", eStatus);
		}

		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(2000), NULL);
	}
	else
	{
		/* Check the next entry in the array and request an APS key as necessary */
		u8MeterOfInterest++;
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vHandleWaitPriceKey
 *
 * DESCRIPTION:
 * Wait for the meter's APS key
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleWaitPriceKey(ZPS_tsAfEvent sStackEvent)
{
	if (sStackEvent.eType == ZPS_EVENT_ZDO_LINK_KEY)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Price Key Received for 0x%016llx\n",sStackEvent.uEvent.sZdoLinkKeyEvent.u64IeeeLinkAddr);

		if (sStackEvent.uEvent.sZdoLinkKeyEvent.u64IeeeLinkAddr == s_sDevice.asPrice[u8MeterOfInterest].u64DestinationAddress)
		{
		    s_sDevice.asPrice[u8MeterOfInterest].bValidKey = TRUE;
		}

		/* Check the next entry in the array and request an APS key as necessary */
		u8MeterOfInterest++;

		s_sDevice.eState = E_REQ_PRICE_KEY;
		OS_eActivateTask(APP_IPDTask);

	}
	else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Price Key Request Timeout, retrying...\n");
		s_sDevice.eState = E_REQ_PRICE_KEY;
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vHandleMeterKeyRequest
 *
 * DESCRIPTION:
 * Request the meter's APS key
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleMeterKeyRequest(ZPS_tsAfEvent sStackEvent)
{
	if (u8MeterOfInterest >= SE_NUMBER_OF_ENDPOINTS )
	{
		/* All Meter APS keys obtained, continue starting up */
		u8MeterOfInterest = 0;
		s_sDevice.eState = E_READ_METERING_DEVICE_TYPE;
		OS_eActivateTask(APP_IPDTask);
	}
	/* If the current record is the ESP */
	else if (0x0000 == s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress)
	{
		/* No need to request the another key from the ESP */
		s_sDevice.asMeter[u8MeterOfInterest].bValidKey = TRUE;

		/* Check the next entry in the array and request an APS key as necessary */
		u8MeterOfInterest++;
		OS_eActivateTask(APP_IPDTask);
	}
	/* If the current record is a valid entry in the array */
	else if ((0xFFFF != s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress) && (!s_sDevice.asMeter[u8MeterOfInterest].bValidKey))
	{
		ZPS_teStatus eStatus = ZPS_eAplZdoRequestKeyReq(2, ZPS_u64AplZdoLookupIeeeAddr(s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress));
		if (!eStatus)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Meter Key Request Sent: 0x%04x\n", s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress);
			s_sDevice.eState = E_WAIT_METER_KEY;
		}
		else
		{
			DBG_vPrintf(TRACE_IPD_NODE, "Meter Key Request Failed - Error 0x%02x\n", eStatus);
		}

		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(2000), NULL);
	}
	else
	{
		/* Check the next entry in the array and request an APS key as necessary */
		u8MeterOfInterest++;
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vHandleWaitMeterKey
 *
 * DESCRIPTION:
 * Wait for the meter's APS key
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleWaitMeterKey(ZPS_tsAfEvent sStackEvent)
{
	if (sStackEvent.eType == ZPS_EVENT_ZDO_LINK_KEY)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Meter Key Received for 0x%016llx\n",sStackEvent.uEvent.sZdoLinkKeyEvent.u64IeeeLinkAddr);

		if (sStackEvent.uEvent.sZdoLinkKeyEvent.u64IeeeLinkAddr == s_sDevice.asMeter[u8MeterOfInterest].u64DestinationAddress)
		{
		    s_sDevice.asMeter[u8MeterOfInterest].bValidKey = TRUE;
		}

		/* Check the next entry in the array and request an APS key as necessary */
		u8MeterOfInterest++;

		s_sDevice.eState = E_REQ_METER_KEY;
		OS_eActivateTask(APP_IPDTask);

	}
	else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Meter Key Request Timeout, retrying...\n");
		s_sDevice.eState = E_REQ_METER_KEY;
		OS_eActivateTask(APP_IPDTask);
	}
}


/****************************************************************************
 *
 * NAME: vHandleReadMeteringDeviceType
 *
 * DESCRIPTION:
 * Sends an initiate key establishment command to the Meter
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleReadMeteringDeviceType(void)
{
    uint8   u8TransactionSequenceNumber;
    teZCL_Status eStatus;

    if (u8MeterOfInterest >= SE_NUMBER_OF_ENDPOINTS )
    {
        /* All metering device types obtained, continue starting up */
        u8MeterOfInterest = 0;
        s_sDevice.eState = E_READ_COMMODITY_TYPE;
        OS_eActivateTask(APP_IPDTask);
    }
    else if ((0xFFFF != s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress) && (0xFF == s_sDevice.asMeter[u8MeterOfInterest].u8MeterDeviceType))
    {
        tsZCL_Address sAddress;
        uint16 u16AttrId = E_CLD_SM_ATTR_ID_METERING_DEVICE_TYPE;
        sAddress.eAddressMode = E_ZCL_AM_SHORT;
        sAddress.uAddress.u16DestinationAddress = s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress;
        eStatus = eZCL_SendReadAttributesRequest(IPD_BASE_LOCAL_EP,
                                                 s_sDevice.asMeter[u8MeterOfInterest].u8MeterEndPoint,
                                                 SE_CLUSTER_ID_SIMPLE_METERING,
                                                 FALSE,
                                                 &sAddress,
                                                 &u8TransactionSequenceNumber,
                                                 1,
                                                 FALSE,
                                                 0,
                                                 &u16AttrId);
        if(eStatus != E_ZCL_SUCCESS)
        {
            DBG_vPrintf(TRACE_IPD_NODE, "Meter Type Request Error: 0x%02x", eStatus);
        }
        else
        {
        	DBG_vPrintf(TRACE_IPD_NODE, "Meter Type Request Sent: 0x%04x\n", s_sDevice.asMeter[u8MeterOfInterest].u16DestinationAddress);
        }

        OS_eStopSWTimer(APP_RestartTimer);
        OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(3000), NULL);

        s_sDevice.eState = E_WAIT_METERING_DEVICE_TYPE;
        OS_eActivateTask(APP_IPDTask);
    }
    else
    {
        /* Check the next entry in the array and request metering device type as necessary */
        u8MeterOfInterest++;
        OS_eActivateTask(APP_IPDTask);
    }
}


/****************************************************************************
 *
 * NAME: vHandleWaitMeteringDeviceType
 *
 * DESCRIPTION:
 * Wait for the meter's APS key
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleWaitMeteringDeviceType(void)
{
    if (0xFF != s_sDevice.asMeter[u8MeterOfInterest].u8MeterDeviceType)
    {
        DBG_vPrintf(TRACE_IPD_NODE, "Metering device type Received 0x%02x\n", s_sDevice.asMeter[u8MeterOfInterest].u8MeterDeviceType);

        /* Check the next entry in the array and request another metering device type as necessary */
        u8MeterOfInterest++;
        s_sDevice.eState = E_READ_METERING_DEVICE_TYPE;
        OS_eActivateTask(APP_IPDTask);
    }
    else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RestartTimer))
    {
        DBG_vPrintf(TRACE_IPD_NODE, "Metering device type request timed out, retrying\n");
        s_sDevice.eState = E_READ_METERING_DEVICE_TYPE;
        OS_eActivateTask(APP_IPDTask);
    }
}


/****************************************************************************
 *
 * NAME: vHandleReadCommodityType
 *
 * DESCRIPTION:
 * Sends an initiate key establishment command to the Meter
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleReadCommodityType(void)
{
    uint8   u8TransactionSequenceNumber;
    teZCL_Status eStatus;

    if ( u8MeterOfInterest >= NUMBER_OF_SUPPORTED_COMMODITY_TYPES )
    {
        u8MeterOfInterest = 0;
        vFinaliseStartup();
    }
    else if ((0xFFFF != s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress) && (0xFF == s_sDevice.asPrice[u8MeterOfInterest].u8CommodityType))
    {
        tsZCL_Address sAddress;
        uint16 u16AttrId = E_CLD_P_ATTR_COMMODITY_TYPE;
        sAddress.eAddressMode = E_ZCL_AM_SHORT;
        sAddress.uAddress.u16DestinationAddress = s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress;
        eStatus = eZCL_SendReadAttributesRequest(IPD_BASE_LOCAL_EP,
                                                 s_sDevice.asPrice[u8MeterOfInterest].u8PriceEndPoint,
                                                 SE_CLUSTER_ID_PRICE,
                                                 FALSE,
                                                 &sAddress,
                                                 &u8TransactionSequenceNumber,
                                                 1,
                                                 FALSE,
                                                 0,
                                                 &u16AttrId);
        if(eStatus != E_ZCL_SUCCESS)
        {
            DBG_vPrintf(TRACE_IPD_NODE, "Commodity Type Request Error: 0x%02x", eStatus);
        }
        else
        {
        	DBG_vPrintf(TRACE_IPD_NODE, "Commodity Type Request Sent, Addr: 0x%04x, EP: 0x%02x\n", s_sDevice.asPrice[u8MeterOfInterest].u16DestinationAddress, s_sDevice.asPrice[u8MeterOfInterest].u8PriceEndPoint);
        }

        OS_eStopSWTimer(APP_RestartTimer);
        OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(3000), NULL);

        s_sDevice.eState = E_WAIT_COMMODITY_TYPE;
        OS_eActivateTask(APP_IPDTask);
    }
    else
    {
        /* Check the next entry in the array and request metering device type as necessary */
        u8MeterOfInterest++;
        OS_eActivateTask(APP_IPDTask);
    }
}


/****************************************************************************
 *
 * NAME: vHandleWaitCommodityType
 *
 * DESCRIPTION:
 * Wait for the meter's APS key
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE OVERLAY(DISCOVERY) void vHandleWaitCommodityType(void)
{
    if (0xFF != s_sDevice.asPrice[u8MeterOfInterest].u8CommodityType)
    {
        DBG_vPrintf(TRACE_IPD_NODE, "Commodity type Received 0x%02x\n",s_sDevice.asPrice[u8MeterOfInterest].u8CommodityType);

        /* Check the next entry in the array and request an APS key as necessary */
        u8MeterOfInterest++;
        s_sDevice.eState = E_READ_COMMODITY_TYPE;
        OS_eActivateTask(APP_IPDTask);

    }
    else if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RestartTimer))
    {
        DBG_vPrintf(TRACE_IPD_NODE, "Commodity type request timed out, retrying\n");
        s_sDevice.eState = E_READ_COMMODITY_TYPE;
        OS_eActivateTask(APP_IPDTask);
    }
}


/****************************************************************************
 *
 * NAME: vFinaliseStartup
 *
 * DESCRIPTION:
 * Finalise starting up and move to the running state
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE void vFinaliseStartup(void)
{
	uint8 i, j;

	OS_eStopSWTimer(APP_RestartTimer);

	bRejoining = FALSE;
	vScheduleSleep(SLEEP_PERIOD);												// Schedule the next wake up event
	u8TotalNumberOfDiscoveredMeters = 0;
	/* Scan the array for valid entries */
	for (i = 0 ; i < SE_NUMBER_OF_ENDPOINTS ; i++)
	{
		if (TRUE == (s_sDevice.asMeter[i].bValidKey & s_sDevice.asMeter[i].bBindSuccess))
		{
			u8TotalNumberOfDiscoveredMeters++;
		}

		/* Link each discovered meter to the relevant price server */
		for (j = 0 ; j < NUMBER_OF_SUPPORTED_COMMODITY_TYPES; j++)
		{
			if (( s_sDevice.asPrice[j].u8CommodityType == s_sDevice.asMeter[i].u8MeterDeviceType ) ||
			   (( s_sDevice.asPrice[j].u8CommodityType + 0x7F ) == s_sDevice.asMeter[i].u8MeterDeviceType ))
			{
				DBG_vPrintf(TRACE_IPD_NODE, "\r\n*******************************************************");
				DBG_vPrintf(TRACE_IPD_NODE, "Commodity Match Price Index:%d Type:%d, Meter Index:%d Type:%d   Addr: %02x\n", j, s_sDevice.asPrice[j].u8CommodityType ,i, s_sDevice.asMeter[i].u8MeterDeviceType, s_sDevice.asMeter[i].u16DestinationAddress );
				DBG_vPrintf(TRACE_IPD_NODE, "\r\n*******************************************************");

				s_sDevice.asMeter[i].u8PriceIndex = j;
				break;
			}
		}
	}
	DBG_vPrintf(TRACE_IPD_NODE, "Number of discovered meters: %i\n", u8TotalNumberOfDiscoveredMeters);

	/* Change to the running screen */
	APP_vDisplaySetState(APP_E_DISPLAY_STATE_CURRENT_KW);
	APP_vDisplayUpdate();
#ifdef CLD_OTA
	vOtaInitialisation();
#endif

	s_sDevice.eState = E_RUNNING;
	eRunningState = E_INIT;

	/* Save the state in the flash for a reset */
	PDM_vSaveRecord(&s_sDevicePDDesc);

	OS_eActivateTask(APP_IPDTask);
}


#ifdef CLD_OTA
/****************************************************************************
 *
 * NAME: vOtaInitialisation
 *
 * DESCRIPTION:
 * Initialise OTA code
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE void vOtaInitialisation(void)
{
	if (s_sDevice.bOtaDiscComplete)
	{
		tsNvmDefs sNvmDefs;
		teZCL_Status eZCL_Status;
#if (OTA_MAX_IMAGES_PER_ENDPOINT == 3)
        uint8 u8StartSector[3] = {0,2,4};
#else
#ifdef JENNIC_CHIP_FAMILY_JN516x
        uint8 u8StartSector[1] = {0};
#else
        uint8 u8StartSector[2] = {0,3};
#endif
#endif
		eZCL_Status = eOTA_UpdateClientAttributes(IPD_BASE_LOCAL_EP);
		if (eZCL_Status != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "eOTA_UpdateClientAttributes returned error 0x%x", eZCL_Status);
		}

		sNvmDefs.u32SectorSize = 64*1024;
		sNvmDefs.u8FlashDeviceType = E_FL_CHIP_AUTO;
		vOTA_FlashInit(NULL,&sNvmDefs);
#ifdef JENNIC_CHIP_FAMILY_JN516x
		eZCL_Status = eOTA_AllocateEndpointOTASpace(
				IPD_BASE_LOCAL_EP,
				u8StartSector,
				OTA_MAX_IMAGES_PER_ENDPOINT,
				4,
				FALSE,
				au8CAPublicKey);
#else
		eZCL_Status = eOTA_AllocateEndpointOTASpace(
				IPD_BASE_LOCAL_EP,
				u8StartSector,
				OTA_MAX_IMAGES_PER_ENDPOINT,
				3,
				FALSE,
				au8CAPublicKey);
#endif
		if (eZCL_Status != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "eAllocateEndpointOTASpace returned error 0x%x", eZCL_Status);
		}

		eZCL_Status = eOTA_SetServerAddress(
				s_sDevice.sEsp.u8OtaEndPoint,
				ZPS_u64AplZdoLookupIeeeAddr(s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress),
				s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress
				);
		if (eZCL_Status != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_IPD_NODE, "eOTA_SetServerAddress returned error 0x%x", eZCL_Status);
		}

#if (OTA_MAX_CO_PROCESSOR_IMAGES != 0)
	/* If we want to allow upgrade of co-processor image as well, we need to call
	 * eOTA_UpdateCoProcessorOTAHeader() to register header of the image that co-processor
	 * accepts. This will be used to identify if the image is for the co-processor or the
	 * client node.
	 */
    tsOTA_CoProcessorOTAHeader sOTACoProcessorHeader;
    sOTACoProcessorHeader.sOTA_ImageHeader[0].u32FileIdentifier = OTA_FILE_IDENTIFIER;
    sOTACoProcessorHeader.sOTA_ImageHeader[0].u16ManufacturerCode = MANUFACTURER_CODE;
    sOTACoProcessorHeader.sOTA_ImageHeader[0].u32FileVersion = COPROCESSOR_APPLICATION_VERSION;
    sOTACoProcessorHeader.sOTA_ImageHeader[0].u16ImageType = COPROCESSOR_IMAGE_TYPE;

    eZCL_Status = eOTA_UpdateCoProcessorOTAHeader(&sOTACoProcessorHeader,TRUE);
    if (eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "eOTA_UpdateCoProcessorOTAHeader returned error 0x%x", eZCL_Status);
	}
#endif

#if 0

		App_QueryNextImageRequest();

		tsZCL_Address sDestinationAddress;
		sDestinationAddress.eAddressMode = E_ZCL_AM_SHORT;
		sDestinationAddress.uAddress.u16DestinationAddress = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;

		tsOTA_QueryImageRequest sOTA_QueryImageRequest;
		sOTA_QueryImageRequest.u32CurrentFileVersion = APPLICATION_VERSION;
		//sOTA_QueryImageRequest.u16HardwareVersion = N/A;
		sOTA_QueryImageRequest.u16ImageType = IMAGE_TYPE;
		sOTA_QueryImageRequest.u16ManufacturerCode = MANUFACTURER_CODE;
		sOTA_QueryImageRequest.u8FieldControl = 0;

		uint8 u8QueryStatus = eOTA_ClientQueryNextImageRequest(
				IPD_BASE_LOCAL_EP,
				s_sDevice.sEsp.u8OtaEndPoint,
				&sDestinationAddress,
				&sOTA_QueryImageRequest);

		DBG_vPrintf(TRACE_IPD_NODE, "eOTA_ClientQueryNextImageRequest returned 0x%x", u8QueryStatus);
#endif
	}
}
#endif


/****************************************************************************
 *
 * NAME: APP_hAPduAllocateAPduInstance
 *
 * DESCRIPTION:
 * Allocate a pdu, and start the restart timer for 2 seconds if fail
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PUBLIC PDUM_thAPduInstance APP_hAPduAllocateAPduInstance( void )
{
	PDUM_thAPduInstance hAPduInst = PDUM_hAPduAllocateAPduInstance(apduZDP);

	if (hAPduInst == PDUM_INVALID_HANDLE)
	{
		DBG_vPrintf(TRACE_IPD_NODE, "Allocate PDU ERR:\n");

		/* Resource issue, try again in 2 seconds */
		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(2000), NULL);

	}
	return(hAPduInst);
}

/****************************************************************************
 *
 * NAME: ResetServiceDiscovery
 *
 * DESCRIPTION:
 * Reset the service discovery parameters
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PUBLIC void ResetServiceDiscovery(void)
{
	uint8 i;
	/* Initialise the discovered meters structure */
	for (i = 0 ; i < SE_NUMBER_OF_ENDPOINTS ; i++)
	{
		s_sDevice.asMeter[i].u16DestinationAddress = 0xFFFF;
		s_sDevice.asMeter[i].u64DestinationAddress = 0;
		s_sDevice.asMeter[i].bValidKey = FALSE;
		s_sDevice.asMeter[i].u8MeterEndPoint = 0xFF;
		s_sDevice.asMeter[i].u8MeterDeviceType = 0xFF;
		s_sDevice.asMeter[i].u8PriceIndex = 0;
		s_sDevice.asMeter[i].bBindSuccess = FALSE;
		s_sDevice.asMeter[i].bInstantaneousDemandSupported = FALSE;
	}
	DBG_vPrintf(TRACE_IPD_NODE, "Local Metering Data Cleared\n");


	#ifdef CLD_PRICE

	for (i = 0 ; i < NUMBER_OF_SUPPORTED_COMMODITY_TYPES ; i++)
	{
		s_sDevice.asPrice[i].u16DestinationAddress = 0xFFFF;
		s_sDevice.asPrice[i].u64DestinationAddress = 0;
		s_sDevice.asPrice[i].bValidKey = FALSE;
		s_sDevice.asPrice[i].u8PriceEndPoint = 0xFF;
		s_sDevice.asPrice[i].u8CommodityType = 0xFF;
		s_sDevice.asPrice[i].bBindSuccess = FALSE;
	}
	s_sDevice.bPriceDiscComplete = FALSE;
	s_sDevice.sEsp.bPriceBindSuccess = FALSE;

	#endif

	#ifdef CLD_TIME
		s_sDevice.bTimeDiscComplete = FALSE;
	#endif

	#ifdef CLD_MC
		s_sDevice.bMessageDiscComplete = FALSE;
	#endif

	#ifdef CLD_DRLC
		s_sDevice.bDrlcDiscComplete = FALSE;
	#endif

	#ifdef CLD_OTA
		s_sDevice.bOtaDiscComplete = FALSE;
	#endif

	DBG_vPrintf(TRACE_IPD_NODE, "Local Price Data Cleared\n");

}


/****************************************************************************
 *
 * NAME: HandleUnknownDisplayState
 *
 * DESCRIPTION:
 * Handle unknown display states
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PUBLIC void HandleUnknownDisplayState(void)
{
	if(s_sDevice.eState == E_RUNNING)
	{
		vFinaliseStartup();
	}
}


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
