/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_meter_node.c
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        SE Meter - Main Source File
 *
 * $HeadURL:
 *
 * $Revision: 6194 $
 *
 * $LastChangedBy: jpenn $
 *
 * $LastChangedDate: 2010-06-16 17:30:01 +0100 (Wed, 16 Jun 2010) $
 *
 * $Id: app_meter_node.c 6194 2010-06-16 16:30:01Z jpenn $
 *
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include <LedControl.h>
#include "dbg.h"
#include "os.h"
#include "os_gen.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pwrm.h"
#include "zps_apl_af.h"
#include "zps_apl_zdp.h"
#include "zps_apl_aib.h"
#include "AppHardwareAPI.h"
#include "zcl.h"
#include "pdm.h"
#include "zps_apl_af.h"
#include "zps_apl_aib.h"
#include "zps_nwk_nib.h"
#include "zps_nwk_pub.h"

/* Application Includes */
#include <string.h>
#include "app_smartenergy_demo.h"
#include "app_event_handler.h"
#include "app_timer_driver.h"
#include "app_zcl_task.h"
#include "app_meter_node.h"
#include "app_buttons.h"
#include "Time.h"
#include "Utilities.h"
#include "zcl_options.h"
#include "AppHardwareAPI.h"
#include "Printf.h"
#ifdef RADIO_RECALIBRATION
#include "recal.h"
#endif


#include "GenericBoard.h"


/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_METER_NODE
#define TRACE_METER_NODE TRUE
#endif

#ifndef TRACE_METER_NODE_HIGH
#define TRACE_METER_NODE_HIGH TRUE
#endif

#define MAX_SINGLE_CHANNEL_NETWORKS		8

//#define ETSI																	// Remove comments to limit the module to +8dB for ETSI compliance

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vHandleConfigEvent(APP_tsEvent sAppEvent);
PRIVATE void vHandleStartupEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleNetworkDiscoveryEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleNetworkJoinEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleRescanEvent(ZPS_tsAfEvent sStackEvent);
PRIVATE void vJoinedNetwork(void);
PRIVATE void vHandleGetMatchDesc(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleWaitMatchDesc(ZPS_tsAfEvent sStackEvent);
PRIVATE void vHandleBindRespEvent(ZPS_tsAfEvent sStackEvent);

#ifdef CLD_KEY_ESTABLISHMENT
PRIVATE void APP_vStartKeyEstablishment(void);
PRIVATE void APP_vWaitKeyEstablishment(void);
#endif

PRIVATE bool APP_bMaxJoinAttempts(void);
PRIVATE void vLeaveNWK(void);
PRIVATE void APP_vWaitLeave(ZPS_tsAfEvent sStackEvent);

PRIVATE void vSendBindRequest(void);
PRIVATE void vClearDiscNT(void);
PRIVATE void vJoinStoredNWK(void);
PRIVATE void vSaveDiscoveredNWKS(ZPS_tsAfEvent sStackEvent);
PRIVATE void vDisplayDiscoveredNWKS(void);
PRIVATE void vUnhandledEvent(teState eState, ZPS_teAfEventType eType);

PRIVATE void vHandleMaxJoinRequests(APP_tsEvent sAppEvent);

#ifdef PDM_EEPROM
PUBLIC uint8 u8PDM_CalculateFileSystemCapacity(void);
PUBLIC uint8 u8PDM_GetFileSystemOccupancy(void);
#endif

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC	PDM_tsRecordDescriptor	s_sDevicePDDesc;
PUBLIC	uint32					u32LastTimeUpdate;
PUBLIC 	tsDevice 				s_sDevice;
PUBLIC	bool					bRejoining = FALSE;
PUBLIC 	uint8					u8NetworkDiscoveryAttempts = 0;

PUBLIC	uint64					u64CoordinatorMac = 0x00;

#ifdef JENNIC_CHIP_FAMILY_JN516x
#if defined(PRODUCTION_CERTS) || defined(CLD_OTA)
	extern  uint8 	au8MacAddress[];
#else
	uint8 	au8MacAddress[] ={
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04
	};
#endif
#endif
/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE	uint16 u16ClusterToBind = SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL;

PRIVATE ZPS_tsNwkNetworkDescr tsDiscovedNWKList[MAX_SINGLE_CHANNEL_NETWORKS];
PRIVATE uint8	u8DiscovedNWKListCount = 0;
PRIVATE uint8	u8DiscovedNWKJoinCount = 0;
PRIVATE uint8	u8ChildOfInterest = 0;

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern uint8 s_au8LnkKeyArray[16];
extern tsSE_MeterDevice sMeter;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_MeterTask
 *
 * DESCRIPTION:
 * Main State Machine for Meter Node
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_MeterTask)
{
	ZPS_tsAfEvent sStackEvent;
    APP_tsEvent sAppEvent;

    sStackEvent.eType = ZPS_EVENT_NONE;
    sAppEvent.eType = APP_E_EVENT_NONE;

	if( OS_E_OK  == OS_eCollectMessage(APP_msgZpsEvents, &sStackEvent) )
	{
		DBG_vPrintf(TRACE_METER_NODE, "Stack Evt: 0x%02x\n", sStackEvent.eType);

		if (sStackEvent.eType == ZPS_EVENT_ERROR)
		{
			DBG_vPrintf(TRACE_METER_NODE, "ZigBee ERR: %x \r\n", sStackEvent.uEvent.sAfErrorEvent.eError);
		}
	}
	/* If there is a 'failed to join' event and the application state is not in joining/discovery mode the stack must have triggered a rejoin */
	else if ((ZPS_EVENT_NWK_FAILED_TO_JOIN == sStackEvent.eType) &&
			((s_sDevice.eState != E_JOINING_NETWORK) &&
			(s_sDevice.eState != E_DISCOVERING_NETWORKS)))
	{
		/* Stack rejoin failed, force the app to take over and continue retrying */
		DBG_vPrintf(TRACE_METER_NODE, "Stack failed to rejoin\r\n");
		bRejoining = TRUE;
		s_sDevice.eState = E_JOINING_NETWORK;
		PDM_vSaveRecord(&s_sDevicePDDesc);
	}

	/* No stack event at this point, check for app events */
	if (ZPS_EVENT_NONE == sStackEvent.eType)
	{
		if( OS_E_OK  == OS_eCollectMessage(APP_Events, &sAppEvent) )
		{
			DBG_vPrintf(TRACE_METER_NODE, "App Event: %x \r\n", sAppEvent.eType);
		}
	}
	else
	{
		DBG_vPrintf(TRACE_METER_NODE, "No Event \r\n" );
	}

	/* If the time has not been updated for 24 hrs and is not currently being updated... */
	if ((u32ZCL_GetUTCTime() >= u32LastTimeUpdate + SECONDS_IN_A_DAY) && (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_UTC_Timer)))
	{
		vZCL_ClearTimeHasBeenSynchronised();									// ... invalidate the time
		DBG_vPrintf(TRACE_METER_NODE, "Invalidate Time\n");
	}

	/* The main state machine for the Meter Application */
	switch (s_sDevice.eState)
	{
		case E_CONFIG:
			vHandleConfigEvent(sAppEvent);
		break;

		case E_START_NETWORK:
			/* Start the network as an End Device and start network discovery */
			DBG_vPrintf(TRACE_METER_NODE, "E_START_NETWORK");

			if (!APP_bMaxJoinAttempts())
			{
				vHandleStartupEvent(sStackEvent);
			}
			else
			{
				vHandleMaxJoinRequests(sAppEvent);
			}
		break;

		case E_DISCOVERING_NETWORKS:
			/* Handle the network discovery process */
			DBG_vPrintf(TRACE_METER_NODE, "E_DISCOVERING_NETWORKS\r\n");
			vHandleNetworkDiscoveryEvent(sStackEvent);
		break;

		case E_JOINING_NETWORK:
			DBG_vPrintf(TRACE_METER_NODE, "E_JOINING_NETWORK\r\n");
			vHandleNetworkJoinEvent(sStackEvent);
		break;

		case E_RESCAN:
			DBG_vPrintf(TRACE_METER_NODE, "E_RESCAN\r\n");
			if (!APP_bMaxJoinAttempts())
			{
				vHandleRescanEvent(sStackEvent);
			}
		break;

		case E_SEND_MATCH:
			vHandleGetMatchDesc(sStackEvent);
		break;

		case E_WAIT_MATCH:
			vHandleWaitMatchDesc(sStackEvent);
		break;

		case E_STATE_START_KEY_ESTABLISHMENT:
			APP_vStartKeyEstablishment();
		break;

		case E_STATE_WAIT_KEY_ESTABLISHMENT:
			APP_vWaitKeyEstablishment();
		break;

		case E_STATE_WAIT_LEAVE:
			APP_vWaitLeave(sStackEvent);
		break;

		case E_BIND_REQ:
			DBG_vPrintf(TRACE_METER_NODE, "Bind Req\n");
			vSendBindRequest();
		break;

		case E_BIND_RESP:
			DBG_vPrintf(TRACE_METER_NODE, "Bind Resp\n");
			vHandleBindRespEvent(sStackEvent);
		break;

		case E_RUNNING:
			DBG_vPrintf(TRACE_METER_NODE, "Running\n");
            vHandleRunningEvent(sStackEvent, sAppEvent);
		break;

		default:
			vUnhandledEvent(s_sDevice.eState,sStackEvent.eType);
		break;
	}

	/* Catch any un-handled data indications and free the apdu */
	if (ZPS_EVENT_APS_DATA_INDICATION == sStackEvent.eType)
	{
		PDUM_eAPduFreeAPduInstance(sStackEvent.uEvent.sApsDataIndEvent.hAPduInst);
	}
}


/****************************************************************************
 *
 * NAME: APP_InitiateRejoin
 *
 * DESCRIPTION:
 * Prepare the application and initiate a rejoin
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_InitiateRejoin)
{
	ZPS_teStatus eStatus = ZPS_eAplZdoRejoinNetwork();							// Tell the stack to initiate a rejoin
	if (ZPS_E_SUCCESS != eStatus)
	{
		/* Stack not currently able to initiate
		 * a rejoin, back off and try again later */
		DBG_vPrintf(TRACE_METER_NODE, "Rejoin Error: %x, stack may already be rejoining\n", eStatus);
		OS_eStartSWTimer(APP_RejoinTimer, APP_TIME_MS(1000), NULL);
	}
	else
	{
		bRejoining = TRUE;															// Set the rejoin flag
		s_sDevice.eState = E_JOINING_NETWORK;										// Set the state machine to handle a rejoin event
		PDM_vSaveRecord(&s_sDevicePDDesc);
	}
}


/****************************************************************************
 *
 * NAME: APP_AgeOutChildren
 *
 * DESCRIPTION:
 * Cycles through the device's children on a context restore to find out if
 * they now have a different parent
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_AgeOutChildren)
{
	ZPS_tsNwkNib * thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());
	uint8 i;

	for(i = u8ChildOfInterest ; i < thisNib->sTblSize.u16NtActv ; i++)
	{
		if (ZPS_NWK_NT_AP_RELATIONSHIP_CHILD == thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u2Relationship)
		{
			/* Child found in the neighbour table, send out a address request.
			 * If anyone responds we know to age out the child */
			u8ChildOfInterest = i;

			PDUM_thAPduInstance hAPduInst;
			hAPduInst = PDUM_hAPduAllocateAPduInstance(apduZDP);

			if (hAPduInst == PDUM_INVALID_HANDLE)
			{
				DBG_vPrintf(TRACE_METER_NODE, "IEEE Address Request - PDUM_INVALID_HANDLE\n");
			}
			else
			{
				uint8 u8TransactionSequenceNumber;

				/* Broadcast to all Rx-On-When-Idle devices */
				ZPS_tuAddress uAddress;
				uAddress.u16Addr = 0xFFFD;

				ZPS_tsAplZdpNwkAddrReq sAplZdpNwkAddrReq;
				sAplZdpNwkAddrReq.u64IeeeAddr = thisNib->sTbl.psNtActv[u8ChildOfInterest].u64ExtAddr;
				sAplZdpNwkAddrReq.u8RequestType = 0;

				DBG_vPrintf(TRACE_METER_NODE, "Child found in NT, sending route request: 0x%04x\n", thisNib->sTbl.psNtActv[u8ChildOfInterest].u16NwkAddr);
				ZPS_teStatus eStatus = ZPS_eAplZdpNwkAddrRequest(	hAPduInst,
																	uAddress,
																	FALSE,
																	&u8TransactionSequenceNumber,
																	&sAplZdpNwkAddrReq
																	);

				if (eStatus)
				{
					DBG_vPrintf(TRACE_METER_NODE, "Address Request failed: 0x%02x\n", eStatus);
					PDUM_eAPduFreeAPduInstance(hAPduInst);
				}
				else
				{
					u8ChildOfInterest++;
					break;
				}
			}
		}
	}

	if (i >= thisNib->sTblSize.u16NtActv)
	{
		OS_eStopSWTimer(APP_AgeOutChildrenTmr);									// No children left in the NT to query for
		u8ChildOfInterest = 0;
	}
	else
	{
		OS_eStartSWTimer(APP_AgeOutChildrenTmr, APP_TIME_MS(1600), NULL);		// Re-activate this task in 1.6s to scan for the next child
	}
}


/****************************************************************************
 *
 * NAME: APP_RadioRecal
 *
 * DESCRIPTION:
 * Recalibrate the radio once every minute
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_RadioRecal)
{
#ifdef RADIO_RECALIBRATION
	if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_RadioRecalTimer))
	{
		DBG_vPrintf(TRACE_METER_NODE, "Recalibrate the radio\n");
		uint8 eStatus = eAHI_AttemptCalibration();
		if (eStatus)
		{
			DBG_vPrintf(TRACE_METER_NODE, "Recalibration already underway");
			OS_eStartSWTimer(APP_RadioRecalTimer, APP_TIME_MS(1000), NULL);			// Re-activate this task in 1s
		}
		else
		{
			OS_eStartSWTimer(APP_RadioRecalTimer, APP_TIME_MS(60000), NULL);		// Re-activate this task in 60s
		}
	}
#endif
}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vInitialise
 *
 * DESCRIPTION:
 * Initialises the application
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vInitialise(void)
{
	/* If a button is held down over reset, clear the context */
	bool bDeleteRecords = APP_bButtonInitialise();

	if (bDeleteRecords)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Clearing Context\n");
		PDM_vDelete();
	}

	/* set the device state to initial state */
	s_sDevice.eState = E_CONFIG;
	s_sDevice.bKeyEstComplete = FALSE;

	/* Restore any application data previously saved to flash */
	PDM_eLoadRecord(	&s_sDevicePDDesc,
						METER_STATE, &s_sDevice,
						sizeof(s_sDevice),
						FALSE);

	if (E_CONFIG == s_sDevice.eState)											// If context is blank
	{
		#ifdef ZIGBEE_R20
   		/* set the security state default link key */
   		ZPS_vAplSecSetInitialSecurityState(ZPS_ZDO_PRECONFIGURED_LINK_KEY, s_au8LnkKeyArray, 0, ZPS_APS_UNIQUE_LINK_KEY );
		#else
		ZPS_vAplSecSetInitialSecurityState(ZPS_ZDO_PRECONFIGURED_LINK_KEY, s_au8LnkKeyArray, 0 );
		#endif

	}

#ifndef JENNIC_CHIP_FAMILY_JN514x
	ZPS_vSetOverrideLocalMacAddress((uint64 *)&au8MacAddress);
#endif

	/* Initialise ZBPro stack */
	ZPS_eAplAfInit();

#ifdef PDM_EEPROM
    /*
     * The functions u8PDM_CalculateFileSystemCapacity and u8PDM_GetFileSystemOccupancy
     * may be called at any time to monitor space available in  the eeprom
     */
    DBG_vPrintf(TRACE_METER_NODE, "PDM: Capacity %d\n", u8PDM_CalculateFileSystemCapacity() );
    DBG_vPrintf(TRACE_METER_NODE, "PDM: Occupancy %d\n", u8PDM_GetFileSystemOccupancy() );
#endif

#ifdef ETSI
	vAHI_ETSIHighPowerModuleEnable(TRUE);										// Limit the module to +8dB for ETSI compliance
#endif

	/* Initialise the Leds */
	vLedInitRfd();

	/* If the device state has been restored from flash, re-start the stack
	 *  and set the application running again */
	if (E_CONFIG != s_sDevice.eState)											// If the last known state was a running state (i.e. a context restore)
	{
		ZPS_eAplZdoStartStack();
		DBG_vPrintf(TRACE_METER_NODE, "Restoring Context\n");

		/* Set the leds to the 'network up' state */
		vLedControl(0, FALSE);
		vLedControl(1, FALSE);

		/* Activate the child aging task on a context restore to make sure any previous
		 * children haven't jumped to another parent whilst we were offline */
		OS_eActivateTask(APP_AgeOutChildren);
	}
	/* else perform any actions require on initial start-up */
	else
	{
		/* Set the leds to the initial state */
		 vLedControl(0, TRUE);
		 vLedControl(1, TRUE);
	}

	/* Start the tick timer */
	OS_eStartSWTimer(APP_ZclTimer, ONE_SECOND_TICK_TIME, NULL);

	/* Activate the radio recalibration task in 60s */
	OS_eStartSWTimer(APP_RadioRecalTimer, APP_TIME_MS(60000), NULL);

	/* Register functions for the ZCL and SE */
	APP_ZCL_vInitialise();

	if (E_CONFIG != s_sDevice.eState)
	{
		/* If running here, must be a context restore cannot
		 * call ZPS_eAplZdoSetDevicePermission until after ZCL init()
		 */
		if (s_sDevice.bKeyEstComplete)
		{
			ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_ALL_PERMITED);

			if (E_JOINING_NETWORK != s_sDevice.eState)
			{
				/* Unexpected state, recover by triggering a rejoin */
				OS_eActivateTask(APP_InitiateRejoin);
			}
		}
		else
		{
			s_sDevice.eState = E_START_NETWORK;
		}
	}

	// Configure Demo Parameters
	u32MeterDemand = 500;														// Initialise the instantaneous demand to '500'


#ifdef POT_READ
	bPotEnable();
#endif

}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleConfigEvent
 *
 * DESCRIPTION:
 * Waiting for start state, if the button is pushed, moves to start stack
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleConfigEvent(APP_tsEvent sAppEvent)
{
	ZPS_tsAplAib * tsAplAib  = ZPS_psAplAibGetAib();

	DBG_vPrintf(TRACE_METER_NODE, "E_CONFIG, EPID %016llx \r\n", tsAplAib->u64ApsUseExtendedPanid );

	//if (sAppEvent.eType != APP_E_EVENT_NONE)
	{
		/* If join button pressed, update states */
		//if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button && APP_E_EVENT_BUTTON_UP == sAppEvent.eType)
		{
			u8NetworkDiscoveryAttempts = 0;
			s_sDevice.eState = E_START_NETWORK;
			OS_eStopSWTimer(APP_RestartTimer);
			OS_eActivateTask(APP_MeterTask);
		}
	}
}


/****************************************************************************
 *
 * NAME: vSendBindRequest
 *
 * DESCRIPTION:
 * Sends out a bind request to the meter
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vSendBindRequest(void)
{
	uint8 u8SeqNum = 0;
	ZPS_tuAddress tuAddress;

	PDUM_thAPduInstance hAPduInst = PDUM_hAPduAllocateAPduInstance(apduZDP);

	if (hAPduInst == PDUM_INVALID_HANDLE)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Bind PDU ERR\n");
	}

	ZPS_tsAplZdpBindUnbindReq BindUnbindReq;

	BindUnbindReq.u64SrcAddress = u64CoordinatorMac;
	BindUnbindReq.u8SrcEndpoint = s_sDevice.sEsp.u8PriceEndPoint;
	BindUnbindReq.u16ClusterId = u16ClusterToBind; // DRLC
	BindUnbindReq.u8DstAddrMode = 0x03;
	BindUnbindReq.uAddressField.sExtended.u64DstAddress = ZPS_u64NwkNibGetExtAddr(ZPS_pvNwkGetHandle());
	BindUnbindReq.uAddressField.sExtended.u8DstEndPoint = 1;

	tuAddress.u16Addr = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;

	ZPS_teStatus eStatus = ZPS_eAplZdpBindUnbindRequest(	hAPduInst,
															tuAddress,
															FALSE,
															&u8SeqNum,
															TRUE,
															&BindUnbindReq
															);

	if (eStatus)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Bind req ERR: %x\n", eStatus);
		PDUM_eAPduFreeAPduInstance(hAPduInst);
	}
	else
	{
		s_sDevice.eState = E_BIND_RESP;
	}
}


/****************************************************************************
 *
 * NAME: vJoinedNetwork
 *
 * DESCRIPTION:
 * Set the joined network parameters
 *
 * PARAMETERS: void
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vJoinedNetwork(void)
{
	ZPS_tsNwkNib * thisNib;
	thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());

	u64CoordinatorMac = ZPS_psAplAibGetAib()->u64ApsTrustCenterAddress;

	ZPS_bNwkNibAddrMapAddEntry(ZPS_pvAplZdoGetNwkHandle(), 0x0000, u64CoordinatorMac);

	DBG_vPrintf(TRACE_METER_NODE, "Joined CH: %d, PAN: %x, TrustC: %016llx\n", thisNib->sPersist.u8VsChannel, thisNib->sPersist.u16VsPanId, u64CoordinatorMac);

	/* For handling rejoin */
#ifdef CLD_KEY_ESTABLISHMENT

	if( TRUE == s_sDevice.bKeyEstComplete)
	{
		vZCL_ClearTimeHasBeenSynchronised();
		DBG_vPrintf(TRACE_METER_NODE, "KEC Already Complete\r\n");
		s_sDevice.eState = E_RUNNING;
		PDM_vSaveRecord(&s_sDevicePDDesc);
		OS_eActivateTask(APP_MeterTask);
		vLedControl(0, FALSE);
		vLedControl(1, FALSE);
	}
	else
	{
		ZPS_eAplZdoSetDevicePermission(ZPS_DEVICE_PERMISSIONS_DATA_REQUEST_DISALLOWED);
		DBG_vPrintf(TRACE_METER_NODE, "KEC Required\r\n");

		s_sDevice.eState = E_SEND_MATCH;

		/* let the joining and tables settle before we start discovery */
		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, KEY_ESTABLISHMENT_WAIT, NULL);
	}
#else
	s_sDevice.eState = E_BIND_REQ;
	OS_eActivateTask(APP_MeterTask);
#endif
}


/****************************************************************************
 *
 * NAME: vHandleStartupEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its startup state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 *
 *
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleStartupEvent(ZPS_tsAfEvent sStackEvent)
{
	static uint8 u8LocalChannelMask = 11;
	static bool bFirstTime = TRUE;
	static uint32 u32ConfigMask = 0;

	if (bFirstTime)
	{
		bFirstTime = FALSE;
		u32ConfigMask = ZPS_psAplAibGetAib()->apsChannelMask;
		DBG_vPrintf(TRACE_METER_NODE, "Saved Channels: %08x\n", u32ConfigMask);
	}

	/* Clear the discovery neighbour table on each scan */
	vClearDiscNT();

	/* Check channel is in config mask, if so, start stack */
	if((u32ConfigMask) & (1 << u8LocalChannelMask))
	{
		ZPS_psAplAibGetAib()->apsChannelMask = 1 << u8LocalChannelMask;

		ZPS_teStatus eStatus;

		if(bRejoining)
		{
			OS_eActivateTask(APP_InitiateRejoin);
		}
		else
		{
			eStatus = ZPS_eAplZdoStartStack();

			if (ZPS_E_SUCCESS == eStatus)
			{
				DBG_vPrintf(TRACE_METER_NODE, "StartStack: CH: %d \r\n", u8LocalChannelMask);
				s_sDevice.eState = E_DISCOVERING_NETWORKS;
			}
			else
			{
				DBG_vPrintf(TRACE_METER_NODE, "StartStack: ERR: %x \r\n", eStatus);
				/* Try again ... */
				OS_eActivateTask(APP_MeterTask);
			}
	    }
	}
    else
    {
		DBG_vPrintf(TRACE_METER_NODE, "No Scan CH: %d\n", u8LocalChannelMask);
		OS_eActivateTask(APP_MeterTask);
	}

	u8LocalChannelMask++;

	if(u8LocalChannelMask >= 27)
	{
		u8LocalChannelMask = 11;
	}
}


/****************************************************************************
 *
 * NAME: vHandleMaxJoinRequests
 *
 * DESCRIPTION:
 * Handles multiple 'failed to join' events
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 *
 *
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleMaxJoinRequests(APP_tsEvent sAppEvent)
{
	/* Button 1 released */
	if ((APP_E_EVENT_BUTTON_UP == sAppEvent.eType) && (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button))
	{
		u8NetworkDiscoveryAttempts = 0;
		s_sDevice.eState = E_RESCAN;
		OS_eActivateTask(APP_MeterTask);
	}
	else
	{
		DBG_vPrintf(TRACE_METER_NODE_HIGH, "APP: Sync Failed\n");
	}
}


/****************************************************************************
 *
 * NAME: vHandleNetworkDiscoveryEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its network discovery state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleNetworkDiscoveryEvent(ZPS_tsAfEvent sStackEvent)
{
	/* wait for node to discover networks... */
	if (ZPS_EVENT_NONE != sStackEvent.eType)
	{
		if (ZPS_EVENT_NWK_DISCOVERY_COMPLETE == sStackEvent.eType)
		{
#ifdef JOIN_ROUTER_ONLY
        	vRemoveCoordParents();
#endif

			DBG_vPrintf(TRACE_METER_NODE, "Discovery Complete\n");

			if (ZPS_E_SUCCESS == sStackEvent.uEvent.sNwkDiscoveryEvent.eStatus)
			{
				DBG_vPrintf(TRACE_METER_NODE, "Found %d NWKS\n", sStackEvent.uEvent.sNwkDiscoveryEvent.u8NetworkCount);
				DBG_vPrintf(TRACE_METER_NODE, "Unscanned CH: %08x\n", sStackEvent.uEvent.sNwkDiscoveryEvent.u32UnscannedChannels);
			}
			else
			{
				DBG_vPrintf(TRACE_METER_NODE, "Discovery ERR: %x\n",sStackEvent.uEvent.sNwkDiscoveryEvent.eStatus);

				OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(3000), NULL);
				s_sDevice.eState = E_START_NETWORK;
			}

			/* Networks were found */
			if (0 != sStackEvent.uEvent.sNwkDiscoveryEvent.u8NetworkCount)
			{
				vSaveDiscoveredNWKS(sStackEvent);

				vJoinStoredNWK();
			}
			/* No Networks found */
			else
			{
				OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(3000), NULL);
				s_sDevice.eState = E_START_NETWORK;
			}
		}
		else if (ZPS_EVENT_NWK_FAILED_TO_JOIN == sStackEvent.eType)
		{
			DBG_vPrintf(TRACE_METER_NODE, "Join ERR: = %x\n", sStackEvent.uEvent.sNwkJoinFailedEvent.u8Status);

			s_sDevice.eState = E_DISCOVERING_NETWORKS;

			vJoinStoredNWK();
		}
		else if (ZPS_EVENT_NWK_JOINED_AS_ROUTER == sStackEvent.eType)
		{
			DBG_vPrintf(TRACE_METER_NODE, "Rejoined Addr: 0x%04x\n",
					sStackEvent.uEvent.sNwkJoinedEvent.u16Addr);

			/* Store EPID for future rejoin */
			uint64 u64ExtPANID = ZPS_u64NwkNibGetEpid(ZPS_pvAplZdoGetNwkHandle());
			ZPS_eAplAibSetApsUseExtendedPanId(u64ExtPANID);

			vJoinedNetwork();
		}
		else if (ZPS_EVENT_ERROR == sStackEvent.eType)
		{
			DBG_vPrintf(TRACE_METER_NODE, "ZPS ERR: %x\n", sStackEvent.uEvent.sAfErrorEvent.eError);
		}
		else
		{
			vUnhandledEvent(s_sDevice.eState,sStackEvent.eType);
		}
	}
}


/****************************************************************************
 *
 * NAME: vSaveDiscoveredNWKS
 *
 * DESCRIPTION:
 * Stores the list of NWKS from a scan
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vSaveDiscoveredNWKS(ZPS_tsAfEvent sStackEvent)
{
	uint8 i;

	u8DiscovedNWKJoinCount = 0;
	u8DiscovedNWKListCount = 0;

	memset( &tsDiscovedNWKList, 0, ((sizeof(ZPS_tsNwkNetworkDescr))* MAX_SINGLE_CHANNEL_NETWORKS));

	for(i=0; i<sStackEvent.uEvent.sNwkDiscoveryEvent.u8NetworkCount; i++)
	{
#ifndef JENNIC_CHIP_FAMILY_JN514x
		if( sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i].u8PermitJoining &&
				sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i].u8RouterCapacity )
		{
			DBG_vPrintf(TRACE_METER_NODE, "Permit Join - %d\n", sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i].u8PermitJoining);

			tsDiscovedNWKList[u8DiscovedNWKListCount] = sStackEvent.uEvent.sNwkDiscoveryEvent.psNwkDescriptors[i];
			u8DiscovedNWKListCount++;
		}
#else
		if( sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i].u8PermitJoining &&
			sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i].u8RouterCapacity )
		{
			DBG_vPrintf(TRACE_METER_NODE, "Permit Join - %d\n", sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i].u8PermitJoining);

			tsDiscovedNWKList[u8DiscovedNWKListCount] = sStackEvent.uEvent.sNwkDiscoveryEvent.asNwkDescriptors[i];
			u8DiscovedNWKListCount++;
		}
#endif
	}
	vDisplayDiscoveredNWKS();
}


/****************************************************************************
 *
 * NAME: vDisplayDiscoveredNWKS
 *
 * DESCRIPTION:
 * Lists the discovered NWKs to the UART.
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vDisplayDiscoveredNWKS(void)
{
	uint8 i;

	for (i = 0; i < MAX_SINGLE_CHANNEL_NETWORKS; i++)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Index: %d PAN: %016llx, Permit J %d\n", i, tsDiscovedNWKList[i].u64ExtPanId, tsDiscovedNWKList[i].u8PermitJoining);
	}
}


/****************************************************************************
 *
 * NAME: vJoinStoredNWK
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its rescan state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vJoinStoredNWK(void)
{
	ZPS_teStatus eStatus = 0xFF;

	while (u8DiscovedNWKJoinCount < u8DiscovedNWKListCount)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Joining PAN: %016llx, Index: %d, Total NWKs: %d\n", tsDiscovedNWKList[u8DiscovedNWKJoinCount].u64ExtPanId, u8DiscovedNWKJoinCount, u8DiscovedNWKListCount);
		eStatus = ZPS_eAplZdoJoinNetwork(&tsDiscovedNWKList[u8DiscovedNWKJoinCount]);

		if (ZPS_E_SUCCESS == eStatus)
		{
		   s_sDevice.eState = E_JOINING_NETWORK;
		   break;
		}
		else
		{
			DBG_vPrintf(TRACE_METER_NODE, "Join ERR: %x\n", eStatus);
		}
		u8DiscovedNWKJoinCount++;
	}

	if (u8DiscovedNWKJoinCount >= u8DiscovedNWKListCount)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Exhausted NWK Join\r\n");

		s_sDevice.eState = E_START_NETWORK;
		OS_eStopSWTimer(APP_RestartTimer);
		OS_eStartSWTimer(APP_RestartTimer, RESTART_TIME, NULL);
	}
}


/****************************************************************************
 *
 * NAME: vHandleRescanEvent
 *
 * DESCRIPTION:
 * Handles stack events when the Controller is in its rescan state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleRescanEvent(ZPS_tsAfEvent sStackEvent)
{
	/* restart scan with configured channel mask */
	DBG_vPrintf(TRACE_METER_NODE, "Rescan %08x\n", ZPS_psAplAibGetAib()->apsChannelMask);
	ZPS_eAplZdoDiscoverNetworks(ZPS_psAplAibGetAib()->apsChannelMask);
	s_sDevice.eState = E_DISCOVERING_NETWORKS;
}


/****************************************************************************
 *
 * NAME: vHandleNetworkJoinEvent
 *
 * DESCRIPTION:
 * Handles stack events when the router is in its network join state
 *
 * PARAMETERS: Name         RW  Usage
 *             sStackEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleNetworkJoinEvent(ZPS_tsAfEvent sStackEvent)
{
	switch (sStackEvent.eType)
	{
		case ZPS_EVENT_NWK_JOINED_AS_ROUTER:
			bRejoining = FALSE;

			DBG_vPrintf(TRACE_METER_NODE, "Joined Addr: 0x%04x\n", sStackEvent.uEvent.sNwkJoinedEvent.u16Addr);

			/* Store EPID for future rejoin */
			uint64 u64ExtPANID = ZPS_u64NwkNibGetEpid(ZPS_pvAplZdoGetNwkHandle());
			ZPS_eAplAibSetApsUseExtendedPanId(u64ExtPANID);

			vJoinedNetwork();
		break;

		case ZPS_EVENT_NWK_FAILED_TO_JOIN:
			DBG_vPrintf(TRACE_METER_NODE, "Join Failed: %x\n", sStackEvent.uEvent.sNwkJoinFailedEvent.u8Status);
			if(bRejoining)														// Failed to rejoin the network...
			{
				s_sDevice.eState = E_START_NETWORK;
				u8NetworkDiscoveryAttempts = 0;
				OS_eActivateTask(APP_MeterTask);
			}
			else
			{
				u8DiscovedNWKJoinCount++;
				vJoinStoredNWK();
			}
		break;

		default:
			vUnhandledEvent(s_sDevice.eState,sStackEvent.eType);
		break;
	}
}


/****************************************************************************
 *
 * NAME: vUnhandledEvent
 *
 * DESCRIPTION:
 * Displays the unhandled state and event
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vUnhandledEvent(teState eState, ZPS_teAfEventType eType)
{
	DBG_vPrintf(TRACE_METER_NODE, "Unhandled State: %d, Event: 0x%02x\n", eState, eType);
}


/****************************************************************************
 *
 * NAME: vHandleBindRespEvent
 *
 * DESCRIPTION:
 * Handles the ZDP response from the bind request
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the stack event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleBindRespEvent(ZPS_tsAfEvent sStackEvent)
{
	if (ZPS_EVENT_APS_ZDP_REQUEST_RESPONSE == sStackEvent.eType)
	{
		if (BIND_RESP == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
		{
			if(sStackEvent.uEvent.sApsZdpEvent.uZdpData.sBindRsp.u8Status ==  ZPS_E_SUCCESS)
			{
				if(SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL == u16ClusterToBind)
				{
					s_sDevice.eState = E_RUNNING;

					/* Save the state in the flash for a reset */
					PDM_vSaveRecord(&s_sDevicePDDesc);
					OS_eActivateTask(APP_MeterTask);
					vLedControl(0, FALSE);
					vLedControl(1, FALSE);
				}
			}
		}
	}
}


/****************************************************************************
 *
 * NAME: APP_bMaxJoinAttempts
 *
 * DESCRIPTION:
 * Checks if the max number of join attempts has been reached. if so
 * returns TRUE, else returns FALSE
 *
 * PARAMETERS: Name         RW  Usage
 *             void
 *
 * RETURNS:
 *   returns TRUE if max reached, else returns FALSE
 *
 ****************************************************************************/
PRIVATE bool APP_bMaxJoinAttempts(void)
{
	if (++u8NetworkDiscoveryAttempts > MAX_DISCOVERY_ATTEMPT)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Failed To Join NWK\r\n");
		return (TRUE);
	}
	else
	{
		return (FALSE);
	}
}


#ifdef CLD_KEY_ESTABLISHMENT
/****************************************************************************
 *
 * NAME: APP_vStartKeyEstablishment
 *
 * DESCRIPTION:
 * Sends an initiate key establishment command to the Meter
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vStartKeyEstablishment(void)
{
	uint8	u8TransactionSequenceNumber;
	teSE_KECStatus eStatus;

	eStatus = eSE_KECInitiateKeyEstablishment(LOCAL_EP, s_sDevice.sEsp.u8KeyEstablishmentEndPoint, &s_sDevice.sEsp.sAddress, &u8TransactionSequenceNumber);
	if(eStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Key Req ERR: %x", eStatus);
	}

	OS_eStopSWTimer(APP_RestartTimer);
	OS_eStartSWTimer(APP_RestartTimer, KEY_ESTABLISHMENT_TIMEOUT, NULL);

	s_sDevice.eState = E_STATE_WAIT_KEY_ESTABLISHMENT;
	OS_eActivateTask(APP_MeterTask);
}


/****************************************************************************
 *
 * NAME: APP_vWaitKeyEstablishment
 *
 * DESCRIPTION:
 * Waits for the key establishment response
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vWaitKeyEstablishment(void)
{
	if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		DBG_vPrintf(TRACE_METER_NODE, "Key establishment timed out\n");
		vLeaveNWK();

		s_sDevice.eState = E_STATE_WAIT_LEAVE;
	}
}
#endif


/****************************************************************************
 *
 * NAME: APP_vWaitLeave
 *
 * DESCRIPTION:
 * Waits for the leave confirm event
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void APP_vWaitLeave(ZPS_tsAfEvent sStackEvent)
{
	if (ZPS_EVENT_NWK_LEAVE_CONFIRM == sStackEvent.eType)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Leave Confirm :%02x",sStackEvent.uEvent.sNwkLeaveConfirmEvent.eStatus );

		vJoinStoredNWK();
	}
}


/****************************************************************************
 *
 * NAME: vLeaveNWK
 *
 * DESCRIPTION:
 * Calls the stack leave function
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vLeaveNWK(void)
{
	ZPS_teStatus teStatus = ZPS_eAplZdoLeaveNetwork(0, FALSE, FALSE);
	s_sDevice.bKeyEstComplete = FALSE;

	if(teStatus)
	{
		DBG_vPrintf(TRACE_METER_NODE, "Failed Leave: %x", teStatus);
		vAHI_SwReset();
	}
	s_sDevice.eState = E_STATE_WAIT_LEAVE;
}


/****************************************************************************
 *
 * NAME: vHandleGetMatchDesc
 *
 * DESCRIPTION:
 * Send a match descriptor request to the coordinator
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleGetMatchDesc(ZPS_tsAfEvent sStackEvent)
{
	uint8	u8TransactionSequenceNumber;

	if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		PDUM_thAPduInstance hAPduInst = PDUM_hAPduAllocateAPduInstance(apduZDP);
		if (hAPduInst == PDUM_INVALID_HANDLE)
		{
			DBG_vPrintf(TRACE_METER_NODE, "Match PDU ERR\n");
			return;
		}

		ZPS_tuAddress MeterAddress;
		MeterAddress.u16Addr = s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress;

		ZPS_tsAplZdpMatchDescReq matchDesc;

		uint16 pu16InClusterList[] = {};
		uint16 pu16OutClusterList[1];

		pu16OutClusterList[0] = SE_CLUSTER_ID_KEY_ESTABLISHMENT;

		matchDesc.u16NwkAddrOfInterest = 0x0000;
		matchDesc.u16ProfileId         = SE_PROFILE_ID;
		matchDesc.u8NumInClusters      = 0;
		matchDesc.pu16InClusterList    = pu16InClusterList;
		matchDesc.u8NumOutClusters     = 1;
		matchDesc.pu16OutClusterList   = pu16OutClusterList;

		ZPS_teStatus eStatus = ZPS_eAplZdpMatchDescRequest(hAPduInst, MeterAddress, FALSE, &u8TransactionSequenceNumber, &matchDesc);

		if (ZPS_E_SUCCESS == eStatus)
		{
			DBG_vPrintf(TRACE_METER_NODE, "Sent Match" );

			s_sDevice.eState = E_WAIT_MATCH;
			OS_eStopSWTimer(APP_RestartTimer);
			OS_eStartSWTimer(APP_RestartTimer, APP_TIME_MS(5000), NULL);
		}
		else
		{
			DBG_vPrintf(TRACE_METER_NODE, "Match ERR: %x", eStatus);
			PDUM_eAPduFreeAPduInstance(hAPduInst);
		}
	} /*Timer running */
}


/****************************************************************************
 *
 * NAME: vHandleWaitMatchDesc
 *
 * DESCRIPTION:
 * Get the match descriptor response from the coordinator
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE void vHandleWaitMatchDesc(ZPS_tsAfEvent sStackEvent)
{
	if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_RestartTimer))
	{
		ZPS_teStatus teStatus = ZPS_eAplZdoLeaveNetwork(0, FALSE, FALSE);

		if(teStatus)
		{
			DBG_vPrintf(TRACE_METER_NODE, "Failed Leave - Match Desc Timeout: %x", teStatus);
		}
		else
		{
			s_sDevice.eState = E_STATE_WAIT_LEAVE;
		}
	}

	if (ZPS_EVENT_APS_ZDP_REQUEST_RESPONSE == sStackEvent.eType)
    {
		if (0x8006 == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
		{
			if (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8Status != ZPS_E_SUCCESS)
			{
				DBG_vPrintf(TRACE_METER_NODE, "Match Failed\n");

				ZPS_teStatus teStatus = ZPS_eAplZdoLeaveNetwork(0, FALSE, FALSE);

				if(teStatus)
				{
					DBG_vPrintf(TRACE_METER_NODE, "Failed Leave Call: %x", teStatus);
				}
				s_sDevice.eState = E_STATE_WAIT_LEAVE;
			}
			else
			{
				DBG_vPrintf(TRACE_METER_NODE, "Match Success\n");
				if (sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u8MatchLength > 0)
				{
					s_sDevice.eState = E_STATE_START_KEY_ESTABLISHMENT;

					s_sDevice.sEsp.sAddress.eAddressMode = E_ZCL_AM_SHORT;
					s_sDevice.sEsp.sAddress.uAddress.u16DestinationAddress = sStackEvent.uEvent.sApsZdpEvent.uZdpData.sMatchDescRsp.u16NwkAddrOfInterest;

					s_sDevice.sEsp.u8KeyEstablishmentEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
					s_sDevice.sEsp.u8PriceEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
					s_sDevice.sEsp.u8TimeEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
					s_sDevice.sEsp.u8DrlcEndPoint = sStackEvent.uEvent.sApsZdpEvent.uLists.au8Data[0];
				}
				else
				{
					DBG_vPrintf(TRACE_METER_NODE, "Empty match list\n");
				}
				OS_eActivateTask(APP_MeterTask);
			}
		}
		else
		{
			DBG_vPrintf(TRACE_METER_NODE,"ZDP Event ignored\r\n");
		}
    }
}


/****************************************************************************
 *
 * NAME: vClearDiscNT
 *
 * DESCRIPTION:
 * Resets the discovery NT
 *
 * RETURNS:
 * void
 *
 *
 ****************************************************************************/
PRIVATE void vClearDiscNT(void)
{
	ZPS_tsNwkNib * thisNib;

	thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());

	memset(thisNib->sTbl.psNtDisc, 0, sizeof(ZPS_tsNwkDiscNtEntry) * thisNib->sTblSize.u8NtDisc);
}


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
