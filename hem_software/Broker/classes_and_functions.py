from bluepy.btle import Scanner, Peripheral, DefaultDelegate
from struct import Struct
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import tables


bleFunction={
            '0':            'RELAY_OFF',
            '1':             'RELAY_ON',
            '2':           'NOTIFY_OFF',
            '3':            'NOTIFY_ON',
            '4':           'READ_RELAY',
            '5':          'READ_NOTIFY',
            '6':         'READ_VOLTAGE', 
            '7':         'READ_CURRENT',
            '8':           'READ_POWER',
            '9':   'READ_VOLTAGE_BATCH',
            'a':   'READ_CURRENT_BATCH',
            'b':            'STATUS_OK',
            'c':       'STATUS_WARNING',
            'd':        'STATUS_DANGER',
            'e':       'STATUS_NO_LINE',
            'z':       'NOT_RECOGNIZED'}


data_aux = {}
last_command = True


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            pass
            #print('Discovered device', dev.addr)
        elif isNewData:
            pass
            #print('Received new data from', dev.addr)


class NotificationDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleNotification(self, data):
        global data_aux
        data_str = data.decode('utf-8')
        date = ProcessDate(data_str[6:8], data_str[8:10], data_str[10:12],
                           data_str[12:14], data_str[14:16], data_str[16:18])
        function, proc_data = ProcessData(data_str[0], data_str[1:6])
        data_aux[function] = (date, proc_data)
        print(data_str)


def ScanDevices():
    scanner = Scanner().withDelegate(ScanDelegate())
    devices = scanner.scan(2.0)
    count = 0
    for dev in devices:
        print('{0}- {1} {2}'.format(count, dev.addr, dev.getValueText(9)))
        count = count + 1
    return list(devices)


def ConnectDatabase():
    engine = create_engine('mysql://root:1q2w3e4r5t@localhost/smart_home_energy')
    Session = sessionmaker(bind=engine)
    return Session()


def ConnectDevice(devices, session):
    peripheral = Peripheral()

    for dev in devices:
        if dev.addr == 'a4:c1:38:03:38:e4':
        # if dev.getValueText(9) == 'SmartObject2':
            peripheral.connect(dev.addr, dev.addrType, dev.iface)
            break
    char = None
    if peripheral:
        characteristics = peripheral.getCharacteristics()
        print('Device conected!')
        for char in characteristics:
            if char.uuid == '0000ffe1-0000-1000-8000-00805f9b34fb':
                TurnNotificationsOn(peripheral, char)
                break
        InsertSocket(dev.getValueText(9), session)
    else:
        print('Connection error!!!')
    return char, peripheral


def TurnNotificationsOn(peripheral, char):
    peripheral.withDelegate(NotificationDelegate)
    peripheral.writeCharacteristic(char.getHandle()+1, b'\x01\x00', withResponse=False)


def InsertDataToDatabase(session):
    global data_aux
    socket = session.query(tables.Socket).filter_by(nome_tomada='SmartObject1').first()
    if 'STATUS_NO_LINE' in data_aux.keys():
        voltage = 0.0
        current = 0.0
        power = 0.0
        temp = data_aux.get('STATUS_NO_LINE')
        date = temp[0]
        socket.energizada = 0
        session.commit()
    else:
        socket.energizada = 1
        session.commit()
        if 'READ_VOLTAGE' in data_aux.keys():
            temp = data_aux.get('READ_VOLTAGE')
            voltage = temp[1]
            temp = data_aux.get('READ_VOLTAGE')
            date = temp[0]
        if 'READ_RELAY' in data_aux.keys():
            temp = data_aux.get('READ_RELAY')
            relay = temp[1]
            socket.estado = relay
        if 'READ_CURRENT' in data_aux.keys():
            temp = data_aux.get('READ_CURRENT')
            current = temp[1]
        if 'READ_POWER' in data_aux.keys():
            temp = data_aux.get('READ_POWER')
            power = temp[1]

    insert_data = tables.Data(current,voltage,0,date,power,socket.id_tomada)
    session.add(insert_data)
    session.commit()
    data_aux = {}


def ProcessData(function, data):
    func = bleFunction.get(function)
    if func == 'READ_VOLTAGE':
        voltage = int(data)/100
        return func, voltage
    elif func == 'READ_RELAY':
        relay = bool(data)
        return func, relay
    elif func == 'READ_CURRENT':
        current = int(data)/100
        return func, current
    elif func == 'READ_POWER':
        power = int(data)/100
        return func, power
    elif func == 'STATUS_NO_LINE':
        return func, 0
    elif func == 'READ_NOTIFY':
        key = chr(int(data[1:3]))
        return func, key

def ProcessDate(year, month, day, hour, minute, second):
    temp = '20'+year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second
    date = datetime.strptime(temp, '%Y-%m-%d %H:%M:%S')
    return date


def InsertSocket(nome_tomada, session):
    q = session.query(tables.Socket.nome_tomada).all()
    for socket in q:
        if socket[0] == nome_tomada:
            return
    tomada = tables.Socket(nome_tomada, True, True)
    session.add(tomada)
    session.commit()


def VerifyCommandsWeb(session):
    global last_command
    q = session.query(tables.Socket).filter_by(nome_tomada='SmartObject1').first()
    if q.comando == True and last_command == False:
        last_command = True
        return True
    elif q.comando == False and last_command == True:
        last_command = False
        return True
    else:
        return False


def TurnOnAndOffSocket(characteristic, session):
    q = session.query(tables.Socket).filter_by(nome_tomada='SmartObject1').first()
    if q.comando == True:
        q.estado = True
        session.commit()
        characteristic.write(bytes('1','utf-8'), withResponse=False)
    else:
        q.estado = False
        session.commit()
        characteristic.write(bytes('0','utf-8'), withResponse=False)