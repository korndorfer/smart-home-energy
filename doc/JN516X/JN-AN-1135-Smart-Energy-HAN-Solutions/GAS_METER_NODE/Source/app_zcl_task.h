/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_zcl_task.h
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        ZCL Handler Header
 *
 * $HeadURL: http://svn/apps/Application_Notes/JN-AN-1135-ZigBee-Pro-SE-Home-Energy-Monitor/Trunk/METER_NODE/Source/app_zcl_task.h $
 *
 * $Revision: 5862 $
 *
 * $LastChangedBy: jpenn $
 *
 * $LastChangedDate: 2010-04-27 10:58:38 +0100 (Tue, 27 Apr 2010) $
 *
 * $Id: app_zcl_task.h 5862 2010-04-27 09:58:38Z jpenn $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_ZCL_TASK_H
#define APP_ZCL_TASK_H

#include <jendefs.h>
#include "se.h"
#include "SimpleMetering.h"
#include "ESP_Meter.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void	APP_ZCL_vInitialise(void);
PUBLIC void	APP_ZCL_UpdateMeterData(tsSE_MeterDevice *psMeter);
PUBLIC void	vShowTime(uint32 u32Time);

#ifdef CLD_TIME
PUBLIC void vRequestTime(void);
#endif

PUBLIC void vLockZCLMutex(void);
PUBLIC void vUnlockZCLMutex(void);
PUBLIC void vClearExpiredFlag(OS_thSWTimer hSWTimer);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

PUBLIC uint32 	u32MeterDemand;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* APP_ZCL_TASK_H */
