from classes_and_functions import (ScanDevices, ConnectDevice, VerifyCommandsWeb,
                                    ConnectDatabase, InsertDataToDatabase, TurnOnAndOffSocket)

session = ConnectDatabase()

devices = ScanDevices()
char, periph = ConnectDevice(devices, session)

while True:
    count = 0
    if VerifyCommandsWeb(session):
        TurnOnAndOffSocket(char, session)
    while True:
        if periph.waitForNotifications(1):
            count = count + 1
            continue
        break
    if count == 5 or count == 1:
        InsertDataToDatabase(session)
    