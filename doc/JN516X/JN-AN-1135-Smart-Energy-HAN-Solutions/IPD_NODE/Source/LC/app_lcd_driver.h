/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (LC-IPD)
 *
 * COMPONENT:          app_lcd_driver.h
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        LCD Driver (I2C) header
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_lcd_driver.h $
 *
 * $Revision: 8954 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-04-16 13:01:29 +0100 (Mon, 16 Apr 2012) $
 *
 * $Id: app_lcd_driver.h 8954 2012-04-16 12:01:29Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_LCD_DRIVER_H_
#define APP_LCD_DRIVER_H_

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

// I2C Communication Functions
PUBLIC void APP_vI2cInitialise(void);
PUBLIC void vEstablishLcdComms(void);
PUBLIC void vSendLcdCharacterPointer(uint8 CharacterPointer);
PUBLIC void vSendLcdCharacterData(uint8 CharacterData);
PUBLIC void vTerminateLcdComms(void);
PUBLIC void vCheckArbitration(void);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        In line  Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_LCD_DRIVER_H_*/
