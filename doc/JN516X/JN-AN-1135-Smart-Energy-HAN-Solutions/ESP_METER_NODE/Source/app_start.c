/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_start.c
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        Startup the application
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/METER_NODE/Source/app_start.c $
 *
 * $Revision: 8496 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-16 16:07:57 +0000 (Fri, 16 Dec 2011) $
 *
 * $Id: app_start.c 8496 2011-12-16 16:07:57Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "pwrm.h"
#include "pdum_nwk.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pdm.h"
#include "dbg.h"
#include "dbg_uart.h"
#include "zps_apl_af.h"
#include "Utilities.h"
#include "appapi.h"
#include "app_meter_node.h"
#include "zcl_options.h"
#include "zcl.h"
#include "OTA.h"
#ifdef OVERLAYS_BUILD
#include "ovly.h"
#endif
#ifdef RADIO_RECALIBRATION
#include "recal.h"
#endif
#ifdef PDM_EEPROM
PUBLIC uint8 u8PDM_CalculateFileSystemCapacity(void);
PUBLIC uint8 u8PDM_GetFileSystemOccupancy(void);
#endif

#ifdef STACK_MEASURE
	#include "StackMeasure.h"
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_START
#define TRACE_START		FALSE
#endif

#ifndef TRACE_OVERLAYS
#define TRACE_OVERLAYS	FALSE
#endif

#ifndef TRACE_EXCEPTION
#define TRACE_EXCEPTION	TRUE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void vInitialiseApp(void);
PRIVATE void vUnclaimedInterrupt(void);
PRIVATE void vOSError(OS_teStatus eStatus, void *hObject);
#ifdef OVERLAYS_BUILD
PRIVATE void vGrabLock(void);
PRIVATE void vReleaseLock(void);
PRIVATE void vOverlayEvent(OVLY_teEvent eEvent, OVLY_tuEventData *puData);
#endif

#ifdef PDM_EEPROM
PRIVATE void vPdmEventHandlerCallback(uint32 u32EventNumber, PDM_eSystemEventCode eSystemEventCode);
#endif
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

static PWRM_DECLARE_CALLBACK_DESCRIPTOR(PreSleep);
static PWRM_DECLARE_CALLBACK_DESCRIPTOR(Wakeup);

/* encryption key for PDM */
PRIVATE const tsReg128 g_sKey = { 0x45FDF4C9, 0xAE9A6214, 0x7B27285B, 0xDB7E4557 };

#ifdef OVERLAYS_BUILD
PRIVATE	OVLY_tsInitData sInitData;
#endif

#if (OTA_MAX_CO_PROCESSOR_IMAGES != 0)
	/*
	 * This structure keeps information about the Server Endpoint, Image Index on server
	 * File version and if the image is for the server or the client.
	 * The image is transferred by the co-processor to JN5148 flash and stored there, OTA
	 * cluster the takes care of transferring the image stored in the flash over the air.
	 *
	 */

typedef struct {

	uint8 u8Endpoint;
	uint8 u8ImageIndex;
	bool  bServerImage;
	uint32 u32FileVersion;
	uint8 u8QueryJitter;
	uint64 u64DestAddress64;
	uint32 u32RequestUpgradeTime;
	uint32 u32OtaFileOffset;

} tsOTA_CoAttr;

tsOTA_CoAttr OtaCoAttr;
#endif

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

#if defined(PRODUCTION) || defined(CLD_OTA)
extern uint16 u16ImageStartSector;
#else
uint16 u16ImageStartSector = 0;
#endif

/* Linker script externs */
extern void *stack_low_water_mark;
extern void *stack_size;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PWRM_CALLBACK(PreSleep)
{
	DBG_vPrintf(TRACE_START, "APP: Going to sleep (CB) ... ");
}

PWRM_CALLBACK(Wakeup)
{
    DBG_vPrintf(TRACE_START, "\n\nAPP: Woken up (CB)\n");
}


/****************************************************************************
 *
 * NAME: vAppMain
 *
 * DESCRIPTION:
 * Entry point for application from a cold start.
 *
 * RETURNS:
 * Never returns.
 *
 ****************************************************************************/
PUBLIC void vAppMain(void)
{

#ifdef STACK_MEASURE
    vInitStackMeasure();
#endif

#ifndef JENNIC_CHIP_FAMILY_JN514x

	/* Check that the clock source is the external 32MHz, needed for
	* accurate UART timings
	*/
     while (bAHI_GetClkSource() == TRUE);
     // Now we are running on the XTAL, optimise the flash memory wait states.
     vAHI_OptimiseWaitStates();
#endif

    DBG_vUartInit(DBG_E_UART_0, DBG_E_UART_BAUD_RATE_115200);
    DBG_vPrintf(TRACE_START, "Power Up!\n");

    DBG_vPrintf(TRACE_START, "Low water mark: %08x\n", &stack_low_water_mark);

    vAHI_SetStackOverflow(TRACE_START, (uint32)&stack_low_water_mark);

    DBG_vPrintf(TRACE_START, "Stack Size %08d\n",    (uint32)&stack_size );

    if (bAHI_WatchdogResetEvent())
    {
        DBG_vPrintf(TRACE_START, "Watchdog Reset\n");
        vAHI_WatchdogStop();
        while (1);
    }

	u32AppApiInit(NULL, NULL, NULL, NULL, NULL, NULL);

    vAHI_HighPowerModuleEnable(TRUE, TRUE);										// Enable high power mode

    OS_vStart(vInitialiseApp, vUnclaimedInterrupt, vOSError);

	/* idle task commences on exit from OS start call */
    while (TRUE)
    {
        vAHI_WatchdogRestart();
        PWRM_vManagePower();
    }
}

void vAppRegisterPWRMCallbacks(void)
{
    PWRM_vRegisterPreSleepCallback(PreSleep);
    PWRM_vRegisterWakeupCallback(Wakeup);
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vInitialiseApp
 *
 * DESCRIPTION:
 * Initialises Zigbee stack, hardware and application.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vInitialiseApp(void)
{
#ifdef OVERLAYS_BUILD
	sInitData.u32ImageOffset =  u16ImageStartSector * 0x8000;
    sInitData.prGetMutex = vGrabLock;
    sInitData.prReleaseMutex = vReleaseLock;
    sInitData.prOverlayEvent = &vOverlayEvent;
    OVLY_bInit(&sInitData);
#endif

	/* initialise JenOS modules */
	DBG_vPrintf(TRACE_START, "Initialising PWRM ... ");
	PWRM_vInit(E_AHI_SLEEP_OSCON_RAMON);

    DBG_vPrintf(TRACE_START, "Initialising PDM ... ");
    /*
     *  Initialise the PDM, use an application supplied key (g_sKey),
     *  The key value can be set to the desired value here, or the key in eFuse can be used.
     *  To use the key stored in eFuse set the pointer to the key to Null, and remove the
     *  key structure here.
     */
#if JENNIC_CHIP_FAMILY == JN516x

	#ifdef PDM_EEPROM
		 PDM_vInit( 0, 63, 64, NULL, NULL, NULL, NULL);
		 PDM_vRegisterSystemCallback(vPdmEventHandlerCallback);
	#endif
#else
    PDM_vInit(7, 1, 64 * 1024 , mutexPDUM, hSpiMutex, NULL, &g_sKey);
#endif

    DBG_vPrintf(TRACE_START, "Initialising PDUM ... ");
    PDUM_vInit();

	DBG_vPrintf(TRACE_START, "Initialising APP ... ");
	APP_vInitialise();
}


/****************************************************************************
 *
 * NAME: vOSError
 *
 * DESCRIPTION:
 * Catches any unexpected OS errors
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vOSError(OS_teStatus eStatus, void *hObject)
{
    OS_thTask hTask;

    /* ignore queue underruns */
    if (OS_E_QUEUE_EMPTY == eStatus)
    {
        return;
    }

    DBG_vPrintf(TRACE_EXCEPTION, "OS Error %d, offending object handle = 0x%08x\n", eStatus, hObject);

    /* NB the task may have been pre-empted by an ISR which may be at fault */
    OS_eGetCurrentTask(&hTask);
    DBG_vPrintf(TRACE_EXCEPTION, "Currently active task handle = 0x%08x\n", hTask);
#ifdef OS_STRICT_CHECKS
    DBG_vPrintf(TRACE_EXCEPTION, "Currently active ISR fn address = 0x%08x\n", OS_prGetActiveISR());
#endif
}


#if (OTA_MAX_CO_PROCESSOR_IMAGES != 0)
/****************************************************************************
 *
 * NAME: OtaCoprocessor_Init
 *
 * DESCRIPTION:
 * This function has to be called first by the co-processor when it starts transferring image to the FLash of the JN5148
 * The function initializes different parameters in the OtaCoAttr structure of type tsOTA_CoAttr like Endpoint, ImageIndex, type of the Image( Server/ Client)
 * File version, query jitter, if it is for the client destination address, upgrade time and fileoffset or offset of bytes transferred to the flash.
 * This function has to be called once before data from Coprocessor is transferred to the flash.
 *
 * RETURNS:
 * teZCL_Status
 *
 ****************************************************************************/
teZCL_Status  OtaCoprocessor_Init(tsOTA_CoAttr *oAttr)
{

	if(oAttr->u8ImageIndex >= OTA_MAX_IMAGES_PER_ENDPOINT)
			return E_ZCL_FAIL;

	OtaCoAttr.u8Endpoint    = oAttr->u8Endpoint;
	OtaCoAttr.u8ImageIndex  = oAttr->u8ImageIndex;
	OtaCoAttr.bServerImage = oAttr->bServerImage;
	OtaCoAttr.u32FileVersion = oAttr->u32FileVersion;
	OtaCoAttr.u8QueryJitter = oAttr ->u8QueryJitter;
	OtaCoAttr.u64DestAddress64 = oAttr->u64DestAddress64;
	OtaCoAttr.u32RequestUpgradeTime = oAttr->u32RequestUpgradeTime;


	if( eOTA_EraseFlashSectorsForNewImage(OtaCoAttr.u8Endpoint,OtaCoAttr.u8ImageIndex) != E_ZCL_SUCCESS )
		return E_ZCL_FAIL;
	if( eOTA_InvalidateStoredImage( OtaCoAttr.u8Endpoint, OtaCoAttr.u8ImageIndex ) != E_ZCL_SUCCESS )
		return E_ZCL_FAIL;

	return E_ZCL_SUCCESS;

}


/****************************************************************************
 *
 * NAME: OtaCoprocessor_WriteBuffer
 *
 * DESCRIPTION:
 * This function is to be called by Co-Processor everytime new block of data for the upgraded image is available and is to be
 * written to the Flash of the JN5148. The passed arguments are the pointer of uint8 * type to the buffer of data, the length of the buffer
 * in bytes and if this is the last block of image data.
 *
 * RETURNS:
 * teZCL_Status
 *
 ****************************************************************************/
teZCL_Status OtaCoprocessor_WriteBuffer(bool bIsLast,uint8 *OtaCoImagebuffer, uint16 u16OtaCoImagebufflen )
{
	if( eOTA_FlashWriteNewImageBlock(OtaCoAttr.u8Endpoint,OtaCoAttr.u8ImageIndex,OtaCoAttr.bServerImage,OtaCoImagebuffer,u16OtaCoImagebufflen,OtaCoAttr.u32OtaFileOffset) != E_ZCL_SUCCESS )
	{
		return E_ZCL_FAIL;
	}
	else
		OtaCoAttr.u32OtaFileOffset = OtaCoAttr.u32OtaFileOffset + u16OtaCoImagebufflen;

	if( bIsLast && OtaCoAttr.bServerImage)
	{
		if(OtaCoAttr.u8ImageIndex > (OTA_MAX_IMAGES_PER_ENDPOINT - 1))
			return E_ZCL_FAIL;

		return eOTA_ServerSwitchToNewImage( OtaCoAttr.u8Endpoint, OtaCoAttr.u8ImageIndex);
	}
	else if( bIsLast && !OtaCoAttr.bServerImage)
	{
		if ( eOTA_NewImageLoaded(ESP_METER_LOCAL_EP, FALSE, NULL) != E_ZCL_SUCCESS )
		{
			DBG_vPrintf(TRACE_START, "Loading of new Image failed \r\n");
		}
		else
		{
			tsZCL_Address sendAddress;
			tsOTA_ImageNotifyCommand imgCmd;
			tsCLD_PR_Ota otaData;

			sendAddress.eAddressMode = E_ZCL_AM_IEEE;
			sendAddress.uAddress.u64DestinationAddress = OtaCoAttr.u64DestAddress64;
			imgCmd.ePayloadType = E_CLD_OTA_QUERY_JITTER;
			imgCmd.u32NewFileVersion  = OtaCoAttr.u32FileVersion;
			imgCmd.u16ImageType = 0x5148;
			imgCmd.u8QueryJitter = OtaCoAttr.u8QueryJitter;
			imgCmd.u16ManufacturerCode = 0x4A4E;

			if( eOTA_GetServerData(ESP_METER_LOCAL_EP,1, &otaData) != E_ZCL_SUCCESS)
			{
				DBG_vPrintf(TRACE_START, "Error in getting OTA server Parameters\r\n");
			}
			else
			{
				DBG_vPrintf(TRACE_START, "Server Parameter: Current time = %lu \r\n", otaData.u32CurrentTime);
				DBG_vPrintf(TRACE_START, "Server Parameter: Request or Upgrade time = %lu \r\n", otaData.u32RequestOrUpgradeTime);
				DBG_vPrintf(TRACE_START, "Server Parameter: Data Size = %d \r\n", otaData.u8DataSize);
				DBG_vPrintf(TRACE_START, "Server Parameter: Query Jitter = %d \r\n",  otaData.u8QueryJitter);

				otaData.u32RequestOrUpgradeTime = OtaCoAttr.u32RequestUpgradeTime;

				if( eOTA_SetServerParams(ESP_METER_LOCAL_EP,1, &otaData) != E_ZCL_SUCCESS)
				{
					DBG_vPrintf(TRACE_START, "Could not set Server Parameter, Error!\r\n");
				}
			}

			eOTA_ServerImageNotify(ESP_METER_LOCAL_EP,1,&sendAddress, &imgCmd);
		}

	}

	return E_ZCL_SUCCESS;
}
#endif


/****************************************************************************
 *
 * NAME: vUnclaimedInterrupt
 *
 * DESCRIPTION:
 * Catches any unexpected interrupts
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUnclaimedInterrupt(void)
{
	register uint32 u32PICSR, u32PICMR;

	asm volatile ("l.mfspr %0,r0,0x4800" :"=r"(u32PICMR) : );
	asm volatile ("l.mfspr %0,r0,0x4802" :"=r"(u32PICSR) : );

	DBG_vPrintf(TRACE_EXCEPTION, "Unclaimed interrupt : %x : %x\n", u32PICSR,u32PICMR);

	while (1);
}


/****************************************************************************
 *
 * NAME: APP_isrBusErrorException
 *
 * DESCRIPTION:
 * Catches any bus error exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrBusErrorException)
{
	DBG_vPrintf(TRACE_EXCEPTION, "Bus error\n");
	DBG_vDumpStack();
	while(1);
}


/****************************************************************************
 *
 * NAME: APP_isrAlignmentException
 *
 * DESCRIPTION:
 * Catches any address alignment exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrAlignmentException)
{
	DBG_vPrintf(TRACE_EXCEPTION, "Align error\n");
	DBG_vDumpStack();
	while(1);
}


/****************************************************************************
 *
 * NAME: APP_isrIllegalInstructionException
 *
 * DESCRIPTION:
 * Catches any illegal instruction exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrIllegalInstruction)
{
	DBG_vPrintf(TRACE_EXCEPTION, "Illegal error\n");
	DBG_vDumpStack();
	while(1);
}


/****************************************************************************
 *
 * NAME: APP_isrStackOverflowException
 *
 * DESCRIPTION:
 * Catches any stack overflow exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrStackOverflowException)
{
	DBG_vPrintf(TRACE_EXCEPTION, "StackOverflow error\n");
	DBG_vDumpStack();
	while(1);
}


/****************************************************************************
 *
 * NAME: APP_isrUnimplementedModuleException
 *
 * DESCRIPTION:
 * Catches any unimplemented module exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrUnimplementedModuleException)
{
	DBG_vPrintf(TRACE_EXCEPTION, "Unimplemented error\n");
	DBG_vDumpStack();
	while(1);
}


#ifdef OVERLAYS_BUILD
/****************************************************************************
 *
 * NAME: vGrabLock
 *
 * DESCRIPTION:
 *  Implements counting semaphore protection (grab routine)
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vGrabLock(void)
{
	OS_eEnterCriticalSection(hSpiMutex);
}


/****************************************************************************
 *
 * NAME: vReleaseLock
 *
 * DESCRIPTION:
 * Implements counting semaphore protection (release routine)
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vReleaseLock(void)
{
	OS_eExitCriticalSection(hSpiMutex);
}


/****************************************************************************
 *
 * NAME: vOverlayEvent
 *
 * DESCRIPTION:
 * Provides debug capability in the overlays.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vOverlayEvent(OVLY_teEvent eEvent, OVLY_tuEventData *puData)
{
    switch (eEvent)
    {
        case OVLY_E_EVENT_LOAD:
            DBG_vPrintf(TRACE_OVERLAYS, "\nLoad %d %x > %d %x",
                    puData->sLoad.u16ReturnPage,
                    puData->sLoad.u32ReturnAddress,
                    puData->sLoad.u16TargetPage,
                    puData->sLoad.u32TargetAddress);
            break;

        case OVLY_E_EVENT_READ:
            DBG_vPrintf(TRACE_OVERLAYS, "\nRead Off %x %d ",
                    puData->sRead.u32Offset,
                    puData->sRead.u16Length);
            break;

        case OVLY_E_EVENT_INTERRUPTED:
            DBG_vPrintf(TRACE_OVERLAYS, "\nAbort");
            break;

        case OVLY_E_EVENT_ERROR_SIZE:
            DBG_vPrintf(TRACE_OVERLAYS, "\nError Size");
            break;

        case OVLY_E_EVENT_ERROR_INDEX:
            DBG_vPrintf(TRACE_OVERLAYS, "\nError Index");
            break;

        case OVLY_E_EVENT_ERROR_CHECKSUM:
            DBG_vPrintf(TRACE_OVERLAYS, "\nError checksum");
            break;

        case OVLY_E_EVENT_FAILED:
            DBG_vPrintf(TRACE_OVERLAYS, "\nLOAD FAILED");
            while(1);
            break;
    }
}
#endif



#ifdef PDM_EEPROM
/****************************************************************************
 *
 * NAME: vPdmEventHandlerCallback
 *
 * DESCRIPTION:
 * Handles PDM callback, information the application of PDM conditions
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vPdmEventHandlerCallback(uint32 u32EventNumber, PDM_eSystemEventCode eSystemEventCode)
{

    switch (eSystemEventCode) {
        /*
         * The next three events will require the application to take some action
         */
        case E_PDM_SYSTEM_EVENT_WEAR_COUNT_TRIGGER_VALUE_REACHED:
            DBG_vPrintf(TRACE_START, "PDM: Segment %d reached trigger wear level\n", u32EventNumber);
            break;
        case E_PDM_SYSTEM_EVENT_DESCRIPTOR_SAVE_FAILED:
            DBG_vPrintf(TRACE_START, "PDM: Record Id %d failed to save\n", u32EventNumber);
            DBG_vPrintf(TRACE_START, "PDM: Capacity %d\n", u8PDM_CalculateFileSystemCapacity() );
            DBG_vPrintf(TRACE_START, "PDM: Occupancy %d\n", u8PDM_GetFileSystemOccupancy() );
            break;
        case E_PDM_SYSTEM_EVENT_PDM_NOT_ENOUGH_SPACE:
            DBG_vPrintf(TRACE_START, "PDM: Record %d not enough space\n", u32EventNumber);
            DBG_vPrintf(TRACE_START, "PDM: Capacity %d\n", u8PDM_CalculateFileSystemCapacity() );
            DBG_vPrintf(TRACE_START, "PDM: Occupancy %d\n", u8PDM_GetFileSystemOccupancy() );
            break;

        /*
         *  The following events are really for information only
         */
        case E_PDM_SYSTEM_EVENT_EEPROM_SEGMENT_HEADER_REPAIRED:
            DBG_vPrintf(TRACE_START, "PDM: Segment %d header repaired\n", u32EventNumber);
            break;
        case E_PDM_SYSTEM_EVENT_SYSTEM_INTERNAL_BUFFER_WEAR_COUNT_SWAP:
            DBG_vPrintf(TRACE_START, "PDM: Segment %d buffer wear count swap\n", u32EventNumber);
            break;
        case E_PDM_SYSTEM_EVENT_SYSTEM_DUPLICATE_FILE_SEGMENT_DETECTED:
            DBG_vPrintf(TRACE_START, "PDM: Segement %d duplicate selected\n", u32EventNumber);
            break;
        default:
            DBG_vPrintf(TRACE_START, "PDM: Unexpected call back Code %d Number %d\n", eSystemEventCode, u32EventNumber);
            break;
    }
}
#endif

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
