/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_zcl_task.h
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        ZCL Handler Header
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/METER_NODE/Source/app_zcl_task.h $
 *
 * $Revision: 8538 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-01-04 11:06:48 +0000 (Wed, 04 Jan 2012) $
 *
 * $Id: app_zcl_task.h 8538 2012-01-04 11:06:48Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_ZCL_TASK_H
#define APP_ZCL_TASK_H

#include <jendefs.h>
#include "se.h"
#include "SimpleMetering.h"
#include "esp_meter.h"
#include "zps_apl_aib.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void	APP_ZCL_vInitialise(void);
PUBLIC void	APP_ZCL_UpdateMeterData(tsSE_EspMeterDevice *psMeter);
PUBLIC void	vShowTime(uint32 u32Time);

#ifdef CLD_MC
PUBLIC void vSendNewMessageToBound(uint32 u32MessageId, uint8 u8MessageControl, uint32 StartTime, uint16 u16Duration, char *Message);
PUBLIC void vSendCancelMessageToBound(uint32 u32MessageId, uint8 u8MessageControl);
#endif

#ifdef CLD_DRLC
PUBLIC void vSendNewLceToBound(uint32 u32IssuerId, uint16 u16DeviceClass, uint32 u32StartTime, uint16 u16DurationInMinutes, uint8 u8CriticalityLevel, uint8 u8UtilityEnrolmentGroup);
PUBLIC void vSendCancelLceToBound(uint32 u32IssuerId, uint16 u16DeviceClass, uint32 u32effectiveTime);
PUBLIC void vSendCancelAllLceToBound(uint8 eCancelEventControl);
#endif

#ifdef CLD_PRICE
PUBLIC bool APP_bAddPrice(tsSE_PricePublishPriceCmdPayload sPrice, uint8 u8Ep);
#endif

PUBLIC void vSendWriteIndividualAttritbutesReqToBound(uint16 u16AttributeId, uint16 u16ClusterId, bool_t bServerToClient);
PUBLIC void vSendReadIndividualAttritbutesReqToBound(uint16 u16AttributeId, uint16 u16ClusterId, bool_t bServerToClient);
PUBLIC void vLockZCLMutex(void);
PUBLIC void vUnlockZCLMutex(void);
PUBLIC void vClearExpiredFlag(OS_thSWTimer hSWTimer);
PUBLIC teZCL_Status eAPP_GetBinding(uint16 u16Index, ZPS_tsAplApsmeBindingTableEntry* psEntry);
PUBLIC bool_t bIsBindingAvailable(uint16 u16Cluster, uint8 u8SourceEndpoint);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* APP_ZCL_TASK_H */
