/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (IPD)
 *
 * COMPONENT:          app_ipd_node.h
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        SE In-Premise Display - Main Header File
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_ipd_node.h $
 *
 * $Revision: 9278 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-07 17:54:37 +0100 (Thu, 07 Jun 2012) $
 *
 * $Id: app_ipd_node.h 9278 2012-06-07 16:54:37Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/
#ifndef APP_IPD_NODE_H
#define APP_IPD_NODE_H

//#define STACK_MEASURE

#include <jendefs.h>
#include "zcl.h"
#include "app_timer_driver.h"
#include "SimpleMetering.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define IPD_BASE_LOCAL_EP 						1

#define ONE_SECOND_TICK_TIME    				APP_TIME_MS(1000)
#define	INSTANTANEOUS_TIMEOUT					APP_TIME_MS(12000)

#define APPLICATION_REJOIN_ENABLED

#ifdef APPLICATION_REJOIN_ENABLED
#define MAX_MISSED_APS_ACKS 3
#endif

#ifndef JENNIC_CHIP_FAMILY_JN514x
	#define IPD_STATE       0x1
	#define ZIGBEE_R20
#else
	#define IPD_STATE       "IPD_STATE"
#endif



#define MAXIMUM_SW_TIMER_PERIOD					APP_TIME_MS(134210)				// Maximum number of ticks (2147483647) / conversion to mS (16000)
#define RESTART_TIME							APP_TIME_MS(1000)
#define READ_REQUEST_TIMEOUT					APP_TIME_MS(7500)
#define KEY_ESTABLISHMENT_WAIT					APP_TIME_MS(60000)
#define KEY_ESTABLISHMENT_TIMEOUT				APP_TIME_MS(60000)//MAXIMUM_SW_TIMER_PERIOD
#define SECONDS_IN_A_DAY						86400							// Once a day = 24 (hrs) * 60 (min) * 60 (s)

#define MAX_DISCOVERY_ATTEMPT					64
//#define JOIN_ROUTER_ONLY														// Remove comments to force the device to only join routers and not the co-ordinator

#ifdef  CLD_OTA
#define APPLICATION_VERSION				1
#define IMAGE_TYPE						0x0001
#define MANUFACTURER_CODE				0x1234
#if (OTA_MAX_CO_PROCESSOR_IMAGES != 0)
#define COPROCESSOR_APPLICATION_VERSION	1
#define COPROCESSOR_IMAGE_TYPE			0x0002
#endif
#endif
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

typedef enum {
	E_CONFIG,
	E_START_NETWORK,
#ifdef SE_CERTIFICATION
	E_IDLE,
#endif
	E_DISCOVERING_NETWORKS,
	E_STATE_WAIT_LEAVE,
	E_JOINING_NETWORK,
	E_RESCAN,
	E_STATE_START_KEY_ESTABLISHMENT,
	E_STATE_WAIT_KEY_ESTABLISHMENT,

	E_SEND_MATCH,
	E_WAIT_MATCH,
	E_IEEE_ADDR_LOOKUP,
	E_IEEE_ADDR_LOOKUP_RESPONSE,
	E_BIND_REQ,
	E_BIND_RESP,
	E_REQ_PRICE_KEY,
	E_WAIT_PRICE_KEY,
	E_REQ_METER_KEY,
	E_WAIT_METER_KEY,
	E_READ_METERING_DEVICE_TYPE,
	E_WAIT_METERING_DEVICE_TYPE,
	E_READ_COMMODITY_TYPE,
	E_WAIT_COMMODITY_TYPE,
	E_RUNNING,
} teState;

typedef struct {
	tsZCL_Address				sAddress;
	uint8						u8KeyEstablishmentEndPoint;
	bool_t						bTimeBindSuccess;
	bool_t						bMessageBindSuccess;
	bool_t                      bPriceBindSuccess;
	bool_t                      bDrlcBindSuccess;
	uint8						u8TimeEndPoint;
	uint8						u8DrlcEndPoint;
	uint8						u8MessageEndPoint;
	uint8						u8OtaEndPoint;
} tsEsp;

typedef struct {
	uint16						u16DestinationAddress;
	uint64						u64DestinationAddress;
	bool_t						bValidKey;
	uint8						u8PriceEndPoint;
	uint8						u8CommodityType;
	bool_t						bBindSuccess;
}tsPrice;

typedef struct {
	uint16						u16DestinationAddress;
	uint64						u64DestinationAddress;
	bool_t						bValidKey;
	uint8 						u8MeterEndPoint;
	uint8						u8MeterDeviceType;
	uint8						u8PriceIndex;
	bool_t						bBindSuccess;
	bool_t						bInstantaneousDemandSupported;
} tsMeter;



typedef struct {
	teState 					eState;
	bool_t 						bKeyEstComplete;
#ifdef CLD_PRICE
	bool_t 						bPriceDiscComplete;
#endif
#ifdef CLD_TIME
	bool_t 						bTimeDiscComplete;
#endif
#ifdef CLD_MC
	bool_t 						bMessageDiscComplete;
#endif
#ifdef CLD_DRLC
    bool_t                      bDrlcDiscComplete;
#endif
#ifdef CLD_OTA
	bool_t 						bOtaDiscComplete;
#endif
	tsEsp						sEsp;
	tsMeter 					asMeter[SE_NUMBER_OF_ENDPOINTS];
	tsPrice						asPrice[NUMBER_OF_SUPPORTED_COMMODITY_TYPES];
} tsDevice;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void APP_vInitialise(void);
PUBLIC void ResetServiceDiscovery(void);
PUBLIC void HandleUnknownDisplayState(void);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* APP_IPD_NODE_H */
