# Smart Home Energy

Motivação
-----

Com a atual situação da distribuição de energia elétrica, onde o valor do consumo e dos impostos vem aumentando e a qualidade do serviço não acompanha a elevação, vê-se a necessidade de maior controle por parte do cliente.

Hoje o cliente pode ter o monitoramento e controle sobre essas informações de forma rápida, barata e cômoda, devido ao avanço das tecnologias de comunicação, algo que, até poucos anos atrás, nem se imaginava possível.


Objetivo
-----

Auxiliar o usuário a controlar o consumo de energia da sua casa, com a comodidade de acessar os dados via Web para monitorar e interferir de acordo com o seu planejamento financeiro.


Resultado esperado
-----

Possibilidade de monitorar e controlar o consumo de energia da residência, bem como avisar picos e faltas de energia para proteger a rede elétrica e os equipamentos conectados ao smart socket.


Wiki do projeto
-----
Dados construtivos e funcionais podem ser consultados na [wiki](https://gitlab.com/korndorfer/smart-home-energy/wikis/home) do projeto.