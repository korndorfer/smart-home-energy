/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_meter_node.h
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        SE Meter - Main Header File
 *
 * $HeadURL: http://svn/apps/Application_Notes/JN-AN-1135-ZigBee-Pro-SE-Home-Energy-Monitor/Trunk/METER_NODE/Source/app_meter_node.h $
 *
 * $Revision: 5862 $
 *
 * $LastChangedBy: jpenn $
 *
 * $LastChangedDate: 2010-04-27 10:58:38 +0100 (Tue, 27 Apr 2010) $
 *
 * $Id: app_meter_node.h 5862 2010-04-27 09:58:38Z jpenn $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_METER_NODE_H_
#define APP_METER_NODE_H_

#include <jendefs.h>
#include "zcl.h"
#include "app_timer_driver.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define LOCAL_EP 				3

#define ONE_SECOND_TICK_TIME    		APP_TIME_MS(1000)
#define	INSTANTANEOUS_TIMEOUT			APP_TIME_MS(12000)

#define APPLICATION_REJOIN_ENABLED

#ifdef APPLICATION_REJOIN_ENABLED
#define MAX_MISSED_APS_ACKS 			3
#define MAX_ROUTE_DISCOVERY_FAILURES 	3
#endif

#define MAX_NUMBER_OF_SUPPORTED_METERS	3

#ifndef JENNIC_CHIP_FAMILY_JN514x
	#define METER_STATE       0x1
	#define ZIGBEE_R20
#else
	#define METER_STATE       "METER_STATE"
#endif


#define	ELECTRICITY_METER				0
#define GAS_METER						1
#define WATER_METER						2

#define RESTART_TIME    				APP_TIME_MS(1000)
#define READ_REQUEST_TIMEOUT			APP_TIME_MS(7500)
#define KEY_ESTABLISHMENT_WAIT    		APP_TIME_MS(5000)
#define KEY_ESTABLISHMENT_TIMEOUT  		APP_TIME_MS(300000)
#define SECONDS_IN_A_DAY				86400									// Once a day = 24 (hrs) * 60 (min) * 60 (s)

#define MAX_DISCOVERY_ATTEMPT	64
//#define ONLY_JOIN_ROUTER														// Remove comments to force the device to only join routers and not the co-ordinator

#define BIND_RESP               		0x8021

#define NUMBER_OF_PRICE_TIERS	3

#define EMPTY							0
#define ONE_MINUTE						1
#define TWO_MINUTES						2
#define MAX_TIME_INTERVAL 				65535

#define POT_READ

//#define RADIO_RECALIBRATION

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

typedef enum
{
	E_CONFIG,
	E_START_NETWORK,
	E_DISCOVERING_NETWORKS,
	E_JOINING_NETWORK,
	E_RESCAN,
	E_SEND_MATCH,
	E_WAIT_MATCH,
	E_STATE_START_KEY_ESTABLISHMENT,
	E_STATE_WAIT_KEY_ESTABLISHMENT,
	E_STATE_WAIT_LEAVE,
	E_BIND_REQ,
	E_BIND_RESP,
	E_WAITING_START,
	E_MESSAGE,
	E_RUNNING,
} teState;

typedef struct {
	tsZCL_Address	sAddress;
	uint8			u8KeyEstablishmentEndPoint;
	uint8			u8PriceEndPoint;
	uint8			u8TimeEndPoint;
	uint8			u8DrlcEndPoint;
	uint8			u8MessageEndPoint;
} tsEsp;


typedef struct {
	teState 		eState;
	bool 			bKeyEstComplete;
	tsEsp			sEsp;
} tsDevice;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void APP_vInitialise(void);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_METER_NODE_H_*/
