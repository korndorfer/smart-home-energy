/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (EVK-IPD)
 *
 * COMPONENT:          app_led.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        LED Operation
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/LC_IPD_NODE/Source/app_led.c $
 *
 * $Revision: 8491 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-15 14:32:54 +0000 (Thu, 15 Dec 2011) $
 *
 * $Id: app_led.c 8491 2011-12-15 14:32:54Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/


/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include "jendefs.h"
#include "utilities.h"
#include <AppHardwareApi.h>
#include "dbg.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/**************************************************************************///

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vLedInitialise
 *
 * DESCRIPTION:
 * Configure the LED DIO
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vLedInitialise(void)
{
	/* Fall through function, LEDs not used in this application */
}


/****************************************************************************
 *
 * NAME: vLedSet
 *
 * DESCRIPTION:
 * Turn on the LEDs based on the number of price tiers and the current tier
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vLedSet(uint8 u8NumberOfPriceTiers, uint8 u8CurrentElectricityCostTier)
{
	/* Fall through function, LEDs not used in this application */
}


/****************************************************************************
 *
 * NAME: vLedClear
 *
 * DESCRIPTION:
 * Turn off the LEDs
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vLedClear(void)
{
	/* Fall through function, LEDs not used in this application */
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
