from App import app, db, loginManager
from flask import render_template, flash, redirect, url_for, Response, jsonify, request
from flask_login import login_user, logout_user, login_required, current_user
from App.models.forms import Login, Register, Check
from App.models.tables import User, Data, Socket
from datetime import datetime
import json
import time

last_query = Data(0,0,0,0,0,0)
last_socket = None
last_command = None
last_energy = None

@loginManager.user_loader
def load_user(id_usuario):
    return User.query.filter_by(id_usuario=id_usuario).first()


@app.route('/dashboard', methods=['GET', 'POST'])
def dashboard():
    if not current_user.is_authenticated:
        return redirect(url_for('login'))
    socket = Socket.query.filter_by(nome_tomada = 'SmartObject1').first()
    return render_template('index.html', socket=socket)


@app.route('/onoff', methods=['POST'])
def onoff():
    socket = Socket.query.filter_by(nome_tomada = 'SmartObject1').first()
    checkbox = request.form['check']
    print(checkbox)
    if checkbox:
        print(socket.comando)
        if socket.comando == False:
            socket.comando = True
        else:
            socket.comando = False
        db.session.commit()
    return jsonify({'check': checkbox})


@app.route('/login', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    login = Login()
    if login.validate_on_submit():
        user = User.query.filter_by(nome_usuario = login.username.data).first()
        if user and user.senha == login.password.data:
            login_user(user)
            return redirect(url_for('dashboard'))
        else:
            flash('Usuário e/ou senha inválidos')
    return render_template('/examples/login.html', login=login)


@app.route('/register', methods=['GET', 'POST'])
def register():
    register = Register()

    if register.validate_on_submit():
        user = User(register.username.data, register.password.data, register.phone.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('dashboard'))
    return render_template('/examples/register.html', register=register)


@app.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    flash('Logoff efetuado')
    return redirect(url_for('login'))


@app.route('/live-data')
def live_data():
    def get_data():
        data = json.dumps({'nothing':0})
        yield 'data:{0}\n\n'.format(data)
        while True:
            global last_query, last_energy
            last_data = Data.query.order_by(Data.id_dado.desc()).first()
            socket = Socket.query.filter_by(nome_tomada = 'SmartObject1').first()
            if socket.energizada != last_energy:
                last_energy = socket.energizada
                data = json.dumps({'energy':socket.energizada})
                yield 'data:{0}\n\n'.format(data)
                time.sleep(1)
            else:
                if last_data.id_dado != last_query.id_dado:
                    last_query = last_data
                    temp = last_data.horario
                    last_data_time = temp.strftime("%H:%M:%S")
                    data = json.dumps({'time': last_data_time, 'value':last_data.potencia,
                    'power':last_data.potencia, 'voltage':last_data.tensao,
                    'current':last_data.corrente, 'energy':socket.energizada})
                    yield 'data:{0}\n\n'.format(data)
                    time.sleep(1)
                else:
                    break
    return Response(get_data(), mimetype='text/event-stream')
