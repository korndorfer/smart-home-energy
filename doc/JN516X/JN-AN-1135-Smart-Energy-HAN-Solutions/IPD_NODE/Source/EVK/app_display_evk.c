/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (EVK-IPD)
 *
 * COMPONENT:          app_display.c
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        Display Functionality
 *
 *
 * $HeadURL: http://svn/apps/Application_Notes/JN-AN-1135-ZigBee-Pro-SE-Home-Energy-Monitor/Trunk/IPD_NODE/Source/app_display.c $
 *
 * $Revision: 6166 $
 *
 * $LastChangedBy: tchia $
 *
 * $LastChangedDate: 2010-06-04 15:21:28 +0100 (Fri, 04 Jun 2010) $
 *
 * $Id: app_display.c 6166 2010-06-04 14:21:28Z tchia $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/


/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include "jendefs.h"
#include "utilities.h"
#include <AppHardwareApi.h>
#include "os.h"
#include "os_gen.h"
#include "app_display.h"
#include "dbg.h"
#include "app_zcl_task.h"
#include "xsprintf.h"
#include "app_ipd_node.h"
#include "app_timer_driver.h"
#include "Time.h"
#include "app_sleep_functions.h"
#include "LcdDriver.h"
#include "NXPlogo.h"
#include "Symbols.h"
#include "app_event_handler.h"
#include "DRLC.h"
#include "zcl_options.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#ifndef TRACE_DISPLAY
#define TRACE_DISPLAY FALSE
#endif
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/**************************************************************************///

PRIVATE void vBuildSplashScreen(void);
PRIVATE void vBuildJoiningFailScreen(void);
PRIVATE void vBuildJoiningScreen(void);
PRIVATE void vBuildRunningScreen(void);
PRIVATE void vUpdateRunningScreen(void);
#ifdef CLD_PRICE
PRIVATE void vBuildPriceScreen(void);
#endif
#ifdef CLD_DRLC
PRIVATE void vUpdateDRLCScreen(void);
PRIVATE void vBuildDRLCScreen(void);
#endif
PRIVATE void vUpdatePriceScreen(void);
#ifdef CLD_MC
PRIVATE void vUpdateMessageScreen(void);
PRIVATE void vBuildMessageConfirmationScreen(void);
#endif
PRIVATE void vBuildTierScreen(void);
PRIVATE void vUpdateTierScreen(void);
PRIVATE void vBuildSetUPScreen(void);
PRIVATE void vUpdateSetUPScreen(void);

PUBLIC void vUpdateHistory_Statistics(uint16 val1,uint16 val2,uint16 val3);
PUBLIC void vLcdDrawLine(int16 i16x1, int16 i16y1, int16 i16x2, int16 i16y2,uint8 onOff);
PUBLIC void vLcdPlotPoint(int16 i16X, int16 i16Y,uint8 onoff);
PRIVATE void Time(void);
PRIVATE void vShowTime(uint32 u32Time, uint8 * u8ParamHour, uint8 * u8ParamMin, uint8 * u8ParamSec);
PRIVATE void vDisplayTime(uint8 u8format, uint8 u8ScreenHour, uint8 u8ScreenMin, uint8 u8ScreenSec);
PRIVATE void vLinkEnergyBar(uint8 X,uint8 Y);
PRIVATE uint8 LinkQualitytoIndex(uint8 u8LocalLinkQuality);
PRIVATE void vBatteryBar(uint8 X,uint8 Y,uint8 BattEnergy);
PRIVATE void Energy_symbol(uint8 mode, uint32 u32LocalMeterDemand, uint8 u8InstantaneousDigitsRightOfDp);
PRIVATE void vEnergyDial(uint8 X,uint8 Y,uint32 EnergyLevel);
PRIVATE uint8 vEnergyCountIndex(uint32 EnergyLevel);
PRIVATE uint32 TwentyFour(uint8 u8Hour);
PRIVATE void Hi_Med_Low(uint8 Row,uint8 Column);
PRIVATE void vDrawArrows(void);

PUBLIC void vUpdateHistory(void);

PUBLIC uint8 Ycoord = 2;

PUBLIC uint16  u16Hist1 = 1, u16Hist2 = 1, u16Hist3 = 1;

#define ON 1
#define OFF 0
#define hr12AM 0
#define hr12PM 1
#define hr24 3

#define MAXDEPTH 100
#define LCD_MIN_X 0
#define LCD_MIN_Y 0
#define LCD_MAX_X 127
#define LCD_MAX_Y 63

PUBLIC bool bMsgOnScreen = FALSE;

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/* only exported for in line functions */
APP_teDisplayState app_eDisplayState;

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/


PRIVATE tsSE_IPDDevice	*psSE_IPDDevice;
PRIVATE tsDeviceLevels	sDisplayDeviceLevels;

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern	bool_t		bLostComms;
extern 	uint8 		u8CurrentMeterOfInterest;
extern 	uint8 		u8TotalNumberOfDiscoveredMeters;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_DisplayUpdateTask
 *
 * DESCRIPTION:
 * Redraws the display
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_DisplayUpdateTask)
{
	static APP_teDisplayState s_ePrevDisplayState = APP_E_DISPLAY_STATE_INIT;
	bool_t bDisplayStateChanged = FALSE;

	DBG_vPrintf(TRACE_DISPLAY, "APP_DisplayUpdateTask: %d\n", app_eDisplayState);

	/* Take a fresh copy of the ZCL data for displaying */
	APP_ZCL_GetIPDData(&psSE_IPDDevice);

    /* Take a fresh copy of the Device data for displaying */
	vGetDeviceLevels(&sDisplayDeviceLevels);

    /* Called every second, so update history here */
    vUpdateHistory();

    /* check to see if the state of the display is being changed */
	if (app_eDisplayState != s_ePrevDisplayState)								// If the state has changed...
	{
        bDisplayStateChanged = TRUE;
        vLcdClear();
        s_ePrevDisplayState = app_eDisplayState;
    }

	/* update the display ... */
	switch (app_eDisplayState)													// Determine the display state
	{
		case APP_E_DISPLAY_STATE_SPLASH:
			if (bDisplayStateChanged)
			{
				vBuildSplashScreen();
			}
		break;

		case APP_E_DISPLAY_STATE_SYNC:
			if (bDisplayStateChanged)
			{
				vBuildJoiningScreen();
			}
		break;

		case APP_E_DISPLAY_STATE_SYNC_FAILED:
			if (bDisplayStateChanged)
			{
				vBuildJoiningFailScreen();
			}
		break;

		case APP_E_DISPLAY_STATE_CURRENT_KW:
		case APP_E_DISPLAY_STATE_CURRENT_M3:
		case APP_E_DISPLAY_STATE_CURRENT_L:
			if (bDisplayStateChanged)
			{
				vBuildRunningScreen();
			}
			vUpdateRunningScreen();
		break;

		case APP_E_DISPLAY_STATE_SETTINGS:
			if (bDisplayStateChanged)
			{
				vBuildSetUPScreen();
			}
			vUpdateSetUPScreen();
		break;

		case APP_E_DISPLAY_STATE_PRICE:
			if (bDisplayStateChanged)
			{
				vBuildPriceScreen();
			}
			vUpdatePriceScreen();
		break;

		case APP_E_DISPLAY_STATE_DRLC:
			if (bDisplayStateChanged)
			{
				vBuildDRLCScreen();
			}
			vUpdateDRLCScreen();
		break;

		case APP_E_DISPLAY_STATE_HISTORY:
			if (bDisplayStateChanged)
			{
				vBuildTierScreen();
			}
			vUpdateTierScreen();
        break;

		case APP_E_DISPLAY_STATE_MESSAGE:
			vUpdateMessageScreen();
		break;

		case APP_E_DISPLAY_STATE_MESSAGE_CONFIRMATION:
			vBuildMessageConfirmationScreen();
		break;

		default:
			DBG_vPrintf(TRACE_DISPLAY, "Unknown Display State\n");				// error - unknown display state
		break;
	}

	/* Refresh LCD with newly drawn screen */
    OS_eEnterCriticalSection(hSpiMutex);
    vLcdRefreshAll();
	OS_eExitCriticalSection(hSpiMutex);

	DBG_vPrintf(TRACE_DISPLAY, "screen updated: \n");

}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vDisplayInitialise
 *
 * DESCRIPTION:
 * Initialises the LCD display
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vDisplayInitialise(void)
{
	/* reset the LCD */
	OS_eEnterCriticalSection(hSpiMutex);
#if(JENNIC_PCB == DEVKIT4)
	vLcdReset(0, 12);
#else
	vLcdResetDefault();
#endif

	OS_eExitCriticalSection(hSpiMutex);
}


/****************************************************************************
 *
 * NAME: APP_vDisplayUpdate
 *
 * DESCRIPTION:
 * Activates the display update task
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vDisplayUpdate(void)
{
	OS_eActivateTask(APP_DisplayUpdateTask);
}


/****************************************************************************
 *
 * NAME: APP_vDisplayUpdate
 *
 * DESCRIPTION:
 * Activates the display update task
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC APP_teDisplayState APP_vGetDisplayState(void)
{
	 return app_eDisplayState;
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vBuildSplashScreen
 *
 * DESCRIPTION:
 * Updates the Splash screen, when it first appears or when the user
 * changes the channel number.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildSplashScreen(void)
{
    /* Draw the splash screen */
    vLcdWriteBitmap((tsBitmap *)&sNXPLogo, 0, 0);
    vLcdWriteText("ZigBee Pro SE", 5, 25);
    vLcdWriteText("In-Premise Display", 6, 16);
    vLcdWriteText("Join", 7, 0);
}


/****************************************************************************
 *
 * NAME: vBuildJoiningFailScreen
 *
 * DESCRIPTION:
 * Updates the Splash screen, when it first appears or when the user
 * changes the channel number.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildJoiningFailScreen(void)
{
	vLcdClear();
    /* Draw the splash screen */
    vLcdWriteBitmap((tsBitmap *)&sNXPLogo, 0, 0);
    vLcdWriteText("Smart Meter ", 5, 35);
    vLcdWriteText("Not Found   ", 6, 38);
    vLcdWriteText("Join", 7, 0);
}


/****************************************************************************
 *
 * NAME: vBuildJoiningScreen
 *
 * DESCRIPTION:Function to BuildJoining screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildJoiningScreen(void)
{
	vLcdClear();
	vLcdWriteBitmap((tsBitmap *)&sNXPLogo, 0, 0);
    vLcdWriteText("Searching For ", 5, 25);
    vLcdWriteText("Smart Meter   ", 6, 30);
}


/****************************************************************************
*
* NAME: vBatteryBar
*
* DESCRIPTION: Function that Displays a symbol of a battery ,with different levels can be
* 			   selected to indicate the level of charge.
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE void vBatteryBar(uint8 X,uint8 Y,uint8 BattEnergy)
{
	tsBitmap sBitmap;

    sBitmap.u8Width  = 8;
    sBitmap.u8Height = 2;

    sBitmap.pu8Bitmap = (uint8 *)&au8Batt[au32Batt_offsets[BattEnergy]];
    vLcdWriteBitmap(&sBitmap, X, Y);
}


/****************************************************************************
*
* NAME: LinkQualitytoIndex
*
* DESCRIPTION: Function that converts the linQuality to an index, this is to display the
* Link Quality bar
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE uint8 LinkQualitytoIndex(uint8 u8LocalLinkQuality)
{
	uint8 Index = 0;

	if(u8LocalLinkQuality <= 30)
	{
		Index = 0;
	}
	else if((u8LocalLinkQuality>30&&u8LocalLinkQuality<=50))
	{
		Index = 2;
	}
	else if((u8LocalLinkQuality>50&&u8LocalLinkQuality<=70))
	{
		Index = 4;
	}
	else if((u8LocalLinkQuality>70&&u8LocalLinkQuality<=90))
	{
		Index = 6;
	}
	else
	{
		Index = 8;
	}

	return Index;
}


/****************************************************************************
*
* NAME: vLinkEnergyBar
*
* DESCRIPTION: Function that Displays a symbol showing the LinkQuality
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE void vLinkEnergyBar(uint8 X,uint8 Y)
{
    tsBitmap sBitmap;
	sBitmap.u8Width  = 8;
	sBitmap.u8Height = 2;

  	DBG_vPrintf(TRACE_DISPLAY, "LQI: %d\n", sDisplayDeviceLevels.u8Lqi);

	sBitmap.pu8Bitmap = (uint8 *)&au8EnergyMeterBar[au32Energy_offsets[LinkQualitytoIndex(sDisplayDeviceLevels.u8Lqi)]];
	vLcdWriteBitmap(&sBitmap, X, Y);

	sBitmap.pu8Bitmap = (uint8 *)&au8EnergyMeterBar[au32Energy_offsets[LinkQualitytoIndex(sDisplayDeviceLevels.u8Lqi)+1]];
	vLcdWriteBitmap(&sBitmap, X+8, Y);
}


/****************************************************************************
*
* NAME: TwentyFour
*
* DESCRIPTION:Function to convert 24hr clock to 12hr clock
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE uint32 TwentyFour(uint8 u8Hour)
{
	if( u8Hour == 0 )
	{
		u8Hour = 12;
	}
	else if(u8Hour>12)
	{
		u8Hour -= 12;
	}

	return u8Hour;
}


/****************************************************************************
*
* NAME: vDisplayTime
*
* DESCRIPTION: Function to select the format of the time
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE void vDisplayTime(uint8 u8format, uint8 u8ScreenHour, uint8 u8ScreenMin, uint8 u8ScreenSec)
{
	char acBuffer[10] = {0};
	char acBuffer1[3] = {0};

	uint8 X = 8, Y = 0;

	vLcdWriteText("            ",Y,X);

	if(u8format==hr24)
	{
		xsprintf(acBuffer,"%02d:%02d",u8ScreenHour, u8ScreenMin);
	}
	else
	{
		if(u8format==hr12PM)
		{
			xsprintf(acBuffer1,"PM");
		}
		else
		{
			xsprintf(acBuffer1,"AM");
		}

		xsprintf(acBuffer,"%02d:%02d",TwentyFour(u8ScreenHour), u8ScreenMin);
	}
	vLcdWriteText(acBuffer, Y, X);
	vLcdWriteText(acBuffer1, Y, X+46);
}


/****************************************************************************
*
* NAME: Time
*
* DESCRIPTION: Function to display the time in 24hr or 12hr mode and add a
* 			   leading 0.
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE void Time( void )
{
	uint8 u8ScreenHour, u8ScreenMin, u8ScreenSec;

	vShowTime(u32ZCL_GetUTCTime(), &u8ScreenHour,  &u8ScreenMin, &u8ScreenSec);

	if(bZCL_GetTimeHasBeenSynchronised())
	{
		if(sDisplayDeviceLevels.bTimeType == FALSE)
		{
			if(u8ScreenHour>=12&&u8ScreenHour<=23)
			{
				vDisplayTime(hr12PM, u8ScreenHour, u8ScreenMin, u8ScreenSec );
			}
			else
			{
				vDisplayTime(hr12AM, u8ScreenHour, u8ScreenMin, u8ScreenSec );
			}
		}
		else
		{
			vDisplayTime(hr24, u8ScreenHour, u8ScreenMin, u8ScreenSec );
		}
	}
}


/****************************************************************************
 *
 * NAME: vShowTime
 *
 * DESCRIPTION:
 * Calculates the time from UTC and sends to the UART
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vShowTime(uint32 u32Time, uint8 * u8ParamHour, uint8 * u8ParamMin, uint8 * u8ParamSec)
{
	uint32 u32Sec, u32Min, u32Hour, u32Day, u32Month, u32Year;
	uint8 au8MonthTable [] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	u32Day  = u32Time / (60 * 60 * 24);
	u32Time = u32Time % (60 * 60 * 24);

	u32Hour = u32Time / (60 * 60);
	u32Time = u32Time % (60 * 60);

	u32Min  = u32Time / 60;
	u32Time = u32Time % 60;

	u32Sec = u32Time;

    u32Year = 0;
    // Using u32Year%4 will fail in 2100, which isn't a leap year.
    // The printf will also fail in 2100.
    for(;;)
    {
        uint32 u32DaysThisYear = ((u32Year % 4) == 0) ? 366 : 365;
        if (u32Day < u32DaysThisYear)
            break;
        u32Day -= u32DaysThisYear;
        u32Year++;
    }

    if ((u32Year % 4) == 0)
        au8MonthTable[1] = 29;
    else
        au8MonthTable[1] = 28;

    u32Month = 0;
    for(;;)
    {
        uint32 u32DaysThisMonth = au8MonthTable[u32Month];
        if (u32Day < u32DaysThisMonth)
            break;
        u32Day -= u32DaysThisMonth;
        u32Month++;
    }

    // Day and month are calculated zero based but displayed one based.
	//DBG_vPrintf(TRACE_ZCL_TASK, "%d/%d/20%02d %02d:%02d:%02d\n", u32Day+1, u32Month+1, u32Year, u32Hour, u32Min, u32Sec);

    if(u8ParamHour != NULL)
    {
		/* Store the time values for the display funtion */
		*u8ParamHour = u32Hour;
		*u8ParamMin =  u32Min;
		*u8ParamSec = u32Sec;
    }
}


/****************************************************************************
*
* NAME: Energy_symbol
*
* DESCRIPTION:Function to display elements of power consumption on the main screen.
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE void Energy_symbol(uint8 mode, uint32 u32LocalMeterDemand, uint8 u8InstantaneousDigitsRightOfDp)
{
	tsBitmap sBitmap;

	sBitmap.u8Width  = 8;
	sBitmap.u8Height = 2;

	char acBuffer[10] = {0};

	tsSE_PricePublishPriceCmdPayload *pPricePublishPriceCmdPayload;
	uint32 co2 = 0, u32Price = 0;

	uint8 XBitmap = 10, YBitmap = 2;
	uint8 a = 0, b = 0;
	uint8 u8SymbolIndex = 0;

	xsprintf(acBuffer,"         ");
	vLcdWriteText(acBuffer, YBitmap, 20);

	if(mode == 2) //Currency
	{
		u8SymbolIndex = sDisplayDeviceLevels.u8CurrencyType;

		vLockZCLMutex();														// Mutex the price data
		if(E_ZCL_SUCCESS == eSE_PriceGetPriceEntry(u8CurrentMeterOfInterest + 1, FALSE, 0, &pPricePublishPriceCmdPayload ))
		{
			u32Price = pPricePublishPriceCmdPayload->u32Price;
			//u8PriceDp = pPricePublishPriceCmdPayload->u8PriceTrailingDigitAndPriceTier >> 4;	// the number of price digits to the right of the decimal point

			u32Price = u32LocalMeterDemand * u32Price / 100;

		    a = u32Price / 100;
		    b = u32Price % 100;
		}
		vUnlockZCLMutex();														// Clear the mutex
	}
	else if(mode == 1) //Co2
	{
		u8SymbolIndex = 3;

		co2 = (CO2_CONVERSION_CONSTANT * u32LocalMeterDemand) / 1000;

		a = co2 / 100;
		b = co2 % 100;
	}
	else //Kwh
	{
		switch (app_eDisplayState)
		{
			case APP_E_DISPLAY_STATE_CURRENT_KW:
				u8SymbolIndex = 4;
			break;

			case APP_E_DISPLAY_STATE_CURRENT_M3:
				u8SymbolIndex = 6;
			break;

			case APP_E_DISPLAY_STATE_CURRENT_L:
				u8SymbolIndex = 7;
			break;

			default:
			break;
		}

		a = u32LocalMeterDemand / 100;
	    b = u32LocalMeterDemand % 100;
	}

	sBitmap.pu8Bitmap = (uint8 *)&au8Energy_Symbol[au32EnergySymbol_offsets[u8SymbolIndex]];
	vLcdWriteBitmap(&sBitmap, XBitmap, YBitmap);

	if (!bLostComms)
	{
		xsprintf(acBuffer, "%02d`%02d", a, b);
	}
	else
	{
		/* If comms are lost with the meter don't display any readings */
		xsprintf(acBuffer,"`````");
	}

	vLcdWriteText(acBuffer, YBitmap, 20);

	xsprintf(acBuffer, "M%d", u8CurrentMeterOfInterest + 1);
	vLcdWriteText(acBuffer, 2, 112);
}


/****************************************************************************
*
* NAME: vEnergyCount
*
* DESCRIPTION:Threshold to count value conversion
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE uint8 vEnergyCountIndex(uint32 EnergyLevel)
{
	uint8 count = 0;

	if (EnergyLevel > 105)
	{
		count = 21;
	}
	else if (EnergyLevel > 100)
	{
		count = 20;
	}
	else if (EnergyLevel > 95)
	{
		count = 19;
	}
	else if (EnergyLevel > 90)
	{
		count = 18;
	}
	else if (EnergyLevel > 85)
	{
		count = 17;
	}
	else if (EnergyLevel > 80)
	{
		count = 16;
	}
	else if (EnergyLevel > 75)
	{
		count = 15;
	}
	else if (EnergyLevel > 70)
	{
		count = 14;
	}
	else if (EnergyLevel > 65)
	{
		count = 13;
	}
	else if (EnergyLevel > 60)
	{
		count = 12;
	}
	else if (EnergyLevel > 55)
	{
		count = 11;
	}
	else if (EnergyLevel > 50)
	{
		count = 10;
	}
	else if (EnergyLevel > 45)
	{
		count = 9;
	}
	else if (EnergyLevel > 40)
	{
		count = 8;
	}
	else if (EnergyLevel > 35)
	{
		count = 7;
	}
	else if (EnergyLevel > 30)
	{
		count = 6;
	}
	else if (EnergyLevel > 25)
	{
		count = 5;
	}
	else if (EnergyLevel > 20)
	{
		count = 4;
	}
	else if (EnergyLevel > 15)
	{
		count = 3;
	}
	else if (EnergyLevel > 10)
	{
		count = 2;
	}
	else if (EnergyLevel > 5)
	{
		count = 1;
	}
	else if (EnergyLevel >= 0 && EnergyLevel < 5)
	{
		count = 0;
	}

	return count;
}


/****************************************************************************
*
* NAME: EnergyDial
*
* DESCRIPTION:Function to display immediate demand dial on the main screen.
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE void vEnergyDial(uint8 X,uint8 Y,uint32 EnergyLevel)
{
	tsBitmap sBitmap;
	uint8 mxcount = 21, i = 0, p = 0, z = 1;

	sBitmap.u8Width  = 8;
    sBitmap.u8Height = 1;

	vLcdDrawLine(25, 47, 95,  4,  ON);
	vLcdDrawLine(55, 47, 125, 4,  ON);

	vLcdDrawLine(95, 4,  125, 4,  ON);
	vLcdDrawLine(25, 47, 55,  47, ON);

	//draw bar
	for(i = 0 ; i < vEnergyCountIndex(EnergyLevel) ; i++)
	{
			vLcdDrawLine(xx1[p], yyy[p], xx2[p], yyy[p], ON);
			vLcdDrawLine(xx1[z], yyy[z], xx2[z], yyy[z], ON);
			p = p + 2;
			z = z + 2;
	}

	//clear bar
	for(i = vEnergyCountIndex(EnergyLevel) ; i < mxcount ; i++)
	{
			vLcdDrawLine(xx1[p], yyy[p], xx2[p], yyy[p], ON);
			vLcdDrawLine(xx1[z], yyy[z], xx2[z], yyy[z], ON);
			vLcdDrawLine(xx1[p], yyy[p], xx2[p], yyy[p], OFF);
			vLcdDrawLine(xx1[z], yyy[z], xx2[z], yyy[z], OFF);
			p = p + 2;
			z = z + 2;
	}
}


/****************************************************************************
 *
 * NAME: vBuildRunningScreen
 *
 * DESCRIPTION: Function to Build the main screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildRunningScreen(void)
{
    vLcdClear();

	vLcdWriteText("Mode", 7, 0);

	vLcdWriteText("GetP", 7, 32);


	vDrawArrows();
}


/****************************************************************************
*
* NAME: Hi_Med_Low
*
* DESCRIPTION:Function to display and indication of the tariff that is used
* 			  on the main screen.
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PRIVATE void Hi_Med_Low(uint8 Row,uint8 Column)
{
	 tsSE_PricePublishPriceCmdPayload *pPricePublishPriceCmdPayload;
	 uint8 Tier = 0;

	 if(E_ZCL_SUCCESS == eSE_PriceGetPriceEntry(1, FALSE, 0, &pPricePublishPriceCmdPayload ))
	 { //update history as list is consumed
		Tier = pPricePublishPriceCmdPayload->u8PriceTrailingDigitAndPriceTier;

		if(Tier == 1)
		{
			vLcdWriteText("Low ",Row , Column);
		}
		else if(Tier == 2)
		{
			vLcdWriteText("Med ",Row , Column);
		}
		else if(Tier == 3)
		{
			vLcdWriteText("High",Row , Column);
		}
	}
}


/****************************************************************************
 *
 * NAME: vUpdateRunningScreen
 *
 * DESCRIPTION: Function to update the running screen.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUpdateRunningScreen(void)
{
	uint8 u8DigitsRightOfDp = 0;
	uint32 u32LocalDemandInteger, u32LocalDemandRemainder, u32LocalDemand, u32Divisor = 0;

	Hi_Med_Low(3,20);

	vBatteryBar(0,0,sDisplayDeviceLevels.u8BattLevel); 		 	 				//x , y , BatteyEnergy Levels 0-6 possible

	vLinkEnergyBar(108,4); 	 													//x, y

	if ((app_eDisplayState == APP_E_DISPLAY_STATE_CURRENT_KW || app_eDisplayState == APP_E_DISPLAY_STATE_CURRENT_M3) || app_eDisplayState == APP_E_DISPLAY_STATE_CURRENT_L)
	{
		if(E_CLD_SM_MDT_ELECTRIC == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType)
		{
			app_eDisplayState = APP_E_DISPLAY_STATE_CURRENT_KW;
		}
		else if (E_CLD_SM_MDT_GAS == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType || E_CLD_SM_MDT_GAS_MIRRORED == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType)
		{
			app_eDisplayState = APP_E_DISPLAY_STATE_CURRENT_M3;
		}
		else if (E_CLD_SM_MDT_WATER == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType || E_CLD_SM_MDT_WATER_MIRRORED == psSE_IPDDevice->sSimpleMeteringCluster.eMeteringDeviceType)
		{
			app_eDisplayState = APP_E_DISPLAY_STATE_CURRENT_L;
		}
	}

	u32LocalDemandInteger = psSE_IPDDevice->sSimpleMeteringCluster.i24InstantaneousDemand;
	if (psSE_IPDDevice->sSimpleMeteringCluster.u24Multiplier)					// If the multiplier is non-zero
	{
		u32LocalDemandInteger *= psSE_IPDDevice->sSimpleMeteringCluster.u24Multiplier;
	}

	u32LocalDemandRemainder = u32LocalDemandInteger;
	if (psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor)						// If the divisor is non-zero
	{
		u32LocalDemandInteger /= psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor;
	}
	u32LocalDemandRemainder -= (u32LocalDemandInteger * psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor);

	/* Demand formatting =
	Bits 0 to 2: Number of Digits to the right of the Decimal Point.
	Bits 3 to 6: Number of Digits to the left of the Decimal Point.
	Bit 7: If set, suppress leading zeros.
	*/
	/* Ignoring demand formatting as only able to display 4 characters with 2 decimal points */
	//u8DigitsRightOfDp = (0x7 & psSE_IPDDevice->sSimpleMeteringCluster.u8DemandFormatting);

	/* Truncate the instantaneous demand to 2 decimal places */
	u32Divisor = psSE_IPDDevice->sSimpleMeteringCluster.u24Divisor;
	while (u32Divisor > 999)
	{
		if (u32LocalDemandRemainder > 9)
		{
			u32LocalDemandRemainder /= 10;
			u32Divisor /= 10;
		}
		else
		{
			u32LocalDemandRemainder = 0;
			break;
		}
	}
	u32LocalDemand = (u32LocalDemandInteger * 100) + u32LocalDemandRemainder;
	Energy_symbol(sDisplayDeviceLevels.u8EnergyMode, u32LocalDemand, u8DigitsRightOfDp);	// value 00.00 maximum 99.99 , Mode Co2,Price,KwH

	if (!bLostComms)
	{
		vEnergyDial(65, 57, u32LocalDemand); 									// x,y,Radius,Energy level
	}
	else
	{
		/* If comms are lost with the meter show a blank energy bar */
		vEnergyDial(65,57,0);
	}

	Time();
}


/****************************************************************************
 *
 * NAME: vBuildSetUPScreen
 *
 * DESCRIPTION:Function to Build SetUp screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildSetUPScreen(void)
{
	tsOTA_ImageHeader          psOTAHeader;
	char acBuffer[4];

	vLcdClear();
	vLcdWriteText("Settings",0 , 10);
	vLcdWriteText("Version",0 , 55);

	teZCL_Status ZCL_Status =  eOTA_GetCurrentOtaHeader(
									IPD_BASE_LOCAL_EP,				//u8Endpoint
									FALSE,							//bIsServer,
									&psOTAHeader );					//*psOTAHeader);

	DBG_vPrintf(TRACE_IPD_NODE, "eOTA_GetCurrentOtaHeader returned 0x%x", ZCL_Status);

	memset(acBuffer,0,sizeof(acBuffer));
	xsprintf(acBuffer,"%02d",psOTAHeader.u32FileVersion);

	vLcdWriteText(acBuffer,0 , 100);

	vLcdWriteText("Scroll", 7, 0);
	vLcdWriteText("Select", 7, 32);
	vDrawArrows();

}


/****************************************************************************
 *
 * NAME: vUpdateSetUPScreen
 *
 * DESCRIPTION:Function to update SetUp screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUpdateSetUPScreen(void)
{
#ifdef CLD_PRICE

	tsBitmap sBitmap;

	sBitmap.u8Width  = 8;
	sBitmap.u8Height = 2;

	uint8 XBitmap=60,YBitmap=2;

	switch(Ycoord){
		case 2 : //Ycoord==2
			vLcdWriteInvertedText("Currency", 2, 0);
			vLcdWriteText("Time", 3, 0);
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
			vLcdWriteText("Fast Poll Mode", 4, 0);
#endif
		break;

		case 3 : //Ycoord==3
			vLcdWriteText("Currency", 2, 0);
			vLcdWriteInvertedText("Time", 3, 0);
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
			vLcdWriteText("Fast Poll Mode", 4, 0);
#endif
		break;

#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
		case 4: //Ycoord==4
			vLcdWriteText("Currency", 2, 0);
			vLcdWriteText("Time", 3, 0);
			vLcdWriteInvertedText("Fast Poll Mode", 4, 0);
		break;
#endif

		default:
		break;
	}

	sBitmap.pu8Bitmap = (uint8 *)&au8Energy_Symbol[au32EnergySymbol_offsets[sDisplayDeviceLevels.u8CurrencyType]]; //Euro
	vLcdWriteBitmap(&sBitmap, XBitmap, YBitmap);

	if(sDisplayDeviceLevels.bTimeType==FALSE)
	{
		vLcdWriteText("12hr ",3,60);
	}
	else
	{
		vLcdWriteText("24hr ",3,60);
	}
#endif
}


/****************************************************************************
 *
 * NAME: vBuildPriceScreen
 *
 * DESCRIPTION: Function to BuildPrice screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildPriceScreen(void)
{
	tsBitmap sBitmap;

	sBitmap.u8Width  = 8;
	sBitmap.u8Height = 2;

	vLcdClear();
	vLcdWriteText("Price", 0, 0);
	vLcdWriteText("Start", 0, 40);
	vLcdWriteText("Duration", 0, 85);

	sBitmap.pu8Bitmap = (uint8 *)&au8Energy_Symbol[au32EnergySymbol_offsets[sDisplayDeviceLevels.u8CurrencyType]]; //Euro
	vLcdWriteBitmap(&sBitmap, 27, 0);

	vLcdWriteText("GetC", 7, 0);
	vLcdWriteText("GetS", 7, 32);

	vDrawArrows();
}

/****************************************************************************
 *
 * NAME: vUpdatePriceScreen
 *
 * DESCRIPTION:Function to UpdatePrice screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUpdatePriceScreen(void)
{
#ifdef CLD_PRICE
	char acBuffer[10];
	tsSE_PricePublishPriceCmdPayload *pPricePublishPriceCmdPayload;
	uint32 u32Price = 0;
	uint8 u8StartLine = 1;
	uint8 i = 0,a=0,b=0,Tier=0;
	uint8 u8PriceScreenHour, u8PriceScreenMin, u8PriceScreenSec;

	tsBitmap sBitmap1;

	sBitmap1.u8Width  = 8;
	sBitmap1.u8Height = 1;

	vBuildPriceScreen();

	for( i=0;i<SE_PRICE_NUMBER_OF_CLIENT_PRICE_RECORD_ENTRIES;i++)
	{
		DBG_vPrintf(TRACE_DISPLAY, "Loop Count: %x\n" ,i);

		if( E_ZCL_SUCCESS == eSE_PriceGetPriceEntry(u8CurrentMeterOfInterest + 1, FALSE, i,&pPricePublishPriceCmdPayload ))
		{

			DBG_vPrintf(TRACE_DISPLAY, "Got Price: %x\n" ,i);

			u32Price = pPricePublishPriceCmdPayload->u32Price;


			a=u32Price/100;
			b=u32Price%100;

			memset(acBuffer,0,sizeof(acBuffer));
			xsprintf(acBuffer,"%02d`%02d",a,b);

			vLcdWriteText(acBuffer, (u8StartLine + i), 0);

			memset(acBuffer,0,sizeof(acBuffer));

			/* Update the screen time variables to the price message time */
			vShowTime(pPricePublishPriceCmdPayload->u32StartTime, &u8PriceScreenHour,  &u8PriceScreenMin, &u8PriceScreenSec);

			if(sDisplayDeviceLevels.bTimeType==FALSE){
				if(u8PriceScreenHour>=12&&u8PriceScreenHour<=23){xsprintf(acBuffer,"%02d:%02dPM",TwentyFour(u8PriceScreenHour), u8PriceScreenMin );}
				else{xsprintf(acBuffer,"%02d:%02dAM",TwentyFour(u8PriceScreenHour), u8PriceScreenMin );}
			}else{
				xsprintf(acBuffer,"%02d:%02d",u8PriceScreenHour, u8PriceScreenMin);
			}
			vLcdWriteText(acBuffer, (u8StartLine + i), 40);
			memset(acBuffer,0,sizeof(acBuffer));

			a=pPricePublishPriceCmdPayload->u16DurationInMinutes/100;
			b=pPricePublishPriceCmdPayload->u16DurationInMinutes%100;

			xsprintf(acBuffer," %02d:%02d ",a,b);
			vLcdWriteText(acBuffer, (u8StartLine + i), 82);

			Tier=pPricePublishPriceCmdPayload->u8PriceTrailingDigitAndPriceTier;
			if(Tier==1)
			{
				xsprintf(acBuffer,"L");
			}
			else if(Tier==2)
			{
				xsprintf(acBuffer,"M");
			}
			else
			{
				xsprintf(acBuffer,"H");
			}

			vLcdWriteText(acBuffer, (u8StartLine + i), 115);
		}
	}
	memset(acBuffer,0,sizeof(acBuffer));
#endif
}

#ifdef CLD_DRLC
/****************************************************************************
 *
 * NAME: vBuildDRLCScreen
 *
 * DESCRIPTION: Function to BuildPrice screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildDRLCScreen(void)
{
	tsBitmap sBitmap;

	sBitmap.u8Width  = 8;
	sBitmap.u8Height = 2;


	vLcdClear();
	vLcdWriteText("DRLC", 0, 0);
	vLcdWriteText("Start", 0, 40);
	vLcdWriteText("Duration", 0, 85);

	vLcdWriteText("Opt", 7, 0);
	vLcdWriteText("GetS", 7, 32);

	vDrawArrows();
}

/****************************************************************************
 *
 * NAME: vUpdateDRLCScreen
 *
 * DESCRIPTION:Function to UpdatePrice screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUpdateDRLCScreen(void)
{
	char acBuffer[10];
	tsSE_DRLCLoadControlEvent *sSE_DRLCLoadControlEvent;
	uint32 u32DrlcID = 0;
	uint8 u8StartLine = 1;
	uint8 i = 0,a=0,b=0;
	uint8 u8DrlcScreenHour, u8DrlcScreenMin, u8DrlcScreenSec;
	uint8 u8DrlcQueue = 0xff;

	tsBitmap sBitmap1;

	sBitmap1.u8Width  = 8;
	sBitmap1.u8Height = 1;

	vBuildDRLCScreen();

	/* i is the enum for the queue type, 0-3 is
	 *  E_SE_DRLC_EVENT_LIST_SCHEDULED =0x00,
	E_SE_DRLC_EVENT_LIST_ACTIVE,
	E_SE_DRLC_EVENT_LIST_CANCELLED,
	E_SE_DRLC_EVENT_LIST_DEALLOCATED,
	 */
	for(i=0; i<4; i++)
	{
		teSE_DRLCStatus eSE_DRLCStatus = eSE_DRLCGetLoadControlEvent(
		1,
		0,
		i,
		&sSE_DRLCLoadControlEvent);

		if(E_SE_DRLC_EVENT_NOT_FOUND != eSE_DRLCStatus)
		{
			u8DrlcQueue = i;

			u32DrlcID = sSE_DRLCLoadControlEvent->u32IssuerId;

			memset(acBuffer,0,sizeof(acBuffer));
			xsprintf(acBuffer,"%02d", u32DrlcID);

			vLcdWriteText(acBuffer, u8StartLine, 0);

			memset(acBuffer,0,sizeof(acBuffer));

			/* Update the screen time variables to the drlc start time */
			vShowTime(sSE_DRLCLoadControlEvent->u32StartTime, &u8DrlcScreenHour,  &u8DrlcScreenMin, &u8DrlcScreenSec);

			if(sDisplayDeviceLevels.bTimeType==FALSE){
				if(u8DrlcScreenHour>=12&&u8DrlcScreenHour<=23){xsprintf(acBuffer,"%02d:%02dPM",TwentyFour(u8DrlcScreenHour), u8DrlcScreenMin );}
				else{xsprintf(acBuffer,"%02d:%02dAM",TwentyFour(u8DrlcScreenHour), u8DrlcScreenMin );}
			}else{
				xsprintf(acBuffer,"%02d:%02d",u8DrlcScreenHour, u8DrlcScreenMin);
			}
			vLcdWriteText(acBuffer, u8StartLine, 40);
			memset(acBuffer,0,sizeof(acBuffer));

			a=sSE_DRLCLoadControlEvent->u16DurationInMinutes/100;
			b=sSE_DRLCLoadControlEvent->u16DurationInMinutes%100;

			xsprintf(acBuffer," %02d:%02d ",a,b);
			vLcdWriteText(acBuffer, u8StartLine, 82);

			memset(acBuffer,0,sizeof(acBuffer));

			switch(i)
			{
				case 0: acBuffer[0] = 'S'; break;
				case 1: acBuffer[0] = 'A'; break;
				case 2: acBuffer[0] = 'C'; break;
				case 3: acBuffer[0] = 'D'; break;
			}

			vLcdWriteText(acBuffer, u8StartLine, 120);

			u8StartLine++;

		}
	}

	if(0xFF == u8DrlcQueue)
	{
		vLcdWriteText("All DRLC queues empty", u8StartLine, 5);
		DBG_vPrintf(TRACE_DISPLAY, "All DRLC queues empty");
	}

}
#endif

PRIVATE void vDrawArrows(void)
{
	tsBitmap sBitmap;

	sBitmap.u8Width  = 8;
	sBitmap.u8Height = 1;

	sBitmap.pu8Bitmap = (uint8 *)&au8RightArrow; // Right Arrow
	vLcdWriteBitmap(&sBitmap, 116, 7);

	sBitmap.pu8Bitmap = (uint8 *)&au8LeftArrow; // Right Arrow
	vLcdWriteBitmap(&sBitmap, 80, 7);
}


/****************************************************************************
 *
 * NAME: vBuildTierScreen
 *
 * DESCRIPTION:Function to BuildTier Screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildTierScreen(void)
{
	tsBitmap sBitmap;

	sBitmap.u8Width  = 8;
	sBitmap.u8Height = 1;

	vLcdClear();
	vLcdWriteText("Daily Consumption",0 , 16);

	vDrawArrows();

	sBitmap.pu8Bitmap = (uint8 *)&au8Symbols[au32symbol_offsets[1]];
	vLcdWriteBitmap(&sBitmap, 16, 6);

	sBitmap.pu8Bitmap = (uint8 *)&au8Symbols[au32symbol_offsets[2]];
    vLcdWriteBitmap(&sBitmap, 65, 6);

	sBitmap.pu8Bitmap = (uint8 *)&au8Symbols[au32symbol_offsets[3]];
	vLcdWriteBitmap(&sBitmap, 112, 6);


	vLcdWriteText("%",1 ,120);
	vLcdWriteText("%",3 ,120);
	vLcdWriteText("%",5 ,120);

	vLcdWriteText("Low " ,1 ,0);
	vLcdWriteText("Med " ,3 ,0);
	vLcdWriteText("Hi  ",5 ,0);
}


/****************************************************************************
*
* NAME: vUpdateHistory_Statistics
*
* DESCRIPTION:Function to Build and update Tier information
*
* PARAMETERS:      Name            RW  Usage
* None.
*
* RETURNS:
* None.
*
* NOTES:
* None.
****************************************************************************/
PUBLIC void vUpdateHistory_Statistics(uint16 val1,uint16 val2,uint16 val3)
{
	uint8  	i=0;
	uint8 	Y=9,YY=15;
	uint8   XStart=20; //,XEnd=120,Upper=100,Lower=0;
	//uint8   XStep1=((XEnd-XStart)/(Upper-Lower))*val1;
	//uint8   XStep2=((XEnd-XStart)/(Upper-Lower))*val2;
	//uint8   XStep3=((XEnd-XStart)/(Upper-Lower))*val3;

	uint16 u16Total = (val1 + val2 + val3);

	DBG_vPrintf(TRACE_DISPLAY, "Total %d \n" , u16Total);

	uint16   XStep1 = (val1 * 100) / u16Total;
	uint16   XStep2 = (val2 * 100) / u16Total;
	uint16   XStep3 = (val3 * 100) / u16Total;

	vLcdDrawLine(19,YY+1,119,YY+1,ON);
	vLcdDrawLine(19,Y-1,19,YY+1,ON);

	vLcdDrawLine(19,YY+16,119,YY+16,ON);
	vLcdDrawLine(19,YY+8,19,YY+16,ON);

	vLcdDrawLine(19,YY+31,119,YY+31,ON);
	vLcdDrawLine(19,YY+24,19,YY+31,ON);

    for(i=XStart;i<(XStep1+XStart);i++){
		vLcdDrawLine(i,Y,i,YY,ON);
	}

	for(i=XStart;i<(XStep2+XStart);i++){
		vLcdDrawLine(i,YY+9,i,YY+15,ON);
	}

	for(i=XStart;i<(XStep3+XStart);i++){
		vLcdDrawLine(i,YY+25,i,YY+31,ON);
	}
}


/****************************************************************************
 *
 * NAME: vUpdateTierScreen
 *
 * DESCRIPTION:Function to Update Tier Screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUpdateTierScreen(void)
{
	 vUpdateHistory_Statistics(u16Hist1,u16Hist2,u16Hist3);
}


/****************************************************************************
 *
 * NAME: vUpdateHistory
 *
 * DESCRIPTION:
 * Handles the button pushes and timer expiry and read complete
 * events
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PUBLIC void vUpdateHistory(void)
{
	 tsSE_PricePublishPriceCmdPayload *pPricePublishPriceCmdPayload;
	 uint8 Tier;

	 if(E_ZCL_SUCCESS == eSE_PriceGetPriceEntry(u8CurrentMeterOfInterest + 1, FALSE, 0,&pPricePublishPriceCmdPayload )){ //update history as list is consumed

		 Tier=pPricePublishPriceCmdPayload->u8PriceTrailingDigitAndPriceTier;

		 if(Tier==1)
		 {
			 u16Hist1++;
		 }
		 else if(Tier==2)
		 {
			 u16Hist2++;
		 }
		 else
		 {
			 u16Hist3++;
		 }
	 }
	 //DBG_vPrintf(TRACE_DISPLAY, "Hist1 %d,Hist2 %d,Hist2 %d  \n" , u16Hist1, u16Hist2, u16Hist3);
}


#ifdef CLD_MC
/****************************************************************************
 *
 * NAME: vUpdateMessageScreen
 *
 * DESCRIPTION: Build and update the messaging screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUpdateMessageScreen(void)
{
	/* Set an array to the size of the maximum payload defined
	 * in the ZPS configuration diagram (ZCL APDU buffer size) */
	char acBuffer[150] = "No Message";
    teSE_MCStatus eMCStatus;
    tsSE_MCDisplayMessageCommandPayload *psPayload;

    vLcdClear();
    vLcdWriteText("Messaging", 0, 0);

	eMCStatus = eSE_MCGetMessage(IPD_BASE_LOCAL_EP, 0, E_SE_MC_MESSAGE_ACTIVE, &psPayload);
	if (eMCStatus == E_ZCL_SUCCESS)
	{
		/* Format the string */
		xsprintf(acBuffer, "%s", psPayload->sMessage.pu8Data );
		/* Write the string to the LCD buffer ready to be refreshed*/
		vLcdWriteText(acBuffer, 2, 0);
	}
	else
	{
		vLcdWriteText(acBuffer, 3, 20);
	}

	DBG_vPrintf(TRACE_DISPLAY, "%s", acBuffer);

	vLcdWriteText("GetM", 7, 0);
	vLcdWriteText("Clear", 7, 32);

	vDrawArrows();
}


/****************************************************************************
 *
 * NAME: vBuildMessageConfirmationScreen
 *
 * DESCRIPTION: Build the message confirmation screen
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vBuildMessageConfirmationScreen(void)
{
	char acBuffer[150] = "No Message";
    teSE_MCStatus eMCStatus;
    tsSE_MCDisplayMessageCommandPayload *psPayload;

    vLcdClear();
    vLcdWriteText("Confirm Message", 0, 0);

	eMCStatus = eSE_MCGetMessage(IPD_BASE_LOCAL_EP, 0, E_SE_MC_MESSAGE_ACTIVE, &psPayload);
	if (eMCStatus == E_ZCL_SUCCESS)
	{
		xsprintf(acBuffer, "%s", psPayload->sMessage.pu8Data );
	}

	vLcdWriteText(acBuffer, 3, 20);

	DBG_vPrintf(TRACE_DISPLAY, "Confirmation required: %s", acBuffer);

	vLcdWriteText("Ack", 7, 0);
	vLcdWriteText("Cancel", 7, 32);
}
#endif


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vLcdPlotPoint
 *
 * DESCRIPTION:
 * Plots a single point in standard <x,y> coordinates onto shadow memory.
 * Point is OR'd with existing contents.
 *
 * PARAMETERS:      Name   RW  Usage
 *                  u8X    R   X coordinate of point (0-127)
 *                  u8Y    R   Y coordinate of point (0-63)
 *
 * NOTES:
 * Location in shadow is given by:
 *   u8X + (u8Y / 8) * 128
 * Bit in location is given by:
 *   2^(u8Y % 8)
 ****************************************************************************/
PUBLIC void vLcdPlotPoint(int16 i16X, int16 i16Y,uint8 onoff)
{
    uint16 u16ShadowLocation;
    uint8  u8BitField;

    if ((i16X <= LCD_MAX_X) && (i16Y <= LCD_MAX_Y) && (i16X >= 0) && (i16Y >= 0))
    {
        u16ShadowLocation = i16X + (((uint16)(i16Y & 0x38)) << 4);
        u8BitField = 1 << (i16Y & 7);

        if(onoff==OFF){au8Shadow[u16ShadowLocation] ^= u8BitField;}
        if(onoff==ON){au8Shadow[u16ShadowLocation] |= u8BitField;}
    }
}


/****************************************************************************
 *
 * NAME:       vLcdDrawLine
 */
/**
 * Implements Bresenham's Integer Line Drawing Algoritm
 *
 *
 * @param i32x1     Start X coordinate
 * @param i32y1     Start Y coordinate
 * @param i32x2     End X coordinate
 * @param i32y2     End Y coordinate
 *
 * @return
 *
 * @note
 * This should only be called if it is sure that there is at least one
 * channel left to scan in psScan->u32Channels.
 *
 ****************************************************************************/
PUBLIC void vLcdDrawLine(int16 i16x1, int16 i16y1, int16 i16x2, int16 i16y2,uint8 onOff)
{
    int i32CurrentX, i32CurrentY;
    int i32Xinc,     i32Yinc;
    int i32Dx,       i32Dy;
    int i32TwoDx,    i32TwoDy;
    int i32TwoDxAccErr;
    int i32TwoDyAccErr;

    i32Dx       = i16x2 - i16x1;
    i32Dy       = i16y2 - i16y1;
    i32TwoDx    = i32Dx + i32Dx;
    i32TwoDy    = i32Dy + i32Dy;
    i32CurrentX = i16x1;            /*start at (i32x1,i32y1) and move towards (i32x2,i32y2)*/
    i32CurrentY = i16y1;
    i32Xinc     = 1;                /*X and/or Y are incremented/decremented by 1 only */
    i32Yinc     = 1;

    if (i32Dx < 0)
    {
        i32Xinc  = -1; /*  decrement X's  */
        i32Dx    = -i32Dx;
        i32TwoDx = -i32TwoDx;
    }

    if (i32Dy < 0) /*  insure i32Dy >= 0  */
    {
        i32Yinc  = -1;
        i32Dy    = -i32Dy;
        i32TwoDy = -i32TwoDy;
    }

    vLcdPlotPoint(i16x1, i16y1,onOff); /* { the first point is a special case } */

    if ((i32Dx !=0) || (i32Dy != 0))  /* are other points on the line ? */
    {
        if (i32Dy <= i32Dx)  /* is the slope <= 1 */
        {
            i32TwoDxAccErr = 0; /* initialize the error */
            do
            {
                i32CurrentX    += i32Xinc;
                i32TwoDxAccErr += i32TwoDy;
                if (i32TwoDxAccErr>i32Dx)
                {
                    i32CurrentY    += i32Yinc;
                    i32TwoDxAccErr -= i32TwoDx;
                }
                vLcdPlotPoint(i32CurrentX, i32CurrentY,onOff);  /* process next point  */
            } while (i32CurrentX != i16x2);
        }
        else /* then the slope is large, reverse roles of X & Y */
        {
            i32TwoDyAccErr = 0; /* initialize the error */
            do
            {
                i32CurrentY    += i32Yinc;                 /* consider Y's from i32y1 to i32y2 */
                i32TwoDyAccErr += i32TwoDx;
                if (i32TwoDyAccErr > i32Dy)
                {
                    i32CurrentX    += i32Xinc;
                    i32TwoDyAccErr -= i32TwoDy;
                }
                vLcdPlotPoint(i32CurrentX, i32CurrentY,onOff); /* { process next point } */
            } while (i32CurrentY != i16y2);
        }
    }
}


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
