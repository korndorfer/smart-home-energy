/*
 * Smart Socket
*/
/* Library includes ***********************************************************/
#include <Wire.h>
#include "EmonLib.h"
#include <RtcDS3231.h>
#include <EepromAT24C32.h>
#include <SoftwareSerial.h>

/* Hardware configuration *****************************************************/
#define VOLTAGE_SENSOR A0                                  // Voltage analog pin
#define CURRENT_SENSOR A1                                  // Current analog pin
#define RELAY 12                                                    // relay pin
#define BLE_RX 4                                             // bluetooth rx pin
#define BLE_TX 5                                             // bluetooth tx pin
#define LED_RED 7
#define LED_YELLOW 8
#define LED_GREEN 9

/* Constant definitions *******************************************************/
#define ADC_MAX_STEP 1024.0                                     // adc max value
#define SQUARE_OF_2 1.41421356                                    // square of 2
#define countof(a) (sizeof(a) / sizeof(a[0]))

static char daysOfTheWeek[7][12] = {"Domingo",
                                    "Segunda",
                                    "Terça",
                                    "Quarta", 
                                    "Quinta",
                                    "Sexta",
                                    "Sábado"};
enum bleFunction {RELAY_OFF          = '0',
                  RELAY_ON           = '1',
                  NOTIFY_OFF         = '2',
                  NOTIFY_ON          = '3',
                  READ_RELAY         = '4',
                  READ_NOTIFY        = '5',
                  READ_VOLTAGE       = '6', 
                  READ_CURRENT       = '7',
                  READ_POWER         = '8',
                  READ_VOLTAGE_BATCH = '9',
                  READ_CURRENT_BATCH = 'a',
                  STATUS_OK          = 'b',
                  STATUS_WARNING     = 'c',
                  STATUS_DANGER      = 'd',
                  STATUS_NO_LINE     = 'e',
                  NOT_RECOGNIZED     = 'z'
                 };
                  
/* Configuration parameters ***************************************************/
static int cfg_measurements = 20;                      // number of measurements
static float cfg_current_resolution = 7.8;       // resolution of current sensor
static float cfg_voltage_resolution = 290.26;    // resolution of voltage sensor
static long cfg_ble_serial_rate = 38400;               // bluetooth serial speed
static int cfg_notify_interval = 10000;       // notify interval in milliseconds
static int cfg_warning_interval = 1000;// blink warning interval in milliseconds

/* Global variables definitions ***********************************************/
bool relay_is_on = false,
     has_voltage = true,
     notify_is_on = true,
     state_is_warning = false,
     led_yellow_is_on = false,
     led_red_is_on = false,
     led_green_is_on = false;

float eff_power = 0;
float eff_current = 0;
float eff_voltage = 0;

unsigned long base_notify_time, current_notify_time;
unsigned long base_led_time, current_led_time;

/* Object definitions *********************************************************/
RtcDS3231<TwoWire> Rtc(Wire);
EepromAt24c32<TwoWire> RtcEeprom(Wire);
SoftwareSerial ble_serial(BLE_RX, BLE_TX);
EnergyMonitor emon1;

/* Setup **********************************************************************/
void setup(){
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_YELLOW, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  
  Serial.begin(9600);
  ble_serial.begin(cfg_ble_serial_rate);
  
  Rtc.Begin();
  RtcEeprom.Begin();

  Serial.println("Calibrating real time clock ..");
  calibrate_rtc();
  
  Rtc.Enable32kHzPin(false);
  Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);

  //Serial.println("Calibrating sensors ..");
  //calibrate_sensors();
  Serial.println("Starting measurements ..");
  emon1.voltage(VOLTAGE_SENSOR, cfg_voltage_resolution, 1.7);
  emon1.current(CURRENT_SENSOR, cfg_current_resolution);

  pinMode(RELAY, OUTPUT);
  relay_control(HIGH);

  base_notify_time = millis();
  base_led_time = millis();
}

/* Main loop ******************************************************************/
void loop () {
  read_sensors();
  demo();
  process_data();
  ble_receive();
  led_control();
}

/* Function definitions *******************************************************/
void relay_control(int state)
{
  digitalWrite(RELAY, state);
  relay_is_on = state && 1;
}

void demo()
{
  is_rtc_operational();
  RtcDateTime now = Rtc.GetDateTime();
  print_datetime(now);
  RtcTemperature temp = Rtc.GetTemperature();
  Serial.print("\tDevice temperature: ");
  temp.Print(Serial);
  Serial.println(" ºC");
  
  Serial.print("Effective Voltage: ");
  Serial.print(eff_voltage);
  Serial.print(" V\t");
  Serial.print("Effective current: ");
  Serial.print(eff_current);
  Serial.print(" A\t");
  Serial.print("Effective power: ");
  Serial.print(eff_power);
  Serial.println(" W");
  Serial.println();

  
  if (!has_voltage) {
    Serial.println("\t\t*****************************************************");
    Serial.println("\t\t*                 Voltage error!!!                  *");
    Serial.println("\t\t*****************************************************");
    Serial.println();
  }

  delay(500);  
}

void read_sensors()
{
  emon1.calcVI(cfg_measurements, 2000);
  eff_power = emon1.apparentPower;
  eff_voltage = emon1.Vrms;
  eff_current = emon1.Irms;
  has_voltage = eff_voltage && 1;
}

void process_data()
{
  if (notify_is_on)
  {
    current_notify_time = millis();
    if (current_notify_time - base_notify_time >= cfg_notify_interval)
    {
      if (has_voltage)
      {
        notify_subscriber();
      }
      else
      {
        ble_send(STATUS_NO_LINE, STATUS_NO_LINE);
      }
      base_notify_time = millis();
    }    
  }
}

bool is_rtc_operational()
{
  if (!Rtc.IsDateTimeValid()) 
  {
    if (Rtc.LastError() != 0)
    {
      Serial.print("ERROR: RTC communications error = ");
      Serial.println(Rtc.LastError());
    }
    else
    {
      Serial.print("RTC lost confidence in the DateTime! ");
      Serial.println("Perhaps check battery.");
    }
    return false;
  }
  return true;
}

void calibrate_rtc()
{
  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);

  if (!is_rtc_operational())
  {
    Rtc.SetDateTime(compiled);
  }

  if (!Rtc.GetIsRunning())
  {
    Serial.println("RTC was not actively running, starting now");
    Rtc.SetIsRunning(true);
  }

  RtcDateTime now = Rtc.GetDateTime();
  if (now < compiled) 
  {
    Serial.println("RTC is older than compile time!  (Updating DateTime)");
    Rtc.SetDateTime(compiled);
  }
  else if (now > compiled) 
  {
    Serial.println("RTC is newer than compile time. (this is expected)");
  }
  else if (now == compiled) 
  {
    Serial.print("RTC is the same as compile time! ");
    Serial.println("(not expected but all is fine)");
  }
}

void print_datetime(const RtcDateTime& dt)
{
  char datestring[30];

  snprintf_P(datestring, 
             countof(datestring),
             PSTR("%s, %02u/%02u/%04u %02u:%02u:%02u"),
             daysOfTheWeek[dt.DayOfWeek()],
             dt.Day(),
             dt.Month(),
             dt.Year(),
             dt.Hour(),
             dt.Minute(),
             dt.Second());
  Serial.print(datestring);
}

void print_current_data()
{
  RtcDateTime now = Rtc.GetDateTime();
  char datestring[25];

  snprintf_P(datestring,
             countof(datestring),
             PSTR("%05u%05u%04u%02u%02u%02u%02u%02u"),
             int(eff_voltage * 100),
             int(eff_current * 100),
             now.Year(),
             now.Month(),
             now.Day(),
             now.Hour(),
             now.Minute(),
             now.Second());             
  Serial.println(datestring);
}

void ble_receive()
{
  if (ble_serial.available()) {
    byte data[30] = {0};
    char c;
    int idx = 0;
    byte command = 0;
    int parameter = 0;

    Serial.print("Receiving: ");
    while((c = ble_serial.read()) != -1) {
      Serial.print(c);
      data[idx] = c;
      idx++;
      delay(5);
    }
    
    command = data[0];
    parameter = int(data[1]);
    
    Serial.println();
    Serial.print("Received command: ");
    Serial.println(command);
    Serial.print("Received data: ");
    Serial.println(parameter);

    switch (command)
    {
      case RELAY_OFF:
        relay_control(LOW);
        break;
      case RELAY_ON:
        relay_control(HIGH);
        break;
      case NOTIFY_OFF:
        notify_is_on = false;
        break;
      case NOTIFY_ON:
        notify_is_on = true;
        break;
      case READ_RELAY:
        ble_send(command, relay_is_on ? RELAY_ON : RELAY_OFF);
        break;
      case READ_NOTIFY:
        ble_send(command, notify_is_on ? NOTIFY_ON : NOTIFY_OFF);
        break;
      case READ_VOLTAGE:
        ble_send(command, eff_voltage);
        break;
      case READ_CURRENT:
        ble_send(command, eff_current);
        break;
      case READ_POWER:
        ble_send(command, eff_power);
        break;
      case READ_VOLTAGE_BATCH:
        ble_send_batch(command, parameter);
        break;
      case READ_CURRENT_BATCH:
        ble_send_batch(command, parameter);
        break;
      case STATUS_OK:
      case STATUS_WARNING:
      case STATUS_DANGER:
        set_led_status(command);
      default:
        ble_send(NOT_RECOGNIZED, 0);  
    }
  }
}

void notify_subscriber()
{
  ble_send(READ_RELAY, relay_is_on ? RELAY_ON : RELAY_OFF);
  delay(200);
  ble_send(READ_NOTIFY, notify_is_on ? NOTIFY_ON : NOTIFY_OFF);
  delay(200);
  ble_send(READ_VOLTAGE, eff_voltage);
  delay(200);
  ble_send(READ_CURRENT, eff_current);
  delay(200);
  ble_send(READ_POWER, eff_power);  
  delay(200);
}

void ble_send(byte function, float data)
{
  RtcDateTime now = Rtc.GetDateTime();
  char datestring[20];

  snprintf_P(datestring,
             countof(datestring),
             PSTR("%c%05u%02u%02u%02u%02u%02u%02u"),
             function,
             int(data * 100),
             now.Year() - 2000,
             now.Month(),
             now.Day(),
             now.Hour(),
             now.Minute(),
             now.Second());              
  ble_serial.print(datestring);
  Serial.print("Submitted data: ");
  Serial.println(datestring);
}

void ble_send_batch(byte function, int parameter)
{
  float d = 0;

  if (function != READ_VOLTAGE && function != READ_CURRENT)
    return;
    
  for (int i = 0; i < parameter; i++)
  {
    read_sensors();

    if (function == READ_VOLTAGE)
      d = eff_voltage;
    else
      d = eff_current;
      
    ble_send(function, d);
  }
}

void led_control()
{  
  if (!has_voltage)
  {
    current_led_time = millis();
    if (current_led_time - base_led_time >= cfg_warning_interval)
    {
      digitalWrite(LED_YELLOW, led_yellow_is_on);
      led_yellow_is_on = !led_yellow_is_on;

      base_led_time = millis();
    }
  }
  else
  {
    digitalWrite(LED_YELLOW, LOW);
  }
}

void set_led_status(byte led_status)
{
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_YELLOW, LOW);
  digitalWrite(LED_GREEN, LOW);

  switch (led_status)
  {
    case STATUS_OK:
      digitalWrite(LED_GREEN, HIGH);
      break;
    case STATUS_WARNING:
      digitalWrite(LED_YELLOW, HIGH);
      break;
    case STATUS_DANGER:
      digitalWrite(LED_RED, HIGH);
      break;
    default:
      Serial.print("Unknown led status: ");
      Serial.println(led_status);
  }
}
