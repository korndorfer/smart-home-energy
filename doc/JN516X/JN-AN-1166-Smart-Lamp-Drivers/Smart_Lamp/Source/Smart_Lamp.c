/****************************************************************************
 *
 * MODULE          JN-AN-1166 Smart Lamp Test Software
 *
 * COMPONENT       Smart Lamp
 *
 * DESCRIPTION     Basic IEEE802.15.4 wireless P2P communication application
 *                 to implement remote control of real bulb hardware using
 *                 RGB/MONOchrome  driver sub-components
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5168, JN5148]
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2013. All rights reserved
 *****************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#include <AppHardwareApi.h>
#include <AppQueueApi.h>
#include <mac_sap.h>
#include <mac_pib.h>
#include <string.h>

#include "dbg.h"
#include "dbg_uart.h"

#include "config.h"
#include "DriverBulb_Shim.h"
#include "Printf.h"
#include <recal.h>

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/* Debug flags */
#ifdef DBG_ENABLE
#define TRACE_SW    TRUE
#else
#define TRACE_SW    FALSE
#endif

#define UART_TO_DBG                 DBG_E_UART_0        /* Uart to terminal     */
#define BAUD_RATE_DBG               DBG_E_UART_BAUD_RATE_115200 /* Baud rate to use     */


#define RECAL_PERIOD    6000
#define ADC_PERIOD       100
#define TEMP_XTAL_HALF_PULL             95  /*  95C */
#define TEMP_XTAL_HALF_PUSH             93  /*  93C */
#define TEMP_XTAL_FULL_PULL            110  /* 110C */
#define TEMP_XTAL_FULL_PUSH            108  /* 108C */

#define CCT_STEP 10

#define UP  (1L)
#define DN  (-1L)
#define STOP (0)

#define PARAM_COUNT 7
#define PARAM_MIN   0
#define PARAM_MAX 255
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

enum {E_LEVEL,E_RED,E_GREEN,E_BLUE,E_TEMP,E_BALANCE,E_CCT};

typedef struct
{
    int32   ai32Delta[PARAM_COUNT];
    int32   ai32Param[PARAM_COUNT];
	uint32  au32TxRequestFlags[3];
	uint16 	au16AdcRead;
    uint8   u8Channel;

}tsLampRGB ;

typedef enum
{
	E_STATE_XTAL_UNPULLED   = 0,
	E_STATE_XTAL_SEMIPULLED = 1,
	E_STATE_XTAL_PULLED     = 3
} teXtalPullingStates;


/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
PRIVATE void vInitSystem(void);
PRIVATE void vProcessEventQueues(void);
PRIVATE void vProcessIncomingMlme(MAC_MlmeDcfmInd_s *psMlmeInd);
PRIVATE void vProcessIncomingMcps(MAC_McpsDcfmInd_s *psMcpsInd);
PRIVATE void vProcessIncomingHwEvent(AppQApiHwInd_s *psAHI_Ind);
PRIVATE void vHandleMcpsDataInd(MAC_McpsDcfmInd_s *psMcpsInd);
PRIVATE void vHandleMcpsDataDcfm(MAC_McpsDcfmInd_s *psMcpsInd);
PRIVATE void vProcessReceivedDataPacket(uint8 u8BulbCommand);
PRIVATE void vTransmitDataPacket(uint32 u32StatusId, int32 i32Value);

PRIVATE void vPullXtal(int32 i32Temperature);
PRIVATE void vApplyDelta(int32 * pi32VarToChange, int32 i32Delta, int32 i32Min, int32 i32Max);
PRIVATE void vHandleStopCommand(void);
PRIVATE int32  i32GetLevel();


/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
/* Handles from the MAC */
PRIVATE void *s_pvMac;
PRIVATE MAC_Pib_s *s_psMacPib;
PRIVATE tsLampRGB sLampRGB = {{0,0,0,0,0,0,0},{255,255,255,255,0,128,4000},{0,0,0},0,0};


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
/****************************************************************************
 *
 * NAME: AppColdStart
 *
 * DESCRIPTION:
 * Entry point for application from boot loader. Initialises system and runs
 * main loop.
 *
 * RETURNS:
 * Never returns.
 *
 ****************************************************************************/
PUBLIC void AppColdStart(void)
{

    /* wait for clock to stabilise */

	 while (TRUE == bAHI_GetClkSource());
	 vAHI_OptimiseWaitStates();


	/* Disable watchdog if enabled by default */
	#ifdef WATCHDOG_ENABLED
		vAHI_WatchdogStop();
	#endif


	/* Setup interface to MAC */
	(void)u32AHI_Init();
	(void)u32AppQApiInit(NULL, NULL, NULL);

	/* Initialise debugging */

	 DBG_vUartInit(DBG_E_UART_0, DBG_E_UART_BAUD_RATE_115200);


	/* System initialisation */
	vInitSystem();

	/* Set to on-white-max */
	vBULB_Init();
	DBG_vPrintf(TRUE,"\nAPP Ready");


    /*Start Tick Timer */
    vAHI_TickTimerConfigure(E_AHI_TICK_TIMER_RESTART);


    while (1)
    {
		vProcessEventQueues();
    }
}

/****************************************************************************
 *
 * NAME: AppWarmStart
 *
 * DESCRIPTION:
 * Entry point for application from boot loader. Simply jumps to AppColdStart
 * as, in this instance, application will never warm start.
 *
 * RETURNS:
 * Never returns.
 *
 ****************************************************************************/
PUBLIC void AppWarmStart(void)
{
    AppColdStart();
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vInitSystem
 *
 * DESCRIPTION:
 * Initialises chip peripherals and 802.15.4 MAC
 *
 ****************************************************************************/
PRIVATE void vInitSystem(void)
{

	/* Take pullup off DIO1 to alllow ADC4 to be used */

	vAHI_DioSetPullup(0, (1 <<1));



    /* Tick Timer set-up */
    vAHI_TickTimerConfigure(E_AHI_TICK_TIMER_DISABLE);
    vAHI_TickTimerWrite(0);
    vAHI_TickTimerInterval(160000);
    vAHI_TickTimerIntEnable(TRUE);
#ifndef DR1223
    /* Initialise the Analogue Peripherals */
	vAHI_ApConfigure(E_AHI_AP_REGULATOR_ENABLE,
					 E_AHI_AP_INT_DISABLE,
					 E_AHI_AP_SAMPLE_8,
					 E_AHI_AP_CLOCKDIV_500KHZ,
					 E_AHI_AP_INTREF);

	/* wait for regulator to stabilise */
	while (!bAHI_APRegulatorEnabled());
	/* Config and enable ADC */
	vAHI_AdcEnable(E_AHI_ADC_SINGLE_SHOT,
	                   E_AHI_AP_INPUT_RANGE_1,
	                   E_AHI_ADC_SRC_TEMP);     /* adc4 0-2.4v input */
#endif

    /* Set up the MAC handles. Must be called AFTER u32AppQApiInit() */
    s_pvMac = pvAppApiGetMacHandle();
    s_psMacPib = MAC_psPibGetHandle(s_pvMac);

    /* Set Pan ID and short address in PIB (also sets match registers in hardware) */
    MAC_vPibSetPanId(s_pvMac, PAN_ID);

    /* Set the channel to use only one */
    sLampRGB.u8Channel = CHANNEL;
    (void)eAppApiPlmeSet(PHY_PIB_ATTR_CURRENT_CHANNEL, sLampRGB.u8Channel);

    /* Enable receiver to be on when idle */
    MAC_vPibSetRxOnWhenIdle(s_pvMac, TRUE, FALSE);

    /* Allow nodes to associate */
    s_psMacPib->bAssociationPermit = 1;

}

/****************************************************************************
 *
 * NAME: vProcessEventQueues
 *
 * DESCRIPTION:
 * Check each of the three event queues and process and items found.
 *
 * PARAMETERS:      Name            RW  Usage
 * None.
 *
 * RETURNS:
 * None.
 *
 * NOTES:
 * None.
 ****************************************************************************/
PRIVATE void vProcessEventQueues(void)
{
    MAC_MlmeDcfmInd_s *psMlmeInd;
	MAC_McpsDcfmInd_s *psMcpsInd;
    AppQApiHwInd_s    *psAHI_Ind;

    /* Check for anything on the MCPS upward queue */
    do
    {
        psMcpsInd = psAppQApiReadMcpsInd();
        if (psMcpsInd != NULL)
        {
            vProcessIncomingMcps(psMcpsInd);
            vAppQApiReturnMcpsIndBuffer(psMcpsInd);
        }
    } while (psMcpsInd != NULL);

    /* Check for anything on the MLME upward queue */
    do
    {
        psMlmeInd = psAppQApiReadMlmeInd();
        if (psMlmeInd != NULL)
        {
            vProcessIncomingMlme(psMlmeInd);
            vAppQApiReturnMlmeIndBuffer(psMlmeInd);
        }
    } while (psMlmeInd != NULL);

    /* Check for anything on the AHI upward queue */
    do
    {
        psAHI_Ind = psAppQApiReadHwInd();
        if (psAHI_Ind != NULL)
        {
            vProcessIncomingHwEvent(psAHI_Ind);
            vAppQApiReturnHwIndBuffer(psAHI_Ind);
        }
    } while (psAHI_Ind != NULL);
}

/****************************************************************************
 *
 * NAME: vProcessIncomingMlme
 *
 * DESCRIPTION:
 * Process any incoming managment events from the stack.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psMlmeInd
 *
 * RETURNS:
 * None.
 *
 * NOTES:
 * None.
 ****************************************************************************/
PRIVATE void vProcessIncomingMlme(MAC_MlmeDcfmInd_s *psMlmeInd)
{
    switch (psMlmeInd->u8Type)
    {
    case MAC_MLME_IND_ASSOCIATE: /* Incoming association request */

        break;

    case MAC_MLME_DCFM_SCAN: /* Incoming scan results */

        break;

    default:
        break;
    }
}

/****************************************************************************
 *
 * NAME: vProcessIncomingData
 *
 * DESCRIPTION:
 * Process incoming data events from the stack.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psMcpsInd
 *
 * RETURNS:
 * None.
 *
 * NOTES:
 * None.
 ****************************************************************************/
PRIVATE void vProcessIncomingMcps(MAC_McpsDcfmInd_s *psMcpsInd)
{

	switch(psMcpsInd->u8Type)
	{
	case MAC_MCPS_IND_DATA:  /* Incoming data frame */
		vHandleMcpsDataInd(psMcpsInd);
		break;
	case MAC_MCPS_DCFM_DATA: /* Incoming acknowledgement or ack timeout */
		vHandleMcpsDataDcfm(psMcpsInd);
		break;
	default:
		break;
	}
}

/****************************************************************************
 *
 * NAME: vHandleMcpsDataDcfm
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 * NOTES:
 ****************************************************************************/
PRIVATE void vHandleMcpsDataDcfm(MAC_McpsDcfmInd_s *psMcpsInd)
{
    if (psMcpsInd->uParam.sDcfmData.u8Status == MAC_ENUM_SUCCESS)
    {
        /* Data frame transmission successful */
    }
    else
    {
        /* Data transmission falied after 3 retries at MAC layer. */
    }
}

/****************************************************************************
 *
 * NAME: vHandleMcpsDataInd
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 * NOTES:
 ****************************************************************************/
PRIVATE void vHandleMcpsDataInd(MAC_McpsDcfmInd_s *psMcpsInd)
{
    MAC_RxFrameData_s *psFrame;

    psFrame = &psMcpsInd->uParam.sIndData.sFrame;
    vProcessReceivedDataPacket(psFrame->au8Sdu[0]);
}
/****************************************************************************
 *
 * NAME: vProcessReceivedDataPacket
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 *
 * NOTES:
 ****************************************************************************/

PRIVATE void vProcessReceivedDataPacket(uint8 u8BulbCommand)
{

	DBG_vPrintf(TRUE,"\n CMD RX = %s", apcDebugStrings[u8BulbCommand]);
	switch (u8BulbCommand)
	{
		case E_CMD_OFF          : vBULB_SetOnOff(FALSE); break;

		/* to basically test rebroadcast we echo the the level back to     */
		/* the controller if we receive an on command when we are already on */

		case E_CMD_ON           :
			if (TRUE == DriverBulb_bOn())
			{
				int32 i32PerThou = i32GetLevel();
				vTransmitDataPacket(E_STATUS_LEVEL,i32PerThou);
				DBG_vPrintf(TRUE,"\nRebroadcast!");
		    }
		    else
		    {
				vBULB_SetOnOff(TRUE);
			}
			break;

		case E_CMD_LEVEL_UP     : sLampRGB.ai32Delta[E_LEVEL]   = UP; break;
		case E_CMD_LEVEL_DOWN   : sLampRGB.ai32Delta[E_LEVEL]   = DN; break;
		case E_CMD_RED_UP       : sLampRGB.ai32Delta[E_RED]     = UP; break;
		case E_CMD_RED_DOWN     : sLampRGB.ai32Delta[E_RED]     = DN; break;
		case E_CMD_GREEN_UP     : sLampRGB.ai32Delta[E_GREEN]   = UP; break;
		case E_CMD_GREEN_DOWN   : sLampRGB.ai32Delta[E_GREEN]   = DN; break;
		case E_CMD_BLUE_UP      : sLampRGB.ai32Delta[E_BLUE]    = UP; break;
		case E_CMD_BLUE_DOWN    : sLampRGB.ai32Delta[E_BLUE]    = DN; break;
        case E_CMD_BALANCE_RIGHT: sLampRGB.ai32Delta[E_BALANCE] = UP; break;
		case E_CMD_BALANCE_LEFT : sLampRGB.ai32Delta[E_BALANCE] = DN; break;
        case E_CMD_CCT_WARMER   : sLampRGB.ai32Delta[E_CCT]     =  CCT_STEP ; break;
		case E_CMD_CCT_COOLER   : sLampRGB.ai32Delta[E_CCT]     = -CCT_STEP ; break;

		case E_CMD_STOP         : vHandleStopCommand() ; break;
		default : break;
	}

}

/****************************************************************************
 *
 * NAME: vProcessIncomingHwEvent
 *
 * DESCRIPTION:
 * Process any hardware events.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  psAHI_Ind
 *
 * RETURNS:
 * None.
 *
 * NOTES:
 * None.
 ****************************************************************************/

PRIVATE void vProcessIncomingHwEvent(AppQApiHwInd_s *psAHI_Ind)
{
    static uint32 u32RecalibrationCounter = RECAL_PERIOD;
#ifndef DR1223
    static uint32 u32AdcCounter = ADC_PERIOD;
#endif

	if(psAHI_Ind->u32DeviceId == E_AHI_DEVICE_TICK_TIMER)
	{
        int i;


		for(i=0;i<PARAM_COUNT;i++) /* Have we received a 'move parameter' command ? */
		{
			if (sLampRGB.ai32Delta[i])
			{
				if (i != E_CCT)
				{
					vApplyDelta(&sLampRGB.ai32Param[i],sLampRGB.ai32Delta[i],PARAM_MIN,PARAM_MAX);
				}
				else
				{
					if (DriverBulb_vGetColourTempPhyMinMax)
					{
						uint16 u16Min = 0;
						uint16 u16Max = 0;
						DriverBulb_vGetColourTempPhyMinMax(&u16Min,&u16Max );
						vApplyDelta(&sLampRGB.ai32Param[i],sLampRGB.ai32Delta[i],1000000L/u16Max,1000000L/u16Min);
					}
				}
			}
		}

		if (sLampRGB.ai32Delta[E_LEVEL] !=0)		/* Update the driver If the level is moving */
		{
			vBULB_SetLevel(sLampRGB.ai32Param[E_LEVEL]);
		}

		/* Update the driver if the colour is changing */
		if (sLampRGB.ai32Delta[E_RED] + sLampRGB.ai32Delta[E_GREEN]+ sLampRGB.ai32Delta[E_BLUE])
		{
			 vBULB_SetColour(sLampRGB.ai32Param[E_RED],sLampRGB.ai32Param[E_GREEN],sLampRGB.ai32Param[E_BLUE]);
		}

		/* Update Balance */
		if (sLampRGB.ai32Delta[E_BALANCE] && DriverBulb_vSetBalance)
		{
			DriverBulb_vSetBalance((uint8)sLampRGB.ai32Param[E_BALANCE]);
		}

		if (sLampRGB.ai32Delta[E_CCT])
		{
			DBG_vPrintf(TRUE,"vBULB_SetColourTemperature:%d\n",sLampRGB.ai32Param[E_CCT]);
			vBULB_SetColourTemperature(sLampRGB.ai32Param[E_CCT]);
		}


        DriverBulb_vTick();

		/* every 1.28 seconds read the adc values from the driver if supported */



#ifdef DR1223
		static uint32 j=0;

        if (j==0)
        {
        	DBG_vPrintf(TRUE,"\nAPP(DR1223):Radc=%d , Gadc=%d, Badc=%d",  DriverBulb_u16GetAdcValue(E_AHI_ADC_SRC_ADC_4),
											                DriverBulb_u16GetAdcValue(E_AHI_ADC_SRC_ADC_1),
					                                        DriverBulb_u16GetAdcValue(E_AHI_ADC_SRC_ADC_2));

			sLampRGB.ai32Param[E_TEMP] = 25-(((DriverBulb_u16GetAdcValue(E_AHI_ADC_SRC_TEMP)-605)*706)/1000);
			vPullXtal(sLampRGB.ai32Param[E_TEMP]);
			vTransmitDataPacket(E_STATUS_TEMPERATURE,sLampRGB.ai32Param[E_TEMP]);

			DBG_vPrintf(TRUE, "\nAPP(DR1223):Chip Temperature =%d",sLampRGB.ai32Param[E_TEMP]);
	    }
        j=(j+1)& 127UL;
#else

     /* every second read the chip temp */

        if (u32AdcCounter==10)
        {
        	vAHI_AdcStartSample();
        }

        if (u32AdcCounter)
        {
        	u32AdcCounter--;
        	if (u32AdcCounter ==0)
        	{
				u32AdcCounter = ADC_PERIOD;
				if (bAHI_AdcPoll() == FALSE)
				{
					sLampRGB.ai32Param[E_TEMP] =  25-(((u16AHI_AdcRead()-605)*706)/1000);
					vPullXtal(sLampRGB.ai32Param[E_TEMP]);
					vTransmitDataPacket(E_STATUS_TEMPERATURE,sLampRGB.ai32Param[E_TEMP]);
					DBG_vPrintf(TRUE,"\nAPP:Temperature = %d",sLampRGB.ai32Param[E_TEMP]);
				}
        	}
        }
#endif

		/* every minute perform a radio recal */
		if (u32RecalibrationCounter)
		{
			u32RecalibrationCounter--;
			if (u32RecalibrationCounter ==0)
			{
				u32RecalibrationCounter = RECAL_PERIOD;
				(void)eAHI_AttemptCalibration();
				DBG_vPrintf(TRUE,"\nAPP:Recal");
			}
		}
	}
}

/****************************************************************************
 *
 * NAME: vTransmitDataPacket
 *
 * DESCRIPTION:
 *
 *
 ****************************************************************************/
PRIVATE void vTransmitDataPacket(uint32 u32StatusId, int32 i32Value)
{


    MAC_McpsReqRsp_s  sMcpsReqRsp;
    MAC_McpsSyncCfm_s sMcpsSyncCfm;
    uint8 *pu8Payload;

    DBG_vPrintf(TRUE,"\nTX=%s", apcDebugStrings[u32StatusId]);
    /* Create frame transmission request */
    sMcpsReqRsp.u8Type = MAC_MCPS_REQ_DATA;
    sMcpsReqRsp.u8ParamLength = sizeof(MAC_McpsReqData_s);
    /* Set handle so we can match confirmation to request */
    sMcpsReqRsp.uParam.sReqData.u8Handle = 1;
    /* Use short address for source */
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u8AddrMode = 3;
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u16PanId        = PAN_ID;
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.sExt.u32H = *( (uint32 *)pvAppApiGetMacAddrLocation());
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.sExt.u32L = *(((uint32 *)pvAppApiGetMacAddrLocation()) + 1);
    /* Use short address for destination */
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u8AddrMode = 2;
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u16PanId = PAN_ID;
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.u16Short = BROADCAST_ADR;
    /* Frame requires ack but not security, indirect transmit or GTS */
    sMcpsReqRsp.uParam.sReqData.sFrame.u8TxOptions = MAC_TX_OPTION_INDIRECT;

    pu8Payload = sMcpsReqRsp.uParam.sReqData.sFrame.au8Sdu;
    sMcpsReqRsp.uParam.sReqData.sFrame.u8SduLength = 2;

     *pu8Payload++ = (uint8)u32StatusId;
     *pu8Payload++ = (uint8)i32Value;

    /* add extra byte if we echo the colour temperature/level/balance as its a 16bit "per-thou" value */
    /* or in the case of CCT a range of 2700-6500K                                                    */

    switch (u32StatusId)
    {
		case E_STATUS_CCT     :
		case E_STATUS_LEVEL   :
		case E_STATUS_BALANCE :
			*pu8Payload++ = (i32Value >> 8) & 0xff;
        	sMcpsReqRsp.uParam.sReqData.sFrame.u8SduLength++;
        	break;
		default:
        	break;
	}

    /* Request transmit */
    vAppApiMcpsRequest(&sMcpsReqRsp, &sMcpsSyncCfm);
}

/****************************************************************************/
/* NAME: vPullXtal                                                          */
/*                                                                          */
/* DESCRIPTION:                                                             */
/* Oscillator pulling State machine                                         */
/****************************************************************************/
PRIVATE void vPullXtal(int32 i32Temperature)
{
	static teXtalPullingStates eXtalPullingState = E_STATE_XTAL_UNPULLED;

	switch (eXtalPullingState)
	{
		case  E_STATE_XTAL_UNPULLED :
			if (i32Temperature >= TEMP_XTAL_HALF_PULL)
			{
				eXtalPullingState = E_STATE_XTAL_SEMIPULLED;
				vAHI_ClockXtalPull(eXtalPullingState);
			}
			break;

		case  E_STATE_XTAL_SEMIPULLED :
			if (i32Temperature >= TEMP_XTAL_FULL_PULL)
			{
				eXtalPullingState = E_STATE_XTAL_PULLED;
				vAHI_ClockXtalPull(eXtalPullingState);
			}
			else if (i32Temperature < TEMP_XTAL_HALF_PUSH)
			{
				eXtalPullingState = E_STATE_XTAL_UNPULLED;
				vAHI_ClockXtalPull(eXtalPullingState);
			}
			break;

		case  E_STATE_XTAL_PULLED :
			if (i32Temperature < TEMP_XTAL_FULL_PUSH)
			{
				eXtalPullingState = E_STATE_XTAL_SEMIPULLED;
				vAHI_ClockXtalPull(eXtalPullingState);
			}
			break;

		default :
		break;
	}
}

PRIVATE void vApplyDelta(int32 * pi32VarToChange, int32 i32Delta, int32 i32Min, int32 i32Max)
{
	DBG_vPrintf(TRUE,"vApplyDelta  v:%d delta: %d Min:%d Max:%d\n",*pi32VarToChange, i32Delta, i32Min, i32Max);
	*pi32VarToChange +=i32Delta;

	if (*pi32VarToChange > i32Max)
	{
		*pi32VarToChange = i32Max;
	}

	if (*pi32VarToChange <i32Min)
	{
		*pi32VarToChange =i32Min;
	}
}

PRIVATE void vHandleStopCommand(void)
{
	int i;
    for(i=0;i<PARAM_COUNT;i++)
	{
		if (sLampRGB.ai32Delta[i] != STOP)
		{
			sLampRGB.ai32Delta[i] = STOP;

			switch (i+E_STATUS_LEVEL)
			{
				case E_STATUS_LEVEL :
				{
					int32 i32PerThou = i32GetLevel();
					vTransmitDataPacket(E_STATUS_LEVEL,i32PerThou);
				}
				break;

				case E_STATUS_BALANCE :
				{
					int32 i32PerThou = ((1000*sLampRGB.ai32Param[E_BALANCE])/255);
					vTransmitDataPacket(E_STATUS_BALANCE,i32PerThou);
				}
				break;

				default :
					vTransmitDataPacket(E_STATUS_LEVEL+i,sLampRGB.ai32Param[i]);
				break;
			}
		}
	}
}

PRIVATE int32  i32GetLevel()
{
#if (defined DUAL_PWM)
	uint32 u32Rise   = *(uint32 *)0x02005004;
	uint32 u32Period = *(uint32 *)0x02005008;

	//u32Rise +=PWM1_DELAY;

	return (1000*u32Rise )/u32Period;

#elif (defined RGB)

     return sLampRGB.ai32Param[E_LEVEL];

#else

	return ((1000*sLampRGB.ai32Param[E_LEVEL])/255);

#endif

}

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
