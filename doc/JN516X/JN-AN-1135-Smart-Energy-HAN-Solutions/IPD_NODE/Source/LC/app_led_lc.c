/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (LC-IPD)
 *
 * COMPONENT:          app_led.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        LED Operation
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_led_lc.c $
 *
 * $Revision: 8954 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-04-16 13:01:29 +0100 (Mon, 16 Apr 2012) $
 *
 * $Id: app_led_lc.c 8954 2012-04-16 12:01:29Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/


/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include "jendefs.h"
#include "utilities.h"
#include <AppHardwareApi.h>
#include "dbg.h"
#include "os.h"
#include "os_gen.h"
#include "app_zcl_task.h"
#include "app_timer_driver.h"
#include "app_event_handler.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/**************************************************************************///

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vLedInitialise
 *
 * DESCRIPTION:
 * Configure the LED DIO
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vLedInitialise(void)
{
	vAHI_DioSetDirection(0, (1 << 16 | 1 << 8));
    vAHI_DioSetOutput(0,0);
}


/****************************************************************************
 *
 * NAME: vLedSet
 *
 * DESCRIPTION:
 * Turn on the LEDs based on the number of price tiers and the current tier
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vLedSet(uint8 u8NumberOfPriceTiers, uint8 u8CurrentElectricityCostTier)
{
	// As there are 6 price tiers defined in the ZigBee specification and
	// only 2 LEDs (3 colours) to display them, a conversion must take place
	switch (u8NumberOfPriceTiers)
	{
		case 1:		// 1 price tier
			vAHI_DioSetOutput(1 << 8, 0);										// Turn on the green LED
		break;

		case 2:		// 2 price tiers
			if (1 == u8CurrentElectricityCostTier) vAHI_DioSetOutput(1 << 8, 0);// If the current tier is tier 1, turn on the green LED
			else vAHI_DioSetOutput(1 << 16, 0);									// If the current tier is tier 2, turn on the red LED
		break;

		case 3:		// 3 price tiers
			switch (u8CurrentElectricityCostTier)
			{
				case 1:
					vAHI_DioSetOutput(1 << 8, 0);								// If the current tier is tier 1, turn on the green LED
				break;

				case 2:
					vAHI_DioSetOutput((1 << 16 | 1 << 8),0);					// If the current tier is tier 2, turn on the green and red LEDs to make orange
				break;

				case 3:
					vAHI_DioSetOutput(1 << 16, 0);								// If the current tier is tier 3, turn on the red LED
				break;

				default:
					vAHI_DioSetOutput(0,(1 << 16 | 1 << 8));					// Turn off the LEDs if in an unknown state
				break;
			}
		break;

		case 4:		// 4 price tiers
			switch (u8CurrentElectricityCostTier)
			{
				case 1:
					vAHI_DioSetOutput(1 << 8, 0);								// If the current tier is tier 1, turn on the green LED
				break;

				case 2:
				case 3:
					vAHI_DioSetOutput((1 << 16 | 1 << 8),0);					// If the current tier is tier 2 or 3, turn on the green and red LEDs to make orange
				break;

				case 4:
					vAHI_DioSetOutput(1 << 16, 0);								// If the current tier is tier 4, turn on the red LED
				break;

				default:
					vAHI_DioSetOutput(0,(1 << 16 | 1 << 8));					// Turn off the LEDs if in an unknown state
				break;
			}
		break;

		case 5:		// 5 price tiers
			switch (u8CurrentElectricityCostTier)
			{
				case 1:
				case 2:
					vAHI_DioSetOutput(1 << 8, 0);								// If the current tier is tier 1 or 2, turn on the green LED
				break;


				case 3:
					vAHI_DioSetOutput((1 << 16 | 1 << 8),0);					// If the current tier is tier 3, turn on the green and red LEDs to make orange
				break;

				case 4:
				case 5:
					vAHI_DioSetOutput(1 << 16, 0);								// If the current tier is tier 4 or 5, turn on the red LED
				break;

				default:
					vAHI_DioSetOutput(0,(1 << 16 | 1 << 8));					// Turn off the LEDs if in an unknown state
				break;
			}
		break;

		case 6:		// 6 price tiers
			switch (u8CurrentElectricityCostTier)
			{
				case 1:
				case 2:
					vAHI_DioSetOutput(1 << 8, 0);								// If the current tier is tier 1 or 2, turn on the green LED
				break;


				case 3:
				case 4:
					vAHI_DioSetOutput((1 << 16 | 1 << 8),0);					// If the current tier is tier 3 or 4, turn on the green and red LEDs to make orange
				break;

				case 5:
				case 6:
					vAHI_DioSetOutput(1 << 16, 0);								// If the current tier is tier 5 or 6, turn on the red LED
				break;

				default:
					vAHI_DioSetOutput(0,(1 << 16 | 1 << 8));					// Turn off the LEDs if in an unknown state
				break;
			}
		break;

		default:
			vAHI_DioSetOutput(0,(1 << 16 | 1 << 8));							// Turn off the LEDs if in an unknown state
		break;
	}
}


/****************************************************************************
 *
 * NAME: vLedClear
 *
 * DESCRIPTION:
 * Turn off the LEDs
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
void vLedClear(void)
{
	vAHI_DioSetOutput(0,(1 << 16 | 1 << 8));									// Turn off the LEDs
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
