/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Common Code)
 *
 * COMPONENT:          app_buttons.h
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        Monitor the state of the buttons on the DK2 development
 *                     board, ensuring they are debounced.
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/Common/Source/app_buttons.h $
 *
 * $Revision: 9271 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-01 16:40:44 +0100 (Fri, 01 Jun 2012) $
 *
 * $Id: app_buttons.h 9271 2012-06-01 15:40:44Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_BUTTONS_H
#define APP_BUTTONS_H

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define APP_BUTTON_DECLARE(dio) \
    APP_tsButtonDescriptor app_sButton_##name = { NULL, dio, 0 };

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/


#ifdef LC_DEVICE
#define APP_BUTTONS_NUM             (5UL)
#define APP_BUTTONS_BUTTON_1        (17)
#define APP_BUTTONS_BUTTON_2        (18)
#define APP_BUTTONS_BUTTON_3        (19)
#define APP_BUTTONS_BUTTON_4        (0)
#define APP_BUTTONS_BUTTON_RST      (12)
#define APP_BUTTONS_DIO_MASK ( \
    (1 << APP_BUTTONS_BUTTON_1) | \
    (1 << APP_BUTTONS_BUTTON_2) | \
    (1 << APP_BUTTONS_BUTTON_3) | \
    (1 << APP_BUTTONS_BUTTON_4) | \
    (1 << APP_BUTTONS_BUTTON_RST))

#else

#if(JENNIC_PCB == DEVKIT2)
#define APP_BUTTONS_NUM             (3UL)
#define APP_BUTTONS_BUTTON_1        (9)
#define APP_BUTTONS_BUTTON_2        (10)
#define APP_BUTTONS_BUTTON_3        (11)
#define APP_BUTTONS_DIO_MASK ( \
    (1 << APP_BUTTONS_BUTTON_1) | \
    (1 << APP_BUTTONS_BUTTON_2) | \
    (1 << APP_BUTTONS_BUTTON_3))
#endif

#if(JENNIC_PCB == DEVKIT4)
#define APP_BUTTONS_NUM             (4UL)
#define APP_BUTTONS_BUTTON_1        (11)
#define APP_BUTTONS_BUTTON_2        (12)
#define APP_BUTTONS_BUTTON_3        (17)
#define APP_BUTTONS_BUTTON_4        (1)
#define APP_BUTTONS_DIO_MASK ( \
    (1 << APP_BUTTONS_BUTTON_1) | \
    (1 << APP_BUTTONS_BUTTON_2) | \
    (1 << APP_BUTTONS_BUTTON_3) | \
    (1 << APP_BUTTONS_BUTTON_4))
#endif

#endif

typedef enum
{
    APP_E_BUTTONS_BUTTON_1 = 0,
    APP_E_BUTTONS_BUTTON_2,
    APP_E_BUTTONS_BUTTON_3,
    APP_E_BUTTONS_BUTTON_4
#ifdef LC_DEVICE
    ,APP_E_BUTTONS_BUTTON_RST
#endif
} APP_teButtons;

/****************************************************************************/
/***        Exported Functions                                            ***/
/*************************************************************************////

PUBLIC bool_t APP_bButtonInitialise(void);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_BUTTONS_H*/
