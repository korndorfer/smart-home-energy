/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_zcl_task.c
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        ZCL Handler Functions
 *
 * $HeadURL $
 *
 * $Revision: 8546 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-01-09 10:52:15 +0000 (Mon, 09 Jan 2012) $
 *
 * $Id: app_zcl_task.c 8546 2012-01-09 10:52:15Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pdm.h"
#include "pwrm.h"
#include "dbg.h"
#include "zps_apl_af.h"
#include "zps_apl_aib.h"
#include "zps_nwk_sap.h"
#include "zps_nwk_nib.h"
#include "zps_nwk_pub.h"
#include "string.h"

/* Application Includes */
#include "app_timer_driver.h"
#include "zcl.h"
#include "app_zcl_task.h"
#include "app_meter_node.h"
#include "zcl_options.h"
#include "app_smartenergy_demo.h"
#include "app_zbp_utilities.h"
#include "app_certificates.h"
#include "Tunneling.h"
#if(JENNIC_PCB == DEVKIT4)
#include "GenericBoard.h"
#endif



#ifdef STACK_MEASURE
	#include "StackMeasure.h"
	tsStackInfo StackInfo;
#endif

#ifdef OTA_PAGE_REQUEST_SUPPORT
uint32 u32MsTimer;
bool_t bStopTimer = TRUE;
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_ZCL_TASK
#define TRACE_ZCL_TASK	FALSE
#endif

#ifndef TRACE_ZCL_TASK_HIGH
#define TRACE_ZCL_TASK_HIGH	TRUE
#endif

#ifndef TRACE_ZCL_TASK_VERBOSE
#define TRACE_ZCL_TASK_VERBOSE FALSE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void cbZCL_GeneralCallback(tsZCL_CallBackEvent *psEvent);
PRIVATE void cbZCL_EndpointCallback(tsZCL_CallBackEvent *psEvent);

#ifdef CLD_KEY_ESTABLISHMENT
PRIVATE void vHandleKeyEstablishmentEvent(void *pvParam);
#endif

#ifdef CLD_DRLC
PRIVATE void vHandleDrlcEvent(void *pvParam, uint16 u16Addr, uint8 u8RemoteEp);
#endif

#ifdef CLD_PRICE
PRIVATE void vHandlePriceEvent(tsSE_PriceCallBackMessage *psPriceMessage, uint8 u8SrcEndpoint);
#endif

#ifdef CLD_SIMPLE_METERING
PRIVATE void vHandleSimpleMeteringEvent(void *pvParam);
#endif

#ifdef CLD_IDENTIFY
PRIVATE void vHandleIdentifyEvent (tsCLD_IdentifyCallBackMessage *psIdentifyMessage);
#endif

PRIVATE void vHandleTunnelingEvent(void *pvParam);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

PUBLIC tsSE_EspMeterDevice sMeter;
#ifdef NUMBER_OF_SUPPORTED_COMMODITY_TYPES
PUBLIC tsSE_EspDevice sEsp[NUMBER_OF_SUPPORTED_COMMODITY_TYPES - 1];
#endif

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE tsZCL_Address sClientNode;

#ifdef CLD_PRICE
PRIVATE char *acRateLabel = "Rate Label!\0";
#endif

PRIVATE uint32 u32ZCLMutexCount = 0;

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern uint8 	au8CAPublicKey[];
extern uint8 	au8Certificate[];
extern uint8 	au8PrivateKey[];
extern uint8	s_au8LnkKeyArray[16];

#ifdef SE_CERTIFICATION
extern uint8	s_au8LnkKeyArray_Trac_Client_Dongle[16];
#endif

extern tsDevice s_sDevice;
extern PDM_tsRecordDescriptor s_sDevicePDDesc;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_ZCLTask
 *
 * DESCRIPTION:
 * Task to handle to ZCL end point(1) events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_ZCLTask)
{
	tsZCL_CallBackEvent sCallBackEvent;
	ZPS_tsAfEvent sStackEvent;
	static uint8 u8Seconds = 0;

	u8Seconds++;

	/* Clear the ZigBee stack event */
	sStackEvent.eType = ZPS_EVENT_NONE;

	/* Point the ZCL event at the defined ZigBee stack event */
	sCallBackEvent.pZPSevent = &sStackEvent;

	/* 1 second tick, pass it to the ZCL */
	if(OS_eGetSWTimerStatus(APP_ZclTimer) == OS_E_SWTIMER_EXPIRED)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "ZCL Task activated by timer\n");
		sCallBackEvent.eEventType = E_ZCL_CBET_TIMER;
		vZCL_EventHandler(&sCallBackEvent);
		OS_eContinueSWTimer(APP_ZclTimer, ONE_SECOND_TICK_TIME, NULL);

		/* Update time in time clusters */
		sMeter.sTimeCluster.utctTime = u32ZCL_GetUTCTime();

		DBG_vPrintf(TRACE_ZCL_TASK, "Time: ");
		vShowTime(sMeter.sTimeCluster.utctTime);
	}
#ifdef OTA_PAGE_REQUEST_SUPPORT
	/* mili second tick, pass it to the ZCL */
	if(OS_eGetSWTimerStatus(APP_MsTimer) == OS_E_SWTIMER_EXPIRED)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "ZCL Task activated by  mili second timer\n");
		sCallBackEvent.eEventType = E_ZCL_CBET_TIMER_MS;

		vZCL_EventHandler(&sCallBackEvent);

		DBG_vPrintf(TRACE_ZCL_TASK, "bStopTimer Value %d\n", bStopTimer);
		if(!bStopTimer)
		{
			OS_eContinueSWTimer(APP_MsTimer, APP_TIME_MS(u32MsTimer), NULL);
		}
		else
		{
			OS_eStopSWTimer(APP_MsTimer);
		}
    }
#endif

	/* If there is a stack event to process, pass it on to ZCL */
	if (OS_eCollectMessage(APP_msgZCLEvents, &sStackEvent) == OS_E_OK)
	{
		if( (ZPS_EVENT_APS_DATA_INDICATION == sStackEvent.eType) || (ZPS_EVENT_APS_DATA_ACK == sStackEvent.eType) )
		{
			DBG_vPrintf(TRACE_ZCL_TASK, "ZCL_Task received event %d\r\n",sStackEvent.eType);

			sCallBackEvent.eEventType = E_ZCL_CBET_ZIGBEE_EVENT;
			vZCL_EventHandler(&sCallBackEvent);
		}
	}

#ifdef CLD_KEY_ESTABLISHMENT
	if (u8Seconds > 10)
	{
		vDisplayAPSTable();
	}
#endif

	/* Housekeeping, reset the second counter */
	if (u8Seconds > 10)
	{
		u8Seconds = 0;
	}
}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_ZCL_vInitialise
 *
 * DESCRIPTION:
 * Initialises the Meter ZCL
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_ZCL_vInitialise(void)
{
	teZCL_Status eZCL_Status;

#ifdef NUMBER_OF_SUPPORTED_COMMODITY_TYPES
	uint8 u8LoopCntr;
#endif
	sClientNode.eAddressMode = E_ZCL_AM_BOUND;

	/* Initialise smart energy functions */
	eZCL_Status = eSE_Initialise(&cbZCL_GeneralCallback, apduZCL);
	if (eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "eSE_Init ERR: %x\r\n", eZCL_Status);
	}

	/* Initialise Smart Energy Clusters etc */
	eZCL_Status = eSE_RegisterEspMeterEndPoint(ESP_METER_LOCAL_EP, &cbZCL_EndpointCallback, &sMeter, MIRROR_START_EP);
	if (eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "eSE_Register ERR: 0x%x\r\n", eZCL_Status);
	}

#ifdef NUMBER_OF_SUPPORTED_COMMODITY_TYPES
	/* Initialize multiple ESP endpoints other than ESP/Meter base endpoint */
	for(u8LoopCntr = 0; u8LoopCntr<( (uint8)(NUMBER_OF_SUPPORTED_COMMODITY_TYPES-1) ); u8LoopCntr++)
	{
	    eZCL_Status = eSE_RegisterEspEndPoint(MIRROR_START_EP + CLD_SM_NUMBER_OF_MIRRORS + u8LoopCntr,
	                                          &cbZCL_EndpointCallback,
	                                          &sEsp[u8LoopCntr],
											  MIRROR_START_EP);
	     if (eZCL_Status != E_ZCL_SUCCESS)
	     {
	         DBG_vPrintf(TRACE_ZCL_TASK, "eSE_RegisterEspEndPoint ERR: %x\r\n", eZCL_Status);
	     }
	}
#endif
	/* Set TC address to be me */
	ZPS_eAplAibSetApsTrustCenterAddress(ZPS_u64NwkNibGetExtAddr(ZPS_pvAplZdoGetNwkHandle()));

	/* Set the security keys to default if the context has not been restored */
	if(E_NETWORK_RUN != s_sDevice.eState)
	{
		#ifdef ZIGBEE_R20
		ZPS_vAplSecSetInitialSecurityState(ZPS_ZDO_PRECONFIGURED_LINK_KEY, (uint8 *)&s_au8LnkKeyArray, 0x00, ZPS_APS_UNIQUE_LINK_KEY);

		/* Add the pre-shared link keys to our table */
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR1, s_au8LnkKeyArray, ZPS_APS_UNIQUE_LINK_KEY);
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR3, s_au8LnkKeyArray, ZPS_APS_UNIQUE_LINK_KEY);
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR4, s_au8LnkKeyArray, ZPS_APS_UNIQUE_LINK_KEY);
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR5, s_au8LnkKeyArray, ZPS_APS_UNIQUE_LINK_KEY);
		#ifdef SE_CERTIFICATION
		ZPS_eAplZdoAddReplaceLinkKey(TRAC_CLIENT_DONGLE, s_au8LnkKeyArray_Trac_Client_Dongle, ZPS_APS_UNIQUE_LINK_KEY);
		#endif
		#else
		ZPS_vAplSecSetInitialSecurityState(ZPS_ZDO_PRECONFIGURED_LINK_KEY, (uint8 *)&s_au8LnkKeyArray, 0x00);

		/* Add the pre-shared link keys to our table */
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR1, s_au8LnkKeyArray);
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR3, s_au8LnkKeyArray);
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR4, s_au8LnkKeyArray);
		ZPS_eAplZdoAddReplaceLinkKey(IPD_MAC_ADDR5, s_au8LnkKeyArray);
		#ifdef SE_CERTIFICATION
		ZPS_eAplZdoAddReplaceLinkKey(TRAC_CLIENT_DONGLE, s_au8LnkKeyArray_Trac_Client_Dongle);
		#endif
		#endif

		/* Prevent APS acknowledgements until key establishment is complete */
		ZPS_bAplZdoTrustCenterSetDevicePermissions(IPD_MAC_ADDR1, ZPS_TRUST_CENTER_DATA_REQUEST_DISALLOWED);
		ZPS_bAplZdoTrustCenterSetDevicePermissions(IPD_MAC_ADDR3, ZPS_TRUST_CENTER_DATA_REQUEST_DISALLOWED);
		ZPS_bAplZdoTrustCenterSetDevicePermissions(IPD_MAC_ADDR4, ZPS_TRUST_CENTER_DATA_REQUEST_DISALLOWED);
		ZPS_bAplZdoTrustCenterSetDevicePermissions(IPD_MAC_ADDR5, ZPS_TRUST_CENTER_DATA_REQUEST_DISALLOWED);
#ifdef SE_CERTIFICATION
		ZPS_bAplZdoTrustCenterSetDevicePermissions(TRAC_CLIENT_DONGLE, ZPS_TRUST_CENTER_DATA_REQUEST_DISALLOWED);
#endif
    }
#ifdef CLD_KEY_ESTABLISHMENT
	/* Load certificate and keys */
	eSE_KECLoadKeys(ESP_METER_LOCAL_EP, (uint8 *)au8CAPublicKey, (uint8 *)au8Certificate, au8PrivateKey);
#endif

	/* Restore the ESP parameters here,
	 * readings, time, prices etc...
	 */

    sMeter.sSimpleMeteringCluster.u48CurrentTier1SummationDelivered = 0;
    sMeter.sSimpleMeteringCluster.u48CurrentTier2SummationDelivered = 0;
    sMeter.sSimpleMeteringCluster.u48CurrentTier3SummationDelivered = 0;

    sMeter.sSimpleMeteringCluster.i24InstantaneousDemand = 0;
    sMeter.sSimpleMeteringCluster.eMeteringDeviceType = E_CLD_SM_MDT_ELECTRIC;
    sMeter.sSimpleMeteringCluster.u8MeterStatus = 0x00;

    sMeter.sPriceCluster.e8CommodityType = E_CLD_SM_MDT_ELECTRIC;

#ifdef NUMBER_OF_SUPPORTED_COMMODITY_TYPES

    /*E_CLD_SM_MDT_ELECTRIC= 0x00, E_CLD_SM_MDT_GAS, E_CLD_SM_MDT_WATER */

    uint8 i;
    for(i=0; i<( (uint8)(NUMBER_OF_SUPPORTED_COMMODITY_TYPES - 1) ); i++)
    {
    	sEsp[i].sPriceCluster.e8CommodityType = i+1;
    }
#endif

    /* Set time Master */
    sMeter.sTimeCluster.u8TimeStatus = 0x03;
    sMeter.sBasicCluster.ePowerSource = E_CLD_BAS_PS_SINGLE_PHASE_MAINS;
}


/****************************************************************************
 *
 * NAME: APP_ZCL_UpdateMeterData
 *
 * DESCRIPTION:
 * Mutexes then copies the Meter data for application manipulation
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_ZCL_UpdateMeterData(tsSE_EspMeterDevice *psMeter)
{
	vLockZCLMutex();
	sMeter = *psMeter;
	vUnlockZCLMutex();
}


/****************************************************************************
 *
 * NAME: vShowTime
 *
 * DESCRIPTION:
 * Displays the time
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vShowTime(uint32 u32Time)
{
	uint32 u32Sec, u32Min, u32Hour, u32Day;

	u32Day  = u32Time / (60 * 60 * 24);
	u32Time = u32Time % (60 * 60 * 24);

	u32Hour = u32Time / (60 * 60);
	u32Time = u32Time % (60 * 60);

	u32Min  = u32Time / 60;
	u32Time = u32Time % 60;

	u32Sec = u32Time;

	DBG_vPrintf(TRACE_ZCL_TASK, "%d %02d:%02d:%02d\r\n", u32Day, u32Hour, u32Min, u32Sec);
}


#ifdef CLD_PRICE
/****************************************************************************
 *
 * NAME: APP_bAddPrice
 *
 * DESCRIPTION:
 * Adds a price into the price table, if the network is up, a publish price
 * message will be transmitted
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC bool APP_bAddPrice(tsSE_PricePublishPriceCmdPayload sPrice, uint8 u8Ep)
{
	teSE_PriceStatus ePriceStatus;
	//tsSE_PricePublishPriceCmdPayload sPrice;
	uint8 u8SeqNum = 0;
	uint8 e8CommodityType;
	//DBG_vPrintf(TRACE_ZCL_TASK, "Adding price: Start %d Duration %d Price %d, Tier %d \r\n", u32StartTime, u16Duration, u32Price, Tier);

	/* Populate the command payload structure with default values if not already specified */
	if (!sPrice.u32ProviderId)
	{
		sPrice.u32ProviderId =  u32ZCL_GetUTCTime();
	}
	if (!sPrice.sRateLabel.pu8Data)
	{
		sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabel;
		sPrice.sRateLabel.u8Length = strlen((char *)acRateLabel);
		sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabel);
	}
	if (!sPrice.u32IssuerEventId)
	{
		sPrice.u32IssuerEventId = u32ZCL_GetUTCTime();
	}
	if (0xFF == sPrice.u8UnitOfMeasure)
	{
		ePriceStatus = eZCL_ReadLocalAttributeValue(u8Ep, SE_CLUSTER_ID_PRICE,TRUE,FALSE,FALSE, E_CLD_P_ATTR_COMMODITY_TYPE,&e8CommodityType);
		if(ePriceStatus != E_ZCL_SUCCESS)
		{
			DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "Error: eZCL_ReadLocalAttributeValue() returned 0x%x\n", ePriceStatus);
			return FALSE;
		}
		if(E_CLD_SM_MDT_GAS == e8CommodityType)
		{
			sPrice.u8UnitOfMeasure = E_CLD_SM_UOM_CUBIC_METER;
		}
		else if(E_CLD_SM_MDT_WATER == e8CommodityType)
		{
			sPrice.u8UnitOfMeasure = E_CLD_SM_UOM_LITERS;
		}
		else
		{
			sPrice.u8UnitOfMeasure = E_CLD_SM_UOM_KILO_WATTS;
		}
	}
	if(!sPrice.u16Currency)
	{
		sPrice.u16Currency = 0x348;	// $
	}

	if(bIsBindingAvailable(SE_CLUSTER_ID_PRICE,u8Ep))
	    sClientNode.eAddressMode = E_ZCL_AM_BOUND;
	else
		sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;

	vLockZCLMutex();
	ePriceStatus = eSE_PriceAddPriceEntry(u8Ep, 0, &sClientNode, TRUE, &sPrice, &u8SeqNum);
	vUnlockZCLMutex();

    if(ePriceStatus != E_ZCL_SUCCESS)
    {
    	DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "Error: eSE_PriceAddPriceEntry() returned %d\r\n", ePriceStatus);
    	return TRUE;
    }
    else
    {
    	DBG_vPrintf(TRACE_ZCL_TASK, "Price Published\r\n");
    	return FALSE;
    }
}
#endif


#ifdef CLD_MC
/****************************************************************************
 *
 * NAME: vSendNewMessageToBound
 *
 * DESCRIPTION:
 * Sends Messages to Bound devices
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSendNewMessageToBound(uint32 u32MessageId, uint8 u8MessageControl, uint32 StartTime, uint16 u16Duration, char *Message)
{
	teSE_MCStatus eMcStatus;
	tsSE_MCDisplayMessageCommandPayload sPayload;
	uint8 u8SequenceNumber;

	DBG_vPrintf(TRACE_ZCL_TASK, "Sending msg Iss Id %d\n", u32MessageId);

	sPayload.u32MessageId = u32MessageId;
	sPayload.u8MessageControl = u8MessageControl;
	sPayload.u32StartTime = StartTime;
	sPayload.u16DurationInMinutes = u16Duration;
	sPayload.sMessage.u8Length = strlen((char *)Message);
	sPayload.sMessage.u8MaxLength = strlen((char *)Message);
	sPayload.sMessage.pu8Data =(unsigned char*) Message;

    if(bIsBindingAvailable(SE_CLUSTER_ID_MESSAGE,ESP_METER_LOCAL_EP))
        sClientNode.eAddressMode = E_ZCL_AM_BOUND;
    else
        sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;

	eMcStatus = eSE_MCDisplayMessage(ESP_METER_LOCAL_EP, 0, &sClientNode,  &sPayload, &u8SequenceNumber);
	if(eMcStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "MCDisplayMessage: 0x%x, Last stk: 0x%x\n", eMcStatus, eZCL_GetLastZpsError());
	}
}


/****************************************************************************
 *
 * NAME: vSendCancelMessageToBound
 *
 * DESCRIPTION:
 * Sends Messages to Bound devices
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSendCancelMessageToBound(uint32 u32MessageId, uint8 u8MessageControl)
{
	teSE_MCStatus eMcStatus;
	tsSE_MCCancelMessageCommandPayload sPayload;
	uint8 u8SequenceNumber;

	DBG_vPrintf(TRACE_ZCL_TASK, "Sending cancel msg Iss Id %d\n", u32MessageId);

	sPayload.u32MessageId = u32MessageId;
	sPayload.u8MessageControl = u8MessageControl;

    if(bIsBindingAvailable(SE_CLUSTER_ID_MESSAGE,ESP_METER_LOCAL_EP))
        sClientNode.eAddressMode = E_ZCL_AM_BOUND;
    else
        sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;

	eMcStatus = eSE_MCCancelMessage(ESP_METER_LOCAL_EP, 0, &sClientNode,  &sPayload, &u8SequenceNumber);
	if(eMcStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "MCCancelMessage: 0x%x, Last stk: 0x%x\n", eMcStatus, eZCL_GetLastZpsError());
	}
}
#endif


#ifdef CLD_DRLC
/****************************************************************************
 *
 * NAME: vSendNewLceToBound
 *
 * DESCRIPTION:
 *  Send a new load control event to bound clients by adding it to the list.
 *
 * PARAMETERS: Name                     RW  Usage
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSendNewLceToBound(uint32 u32IssuerId, uint16 u16DeviceClass, uint32 u32StartTime, uint16 u16DurationInMinutes, uint8 u8CriticalityLevel, uint8 u8UtilityEnrolmentGroup)
{
    teSE_DRLCStatus eSE_DRLCStatus;
    tsSE_DRLCLoadControlEvent sLCE;

    uint8 u8SequenceNumber;

    DBG_vPrintf(TRACE_ZCL_TASK, "Sending LCE bound  Issuer Id 0x%x\n", u32IssuerId);

    sLCE.u32IssuerId = u32IssuerId;										// Generate a unique ID
    sLCE.u16DeviceClass = u16DeviceClass;
    sLCE.u8UtilityEnrolmentGroup = u8UtilityEnrolmentGroup;
    sLCE.u16DurationInMinutes = u16DurationInMinutes;
    sLCE.u8CriticalityLevel = u8CriticalityLevel;
    sLCE.u8CoolingTemperatureOffset = 0xFF;
    sLCE.u8HeatingTemperatureOffset = 0xFF;
    sLCE.u16CoolingTemperatureSetPoint = 0x8000;
    sLCE.u16HeatingTemperatureSetPoint = 0x8000;
    sLCE.u8AverageLoadAdjustmentSetPoint = 0x80;
    sLCE.u32StartTime = u32StartTime;
    sLCE.u8DutyCycle = 0xFF;
    sLCE.u8EventControl = 0;

    if(bIsBindingAvailable(SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL,ESP_METER_LOCAL_EP))
         sClientNode.eAddressMode = E_ZCL_AM_BOUND;
     else
         sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;

    eSE_DRLCStatus = eSE_DRLCAddLoadControlEvent(ESP_METER_LOCAL_EP, 0, &sClientNode,  &sLCE, &u8SequenceNumber);
    if(eSE_DRLCStatus != E_ZCL_SUCCESS)
    {
        DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "AddLoadControlEvent() 0x%x Last stk 0x%x\n", eSE_DRLCStatus, eZCL_GetLastZpsError());
    }
}


/****************************************************************************
 *
 * NAME: vSendCancelLceToBound
 *
 * DESCRIPTION:
 *  Send a cancel load control event to bound clients.
 *
 * PARAMETERS: Name                     RW  Usage
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSendCancelLceToBound(uint32 u32IssuerId, uint16 u16DeviceClass, uint32 u32effectiveTime)
{
	uint8 u8SequenceNumber;

	tsSE_DRLCCancelLoadControlEvent sSE_DRLCCancelLoadControlEvent;
	sSE_DRLCCancelLoadControlEvent.u32IssuerId = u32IssuerId;
	sSE_DRLCCancelLoadControlEvent.u16DeviceClass = u16DeviceClass;
	sSE_DRLCCancelLoadControlEvent.u8UtilityEnrolmentGroup = 0x01;
	sSE_DRLCCancelLoadControlEvent.eCancelControl = E_SE_DRLC_CANCEL_CONTROL_IMMEDIATE;
	sSE_DRLCCancelLoadControlEvent.u32effectiveTime = u32effectiveTime;

    if(bIsBindingAvailable(SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL,ESP_METER_LOCAL_EP))
         sClientNode.eAddressMode = E_ZCL_AM_BOUND;
     else
         sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;

	teSE_DRLCStatus eSE_DRLCStatus = eSE_DRLCCancelLoadControlEvent(
			ESP_METER_LOCAL_EP,					// uint8 u8SourceEndPointId,
			1,									// uint8 u8DestinationEndPointId, not relevant when sending bound
			&sClientNode,						// tsZCL_Address psDestinationAddress,
			&sSE_DRLCCancelLoadControlEvent,	// tsSE_DRLCCancelLoadControlEvent *psCancelLoadControlEvent,
			&u8SequenceNumber);					// uint8 *pu8TransactionSequenceNumber);

	if(eSE_DRLCStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "eSE_DRLCCancelLoadControlEvent() 0x%x Last stk 0x%x\n", eSE_DRLCStatus, eZCL_GetLastZpsError());
	}
}


/****************************************************************************
 *
 * NAME: vSendCancelAllLceToBound
 *
 * DESCRIPTION:
 *  Send a cancel load control event to bound clients.
 *
 * PARAMETERS: Name                     RW  Usage
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSendCancelAllLceToBound(uint8 eCancelEventControl)
{
	uint8 u8SequenceNumber;

    if(bIsBindingAvailable(SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL,ESP_METER_LOCAL_EP))
         sClientNode.eAddressMode = E_ZCL_AM_BOUND;
     else
         sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;

	teSE_DRLCStatus eSE_DRLCStatus = eSE_DRLCCancelAllLoadControlEvents(
			ESP_METER_LOCAL_EP,
			1,
			&sClientNode,
			eCancelEventControl,
			&u8SequenceNumber);

	if(eSE_DRLCStatus != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "eSE_DRLCCancelAllLoadControlEvents() 0x%x Last stk 0x%x\n", eSE_DRLCStatus, eZCL_GetLastZpsError());
	}
}
#endif


/****************************************************************************
 *
 * NAME: vSendWriteIndividualAttritbutesReqToBound
 *
 * DESCRIPTION:
 *  Send a write attribute request to bound clients.
 *
 * PARAMETERS: Name                     RW  Usage
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSendWriteIndividualAttritbutesReqToBound( uint16 u16AttributeId, uint16 u16ClusterId, bool_t bServerToClient )
{
	uint8	u8SequenceNumber;
	uint16	u16ArrayOfAttributes[] ={ u16AttributeId };

    if(bIsBindingAvailable( u16ClusterId, ESP_METER_LOCAL_EP ))
         sClientNode.eAddressMode = E_ZCL_AM_BOUND;
     else
         sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;

    teZCL_Status eZCL_Status = eZCL_SendWriteAttributesRequest(
									ESP_METER_LOCAL_EP,
									0,
									u16ClusterId,
									bServerToClient,
									&sClientNode,
									&u8SequenceNumber,
									1,
									FALSE,
									0,
									u16ArrayOfAttributes);

	if(eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "eZCL_SendWriteAttributesRequest() 0x%x Last stk 0x%x\n", eZCL_Status, eZCL_GetLastZpsError());
	}
}


/****************************************************************************
 *
 * NAME: vSendReadIndividualAttritbutesReqToBound
 *
 * DESCRIPTION:
 *  Send a read attribute request to bound clients.
 *
 * PARAMETERS: Name                     RW  Usage
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSendReadIndividualAttritbutesReqToBound( uint16 u16AttributeId, uint16 u16ClusterId, bool_t bServerToClient )
{
	uint8	u8SequenceNumber;
	uint16	u16ArrayOfAttributes[] ={ u16AttributeId };

    if(bIsBindingAvailable( u16ClusterId, ESP_METER_LOCAL_EP ))
    {
    	sClientNode.eAddressMode = E_ZCL_AM_BOUND;
    }
    else
    {
    	if (GENERAL_CLUSTER_ID_BASIC != u16ClusterId)
    	{
    		sClientNode.eAddressMode = E_ZCL_AM_NO_TRANSMIT;
    	}
#ifdef SE_CERTIFICATION
    	else
    	{
    		sClientNode.eAddressMode = E_ZCL_AM_IEEE;
    		sClientNode.uAddress.u64DestinationAddress = TRAC_CLIENT_DONGLE;
    	}
#endif
    }

    teZCL_Status eZCL_Status = eZCL_SendReadAttributesRequest(
									ESP_METER_LOCAL_EP,
									1,
									u16ClusterId,
									bServerToClient,
									&sClientNode,
									&u8SequenceNumber,
									1,
									FALSE,
									0,
									u16ArrayOfAttributes);

	if(eZCL_Status != E_ZCL_SUCCESS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "eZCL_SendReadAttributesRequest() 0x%x Last stk 0x%x\n", eZCL_Status, eZCL_GetLastZpsError());
	}
}


/****************************************************************************
 *
 * NAME: vLockZCLMutex
 *
 * DESCRIPTION:
 * Grabs and maintains a counting mutex
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vLockZCLMutex(void)
{
	if (u32ZCLMutexCount == 0)
	{
		OS_eEnterCriticalSection(ZCL);
	}
	u32ZCLMutexCount++;
}


/****************************************************************************
 *
 * NAME: vUnlockZCLMutex
 *
 * DESCRIPTION:
 * Releases and maintains a counting mutex
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vUnlockZCLMutex(void)
{
	u32ZCLMutexCount--;
	if (u32ZCLMutexCount == 0)
	{
		OS_eExitCriticalSection(ZCL);
	}
}


/****************************************************************************
 *
 * NAME: vClearExpiredFlag
 *
 * DESCRIPTION:
 * Starts and immediately stops a timer to clear the expired flag
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vClearExpiredFlag(OS_thSWTimer hSWTimer)
{
	OS_eStartSWTimer(hSWTimer, APP_TIME_MS(10000), NULL);
	OS_eStopSWTimer(hSWTimer);
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: cbZCL_GeneralCallback
 *
 * DESCRIPTION:
 * General callback for ZCL events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void cbZCL_GeneralCallback(tsZCL_CallBackEvent *psEvent)
{
	ZPS_teStatus eStatus;

	switch(psEvent->eEventType)
	{
		case E_ZCL_CBET_LOCK_MUTEX:
			vLockZCLMutex();
		break;

		case E_ZCL_CBET_UNLOCK_MUTEX:
			vUnlockZCLMutex();
		break;

		case E_ZCL_CBET_UNHANDLED_EVENT:
		break;

		case E_ZCL_CBET_READ_ATTRIBUTES_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Read attributes response\r\n");
		break;

		case E_ZCL_CBET_READ_REQUEST:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Read request\r\n");
		break;

		case E_ZCL_CBET_DEFAULT_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK_VERBOSE, "EVT: Default response\r\n");
		break;

		case E_ZCL_CBET_ERROR:
			eStatus = eZCL_GetLastZpsError();
			DBG_vPrintf(TRACE_ZCL_TASK, "EVT: Error - Stack returned 0x%x\r\n", eStatus);
		break;

		case E_ZCL_CBET_TIMER:
		    DBG_vPrintf(TRACE_ZCL_TASK, "EVT: Timer\r\n");
		break;

		case E_ZCL_CBET_ZIGBEE_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EVT: ZigBee\r\n");
		break;

		case E_ZCL_CBET_CLUSTER_CUSTOM:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Custom\r\n");
		break;

		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "Invalid event type\r\n");
		break;
	}
}


/****************************************************************************
 *
 * NAME: cbZCL_EndpointCallback
 *
 * DESCRIPTION:
 * Endpoint specific callback for ZCL events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void cbZCL_EndpointCallback(tsZCL_CallBackEvent *psEvent)
{
	switch (psEvent->eEventType)
	{
		case E_ZCL_CBET_LOCK_MUTEX:
			vLockZCLMutex();
		break;

		case E_ZCL_CBET_UNLOCK_MUTEX:
			vUnlockZCLMutex();
		break;

		case E_ZCL_CBET_UNHANDLED_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Unhandled event\r\n");
		break;

		case E_ZCL_CBET_REPORT_INDIVIDUAL_ATTRIBUTE:
		case E_ZCL_CBET_READ_INDIVIDUAL_ATTRIBUTE_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Read/Report individual attribute response\r\n");
			DBG_vPrintf(TRACE_ZCL_TASK, "* Attr 0x%04x\r\n", psEvent->uMessage.sIndividualAttributeResponse.u16AttributeEnum);

#ifdef  CLD_SM_SUPPORT_MIRROR
			if(psEvent->uMessage.sIndividualAttributeResponse.u16AttributeEnum 	== E_CLD_SM_ATTR_ID_METERING_DEVICE_TYPE &&
			  (psEvent->psClusterInstance->psClusterDefinition->u8ClusterControlFlags &  CLUSTER_MIRROR_BIT) )
			{
				zbmap8 eMeterDevice = *(zbmap8 *)psEvent->uMessage.sIndividualAttributeResponse.pvAttributeData;
				eMeterDevice = eMeterDevice + 0x7F; // Adds offset for the Mirrored Meter Type

				if( eZCL_WriteLocalAttributeValue(ESP_METER_LOCAL_EP,SE_CLUSTER_ID_SIMPLE_METERING,TRUE,FALSE,FALSE,E_CLD_SM_ATTR_ID_METERING_DEVICE_TYPE,&eMeterDevice)
					!= E_ZCL_SUCCESS )
				{
					DBG_vPrintf(TRACE_ZCL_TASK,"Unable to Change Device Type for the Mirror\n");
				}
			}
#endif
		break;

		case E_ZCL_CBET_READ_ATTRIBUTES_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "Read Attributes Resp\r\n");
		break;

		case E_ZCL_CBET_READ_REQUEST:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Read request\r\n");

#ifdef POT_READ

			sMeter.sSimpleMeteringCluster.i24InstantaneousDemand = (1.2 * u16ReadPotValue());
			DBG_vPrintf(TRACE_ZCL_TASK, "\r\nPotVal: %d",sMeter.sSimpleMeteringCluster.i24InstantaneousDemand);
#else
			sMeter.sSimpleMeteringCluster.i24InstantaneousDemand = 500;
#endif
			sMeter.sSimpleMeteringCluster.u24Multiplier = 1;
			sMeter.sSimpleMeteringCluster.u24Divisor = 1000;
		break;

#ifdef CLD_SM_SUPPORT_MIRROR
		case E_ZCL_CBET_ATTRIBUTE_REPORT_MIRROR:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Report Mirror\r\n");
			eSM_IsMirrorSourceAddressValid(&psEvent->uMessage.sReportAttributeMirror);
		break;
#endif

		case E_ZCL_CBET_DEFAULT_RESPONSE:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Default response\r\n");
			DBG_vPrintf(TRACE_ZCL_TASK, "Command ID: %d Status Code: %d\r\n", psEvent->uMessage.sDefaultResponse.u8CommandId, psEvent->uMessage.sDefaultResponse.u8StatusCode);
		break;

		case E_ZCL_CBET_ERROR:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Error\n");
		break;

		case E_ZCL_CBET_TIMER:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Timer\r\n");
		break;

		case E_ZCL_CBET_ZIGBEE_EVENT:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: ZigBee\r\n");
		break;

		case E_ZCL_CBET_CLUSTER_CUSTOM:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Custom\r\n");

			switch(psEvent->uMessage.sClusterCustomMessage.u16ClusterId)
			{
#ifdef CLD_PRICE
				case SE_CLUSTER_ID_PRICE:
					vHandlePriceEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData, psEvent->u8EndPoint);
					DBG_vPrintf(TRACE_ZCL_TASK, "Price Cluster Event Actioned\r\n");
				break;
#endif

#ifdef CLD_MC
				case SE_CLUSTER_ID_MESSAGE:
				{
					tsSE_PriceCallBackMessage *psMessage = psEvent->uMessage.sClusterCustomMessage.pvCustomData;
					DBG_vPrintf(TRACE_ZCL_TASK, "Message Cluster Event: %i\n", psMessage->eEventType);
				}
				break;
#endif

#ifdef CLD_KEY_ESTABLISHMENT
				case SE_CLUSTER_ID_KEY_ESTABLISHMENT:
					vHandleKeyEstablishmentEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif

#ifdef CLD_DRLC
				case SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL:
					vHandleDrlcEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData,
							psEvent->pZPSevent->uEvent.sApsDataIndEvent.uSrcAddress.u16Addr,
							psEvent->pZPSevent->uEvent.sApsDataIndEvent.u8SrcEndpoint);
				break;
#endif
#ifdef CLD_SIMPLE_METERING
				case SE_CLUSTER_ID_SIMPLE_METERING:
					vHandleSimpleMeteringEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif

#ifdef CLD_TUNNELING
				case SE_CLUSTER_ID_SMART_ENERGY_TUNNELING:
					vHandleTunnelingEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif


#ifdef CLD_OTA
				case OTA_CLUSTER_ID:
				{
					tsOTA_CallBackMessage *psMessage = (tsOTA_CallBackMessage*)psEvent->uMessage.sClusterCustomMessage.pvCustomData;
					if(psMessage->eEventId == E_CLD_OTA_COMMAND_BLOCK_REQUEST)
					{
						if(psMessage->uMessage.sBlockRequestPayload.u16BlockRequestDelay == 0)
						{
							tsOTA_WaitForData sData;
							sData.u16BlockRequestDelayMs = 500;
							sData.u32RequestTime = 0;
							sData.u32CurrentTime = 0;
							eOTA_SetWaitForDataParams(1,psEvent->pZPSevent->uEvent.sApsDataIndEvent.uSrcAddress.u16Addr,&sData);
						}
					}
					DBG_vPrintf(TRACE_ZCL_TASK, "OTA event\n");
				}
				break;
#endif
#ifdef CLD_IDENTIFY
				case GENERAL_CLUSTER_ID_IDENTIFY:
					vHandleIdentifyEvent(psEvent->uMessage.sClusterCustomMessage.pvCustomData);
				break;
#endif
				default:
					DBG_vPrintf(TRACE_ZCL_TASK, "Custom event for unknown cluster %d\r\n", psEvent->uMessage.sClusterCustomMessage.u16ClusterId);
				break;
			}
		break;
#ifdef OTA_PAGE_REQUEST_SUPPORT
		case E_ZCL_CBET_ENABLE_MS_TIMER:
		    OS_eStartSWTimer(APP_MsTimer, APP_TIME_MS(psEvent->uMessage.u32TimerPeriodMs), NULL);
		    u32MsTimer = psEvent->uMessage.u32TimerPeriodMs;
			DBG_vPrintf(TRACE_ZCL_TASK, "Timer update event %d\r\n",psEvent->uMessage.u32TimerPeriodMs);
			bStopTimer = FALSE;

		break;
		case E_ZCL_CBET_DISABLE_MS_TIMER:
			//OS_eStopSWTimer(APP_MsTimer);
			DBG_vPrintf(TRACE_ZCL_TASK, "Timer update event timer stop... %d\r\n",OS_eStopSWTimer(APP_MsTimer));
			bStopTimer = TRUE;
			break;
#endif
		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "EP EVT: Invalid event type\r\n");
		break;
	}
}

#ifdef CLD_TUNNELING

PRIVATE void vHandleTunnelingEvent(void *pvParam)
{
	tsSE_TunnelCallBackMessage *psMessage = (tsSE_TunnelCallBackMessage*)	(pvParam);
	tsSE_TunnelTransferDataReqCmdPyld sData;
	uint8 u8SeqNo;
	static uint16 tunnelID;
	tsZCL_Address psDestinationAddress;
	uint8 u8TunnelData[] = "tunnel data from Client\r\n";

	psDestinationAddress.eAddressMode = E_ZCL_AM_IEEE;
	psDestinationAddress.uAddress.u64DestinationAddress = 0x0000000000000003;


	switch( psMessage->eEventType)
	{
		case E_SE_TUN_REQUEST_TUNNEL_REQUEST_RECEIVED:

			DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_REQUEST_TUNNEL_REQUEST_RECEIVED\n");

		break;

		case E_SE_TUN_REQUEST_TUNNEL_RESPONSE_RECEIVED:

			DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_REQUEST_TUNNEL_RESPONSE_RECEIVED, ID= %x\n", psMessage->uMessage.sTunnelRequestTunnelRspCmd.u16TunnelID);
			tunnelID = psMessage->uMessage.sTunnelRequestTunnelRspCmd.u16TunnelID;

			sData.u16TunnelID = tunnelID;
			sData.pu8Data = u8TunnelData;
			sData.u16dataLength = 28;

			/* If tunnel was success, send data to server */
			if(0x00 == psMessage->uMessage.sTunnelRequestTunnelRspCmd.eStatus)
			{
				teSE_TunnelStatus TunnelStatus = eSE_TunnelTransferDataSend(
											0x01,
											0x03,
											FALSE,
											&psDestinationAddress,
											&u8SeqNo,
											&sData);

				DBG_vPrintf(TRACE_ZCL_TASK,"eSE_TunnelTransferDataSend: %x\n", TunnelStatus);

			}

		break;

		case E_SE_TUN_CREATED:

				DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_CREATED received\n");

		break;

		case E_SE_TUN_TRANSFER_DATA_COMMAND_RECEIVED:

			DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_TRANSFER_DATA_COMMAND_RECEIVED\r\n %s \r\n", psMessage->uMessage.sTunnelTransferDataCmd.pu8Data );

			/* Data packer received from the server, now close the tunnel */
			teSE_TunnelStatus TunnelStatus = eSE_TunnelCloseTunnelSend(
												1,
												3,
												&psDestinationAddress,
												&u8SeqNo,
												tunnelID );

			DBG_vPrintf(TRACE_ZCL_TASK,"eSE_TunnelCloseTunnelSend: %x\n", TunnelStatus);

		break;

		case E_SE_TUN_TUNNEL_DATA_TRANSFER_COMPLETED:

			DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_TUNNEL_DATA_TRANSFER_COMPLETED\n");

		break;

		case E_SE_TUN_TUNNEL_DATA_TRANSFER_ERROR:

			DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_TUNNEL_DATA_TRANSFER_ERROR\n");

		break;

		case E_SE_TUN_GET_SUPPORTED_PROTOCOL_RESPONSE_RECEIVED:

			DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_GET_SUPPORTED_PROTOCOL_RESPONSE_RECEIVED\n");

		break;

		case E_SE_TUN_CLOSED:

			DBG_vPrintf(TRACE_ZCL_TASK,"E_SE_TUN_CLOSED\n");

		break;

		default:
			DBG_vPrintf(TRACE_ZCL_TASK,"un-handled SE TUNNEL EVENT\n");
		break;
	}
}
#endif


#ifdef CLD_PRICE
/****************************************************************************
 *
 * NAME: vHandlePriceEvent
 *
 * DESCRIPTION:
 * Handles the price events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandlePriceEvent(tsSE_PriceCallBackMessage *psPriceMessage, uint8 u8SrcEndpoint)
{
	DBG_vPrintf(TRACE_ZCL_TASK, "Price event %d\r\n", psPriceMessage->eEventType);

	switch(psPriceMessage->eEventType)
	{
		case E_SE_PRICE_TABLE_ADD:
			if(psPriceMessage->uMessage.sPriceTableCommand.ePriceStatus == E_ZCL_SUCCESS)
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "Added price\r\n");
			}
			else
			{
				DBG_vPrintf(TRACE_ZCL_TASK, "Add Price ERR:\r\n", psPriceMessage->uMessage.sPriceTableCommand.ePriceStatus);
			}
		break;

		case E_SE_PRICE_TABLE_ACTIVE:
			DBG_vPrintf(TRACE_ZCL_TASK, "Price active\r\n");
		break;

		case E_SE_PRICE_GET_CURRENT_PRICE_RECEIVED:
			DBG_vPrintf(TRACE_ZCL_TASK, "Get current price received event\r\n");
		break;

		case E_SE_PRICE_TIME_UPDATE:
		case E_SE_PRICE_ACK_RECEIVED:
		case E_SE_PRICE_NO_PRICE_TABLES:
		{
		    switch(u8SrcEndpoint)
		    {
                case 1:
                {
                    DBG_vPrintf(TRACE_ZCL_TASK, "Electricity Price Server List Empty\r\n");
                    break;
                }

                case 2:
                {
                    DBG_vPrintf(TRACE_ZCL_TASK, "Gas Price Server List Empty\r\n");
                    break;
                }
		    }

		    break;
		}
		case E_SE_PRICE_READ_BLOCK_PRICING:
		case E_SE_PRICE_BLOCK_PERIOD_TABLE_ACTIVE:
		case E_SE_PRICE_NO_BLOCK_PERIOD_TABLES:
		case E_SE_PRICE_BLOCK_PERIOD_ADD:
		case E_SE_PRICE_READ_BLOCK_THRESHOLDS:
		case E_SE_PRICE_CBET_ENUM_END:
		//break;

		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "Unhandled price event: %d\r\n", psPriceMessage->eEventType);
		break;
	}
}
#endif


#ifdef CLD_KEY_ESTABLISHMENT
/****************************************************************************
 *
 * NAME: vHandleKeyEstablishmentEvent
 *
 * DESCRIPTION:
 * Handles the Key Establishment events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleKeyEstablishmentEvent(void *pvParam)
{
    uint64 u64MacAddress = 0;
    tsSE_KECCallBackMessage *psKECMessage = (tsSE_KECCallBackMessage*)pvParam;
    DBG_vPrintf(TRACE_ZCL_TASK, "KEC Event: %d, Command: %d Status: %x\r\n", psKECMessage->eEventType, psKECMessage->u8CommandId, psKECMessage->eKECStatus);

	/* If key establishment was successful */
	if((psKECMessage->eEventType == E_SE_KEC_EVENT_COMMAND) &&
       (psKECMessage->eKECStatus == E_ZCL_SUCCESS) &&
       (psKECMessage->u8CommandId == E_SE_CONFIRM_KEY_DATA_REQUEST))
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "Key est OK\n");
		/* Set the link key to that returned from the key establishment
		 * First parameter is the mac address of the other node */
		memcpy(&u64MacAddress, &psKECMessage->psKEC_Common->uMessage.au8RemoteCertificate[22], 8);

#ifdef ZIGBEE_R20
		teZCL_Status eZCL_Status = ZPS_eAplZdoAddReplaceLinkKey(u64MacAddress, psKECMessage->psKEC_Common->au8Key, ZPS_APS_UNIQUE_LINK_KEY);
#else
		teZCL_Status eZCL_Status = ZPS_eAplZdoAddReplaceLinkKey(u64MacAddress, psKECMessage->psKEC_Common->au8Key);
#endif

		if (eZCL_Status)

		{
			DBG_vPrintf(TRACE_ZCL_TASK, "eAplZdoAdd ERR: %x\n", eZCL_Status);
		}
		ZPS_bAplZdoTrustCenterSetDevicePermissions(u64MacAddress, ZPS_TRUST_CENTER_JOIN_DISALLOWED);
    }
}
#endif


#ifdef CLD_DRLC
/****************************************************************************
 *
 * NAME: vHandleDrlcEvent
 *
 * DESCRIPTION:
 * Handles the Demand Response Load Control events
 *
 * PARAMETERS: Name                     RW  Usage
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleDrlcEvent(void *pvParam, uint16 u16Addr, uint8 u8RemoteEp)
{
	tsSE_DRLCCallBackMessage *psDrlcMessage = (tsSE_DRLCCallBackMessage*) (pvParam);

	DBG_vPrintf(TRACE_ZCL_TASK, "\npsDrlcMessage event %d, u8CommandId %d, u32CurrentTime %d, eDRLCStatus %d\n",
			psDrlcMessage->eEventType, psDrlcMessage->u8CommandId, psDrlcMessage->u32CurrentTime,
			psDrlcMessage->eDRLCStatus);

/*	add this back in for more than one device */
/*
	if (psDrlcMessage->eEventType == E_SE_DRLC_EVENT_COMMAND && psDrlcMessage->u8CommandId
			== SE_DRLC_GET_SCHEDULED_EVENTS)
	{

		teZCL_Status eStatus;
		uint16 au16AttList[3] =
		{   E_CLD_DRLC_UTILITY_ENROLMENT_GROUP,
			E_CLD_DRLC_START_RANDOMIZE_MINUTES,
			E_CLD_DRLC_STOP_RANDOMIZE_MINUTES};
		uint8 u8Tsn;
		tsZCL_Address sZCL_Address;

		sZCL_Address.eAddressMode = E_ZCL_AM_SHORT;
		sZCL_Address.uAddress.u16DestinationAddress = u16Addr;

		if (bHaveWrittenClientAtts)
		{
			return;
		}
		bHaveWrittenClientAtts = TRUE;

		sMeter.sDRLCCluster.u8UtilityEnrolmentGroup = 0xab;
		sMeter.sDRLCCluster.u8StartRandomizeMinutes = 0x12;
		sMeter.sDRLCCluster.u8StopRandomizeMinutes = 0x34;


		eStatus = eZCL_SendWriteAttributesRequest(ESP_METER_LOCAL_EP, 			// u8SourceEndPointId
				u8RemoteEp, 													// u8DestinationEndPointId
				SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL, 				// u16ClusterId
				TRUE, 															// bDirectionIsServerToClient
				&sZCL_Address, 													// psDestinationAddress
				&u8Tsn, 														// pu8TransactionSequenceNumber
				3, 																// u8NumberOfAttributesInRequest
				FALSE,
				0,
				(uint16*)&au16AttList); 										// pu16AttributeRequestList

		DBG_vPrintf(TRACE_ZCL_TASK, "\neZCL_SendWriteAttributesRequest returned %d, tsn %d\n", eStatus, u8Tsn);
	}
*/

	if (psDrlcMessage->eEventType == E_SE_DRLC_EVENT_COMMAND && psDrlcMessage->u8CommandId
			== SE_DRLC_REPORT_EVENT_STATUS)
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "\nEvent Status = %d\n", psDrlcMessage->uMessage.sReportEvent.u8EventStatus);
	}
}
#endif


/****************************************************************************
 *
 * NAME: vHandleSimpleMeteringEvent
 *
 * DESCRIPTION:
 * Handles the simple metering events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleSimpleMeteringEvent(void *pvParam)
{
    uint16 u16FoundEP;
    tsSM_CallBackMessage *psSMMessage = (tsSM_CallBackMessage*)pvParam;
    DBG_vPrintf(TRACE_ZCL_TASK, "SM Event: %d, Command: %d\r\n", psSMMessage->eEventType, psSMMessage->u8CommandId);

#ifdef CLD_SM_SUPPORT_MIRROR
	if((psSMMessage->eEventType == E_CLD_SM_CLIENT_RECEIVED_COMMAND) &&
       ((psSMMessage->u8CommandId ==  E_CLD_SM_REQUEST_MIRROR)||(psSMMessage->u8CommandId ==  E_CLD_SM_REMOVE_MIRROR)))
	{
		DBG_vPrintf(TRACE_ZCL_TASK_HIGH, "Request/Remove Mirror cmd recvd\n");
		eSM_GetFreeMirrorEndPoint(&u16FoundEP);
		if (u16FoundEP == 0xFFFF)
		{
			sMeter.sBasicCluster.u8PhysicalEnvironment = 0x00;
		}
		else
		{
			sMeter.sBasicCluster.u8PhysicalEnvironment = 0x01;
		}

		/* maintain an array of the IEEE addresses of the mirrored
           Metering Devices and keep a copy of this array in NVM*/
		uint8 u8LoopCntr;
		for (u8LoopCntr =0; u8LoopCntr < CLD_SM_NUMBER_OF_MIRRORS; u8LoopCntr++)
		{
			s_sDevice.u64MirroredDeviceAddress[u8LoopCntr] =
						 sMeter.sSE_Mirrors[u8LoopCntr].u64SourceAddress;
		}
		PDM_vSaveRecord(&s_sDevicePDDesc);

	}
#endif
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
	if((psSMMessage->eEventType == E_CLD_SM_SERVER_RECEIVED_COMMAND)&&
	   (psSMMessage->u8CommandId ==  E_CLD_SM_REQUEST_FAST_POLL_MODE))
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "Fast Poll Mode Request is Rcvd\n");
		psSMMessage->uMessage.sRequestFastPollCommand.bReadyToPoll = TRUE;
	}
	else if(psSMMessage->eEventType == E_CLD_SM_FAST_POLLING_TIMER_EXPIRED)
	{
		DBG_vPrintf(TRACE_ZCL_TASK, "Fast Poll Mode is over\n");
	}
#endif
}


/****************************************************************************
 *
 * NAME: vHandleIdentifyEvent
 *
 * DESCRIPTION:
 * Handles the identify events
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vHandleIdentifyEvent (tsCLD_IdentifyCallBackMessage *psIdentifyMessage)
{
	uint16 u16IdentifyTime;

	switch(psIdentifyMessage -> u8CommandId)
	{
		case E_CLD_IDENTIFY_CMD_IDENTIFY:
			u16IdentifyTime = psIdentifyMessage->uMessage.psIdentifyRequestPayload->u16IdentifyTime;
			DBG_vPrintf(TRACE_ZCL_TASK, "E_CLD_IDENTIFY_CMD_IDENTIFY,  Identify Time: %d\n", u16IdentifyTime);
		break;

		case E_CLD_IDENTIFY_CMD_IDENTIFY_QUERY:
			DBG_vPrintf(TRACE_ZCL_TASK, " E_CLD_IDENTIFY_CMD_IDENTIFY_QUERY \n");
		break;

		default:
			DBG_vPrintf(TRACE_ZCL_TASK, "Unhandled identify command: %d\r\n", psIdentifyMessage -> u8CommandId);
		break;
	}
}


/****************************************************************************
 *
 * NAME: eAPP_GetBinding
 *
 * DESCRIPTION:
 * Add a binding
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC teZCL_Status eAPP_GetBinding(uint16 u16Index, ZPS_tsAplApsmeBindingTableEntry* psEntry)
{
    ZPS_tsAplAib *psAib  = ZPS_psAplAibGetAib();

    if(psAib->psAplApsmeAibBindingTable->psAplApsmeBindingTable->u32SizeOfBindingTable <= u16Index)
    {
        return(E_ZCL_ERR_PARAMETER_RANGE);
    }
    *psEntry = psAib->psAplApsmeAibBindingTable->psAplApsmeBindingTable->pvAplApsmeBindingTableEntryForSpSrcAddr[u16Index];

    return(E_ZCL_SUCCESS);
}


/****************************************************************************
 *
 * NAME: bIsBindingAvailable
 *
 * DESCRIPTION:
 * Is there enough allocated space for another binary
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC bool_t bIsBindingAvailable(uint16 u16Cluster, uint8 u8SourceEndpoint)
{
    uint16 u16Index = 0;
    ZPS_tsAplApsmeBindingTableEntry sEntry;
    while(E_ZCL_SUCCESS == eAPP_GetBinding(u16Index++,&sEntry))
    {
        if((sEntry.u16ClusterId == u16Cluster)&&(sEntry.u8SourceEndpoint == u8SourceEndpoint))
        {
            return(TRUE);
        }
    }
    return(FALSE);
}

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
