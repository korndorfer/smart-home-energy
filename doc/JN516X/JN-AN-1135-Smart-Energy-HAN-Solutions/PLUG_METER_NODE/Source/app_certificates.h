/*****************************************************************************
 *
 * MODULE:             JN-AN-1135
 *
 * COMPONENT:          app_certificates.h
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Certificates Header
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_adc.h $
 *
 * $Revision: 8954 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-04-16 13:01:29 +0100 (Mon, 16 Apr 2012) $
 *
 * $Id: app_adc.h 8954 2012-04-16 12:01:29Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_CERTIFICATES_H_
#define APP_CERTIFICATES_H_
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/* Pre configured link key - derived from installation code
 * Also used for APS security until KEC is completed */

#ifdef PRODUCTION_CERTS
/* Create blank spaces ready to be populated with the relevant information during production */

/* Certicom Production Public Key */
/* NOTE: should be replaced with your issuing authority's production public key */
PUBLIC	uint8 au8CAPublicKey[] = {
		0x02, 0x00, 0xfd, 0xe8, 0xa7, 0xf3, 0xd1, 0x08,
		0x42, 0x24, 0x96, 0x2a, 0x4e, 0x7c, 0x54, 0xe6,
		0x9a, 0xc3, 0xf0, 0x4d, 0xa6, 0xb8
};

/* Certicom Certificate */
PUBLIC uint8 au8Certificate[48] __attribute__ ((section (".ro_se_cert"))) = {
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

/* Certicom Private Key */
PUBLIC uint8 au8PrivateKey[21] __attribute__ ((section (".ro_se_pvKey"))) = {
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff
};

/* Pre-configured Link Key */
PUBLIC	uint8 s_au8LnkKeyArray[16] __attribute__ ((section (".ro_se_lnkKey"))) = {
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

#else //DEVELOPMENT_CERTS

/* Certicom Development Public Key */
PUBLIC	uint8 au8CAPublicKey[] = {
		0x02, 0x00, 0xfd, 0xe8, 0xa7, 0xf3, 0xd1, 0x08,
		0x42, 0x24, 0x96, 0x2a, 0x4e, 0x7c, 0x54, 0xe6,
		0x9a, 0xc3, 0xf0, 0x4d, 0xa6, 0xb8
};

/* Certicom Certificate */
PUBLIC uint8 au8Certificate[] = {
		0x02, 0x01, 0x43, 0xba, 0xd7, 0xea, 0x50, 0x08,
		0x4e, 0x69, 0x99, 0xa7, 0x31, 0x37, 0x09, 0x7a,
		0x2f, 0x93, 0xe0, 0xab, 0xdc, 0xf9, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x54, 0x45,
		0x53, 0x54, 0x53, 0x45, 0x43, 0x41, 0x01, 0x09,
		0x10, 0x83, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/* Certicom Private Key */
PUBLIC uint8 au8PrivateKey[] =  {
		0x03, 0xcd, 0xf8, 0xa0, 0xad, 0x0b, 0x14, 0xa6,
		0xa9, 0xeb, 0x51, 0xeb, 0x92, 0xe5, 0xf2, 0x74,
		0x30, 0x09, 0x57, 0x5c, 0xb5
};

/* Pre-configured Link Key */
#ifdef ALL_ONES
PUBLIC	uint8 s_au8LnkKeyArray[16] = {
		0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
		0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11
};
#else //default NXP Pre-configured Link Key
PUBLIC	uint8 s_au8LnkKeyArray[16] = {
		0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
		0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x00
};
#endif

#endif


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_CERTIFICATES_H_*/
