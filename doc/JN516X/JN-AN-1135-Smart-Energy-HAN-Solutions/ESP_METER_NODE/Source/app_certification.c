/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_certification.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        SE Certification Functionality
 *
 * $HeadURL $
 *
 * $Revision: 8546 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-01-09 10:52:15 +0000 (Mon, 09 Jan 2012) $
 *
 * $Id: app_zcl_task.c 8546 2012-01-09 10:52:15Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */

/* Application Includes */
#include "app_certification.h"
#include "app_meter_node.h"
#include "app_zcl_task.h"
#include "zcl_options.h"
#include "app_buttons.h"
#include "pdum_gen.h"
#include "string.h"
#include "dbg.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_CERTIFICATION
#define TRACE_CERTIFICATION	FALSE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE teSeCertificationStates eSeCertificationStates = E_INIT;
uint32							u32StartTime;

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern tsSE_EspMeterDevice	sMeter;
extern tsZCL_Address		sClientAddress;
extern uint8				u8ClientKeyEstablishmentEndPoint;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: APP_vCertification
 *
 * DESCRIPTION:
 * Certification state machine
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void APP_vCertification(uint8 u8Button)
{
#ifdef CLD_PRICE
	PRIVATE char *acRateLabelBase;
	tsSE_PricePublishPriceCmdPayload sPrice;
#endif

	if (APP_E_BUTTONS_BUTTON_3 == u8Button)
	{
		eSeCertificationStates++;
	}

	switch (eSeCertificationStates)
	{
		case E_INIT:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_INIT\n");
		break;

#ifdef CLD_DRLC
		case E_DRLC_10_5_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_5_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10050001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_6_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_6_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10060001, E_SE_DRLC_IRRIGATION_PUMP_BIT, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_7_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_7_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10070001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + 60 ), 2, 1, 0);
			}
		break;

		case E_DRLC_10_7_CANCEL_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_7_CANCEL_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10070001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0);
			}
		break;

		case E_DRLC_10_7_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_7_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10070002, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + 60 ), 2, 1, 0);
			}
		break;

		case E_DRLC_10_7_CANCEL_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_7_CANCEL_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10070002, E_SE_DRLC_IRRIGATION_PUMP_BIT, 0);
			}
		break;

		case E_DRLC_10_8_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_8_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10080001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + 60 ), 10, 1, 0);
			}
		break;

		case E_DRLC_10_8_CANCEL_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_8_CANCEL_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10080001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + 60 ));
			}
		break;

		case E_DRLC_10_9_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_9_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10090001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_9_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_9_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10090002, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + (4 * 60) ), 2, 1, 0);
			}
		break;

		case E_DRLC_10_10_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_10_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10100001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_10_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_10_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10100002, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() - 10 ), 2, 1, 0);
			}
		break;

		case E_DRLC_10_11_LC_EVENT_1:
			u32StartTime = ( u32ZCL_GetUTCTime() + (4 * 60) );
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_11_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10110001, E_SE_DRLC_SMART_APPLIANCES_BIT, u32StartTime, 2, 1, 0);
			}
		break;

		case E_DRLC_10_11_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_11_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10110002, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32StartTime + 60 ), 1, 1, 0);
			}
		break;

		case E_DRLC_10_12_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_12_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10120001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + 60 ), 1, 1, 0);
			}
		break;

		case E_DRLC_10_13_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_13_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10130001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + (2 * 60) ), 1, 1, 0);
			}
		break;

		case E_DRLC_10_13_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_13_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10130002, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + (10 * 60) ), 1, 1, 0);
			}
		break;

		case E_DRLC_10_13_LC_EVENT_3:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_13_LC_EVENT_3\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10130003, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + (20 * 60) ), 1, 1, 0);
			}
		break;

		case E_DRLC_10_13_CANCEL_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_13_CANCEL_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelAllLceToBound(E_SE_DRLC_CANCEL_CONTROL_IMMEDIATE);
			}
		break;

		case E_DRLC_10_14_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_14_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10140001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_14_CANCEL_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_14_CANCEL_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10140001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0);
			}
		break;

		case E_DRLC_10_14_CANCEL_LC_EVENT_2:
			/* Test harness only - unable to cancel an event that does not exist in the local table */
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_14_CANCEL_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10140002, E_SE_DRLC_SMART_APPLIANCES_BIT, 0);
			}
		break;

		case E_DRLC_10_15_LC_EVENT_1:
			u32StartTime = u32ZCL_GetUTCTime();
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_15_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10150001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_15_CANCEL_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_15_CANCEL_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10150001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32StartTime - 10 ));
			}
		break;

		case E_DRLC_10_16_LC_EVENT_1:
			u32StartTime = u32ZCL_GetUTCTime() + 210;
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_16_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10160001, E_SE_DRLC_SMART_APPLIANCES_BIT, u32StartTime, 1, 1, 0);
			}
		break;

		case E_DRLC_10_16_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_16_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10160002, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32StartTime - 70 ), 1, 1, 0);
			}
		break;

		case E_DRLC_10_16_LC_EVENT_3:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_16_LC_EVENT_3\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10160003, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 1, 1, 0);
			}
		break;

		case E_DRLC_10_17_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_17_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10170001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 1, 1, 0);
			}
		break;

		case E_DRLC_10_17_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_17_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10170002, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + 60 ), 1, 1, 0);
			}
		break;

		case E_DRLC_10_18_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_18_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10180001, 0xF000, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_19_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_19_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10190001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 1, 0x2F, 0);
			}
		break;

		case E_DRLC_10_23_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_23_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10230001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() - (60 * 3) ), 2, 1, 0);
			}
		break;

		case E_DRLC_10_24_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_24_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x00000006, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 1500, 1, 0);
			}
		break;

		case E_DRLC_10_25_LC_WRITE_ATTR_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_LC_WRITE_ATTR_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				sMeter.sDRLCCluster.u8UtilityEnrolmentGroup = 1;
				vSendWriteIndividualAttritbutesReqToBound(E_CLD_DRLC_UTILITY_ENROLMENT_GROUP, SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL, TRUE);
			}
		break;

		case E_DRLC_10_25_LC_READ_ATTR_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_LC_READ_ATTR_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendReadIndividualAttritbutesReqToBound(E_CLD_DRLC_UTILITY_ENROLMENT_GROUP, SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL, TRUE);
			}
		break;

		case E_DRLC_10_25_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10250001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 2, 1, 0);
			}
		break;

		case E_DRLC_10_25_CANCEL_LC_EVENT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_CANCEL_LC_EVENT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10250001, E_SE_DRLC_SMART_APPLIANCES_BIT, 0);
			}
		break;

		case E_DRLC_10_25_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10250002, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 2, 1, 0x01);
			}
		break;

		case E_DRLC_10_25_CANCEL_LC_EVENT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_CANCEL_LC_EVENT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelLceToBound(0x10250002, E_SE_DRLC_SMART_APPLIANCES_BIT, 0);
			}
		break;

		case E_DRLC_10_25_LC_WRITE_ATTR_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_LC_WRITE_ATTR_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				sMeter.sDRLCCluster.u8UtilityEnrolmentGroup = 2;
				vSendWriteIndividualAttritbutesReqToBound(E_CLD_DRLC_UTILITY_ENROLMENT_GROUP, SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL, TRUE);
			}
		break;

		case E_DRLC_10_25_LC_READ_ATTR_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_LC_READ_ATTR_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendReadIndividualAttritbutesReqToBound(E_CLD_DRLC_UTILITY_ENROLMENT_GROUP, SE_CLUSTER_ID_DEMAND_RESPONSE_AND_LOAD_CONTROL, TRUE);
			}
		break;

		case E_DRLC_10_25_LC_EVENT_3:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_25_LC_EVENT_3\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10250003, E_SE_DRLC_SMART_APPLIANCES_BIT, 0, 1, 1, 0xAA);
			}
		break;

		case E_DRLC_10_26_LC_INIT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_26_LC_INIT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10260001, E_SE_DRLC_SMART_APPLIANCES_BIT, ( u32ZCL_GetUTCTime() + (60 * 60 * 24) ), 10, 1, 0);
			}
		break;

		case E_DRLC_10_26_LC_INIT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_DRLC_10_26_LC_INIT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewLceToBound(0x10260002, E_SE_DRLC_SMART_APPLIANCES_BIT, u32ZCL_GetUTCTime() + (60 * 60 * 24 * 3), 10, 1, 0);
			}
		break;
#endif

#ifdef CLD_SIMPLE_METERING
		case E_SM_11_31_WRITE_ATTR_INIT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_SM_11_31_WRITE_ATTR_INIT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				sMeter.sSimpleMeteringCluster.u8MeterStatus = 0x00;
			}
		break;

		case E_SM_11_31_WRITE_ATTR_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_SM_11_31_WRITE_ATTR_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				sMeter.sSimpleMeteringCluster.u8MeterStatus = 0x01;
			}
		break;
#endif

#ifdef CLD_PRICE
		case E_PRICE_12_3_PUBLISH_PRICE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_3_PUBLISH_PRICE\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "BASE";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12030001;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x10;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x40;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 0x0FFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0x12;
				sPrice.u32GenerationPrice = 0x00000015;
				sPrice.u8GenerationPriceRatio = 0x25;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_5_PRICE_INIT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_5_PRICE_INIT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "BASE";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12050001;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x10;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x40;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 0xFFFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0xFF;
				sPrice.u32GenerationPrice = 0xFFFFFFFF;
				sPrice.u8GenerationPriceRatio = 0xFF;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_7_PRICE_INIT_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_7_PRICE_INIT_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "BASE";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12070001;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x11;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x21;
				sPrice.u32StartTime = u32ZCL_GetUTCTime() + (60 * 60 * 24);
				sPrice.u16DurationInMinutes = 0x003C;
				sPrice.u32Price = 0x00000018;
				sPrice.u8PriceRatio = 0xFF;
				sPrice.u32GenerationPrice = 0xFFFFFFFF;
				sPrice.u8GenerationPriceRatio = 0xFF;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_7_PRICE_INIT_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_7_PRICE_INIT_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "TAR1";
				sPrice.u32ProviderId =  0x00000002;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12070002;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x12;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x22;
				sPrice.u32StartTime = u32ZCL_GetUTCTime() + (60 * 60 * 24 * 3);
				sPrice.u16DurationInMinutes = 0xFFFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0xFF;
				sPrice.u32GenerationPrice = 0xFFFFFFFF;
				sPrice.u8GenerationPriceRatio = 0xFF;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_8_PUBLISH_PRICE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_8_PUBLISH_PRICE_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "Tier5";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12080001;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x15;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x55;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 0x0FFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0x12;
				sPrice.u32GenerationPrice = 0x00000015;
				sPrice.u8GenerationPriceRatio = 0x25;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_8_PUBLISH_PRICE_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_8_PUBLISH_PRICE_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "Tier5";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12080002;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x12;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x52;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 0x0FFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0x12;
				sPrice.u32GenerationPrice = 0x00000015;
				sPrice.u8GenerationPriceRatio = 0x25;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_8_PUBLISH_PRICE_3:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_8_PUBLISH_PRICE_3\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "Tier5";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12080003;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x13;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x53;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 0x0FFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0x12;
				sPrice.u32GenerationPrice = 0x00000015;
				sPrice.u8GenerationPriceRatio = 0x25;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_8_PUBLISH_PRICE_4:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_8_PUBLISH_PRICE_4\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "Tier5";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12080004;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x14;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x54;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 0x0FFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0x12;
				sPrice.u32GenerationPrice = 0x00000015;
				sPrice.u8GenerationPriceRatio = 0x25;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_8_PUBLISH_PRICE_5:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_8_PUBLISH_PRICE_5\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "Tier5";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12080005;
				sPrice.u8UnitOfMeasure = 0x00;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x15;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x55;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 0x0FFF;
				sPrice.u32Price = 0x00000012;
				sPrice.u8PriceRatio = 0x12;
				sPrice.u32GenerationPrice = 0x00000015;
				sPrice.u8GenerationPriceRatio = 0x25;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_18_PUBLISH_PRICE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_18_PUBLISH_PRICE_1\n");

			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				/* Populate the command payload structure */
				acRateLabelBase = "BASE";
				sPrice.u32ProviderId =  0x00000001;
				sPrice.sRateLabel.pu8Data = (uint8 *)acRateLabelBase;
				sPrice.sRateLabel.u8Length = strlen((char *)acRateLabelBase);
				sPrice.sRateLabel.u8MaxLength = strlen((char *)acRateLabelBase);

				sPrice.u32IssuerEventId = 0x12180001;
				sPrice.u8UnitOfMeasure = 0;
				sPrice.u16Currency = 840;
				sPrice.u8PriceTrailingDigitAndPriceTier = 0x11;
				sPrice.u8NumberOfPriceTiersAndRegisterTiers = 0x21;
				sPrice.u32StartTime = 0;
				sPrice.u16DurationInMinutes = 60;
				sPrice.u32Price = 0x00000018;
				sPrice.u8PriceRatio = 0xFF;
				sPrice.u32GenerationPrice = 0xFFFFFFFF;
				sPrice.u8GenerationPriceRatio = 0xFF;
				sPrice.u32AlternateCostDelivered = 0xFFFFFFFF;
				sPrice.u8AlternateCostUnit = 0xFF;
				sPrice.u8AlternateCostTrailingDigit = 0xFF;
				sPrice.u8NumberOfBlockThresholds = 0xFF;
				sPrice.u8PriceControl = 1;
				APP_bAddPrice(sPrice, ESP_METER_LOCAL_EP);
			}
		break;

		case E_PRICE_12_21_READ_ATTR_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_21_READ_ATTR_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendReadIndividualAttritbutesReqToBound(0x0000, SE_CLUSTER_ID_PRICE, TRUE);
			}
		break;

		case E_PRICE_12_21_READ_ATTR_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_21_READ_ATTR_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendReadIndividualAttritbutesReqToBound(0x0001, SE_CLUSTER_ID_PRICE, TRUE);
			}
		break;

		case E_PRICE_12_21_READ_ATTR_3:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_PRICE_12_21_READ_ATTR_3\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendReadIndividualAttritbutesReqToBound(E_CLD_P_ATTR_COMMODITY_TYPE, SE_CLUSTER_ID_PRICE, TRUE);
			}
		break;
#endif
#ifdef CLD_MC
		case E_MC_13_3_DISPLAY_MESSAGE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_3_DISPLAY_MESSAGE_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x00000001, 0, 0, 0xFFFF, "trhED Message");
			}
		break;

		case E_MC_13_3_DISPLAY_MESSAGE_3:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_3_DISPLAY_MESSAGE_3\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x00000002, 1, (u32ZCL_GetUTCTime() + 60), 0x0001, "trhED Message, display for 10 minutes");
			}
		break;

		case E_MC_13_3_DISPLAY_MESSAGE_5:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_3_DISPLAY_MESSAGE_5\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x00000003, 0, (u32ZCL_GetUTCTime() - (5 * 60)), 0x0001, "trhED Message");
			}
		break;

		case E_MC_13_3_DISPLAY_MESSAGE_7:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_3_DISPLAY_MESSAGE_7\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x00000006, 1, 0, 0x0001, "");
			}
		break;

		case E_MC_13_3_DISPLAY_MESSAGE_9:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_3_DISPLAY_MESSAGE_9\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x00000007, 1, 0, 0x0001, "AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA");
			}
		break;

		case E_MC_13_4_DISPLAY_MESSAGE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_4_DISPLAY_MESSAGE_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x13040001, 0x80, 0, 0x000F, "Confirm Message");
			}
		break;

		case E_MC_13_4_DISPLAY_MESSAGE_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_4_DISPLAY_MESSAGE_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x13040002, 0x80, 0, 0x0002, "Don't Confirm Message");
			}
		break;

		case E_MC_13_5_DISPLAY_MESSAGE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_5_DISPLAY_MESSAGE_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x13050001, 0x00, 0, 0x000F, "trhED Message to be cancelled");
			}
		break;

		case E_MC_13_5_CANCEL_MESSAGE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_5_CANCEL_MESSAGE_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelMessageToBound(0x13050001, 0x00);
			}
		break;

		case E_MC_13_5_DISPLAY_MESSAGE_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_5_DISPLAY_MESSAGE_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x13050002, 0x00, 0, 0x000F, "trhED Message Cancel to be confirmed");
			}
		break;

		case E_MC_13_5_CANCEL_MESSAGE_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_5_CANCEL_MESSAGE_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelMessageToBound(0x13050002, 0x80);
			}
		break;

		case E_MC_13_6_DISPLAY_MESSAGE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_6_DISPLAY_MESSAGE_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x13060001, 0x00, 0, 0x0005, "trhED Get Last Message test");
			}
		break;

		case E_MC_13_6_DISPLAY_MESSAGE_2:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_6_DISPLAY_MESSAGE_2\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x13060002, 0x00, 0, 0x000F, "trhED Message to be cancelled");
			}
		break;

		case E_MC_13_6_CANCEL_MESSAGE_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_6_CANCEL_MESSAGE_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendCancelMessageToBound(0x13060002, 0x00);
			}
		break;

		case E_MC_13_6_DISPLAY_MESSAGE_3:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_MC_13_6_DISPLAY_MESSAGE_3\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendNewMessageToBound(0x13060003, 0x01, 0, 0x0002, "trhIPD Get Last Message test");
			}
		break;
#endif

		case E_KEC_15_30_MATCH_DESC_REQ:
		{
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_KEC_15_30_MATCH_DESC_REQ\n");
			uint8 u8TransactionSequenceNumber;
			ZPS_tuAddress uDestinationAddress;
			ZPS_tsAplZdpMatchDescReq matchDesc;

			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				uint16 pu16InClusterList[1];
				uint16 pu16OutClusterList[0] = {};
				pu16InClusterList[0] = 0xFFFF;

				/* Set Destination/Interest address for the requests */
				uDestinationAddress.u16Addr = ZPS_u16AplZdoLookupAddr(1);//0xFFFD;								// Broadcast to 'Rx On When Idle' devices
				matchDesc.u16NwkAddrOfInterest = ZPS_u16AplZdoLookupAddr(1);//0xFFFD;							// Broadcast to 'Rx On When Idle' devices

				pu16InClusterList[0] = SE_CLUSTER_ID_KEY_ESTABLISHMENT;

				matchDesc.u16ProfileId			= SE_PROFILE_ID;
				matchDesc.u8NumInClusters		= 1;
				matchDesc.pu16InClusterList		= pu16InClusterList;
				matchDesc.u8NumOutClusters		= 0;
				matchDesc.pu16OutClusterList	= pu16OutClusterList;

				PDUM_thAPduInstance hAPduInst = PDUM_hAPduAllocateAPduInstance(apduZDP);

				if (hAPduInst == PDUM_INVALID_HANDLE)
				{
					DBG_vPrintf(TRACE_CERTIFICATION, "Allocate PDU ERR:\n");
					return;
				}

				ZPS_teStatus eStatus = ZPS_eAplZdpMatchDescRequest(hAPduInst, uDestinationAddress, FALSE, &u8TransactionSequenceNumber, &matchDesc);

				if (ZPS_E_SUCCESS == eStatus)
				{
					DBG_vPrintf(TRACE_CERTIFICATION, "Sent Key Establishment Match Descriptor Req\n");
				}
				else
				{
					DBG_vPrintf(TRACE_CERTIFICATION, "Match ERR: 0x%x", eStatus);
				}
			}
		}
		break;

		case E_KEC_15_30_INIT_KEY_ESTABLISHMENT:
		{
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_KEC_15_30_INIT_KEY_ESTABLISHMENT\n");
			uint8 u8TransactionSequenceNumber;
			teSE_KECStatus eStatus;

			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				eStatus = eSE_KECInitiateKeyEstablishment(ESP_METER_LOCAL_EP, u8ClientKeyEstablishmentEndPoint, &sClientAddress, &u8TransactionSequenceNumber);

				if(eStatus != E_ZCL_SUCCESS)
				{
					DBG_vPrintf(TRACE_CERTIFICATION, "Key Establishment Req: 0x%x, EP: %i, Address Type: %i, Short Address: 0x%x", eStatus, u8ClientKeyEstablishmentEndPoint, sClientAddress.eAddressMode, sClientAddress.uAddress.u16DestinationAddress);
				}
			}
		}
		break;

		case E_KEC_15_30_READ_ATTR_1:
			DBG_vPrintf(TRACE_CERTIFICATION, "Certification State: E_KEC_15_30_READ_ATTR_1\n");
			if (APP_E_BUTTONS_BUTTON_4 == u8Button)
			{
				vSendReadIndividualAttritbutesReqToBound(E_CLD_BAS_ATTR_ID_ZCL_VERSION, GENERAL_CLUSTER_ID_BASIC, FALSE);
			}
		break;

		default:
			DBG_vPrintf(TRACE_CERTIFICATION, "Unknown certification state, switching to E_INIT\n");
			eSeCertificationStates = E_INIT;
		break;
	}
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
