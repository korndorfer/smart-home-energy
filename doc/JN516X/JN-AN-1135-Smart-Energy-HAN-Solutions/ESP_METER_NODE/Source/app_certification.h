/*****************************************************************************
 *
 * MODULE:             JN-AN-1135
 *
 * COMPONENT:          app_certification.h
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        SE Certification Functionality
 *
 * $HeadURL: $
 *
 * $Revision: 8954 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-04-16 13:01:29 +0100 (Mon, 16 Apr 2012) $
 *
 * $Id: app_adc.h 8954 2012-04-16 12:01:29Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_CERTIFICATION_H_
#define APP_CERTIFICATION_H_

#include "zcl_options.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

typedef enum {
	E_INIT,
#ifdef CLD_DRLC
	E_DRLC_10_5_LC_EVENT_1,

	E_DRLC_10_6_LC_EVENT_1,

	E_DRLC_10_7_LC_EVENT_1,
	E_DRLC_10_7_CANCEL_LC_EVENT_1,
	E_DRLC_10_7_LC_EVENT_2,
	E_DRLC_10_7_CANCEL_LC_EVENT_2,

	E_DRLC_10_8_LC_EVENT_1,
	E_DRLC_10_8_CANCEL_LC_EVENT_1,

	E_DRLC_10_9_LC_EVENT_1,
	E_DRLC_10_9_LC_EVENT_2,

	E_DRLC_10_10_LC_EVENT_1,
	E_DRLC_10_10_LC_EVENT_2,

	E_DRLC_10_11_LC_EVENT_1,
	E_DRLC_10_11_LC_EVENT_2,

	E_DRLC_10_12_LC_EVENT_1,

	E_DRLC_10_13_LC_EVENT_1,
	E_DRLC_10_13_LC_EVENT_2,
	E_DRLC_10_13_LC_EVENT_3,
	E_DRLC_10_13_CANCEL_LC_EVENT_1,

	E_DRLC_10_14_LC_EVENT_1,
	E_DRLC_10_14_CANCEL_LC_EVENT_1,
	E_DRLC_10_14_CANCEL_LC_EVENT_2,

	E_DRLC_10_15_LC_EVENT_1,
	E_DRLC_10_15_CANCEL_LC_EVENT_1,

	E_DRLC_10_16_LC_EVENT_1,
	E_DRLC_10_16_LC_EVENT_2,
	E_DRLC_10_16_LC_EVENT_3,

	E_DRLC_10_17_LC_EVENT_1,
	E_DRLC_10_17_LC_EVENT_2,

	E_DRLC_10_18_LC_EVENT_1,

	E_DRLC_10_19_LC_EVENT_1,

	E_DRLC_10_23_LC_EVENT_1,

	E_DRLC_10_24_LC_EVENT_1,

	E_DRLC_10_25_LC_WRITE_ATTR_1,
	E_DRLC_10_25_LC_READ_ATTR_1,
	E_DRLC_10_25_LC_EVENT_1,
	E_DRLC_10_25_CANCEL_LC_EVENT_1,
	E_DRLC_10_25_LC_EVENT_2,
	E_DRLC_10_25_CANCEL_LC_EVENT_2,
	E_DRLC_10_25_LC_WRITE_ATTR_2,
	E_DRLC_10_25_LC_READ_ATTR_2,
	E_DRLC_10_25_LC_EVENT_3,

	E_DRLC_10_26_LC_INIT_1,
	E_DRLC_10_26_LC_INIT_2,
#endif
#ifdef CLD_SIMPLE_METERING

	E_SM_11_31_WRITE_ATTR_INIT_1,
	E_SM_11_31_WRITE_ATTR_1,
#endif
#ifdef CLD_PRICE

	E_PRICE_12_3_PUBLISH_PRICE_1,
	E_PRICE_12_5_PRICE_INIT_1,
	E_PRICE_12_7_PRICE_INIT_1,
	E_PRICE_12_7_PRICE_INIT_2,
	E_PRICE_12_8_PUBLISH_PRICE_1,
	E_PRICE_12_8_PUBLISH_PRICE_2,
	E_PRICE_12_8_PUBLISH_PRICE_3,
	E_PRICE_12_8_PUBLISH_PRICE_4,
	E_PRICE_12_8_PUBLISH_PRICE_5,
	E_PRICE_12_18_PUBLISH_PRICE_1,
	E_PRICE_12_21_READ_ATTR_1,
	E_PRICE_12_21_READ_ATTR_2,
	E_PRICE_12_21_READ_ATTR_3,
#endif
#ifdef CLD_MC

	E_MC_13_3_DISPLAY_MESSAGE_1,
	E_MC_13_3_DISPLAY_MESSAGE_3,
	E_MC_13_3_DISPLAY_MESSAGE_5,
	E_MC_13_3_DISPLAY_MESSAGE_7,
	E_MC_13_3_DISPLAY_MESSAGE_9,

	E_MC_13_4_DISPLAY_MESSAGE_1,
	E_MC_13_4_DISPLAY_MESSAGE_2,

	E_MC_13_5_DISPLAY_MESSAGE_1,
	E_MC_13_5_CANCEL_MESSAGE_1,
	E_MC_13_5_DISPLAY_MESSAGE_2,
	E_MC_13_5_CANCEL_MESSAGE_2,

	E_MC_13_6_DISPLAY_MESSAGE_1,
	E_MC_13_6_DISPLAY_MESSAGE_2,
	E_MC_13_6_CANCEL_MESSAGE_1,
	E_MC_13_6_DISPLAY_MESSAGE_3,

	/* Only required if the device under test is a client */
	/*
	E_MC_13_8_READ_ATTR_1,
	*/
#endif
	E_KEC_15_30_MATCH_DESC_REQ,
	E_KEC_15_30_INIT_KEY_ESTABLISHMENT,
	E_KEC_15_30_READ_ATTR_1,
} teSeCertificationStates;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void APP_vCertification(uint8 u8Button);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_CERTIFICATION_H_*/
