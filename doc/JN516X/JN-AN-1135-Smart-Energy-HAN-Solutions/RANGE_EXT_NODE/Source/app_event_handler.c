/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (METER)
 *
 * COMPONENT:          app_event_handler.c
 *
 * AUTHOR:             TMudr
 *
 * DESCRIPTION:        Event Handler
 *
 * $HeadURL $
 *
 * $Revision: 8437 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2011-12-05 14:42:12 +0000 (Mon, 05 Dec 2011) $
 *
 * $Id: app_ipd_node.c 8437 2011-12-05 14:42:12Z nxp33194 $
 *
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

/* Stack Includes */
#include <jendefs.h>
#include "app_smartenergy_demo.h"
#include "app_zbp_utilities.h"
#include "app_timer_driver.h"
#include "app_range_ext_node.h"
#include "app_zcl_task.h"
#include "app_buttons.h"
#include "os_gen.h"
#include "dbg.h"
#include "pdm.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_EVENT_HANDLER
#define TRACE_EVENT_HANDLER	TRUE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void vHandleTimerExpiries(APP_tsEvent sAppEvent);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

#ifdef APPLICATION_REJOIN_ENABLED
PRIVATE uint8	u8FailedRouteDiscoveries = 0;
#endif

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleRunningEvent
 *
 * DESCRIPTION:
 * Forwards any event to the relevant event handler
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PUBLIC void vHandleRunningEvent(ZPS_tsAfEvent sStackEvent, APP_tsEvent sAppEvent)
{
	switch (sStackEvent.eType)
	{
		case ZPS_EVENT_NWK_ROUTE_DISCOVERY_CONFIRM:
			DBG_vPrintf(TRACE_EVENT_HANDLER, "Route Discovery Confirm: 0x%02x\n", sStackEvent.uEvent.sNwkRouteDiscoveryConfirmEvent.u8Status);
			/* No route available */
			if(ZPS_NWK_ENUM_ROUTE_DISCOVERY_FAILED == sStackEvent.uEvent.sNwkRouteDiscoveryConfirmEvent.u8Status)
			{
				u8FailedRouteDiscoveries++;
				if(u8FailedRouteDiscoveries >= MAX_ROUTE_DISCOVERY_FAILURES)
				{
					u8FailedRouteDiscoveries = 0;
					OS_eActivateTask(APP_InitiateRejoin);
				}
			}
			else if (ZPS_NWK_ENUM_SUCCESS == sStackEvent.uEvent.sNwkRouteDiscoveryConfirmEvent.u8NwkStatus)
			{
				u8FailedRouteDiscoveries = 0;
			}
		break;

		case ZPS_EVENT_APS_ZDP_REQUEST_RESPONSE:
			if (NWK_RESP == sStackEvent.uEvent.sApsZdpEvent.u16ClusterId)
			{
				if(!sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u8Status)
				{
					ZPS_tsNwkNib * thisNib = ZPS_psNwkNibGetHandle(ZPS_pvAplZdoGetNwkHandle());
					uint8 i;

					DBG_vPrintf(TRACE_EVENT_HANDLER, "NWK Lookup Resp: 0x%04x 0x%016llx\n",sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u16NwkAddrRemoteDev, sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u64IeeeAddrRemoteDev);

					for(i = 0 ; i < thisNib->sTblSize.u16NtActv ; i++)
					{
						if (( sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u16NwkAddrRemoteDev == thisNib->sTbl.psNtActv[i].u16NwkAddr ) &&
							( ZPS_NWK_NT_AP_RELATIONSHIP_CHILD == thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u2Relationship ))
						{
							/* Remove child from the neighbour table */
							thisNib->sTbl.psNtActv[i].u16NwkAddr = 0xFFFE;
							thisNib->sTbl.psNtActv[i].u64ExtAddr = 0x0000000000000000ull;
							thisNib->sTbl.psNtActv[i].u8LinkQuality = 0x00;
							thisNib->sTbl.psNtActv[i].u8TxFailed = 0x00;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1Authenticated = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1DeviceType = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1ExpectAnnc = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1LinkStatusDone = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1PowerSource = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1RxOnWhenIdle = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1SecurityMode = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u1Used = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u2Relationship = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u3Age = 0;
							thisNib->sTbl.psNtActv[i].uAncAttrs.bfBitfields.u3OutgoingCost = 0;
						}
					}
				}
				else
				{
					DBG_vPrintf(TRACE_EVENT_HANDLER, "Lookup Resp - Error: 0x%02x", sStackEvent.uEvent.sApsZdpEvent.uZdpData.sNwkAddrRsp.u8Status);
				}
			}
		break;

		default:
		break;
	}

	if ( sAppEvent.eType != APP_E_EVENT_NONE)									// If the task was called due to an event
	{
		if (APP_E_EVENT_BUTTON_UP == sAppEvent.eType)							// Button release detected
		{
			if (APP_E_BUTTONS_BUTTON_1 == sAppEvent.sButton.u8Button)			// Instantaneous increase button released
			{
				// Do nothing
			}
			else if (APP_E_BUTTONS_BUTTON_2 == sAppEvent.sButton.u8Button)		// Instantaneous decrease button released
			{
				// Do nothing
			}
		}
	}
	else
	{
		vHandleTimerExpiries(sAppEvent);
	}
}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vHandleTimerExpiries
 *
 * DESCRIPTION:
 * Timer expiry and read complete events
 *
 *
 * PARAMETERS: Name         RW  Usage
 *             sAppEvent  R   Contains details of the app event
 *
 * RETURNS:
 * Sequence number
 *
 ****************************************************************************/
PRIVATE void vHandleTimerExpiries(APP_tsEvent sAppEvent)
{
	if (OS_E_SWTIMER_RUNNING != OS_eGetSWTimerStatus(APP_RouteRequestTimer))
	{
		/* Send out a route request to the coordinator */
		ZPS_teStatus eStatus = ZPS_eAplZdoRouteRequest(
				0x0000,
				0x00);

		DBG_vPrintf(TRACE_EVENT_HANDLER, "Route discovery initiated: 0x%02x\n", eStatus);

		/* Repeat in 60s */
		OS_eStartSWTimer(APP_RouteRequestTimer, APP_TIME_MS(60000), NULL);
	}
}

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
