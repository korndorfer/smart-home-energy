/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (IPD)
 *
 * COMPONENT:          app_sleep_functions.c
 *
 * AUTHOR:             jpenn
 *
 * DESCRIPTION:        Application Sleep Handler Functions
 *
 * $HeadURL $
 *
 * $Revision: 9281 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-08 15:13:02 +0100 (Fri, 08 Jun 2012) $
 *
 * $Id: app_sleep_functions.c 9281 2012-06-08 14:13:02Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "pdum_apl.h"
#include "pdum_gen.h"
#include "pdm.h"
#include "pwrm.h"
#include "dbg.h"
#include "app_sleep_functions.h"
#include "app_event_handler.h"
#include "zps_apl_zdp.h"
#include "app_adc.h"
#include "zcl.h"
#include "app_ipd_node.h"
#include "app_zcl_task.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_SLEEP
#define TRACE_SLEEP TRUE
#endif

#ifndef TRACE_POLL
#define TRACE_POLL TRUE
#endif

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE	void	vRestoreTime(void);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE	pwrm_tsWakeTimerEvent	sWake;
PRIVATE bool_t					bWaitingSleep = FALSE;

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

extern	teRunningStates	eRunningState;
extern	tsDevice 		s_sDevice;
extern	bool_t			bRejoining;
extern	bool_t			bDataPending;
extern	uint8			u8NetworkDiscoveryAttempts;
extern	uint32       	u32FastPollEndTime;

/****************************************************************************/
/***		Tasks														  ***/
/****************************************************************************/
/****************************************************************************
 *
 * NAME: APP_PollTask
 *
 * DESCRIPTION:
 * Polls the parent for data
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_PollTask)
{
	if (OS_E_SWTIMER_EXPIRED == OS_eGetSWTimerStatus(APP_PollTimer))
	{
		/* if an OTA download is in progress */
		if (E_CLD_OTA_STATUS_DL_IN_PROGRESS == u8QueryOtaImageUpgradeStatus())
		{
			ZPS_teStatus eStatus = ZPS_eAplZdoPoll();
			OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(100), NULL);
			DBG_vPrintf(TRACE_POLL,"Polling 100ms: 0x%x\n", eStatus);
		}
		/* if waiting to sleep and there is no data pending from the parent */
		else if (bGetAppSleep() && !bDataPending)
		{
			DBG_vPrintf(TRACE_POLL, "Polling: No Data Pending\n");
			vClearExpiredFlag(APP_PollTimer);
		}
		/* else continue polling */
		else
		{
			ZPS_teStatus eStatus = ZPS_eAplZdoPoll();

			if(s_sDevice.bKeyEstComplete)
			{
				OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(300), NULL);
				DBG_vPrintf(TRACE_POLL,"Polling 300ms: 0x%x\n", eStatus);
			}
			else
			{
				OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(1000), NULL);
				DBG_vPrintf(TRACE_POLL,"Polling 1s: 0x%x\n", eStatus);
			}
		}
	}
}


/****************************************************************************
 *
 * NAME: APP_WakeUpTask
 *
 * DESCRIPTION:
 * Wakeup initialisation task
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_TASK(APP_WakeUpTask)
{
	if (bGetAppSleep())															// If the IPD did sleep...
	{
		vScheduleSleep(SLEEP_PERIOD);											// Schedule the next wake up event
		vSetAppSleep(FALSE);													// clear down the waiting to sleep flag

		vRestoreTime();															// Update the UTC based on how long we have been asleep

		if ( bRejoining )														// If the application was attempting a rejoin before it went to sleep...
		{
			s_sDevice.eState = E_START_NETWORK;
			u8NetworkDiscoveryAttempts = 0;
			OS_eActivateTask(APP_IPDTask);
		}
		else
		{
			OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(1), NULL);				// ... poll for any data that was sent to us while we were asleep
			eRunningState = E_INIT;
			OS_eActivateTask(APP_IPDTask);
		}
	}
#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
	else if(u32FastPollEndTime >= u32ZCL_GetUTCTime())
	{
		return;
	}
#endif
	else
	{
		if ( !bRejoining )														// If the application wasn't attempting a rejoin before it went to sleep...
		{
			vScheduleSleep(SLEEP_PERIOD);										// Schedule the next wake up event
			vConfigureSleep();													// Data cycle complete, initiate sleep
			OS_eStartSWTimer(APP_PollTimer, APP_TIME_MS(1), NULL);
		}
	}
	DBG_vPrintf(TRACE_SLEEP, "WakeCallBack Complete\n");
}


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vConfigureSleep
 *
 * DESCRIPTION:
 * Configure the device for sleep
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vConfigureSleep(void)
{
	DBG_vPrintf(TRACE_SLEEP, "vConfigureSleep %d, %d\n", bRejoining, s_sDevice.eState);

	vSetAppSleep(TRUE);
	vStopSWTimers();
}


/****************************************************************************
 *
 * NAME: vScheduleSleep
 *
 * DESCRIPTION:
 * Schedule a wake event
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vScheduleSleep(uint32 u32SleepPeriod)
{
	int i = PWRM_eScheduleActivity(&sWake, u32SleepPeriod , vWakeCallBack);		// Schedule the next sleep point
	DBG_vPrintf(TRACE_SLEEP, "Scheduling sleep point, status = %d\n", i);
}


/****************************************************************************
 *
 * NAME: vSetAppSleep
 *
 * DESCRIPTION:
 * set the waiting for sleep flag
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vSetAppSleep (bool bSleep)
{
	bWaitingSleep = bSleep;
}


/****************************************************************************
 *
 * NAME: bGetAppSleep
 *
 * DESCRIPTION:
 * Returns the waiting for sleep flag
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC bool bGetAppSleep (void)
{
	return bWaitingSleep;
}


/****************************************************************************
 *
 * NAME: vWakeCallBack
 *
 * DESCRIPTION:
 * Passed to the schedule activity, and then called by the PWRM on wake
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vWakeCallBack(void)
{
	// Cannot schedule the next wake event until the wake up callback
	// function has completed. Instead, activate a high priority task
	// so that it runs immediately after the callback
	OS_eActivateTask(APP_WakeUpTask);
}


/****************************************************************************
 *
 * NAME: vStopSWTimers
 *
 * DESCRIPTION:
 * Stops any timers ready for sleep
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PUBLIC void vStopSWTimers (void)
{
	if (OS_eGetSWTimerStatus(APP_MsTimer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_MsTimer);
	}
	if (OS_eGetSWTimerStatus(APP_ZclTimer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_ZclTimer);
	}
	if (OS_eGetSWTimerStatus(APP_PriceTimer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_PriceTimer);
	}
	if (OS_eGetSWTimerStatus(APP_MsgTimer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_MsgTimer);
	}
	if (OS_eGetSWTimerStatus(APP_tmrModeButton) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_tmrModeButton);
	}
	if (OS_eGetSWTimerStatus(APP_LED_Timer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_LED_Timer);
	}
	/* Don't stop timer when waiting for KEC */
	if(s_sDevice.eState != E_STATE_WAIT_KEY_ESTABLISHMENT)
	{
		if (OS_eGetSWTimerStatus(APP_RestartTimer) != OS_E_SWTIMER_STOPPED)
		{
			OS_eStopSWTimer(APP_RestartTimer);
		}
	}
	if (OS_eGetSWTimerStatus(APP_UTC_Timer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_UTC_Timer);
	}
	if (OS_eGetSWTimerStatus(APP_Meter_Timer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_Meter_Timer);
	}
	if (OS_eGetSWTimerStatus(APP_ButtonHoldTimer) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_ButtonHoldTimer);
	}
	if (OS_eGetSWTimerStatus(APP_tmrResetButton) != OS_E_SWTIMER_STOPPED)
	{
		OS_eStopSWTimer(APP_tmrResetButton);
	}

}


/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vRestoreTime
 *
 * DESCRIPTION:
 * Restore the UTC based on how long we have been asleep
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
void vRestoreTime(void)
{
	if(bZCL_GetTimeHasBeenSynchronised())
	{
		uint32 u32TempTime = u32ZCL_GetUTCTime();
		DBG_vPrintf(TRACE_SLEEP, "UTC before: %d\r\n", u32TempTime);

		// Increase the UTC by the amount of time we were asleep,
		// subtract one second as we are about to activate the ZCL and increment the UTC anyway,
		// Every other wakeup, increase the UTC by one second to account for the non-integer sleep period
		u32TempTime = u32TempTime + SLEEP_PERIOD_SECONDS - 1;

		DBG_vPrintf(TRACE_SLEEP, "UTC after: %d\r\n", u32TempTime);
		vZCL_SetUTCTime(u32TempTime);
	}
    else
    {
       DBG_vPrintf(TRACE_SLEEP, "UTC not syncrhonised yet\n");
    }
	OS_eStartSWTimer(APP_ZclTimer, APP_TIME_MS(1), NULL);
}


/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
