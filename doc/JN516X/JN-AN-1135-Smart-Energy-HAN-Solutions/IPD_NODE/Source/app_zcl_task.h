/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (IPD)
 *
 * COMPONENT:          app_zcl_task.h
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        ZCL Handler Header
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/IPD_NODE/Source/app_zcl_task.h $
 *
 * $Revision: 9281 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-06-08 15:13:02 +0100 (Fri, 08 Jun 2012) $
 *
 * $Id: app_zcl_task.h 9281 2012-06-08 14:13:02Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_ZCL_TASK_H
#define APP_ZCL_TASK_H

#include <jendefs.h>
#include "se.h"
#include "ipd.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void	APP_ZCL_vInitialise(void);
PUBLIC void	APP_ZCL_GetIPDData(tsSE_IPDDevice **psSE_IPDDevice);
PUBLIC void	APP_ZCL_RefreshIPDData(void);

#ifdef CLD_TIME
PUBLIC void vRequestTime(void);
#endif

#ifdef CLD_PRICE
PUBLIC void	APP_vGetScheduledPrices(void);
PUBLIC void	APP_vGetCurrentPrice(void);
#endif

#ifdef CLD_DRLC
PUBLIC void APP_vGetScheduledEvents(void);
#endif

#ifdef CLD_MC
PUBLIC void APP_vStartGetMessage(void);
#endif

#ifdef CLD_SM_SUPPORT_GET_PROFILE
PUBLIC void APP_vHandleGetProfileRequest(void);
#endif

#ifdef CLD_SM_SUPPORT_FAST_POLL_MODE
PUBLIC void APP_vHandleFastPollModeRequest(void);
#endif

PUBLIC void vLockZCLMutex(void);
PUBLIC void vUnlockZCLMutex(void);
PUBLIC void vClearExpiredFlag(OS_thSWTimer hSWTimer);
PUBLIC teZCL_Status App_QueryNextImageRequest( void );
PUBLIC uint8 u8QueryOtaImageUpgradeStatus(void);

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /* APP_ZCL_TASK_H */
