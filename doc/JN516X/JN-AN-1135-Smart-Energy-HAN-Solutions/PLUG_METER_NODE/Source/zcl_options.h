/*****************************************************************************
 *
 * MODULE:             ZCL Options
 *
 * COMPONENT:          zcl_options.h
 *
 * AUTHOR:             Lee Mitchell
 *
 * DESCRIPTION:        Options Header for ZigBee Cluster Library functions
 *
 * $HeadURL: http://svn/apps/Application_Notes/JN-AN-1135-ZigBee-Pro-SE-Home-Energy-Monitor/Trunk/METER_NODE/Source/zcl_options.h $
 *
 * $Revision: 6110 $
 *
 * $LastChangedBy: tchia $
 *
 * $LastChangedDate: 2010-05-24 10:50:22 +0100 (Mon, 24 May 2010) $
 *
 * $Id: zcl_options.h 6110 2010-05-24 09:50:22Z tchia $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef ZCL_OPTIONS_H
#define ZCL_OPTIONS_H

#include <jendefs.h>

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#define ZCL_ATTRIBUTE_READ_SERVER_SUPPORTED
#define ZCL_ATTRIBUTE_READ_CLIENT_SUPPORTED
#define ZCL_ATTRIBUTE_WRITE_SERVER_SUPPORTED
#define ZCL_ATTRIBUTE_WRITE_CLIENT_SUPPORTED

/* Sets the number of endpoints that will be created by the ZCL library */
#define SE_NUMBER_OF_ENDPOINTS                              	3

#define SE_MANUFACTURER_CODE                                	0x0000

#define SE_NUMBER_OF_ZCL_APPLICATION_TIMERS                 	2
#define CLD_P_ATTR_COMMODITY_TYPE


//#define SE_PRICE_NUMBER_OF_METER_PRICE_RECORD_ENTRIES			5

//#define SE_PRICE_NUMBER_OF_METER_BLOCK_PERIOD_RECORD_ENTRIES	2
/* Clusters used by this application */
#define BASIC_CLIENT
#define CLD_BASIC

#define CLD_IDENTIFY
#define IDENTIFY_SERVER

#define CLD_TIME
#define TIME_CLIENT
//#define CLD_PRICE
//#define PRICE_SERVER

#define CLD_SIMPLE_METERING
#define SM_SERVER
#define SM_CLIENT

//#define CLD_SM_SUPPORT_GET_PROFILE
//#define CLD_SM_SUPPORT_MIRROR
//#define CLD_SM_SUPPORT_FAST_POLL_MODE

//#define CLD_DRLC
//#define DRLC_SERVER
//#define DRLC_CLIENT

#define CLD_KEY_ESTABLISHMENT

#define CLD_TUNNELING

	#ifdef CLD_TUNNELING
	#define TUNNELING_SERVER
	/* The maximum outstanding tunneling messages supported */
	//#define CLD_TUN_MAX_SIMULTANEOUS_TUNNELS 2
#endif


//#define CLD_MC
//#define MC_SERVER

//#define CLD_OTA
#ifdef CLD_OTA
//#define OTA_SERVER
//#define OTA_CLIENT

#define OTA_ACKS_ON								FALSE	// Can't set this to FALSE if using Fragmentation.
#define OTA_CLIENT_DISABLE_DEFAULT_RESPONSE		TRUE	// FALSE if default response is required.

/* OTA configuration */
#define OTA_CLD_ATTR_FILE_OFFSET
#define OTA_CLD_CURRENT_FILE_VERSION
#define OTA_CLD_CURRENT_ZIGBEE_STACK_VERSION
#define OTA_CLD_DOWNLOADED_FILE_VERSION
#define OTA_CLD_DOWNLOADED_ZIGBEE_STACK_VERSION
#define OTA_CLD_IMAGE_UPGRADE_STATUS
#define OTA_CLD_SECURITY_CREDENTIAL_VERSION
#define OTA_CLD_UPGRADE_FILE_DESTINATION
#define OTA_CLD_MANF_ID
#define OTA_CLD_IMAGE_TYPE
//#define OTA_CLD_HARDWARE_VERSIONS_PRESENT

#define OTA_CLD_IMAGE_PAGE_REQUEST
#define OTA_CLD_QUERY_SPECIFIC_FILE_REQUEST
#define OTA_CLD_QUERY_SPECIFIC_FILE_RESPONSE
//#define OTA_PC_BUILD
//#define OTA_MAINTAIN_CUSTOM_SERIALISATION_DATA
#define OTA_MAX_IMAGES_PER_ENDPOINT              1
#define OTA_MAX_BLOCK_SIZE                       30
#define OTA_TIME_INTERVAL_BETWEEN_RETRIES        10 // Valid only if OTA_TIME_INTERVAL_BETWEEN_REQUESTS not defined
//#define OTA_TIME_INTERVAL_BETWEEN_REQUESTS     1
//#define OTA_SIGN_IMAGE
/******* OTA configuration End *******************/
#endif



/****************************************************************************/
/*             Basic Cluster - Optional Attributes                          */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the basic cluster.                                         */
/****************************************************************************/

//#define   CLD_BAS_ATTR_APPLICATION_VERSION
//#define   CLD_BAS_ATTR_STACK_VERSION
//#define   CLD_BAS_ATTR_HARDWARE_VERSION
//#define   CLD_BAS_ATTR_MANUFACTURER_NAME
//#define   CLD_BAS_ATTR_MODEL_IDENTIFIER
//#define   CLD_BAS_ATTR_DATE_CODE
//#define   CLD_BAS_ATTR_LOCATION_DESCRIPTION
//#define   CLD_BAS_ATTR_PHYSICAL_ENVIRONMENT
//#define   CLD_BAS_ATTR_DEVICE_ENBLED
//#define   CLD_BAS_ATTR_ALARM_MASK

/****************************************************************************/
/*             Time Cluster - Optional Attributes                           */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the time cluster.                                          */
/****************************************************************************/

//#define E_CLD_TIME_ATTR_TIME_ZONE
//#define E_CLD_TIME_ATTR_DST_START
//#define E_CLD_TIME_ATTR_DST_END
//#define E_CLD_TIME_ATTR_DST_SHIFT
//#define E_CLD_TIME_ATTR_STANDARD_TIME
//#define E_CLD_TIME_ATTR_LOCAL_TIME

/****************************************************************************/
/*             Simple Metering Cluster - Optional Attributes                */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the simple metering cluster.                               */
/****************************************************************************/

/* Reading information attribute set attribute ID's (D.3.2.2.1) */
//#define   CLD_SM_ATTR_CURRENT_SUMMATION_RECEIVED
//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_RECEIVED
//#define   CLD_SM_ATTR_DFT_SUMMATION
//#define   CLD_SM_ATTR_DAILY_FREEZE_TIME
//#define   CLD_SM_ATTR_POWER_FACTOR
//#define   CLD_SM_ATTR_READING_SNAPSHOT_TIME
//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_DELIVERED_TIME
//#define   CLD_SM_ATTR_CURRENT_MAX_DEMAND_RECEIVED_TIME

/* Time Of Use Information attribute attribute ID's set (D.3.2.2.2) */
#define   CLD_SM_ATTR_CURRENT_TIER_1_SUMMATION_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_TIER_1_SUMMATION_RECEIVED
#define   CLD_SM_ATTR_CURRENT_TIER_2_SUMMATION_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_TIER_2_SUMMATION_RECEIVED
#define   CLD_SM_ATTR_CURRENT_TIER_3_SUMMATION_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_TIER_3_SUMMATION_RECEIVED
//#define   CLD_SM_ATTR_CURRENT_TIER_4_SUMMATION_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_TIER_4_SUMMATION_RECEIVED
//#define   CLD_SM_ATTR_CURRENT_TIER_5_SUMMATION_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_TIER_5_SUMMATION_RECEIVED
//#define   CLD_SM_ATTR_CURRENT_TIER_6_SUMMATION_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_TIER_6_SUMMATION_RECEIVED

/* Formatting attribute set attribute ID's (D.3.2.2.4) */
#define   CLD_SM_ATTR_MULTIPLIER
#define   CLD_SM_ATTR_DIVISOR
#define   CLD_SM_ATTR_DEMAND_FORMATING
//#define   CLD_SM_ATTR_HISTORICAL_CONSUMPTION_FORMATTING

/* ESP Historical Consumption set attribute ID's (D.3.2.2.5) */
#define     CLD_SM_ATTR_INSTANTANEOUS_DEMAND
//#define   CLD_SM_ATTR_CURRENT_DAY_CONSUMPTION_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_DAY_CONSUMPTION_RECEIVED
//#define   CLD_SM_ATTR_PREVIOUS_DAY_CONSUMPTION_DELIVERED
//#define   CLD_SM_ATTR_PREVIOUS_DAY_CONSUMPTION_RECEIVED
//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_START_TIME_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_START_TIME_RECEIVED
//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_VALUE_DELIVERED
//#define   CLD_SM_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_VALUE_RECEIVED

/* Load Profile attribute set attribute ID's (D.3.2.2.6) */
//#define   CLD_SM_ATTR_MAX_NUMBER_OF_PERIODS_DELIVERED

/* Supply Limit attribute set attribute ID's (D.3.2.2.7) */
//#define   CLD_SM_ATTR_CURRENT_DEMAND_DELIVERED
//#define   CLD_SM_ATTR_DEMAND_LIMIT
//#define   CLD_SM_ATTR_DEMAND_INTEGRATION_PERIOD
//#define   CLD_SM_ATTR_NUMBER_OF_DEMAND_SUBINTERVALS

// Define macros below in zcl_config.h to enable support for the optional fields
// Optional fields must be enabled in message order, it is not permissible to
// have alternate cost fields without price ratio fields


/****************************************************************************/
/*             Price Cluster - Optional Attributes                          */
/*                                                                          */
/* Add the following #define's to your zcl_options.h file to add optional   */
/* attributes to the price cluster.                                         */
/****************************************************************************/

//#define CLD_P_ATTR_TIER_1_PRICE_LABEL
//#define CLD_P_ATTR_TIER_2_PRICE_LABEL
//#define CLD_P_ATTR_TIER_3_PRICE_LABEL
//#define CLD_P_ATTR_TIER_4_PRICE_LABEL
//#define CLD_P_ATTR_TIER_5_PRICE_LABEL
//#define CLD_P_ATTR_TIER_6_PRICE_LABEL

/****************************************************************************/
/*             					Get Profile                        			*/
/****************************************************************************/
#ifdef CLD_SM_SUPPORT_GET_PROFILE
#define	CLD_SM_GETPROFILE_MAX_NO_INTERVALS	6
#endif

/****************************************************************************/
/*             					Mirroring                          			*/
/****************************************************************************/
//#define CLD_SM_SUPPORT_MIRROR

#ifdef CLD_SM_SUPPORT_MIRROR
/* All the below mentioned macros are only required at the SERVER end */
/* Mirrored Basic cluster Physical environment */
#define CLD_BAS_MIRROR_ATTR_PHYSICAL_ENVIRONMENT

/* Define the maximum number of Supported Mirrors */
#define CLD_SM_NUMBER_OF_MIRRORS		1

/* Attribute reporting */
#define ZCL_ATTRIBUTE_REPORTING_CLIENT_SUPPORTED

/* Attribute definition for mirroring (attribute enabling is done only once) */
#define CLD_SM_MIRROR_ATTR_CURRENT_SUMMATION_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_MAX_DEMAND_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_MAX_DEMAND_RECEIVED
#define CLD_SM_MIRROR_ATTR_DFT_SUMMATION
#define CLD_SM_MIRROR_ATTR_DAILY_FREEZE_TIME
#define CLD_SM_MIRROR_ATTR_POWER_FACTOR
#define CLD_SM_MIRROR_ATTR_READING_SNAPSHOT_TIME
#define CLD_SM_MIRROR_ATTR_CURRENT_MAX_DEMAND_DELIVERED_TIME
#define CLD_SM_MIRROR_ATTR_CURRENT_MAX_DEMAND_RECEIVED_TIME

/* Time Of Use Information attribute attribute ID's set (D.3.2.2.2)*/
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_1_SUMMATION_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_1_SUMMATION_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_2_SUMMATION_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_2_SUMMATION_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_3_SUMMATION_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_3_SUMMATION_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_4_SUMMATION_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_4_SUMMATION_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_5_SUMMATION_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_5_SUMMATION_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_6_SUMMATION_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_TIER_6_SUMMATION_RECEIVED
#define CLD_SM_MIRROR_ATTR_MULTIPLIER
#define CLD_SM_MIRROR_ATTR_DIVISOR
#define CLD_SM_MIRROR_ATTR_DEMAND_FORMATING
#define CLD_SM_MIRROR_ATTR_HISTORICAL_CONSUMPTION_FORMATTING

/* ESP Historical Consumption set attribute ID's (D.3.2.2.5) */
#define CLD_SM_MIRROR_ATTR_INSTANTANEOUS_DEMAND
#define CLD_SM_MIRROR_ATTR_CURRENT_DAY_CONSUMPTION_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_DAY_CONSUMPTION_RECEIVED
#define CLD_SM_MIRROR_ATTR_PREVIOUS_DAY_CONSUMPTION_DELIVERED
#define CLD_SM_MIRROR_ATTR_PREVIOUS_DAY_CONSUMPTION_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_START_TIME_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_START_TIME_RECEIVED
#define CLD_SM_MIRROR_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_VALUE_DELIVERED
#define CLD_SM_MIRROR_ATTR_CURRENT_PARTIAL_PROFILE_INTERVAL_VALUE_RECEIVED

/* Load Profile attribute set attribute ID's (D.3.2.2.6) */
#define CLD_SM_MIRROR_ATTR_MAX_NUMBER_OF_PERIODS_DELIVERED

/* Supply Limit attribute set attribute ID's (D.3.2.2.7) */
#define CLD_SM_MIRROR_ATTR_CURRENT_DEMAND_DELIVERED
#define CLD_SM_MIRROR_ATTR_DEMAND_LIMIT
#define CLD_SM_MIRROR_ATTR_DEMAND_INTEGRATION_PERIOD
#define CLD_SM_MIRROR_ATTR_NUMBER_OF_DEMAND_SUBINTERVALS
#else
/* If mirroring is not support define the number of Supported Mirrors as 0 */
#define CLD_SM_NUMBER_OF_MIRRORS		0
#endif

/****************************************************************************/
/*             					Fast Polling                       			*/
/****************************************************************************/
#ifdef	CLD_SM_SUPPORT_FAST_POLL_MODE

//SM Attribute Required by Fast Polling Mode
#ifndef CLD_SM_ATTR_DEFAULT_UPDATE_PERIOD
#define 	CLD_SM_ATTR_DEFAULT_UPDATE_PERIOD
#endif

#ifndef CLD_SM_ATTR_FAST_POLL_UPDATE_PERIOD
#define CLD_SM_ATTR_FAST_POLL_UPDATE_PERIOD
#endif

//For fast polling period
#define CLD_SM_FAST_POLLING_PERIOD 15

#endif /*CLD_SM_SUPPORT_FAST_POLL_MODE*/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

#endif /* ZCL_OPTIONS_H */
