Requisitos do Sistema
====

Requisitos Funcionais
-----
* Tomada inteligente com funcionalidades IoT
* Sensor de voltagem (sub, sobre, falta)
* Sensor de corrente (sobre, falta)
* Sensor de temperatura e umidade ambiente
* Atuador tipo chave (relé)

Requisitos Não Funcionais
-----
* Comunicação por WI-FI ou Bluetooth
* Conexão a uma ''central'' com capacidade de armazenar dados históricos sobre os sensores
* Conexão a serviço em nuvem
* Monitoração gráfica de grandezas elétricas
* Alarmes configuráveis
* Comportamentos pré configurados

