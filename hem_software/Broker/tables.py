from sqlalchemy import Column, Integer, Numeric, String, Float, DateTime, ForeignKey, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import PasswordType
from sqlalchemy.orm import relationship

Model = declarative_base()


class User(Model):
    __tablename__ = "Usuarios"

    id_usuario = Column(Integer, primary_key = True)
    nome_usuario = Column(String(255), unique=True, nullable=False)
    telefone = Column(String(255), nullable=False)
    senha = Column(PasswordType(schemes=['pbkdf2_sha512','md5_crypt'], deprecated=['md5_crypt']), nullable = False)

    def __init__(self, nome_usuario, senha, telefone):
        self.nome_usuario = nome_usuario
        self.senha = senha
        self.telefone = telefone
    
    def __repr__(self):
        return '<ID {0}>'.format(self.id_usuario)


class Data(Model):
    __tablename__ = "Dados"

    id_dado = Column(Integer, primary_key = True)
    corrente = Column(Float, nullable=False)
    tensao = Column(Float, nullable=False)
    temperatura = Column(Float, nullable=False)
    horario = Column(DateTime, nullable=False)
    potencia = Column(Float, nullable=False)
    
    id_tomada = Column(Integer, ForeignKey('Tomadas.id_tomada'))
    fonte = relationship('Socket', foreign_keys=id_tomada)
    
    def __init__(self, corrente, tensao, temperatura, horario, potencia, id_tomada):
        self.corrente = corrente
        self.tensao = tensao
        self.temperatura = temperatura
        self.horario = horario
        self.potencia = potencia
        self.id_tomada = id_tomada

    def __repr__(self):
        return '<ID {0}>'.format(self.id_dado)


class Socket(Model):
    __tablename__ = "Tomadas"

    id_tomada = Column(Integer, primary_key = True)
    nome_tomada = Column(String(255), unique = True, nullable = False)
    estado = Column(Boolean, nullable = False)
    comando = Column(Boolean)
    energizada = Column(Boolean)

    def __init__(self, nome_tomada, estado, comando):
        self.nome_tomada = nome_tomada
        self.estado = estado
        self.comando = comando
    
    def __repr__(self):
        return '<ID {0}>'.format(self.id_tomada)


class Access(Model):
    __tablename__ = "Acessa"
    
    id_dado = Column(Integer, ForeignKey('Dados.id_dado'), primary_key = True, nullable = False)
    id_usuario = Column(Integer, ForeignKey('Usuarios.id_usuario'))

    solicitante = relationship('User', foreign_keys=id_usuario)
    solicitado = relationship('Data', foreign_keys=id_dado)
    
    def __init__(self):
        pass
    
    def __repr__(self):
        pass