/*****************************************************************************
 *
 * MODULE:             JN-AN-1135 (Meter)
 *
 * COMPONENT:          app_meter_node.h
 *
 * AUTHOR:             MRW
 *
 * DESCRIPTION:        SE Meter - Main Header File
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_apps/Application_Notes/JN-AN-1135-Smart-Energy-HAN-Solutions/Branches/MergeExercise/Tom/METER_NODE/Source/app_meter_node.h $
 *
 * $Revision: 8538 $
 *
 * $LastChangedBy: nxp33194 $
 *
 * $LastChangedDate: 2012-01-04 11:06:48 +0000 (Wed, 04 Jan 2012) $
 *
 * $Id: app_meter_node.h 8538 2012-01-04 11:06:48Z nxp33194 $
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142,
 * JN5139]. You, and any third parties must reproduce the copyright and
 * warranty notice and any other legend of ownership on each copy or partial
 * copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef APP_METER_NODE_H_
#define APP_METER_NODE_H_

typedef enum
{
	E_CONFIGURE_NETWORK,
	E_NETWORK_STARTUP,
	E_NETWORK_RUN

} teState;

typedef struct
{
    teState eState;
    bool_t bPermitJoining;
#ifdef CLD_SM_SUPPORT_MIRROR
    uint64    u64MirroredDeviceAddress[CLD_SM_NUMBER_OF_MIRRORS];
#endif
} tsDevice;


/* address of the IPD. In the field this address will be added to
 * the table out of band, not hard coded
 */
#define IPD_MAC_ADDR1 			0x0000000000000001LL
#define IPD_MAC_ADDR2 			0x0000000000000002LL
#define IPD_MAC_ADDR3 			0x0000000000000003LL
#define IPD_MAC_ADDR4 			0x0000000000000004LL
#define IPD_MAC_ADDR5 			0x0000000000000005LL

#ifdef SE_CERTIFICATION
#define TRAC_CLIENT_DONGLE		0x0022080000000002LL
#endif

#ifndef JENNIC_CHIP_FAMILY_JN514x
	#define ESP_STATE       0x1
	#define ZIGBEE_R20
#else
	#define ESP_STATE       "ESP_STATE"
#endif
#if(JENNIC_PCB == DEVKIT4)
#define POT_READ
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define ESP_METER_LOCAL_EP 		1
#define MIRROR_START_EP			2

#define RESTART_TIME    		APP_TIME_MS(1000)
#define METER_READ_TIME    		APP_TIME_MS(10000)
#define NUMBER_OF_PRICE_TIERS	3

#define ONE_SECOND_TICK_TIME    APP_TIME_MS(1000)

#define EMPTY					0
#define ONE_MINUTE				1
#define TWO_MINUTES				2
#define ONE_HOUR				60
#define MAX_TIME_INTERVAL 		65535

//#define RADIO_RECALIBRATION

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC void APP_vInitialise(void);

/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*APP_METER_NODE_H_*/
